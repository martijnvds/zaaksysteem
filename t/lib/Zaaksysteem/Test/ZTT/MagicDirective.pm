package Zaaksysteem::Test::ZTT::MagicDirective;

use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::ZTT::MagicDirective;

sub ztt_magic_directive_parser : Tests {
    my $directive_parser = Zaaksysteem::ZTT::MagicDirective->new;

    is_deeply $directive_parser->parse('case.id'), {
        expression => \'case.id'
    }, 'simple attribute is "parsed"';

    is_deeply $directive_parser->parse('case.startdate | date'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => []
        }
    }, 'simple attribute + filter parses';

    is_deeply $directive_parser->parse('case.startdate | date(short)'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => [qw[short]]
        }
    }, 'simple attribute + filter with bareword argument parses';

    is_deeply $directive_parser->parse('case.startdate | date("ABC")'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => [qw[ABC]]
        }
    }, 'simple attribute + filter with string argument parses';

    is_deeply $directive_parser->parse('case.startdate | date("ABC", case, "herp")'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => [qw[ABC case herp]]
        }
    }, 'simple attribute + filter with mixed arguments parses';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id'), {
        expression => \'case.id',
        iterate_context => 'case_relations'
    }, 'simple attribute + inline iteration syntax ok';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id | link'), {
        expression => \'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => []
        }
    }, 'simple attribute + inline iteration syntax + arg-less filter parses';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id | link(case, herp)'), {
        expression => \'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => [qw[case herp]]
        }
    }, 'simple attribute + inline iteration syntax + filter + filter_args parses';

    is_deeply $directive_parser->parse('ITerEER:case_RELATIONS:case.ID | LiNK(CaSe, "AbC")'), {
        expression => \'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => [qw[CaSe AbC]]
        }
    }, 'mixed-case attribute name, iterate keyword, iterate context and filtername parses and returns lower-cased';

    is $directive_parser->parse('magic_string_with_ space'), undef, 'syntax error in attribute results in undef parse';
}

sub ztt_mathemagic_script_parser : Tests {
    my $parser = Zaaksysteem::ZTT::MagicDirective->new;

    my %data_tests = (
        'simple iteration' => [
            'iterate something',
            [
                { iterate => 'something' }
            ]
        ],

        'constraint iteration' => [
            'iterate something { a == b }',
            [
                {
                    iterate => 'something',
                    constraint => {
                        expression => ['==', \'a', \'b']
                    }
                }
            ]
        ],

        'simple show_when constraint' => [
            'show_when { a == b }',
            [
                { show_when => { expression => ['==', \'a', \'b'] } }
            ]
        ],

        'simple show_when inequality constraint' => [
            'show_when { a != b }',
            [
                { show_when => { expression => ['!=', \'a', \'b'] } }
            ]
        ],

        'simple multiline script' => [
            "iterate zaak_relaties\nshow_when { a == b }",
            [
                { iterate => 'zaak_relaties' },
                { show_when => { expression => ['==', \'a', \'b'] } }
            ]
        ],
    );

    for (keys %data_tests) {
        my ($code, $expected_value) = @{ $data_tests{ $_ } };
        is_deeply [ $parser->parse_script($code) ], $expected_value, "$_: $code";
    }
}

sub ztt_mathemagic_directive_parser : Tests {
    my $parser = Zaaksysteem::ZTT::MagicDirective->new;

    is_deeply $parser->parse('1 + 1'), {
        expression => [qw[+ 1 1]]
    }, 'simple expression parses';

    is_deeply $parser->parse('1.1 + 2.2'), {
        expression => [qw[+ 1.1 2.2]]
    }, 'simple decimal expression parses';

    is_deeply $parser->parse('1 + 1 + 1'), {
        # infix: (1 + 1) + 1
        expression => [ '+', [qw[+ 1 1]], 1]
    }, 'left-associative expression parses';

    is_deeply $parser->parse('1 + 1 - 1'), {
        # infix: (1 + 1) - 1
        expression => [ '-', [ '+', 1, 1 ], 1 ]
    }, 'left-associative w/balanced precedence expression parses';

    is_deeply $parser->parse('1 + 2 * 3'), {
        # infix: 1 + (2 * 3)
        expression => [ '+', 1, [ '*', 2, 3] ]
    }, 'left-associative w/unbalanced precendence expression parses';

    is_deeply $parser->parse('(1 + 2) * 3'), {
        expression => [ '*', [ '+', 1, 2 ], 3 ]
    }, 'parenthesised subexpression has precedence over multiplication';

    is_deeply $parser->parse('2 * 3 / 4'), {
        expression => [ '/', [ '*', 2, 3 ], 4 ]
    }, 'left-associative w/balanced higher precedence expression parses';

    is_deeply $parser->parse('pow(2, 2)'), {
        expression => {
            call => 'pow',
            args => [ 2, 2 ]
        }
    }, 'function-calling expression parses';

    is_deeply $parser->parse('magic_string + 4'), {
        expression => [ '+', \'magic_string', '4' ]
    }, 'bareword magic string expression parses';

    is_deeply $parser->parse('magic_string / pow(3, 3)'), {
        expression => [
            '/',
            \'magic_string',
            { call => 'pow', args => [ 3, 3 ] }
        ]
    }, 'bareword magic string w/function-calling expression';

    is_deeply $parser->parse('1 > 1'), {
        expression => [ '>', 1, 1 ]
    }, 'simple relational expression';

    is_deeply $parser->parse('1 == 2 == 3'), {
        expression => [ '==', [ '==', 1, 2 ], 3 ]
    }, 'chained relational expression';

    is_deeply $parser->parse('1 + 2 == 3 + 4'), {
        expression => [ '==', [ '+', 1, 2 ], [ '+', 3, 4 ] ]
    }, 'relational expression precedence lower than addition';

    for (qw[== != ~= >= <= > < in]) {
        is_deeply $parser->parse(sprintf('1 %s 2', $_)), {
            expression => [ $_, 1, 2 ]
        }, sprintf('relational operator "%s" supported', $_);
    }

    is_deeply $parser->parse(':my_const + :my_other_const'), {
        expression => [
            '+',
            { args => [ 'my_const' ], call => 'constant' },
            { args => [ 'my_other_const' ], call => 'constant' }
        ]
    }, 'call to function constant() via :sugar parsed';

    is_deeply $parser->parse(':constant + 1 == my_var'), {
        expression => [
            '==',
            [ '+', { call => 'constant', args => ['constant'] }, 1 ],
            \'my_var'
        ]
    }, 'yoda-speak comparison parses';

    is_deeply $parser->parse('[]'), {
        expression => {
            call => 'array',
            args => []
        }
    }, 'call to function array() via [sugar] parsed';

    is_deeply $parser->parse('[ 1, 2, "string" ]'), {
        expression => {
            call => 'array',
            args => [ 1, 2, "string" ]
        }
    }, 'array construction with mixed-type scalars';

    is_deeply $parser->parse('[ 1 + 2, [ a + b ] ]'), {
        expression => {
            call => 'array',
            args => [
                [ '+', 1, 2 ],
                {
                    call => 'array',
                    args => [ [ '+', \'a', \'b' ] ]
                }
            ]
        }
    }, 'nested array construction with expression and varrefs';
}

sub ztt_script_parser : Tests {
    my $script_parser = Zaaksysteem::ZTT::MagicDirective->new;

    is_deeply $script_parser->parse_script('iterate zaak_relaties'), {
        iterate => 'zaak_relaties'
    }, 'simple iteration command';

    is_deeply $script_parser->parse_script('show_when { a == b }'), {
        show_when => { expression => [ '==', \'a', \'b' ] }
    }, 'simple show_when command';

    is_deeply $script_parser->parse_script('show_when { a != "b" }'), {
        show_when => { expression => [ '!=', \'a', 'b' ] }
    }, 'simple show_when command with inequality and string arg';

    my ($iterate, $show_when) = $script_parser->parse_script(
        "iterate zaak_relaties\nshow_when { a == b }"
    );

    is_deeply $iterate, {
        iterate => 'zaak_relaties'
    }, 'multiline script 1/2: iteration command';

    is_deeply $show_when, {
        show_when => { expression => [ '==', \'a', \'b' ] }
    }, 'multiline script 2/2: show_when command';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
