package Zaaksysteem::Test::Filestore::Engine::UStore;
use strict;
use warnings;

=head1 NAME

Zaaksysteem::Test::Filestore::Engine::UStore - Test methods

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Filestore::Engine::UStore;

=head1 METHODS

=cut

use Zaaksysteem::Filestore::Engine::UStore;


use Zaaksysteem::Test;

use Test::MockModule;
use Test::MockObject;

=head2 erase

A simple check that we do call an external erase method for this specific store
and that we do call it with the right arguments, a lowercase UUID. Besides that
it checks that we do log what we are doing.

=cut


my $test_obj;

sub test_erase {

    my %mock;

    $mock{obj}{'Log::Log4perl'}{'new'}
        = _mock_obj__Log_Log4perl__new();
    $mock{mod}{'File::UStore'}
        = _mock_mod__File_UStore();

    $mock{obj}{'Log::Log4perl'}{'new'}->clear();

    my $UUID = '976D6037-4336-4BB1-A4A3-34A6C5A23199'; # implicit testing lowercase

    lives_ok {
        $test_obj = Zaaksysteem::Filestore::Engine::UStore->new(
            name                      => 'Swift',
            local_storage_path        => '/tmp/local_storage_path/',
            accelerated_download_root => '/tmp/accelerated_download_root/',
    #       ustore                    => # use builder
            logger                    => $mock{obj}{'Log::Log4perl'}{'new'},
        );
    } "Created 'Zaaksysteem::Filestore::Engine::UStore' object";

    lives_ok {
        $test_obj->erase($UUID);
    } "... and could call `erase` method";

    $mock{obj}{'Log::Log4perl'}{'new'}->called_ok( 'info',
        "... did log an INFO"
    );

    cmp_deeply (
        [$mock{obj}{'Log::Log4perl'}{'new'}->call_args(1)] => [
            $mock{obj}{'Log::Log4perl'}{'new'}, #self
            re( qr|Erasing object '([-A-F0-9]{36})' from UStore| )
        ],
        "... ... with the right message"
    );

}

# done testing - mocks follow

sub _mock_mod__File_UStore {
    my $mock_mod = Test::MockModule->new('File::UStore');
    $mock_mod->mock(
        'remove' => sub
        {
            cmp_deeply (
                [ @_ ] => [
                    $test_obj->ustore,
                    re ( qr|^([-a-f0-9]{36})$| )
                ],
                "... did call external erase, with right UUID"
            );
        }
    );
    return $mock_mod;
}

sub _mock_obj__Log_Log4perl__new {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_always( info => undef );
    return $mock_obj;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
