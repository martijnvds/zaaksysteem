package Zaaksysteem::Test::DB::Component::ContactData;

use Moose;

extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::DB::Component::ContactData - Test
L<Zaaksysteem::DB::Component::ContactData>

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::DB::Component::ContactData

=cut

use Zaaksysteem::Test;
use Zaaksysteem::DB::Component::ContactData;

sub test_exec_and_log_update {
    my @dirty_columns;
    my $txn_do_count = 0;
    my %columns;
    my $except;

    # Pretend we're a well-behaved ContactData row instance with all the
    # DBIx::Class cruft we need.
    my $mock_row = mock_one(
        is_changed => sub { return @dirty_columns },
        txn_do     => sub { $txn_do_count++; return shift->(@_); },
        get_column => sub { return $columns{ shift() } },
        identifier => sub { return 'betrokkene-natuurlijk_persoon-123' },
        update     => sub { die $except if $except; return shift; },
    );

    ok not Zaaksysteem::DB::Component::ContactData::exec_and_log_update($mock_row, sub {});

    is $txn_do_count, 0, 'NOP update does not trigger update transaction';

    push @dirty_columns, 'note';

    my $event = Zaaksysteem::DB::Component::ContactData::exec_and_log_update(
        $mock_row,
        sub { return 'event' }
    );

    is $txn_do_count, 1, 'note update triggers update transaction';
    is $event, 'event', 'logger return value returned by exec_and_log_update()';

    $except = 'msg';

    my $ex = exception {
        Zaaksysteem::DB::Component::ContactData::exec_and_log_update(
            $mock_row,
            sub { fail('logger may not be used during exception conditions') }
        );
    };

    is $txn_do_count, 2, 'note update triggers update transaction';
    ok $ex =~ m[^msg], 'exception thrown by exec_and_log_update via update';
    undef $except;

    my $logger_called;
    $columns{ gegevens_magazijn_id } = 42;

    Zaaksysteem::DB::Component::ContactData::exec_and_log_update(
        $mock_row,
        sub {
            $logger_called = 1;
            is shift, 'subject/update_contact_data', 'event type';
            cmp_deeply shift, {
                component => 'betrokkene',
                component_id => 42,
                created_for => 'betrokkene-natuurlijk_persoon-123',
                data => {
                    subject_id => 'betrokkene-natuurlijk_persoon-123',
                    fields => \@dirty_columns
                }
            }, 'event data structure and content check';
        }
    );

    ok $logger_called, 'sanity check ok, logger was called';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
