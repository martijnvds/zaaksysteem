package Zaaksysteem::Test::Tools::Number;

use Zaaksysteem::Test;

use Zaaksysteem::Tools::Number qw(
    reformat_telephone_number
    phonenumber_to_object
    sloppy_phonenumber_to_cm
);

sub _test_phonenumber {
    my ($orig, $format, $reformat) = @_;
    my $number = phonenumber_to_object($orig);

    if (isa_ok($number, "Number::Phone")) {
        is($number->format, $format, "$orig formats to $format");
        is(
            reformat_telephone_number($number),
            $reformat,
            "Reformated $orig to $reformat"
        );
    }
    else {
        fail("$orig isn't a correct phonenumber");
    }

}

sub _test_phonenumber_fail {
    my $orig = shift;
    is(phonenumber_to_object($orig), undef, "$orig isn't a valid phonenumber");

}

sub test_phonenumber_reformating {

    my @valid_dutch_numbers = qw(
        0031626242807
        0626242807
        06-26242807
        +31626242807
        +316-262-428-07
    );

    foreach(@valid_dutch_numbers) {
        _test_phonenumber($_, '+31 6 26242807', '0031626242807');
    }

    my @valid_dutch_fixed_numbers = (
        '+3120 737 0005',
        '003120 737 0005',
        '020 737 0005',
        '020-7370005',
        '+31 (20) 7370005',
        '(020) 7370005',
    );

    foreach(@valid_dutch_fixed_numbers) {
        _test_phonenumber($_, '+31 20 737 0005', '0031207370005');
    }

    _test_phonenumber('0610094296',    '+31 6 10094296',  '0031610094296');
    _test_phonenumber('+297 522 1500', '+297 522 1500',   '002975221500');
    _test_phonenumber('+492111793010', '+49 211 1793010', '00492111793010');
    _test_phonenumber('+3226791711', '+32 2 679 17 11', '003226791711');

    _test_phonenumber_fail('0671988729');
    _test_phonenumber_fail('+32 482 45 56 422');
    _test_phonenumber_fail('061435903257');

}

sub _test_sloppy {
    my ($orig, $want) = @_;
    is(sloppy_phonenumber_to_cm($orig), $want, "Sloppy numbers be fine: $orig");
}

sub test_sloppy_to_cm {

    my @valid_dutch_numbers = qw(
        0031626242807
        0626242807
        06-26242807
        +31626242807
        +316-262-428-07
    );

    foreach(@valid_dutch_numbers) {
        _test_sloppy($_, '0031626242807');
    }

    my @valid_dutch_fixed_numbers = (
        '+3120 737 0005',
        '003120 737 0005',
        '020 737 0005',
        '020-7370005',
        '+31 (20) 7370005',
        '(020) 7370005',
    );

    foreach(@valid_dutch_fixed_numbers) {
        _test_sloppy($_, '0031207370005');
    }

    _test_sloppy('0610094296',    '0031610094296');
    _test_sloppy('+297 522 1500', '002975221500');
    _test_sloppy('+492111793010', '00492111793010');
    _test_sloppy('+3226791711', '003226791711');

    # Known invalid numbers that are suddenly ok
    _test_sloppy('0671988729', '0031671988729');
    _test_sloppy('+32 482 45 56 42', '0032482455642');
    _test_sloppy('088123456789012', '003188123456789012');

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
