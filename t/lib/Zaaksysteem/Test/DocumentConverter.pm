package Zaaksysteem::Test::DocumentConverter;
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::DocumentConverter - Test for the document converter module

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::DocumentConverter

=cut

use HTTP::Response;
use JSON::MaybeXS;
use MIME::Base64;
use Test::LWP::UserAgent;
use Zaaksysteem::DocumentConverter;

sub test_converter_can_convert_true {
    my $ua = Test::LWP::UserAgent->new();
    my ($from_type, $to_type);
    $ua->map_response(
        qr{http://allowed\.example\.com/v1/can_convert},
        sub {
            my $req = shift;

            cmp_deeply(
                { $req->uri->query_form },
                {
                    from_type => $from_type,
                    to_type   => $to_type,
                },
                "Correct query parameters are sent to document conversion service"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"can_convert": true}'
            ),
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://allowed.example.com/',
    );

    $from_type = 'test/plain';
    $to_type   = 'test/xml';

    my $rv = $dc->can_convert(
        source_type => $from_type,
        destination_type => $to_type
    );
    ok($rv, "Allowed conversion returns a true value");
}

sub test_converter_can_convert_false {
    my $ua = Test::LWP::UserAgent->new();
    my ($from_type, $to_type);
    $ua->map_response(
        qr{http://disallowed\.example\.com/v1/can_convert},
        sub {
            my $req = shift;

            cmp_deeply(
                { $req->uri->query_form },
                {
                    from_type => $from_type,
                    to_type   => $to_type,
                },
                "Correct query parameters are sent to document conversion service"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"can_convert": false}'
            ),
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://disallowed.example.com/',
    );

    $from_type = 'test/plain';
    $to_type   = 'test/xml';

    my $rv = $dc->can_convert(
        source_type => $from_type,
        destination_type => $to_type
    );
    ok(!$rv, "Disallowed conversion returns a false value");
}

sub test_converter_can_convert_error {
    my $ua = Test::LWP::UserAgent->new();
    $ua->map_response(
        qr{http://error\.example\.com/},
        HTTP::Response->new(
            '500' => 'Not OK',
            ['Content-Type' => 'application/json'],
            '{"error": "You did something wrong"}'
        ),
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://error.example.com/',
    );

    throws_ok(
        sub {
            my $rv = $dc->can_convert(
                source_type => 'foo/bar',
                destination_type => 'baz/quux',
            );
        },
        "BTTW::Exception::Base",
        "Error in call response leads to an exception",
    );
}

sub test_converter_can_convert_error2 {
    my $ua = Test::LWP::UserAgent->new();
    $ua->map_response(
        qr{http://error2\.example\.com/},
        HTTP::Response->new(
            '500' => 'Not OK',
            ['Content-Type' => 'text/plain'],
            'It broke'
        ),
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://error2.example.com/',
    );

    throws_ok(
        sub {
            my $rv = $dc->can_convert(
                source_type => 'foo/bar',
                destination_type => 'baz/quux',
            );
        },
        "BTTW::Exception::Base",
        "Another kind of error in call response leads to an exception",
    );
}

sub test_converter_convert_scalar_good {
    my $ua = Test::LWP::UserAgent->new();
    $ua->map_response(
        qr{http://convert\.example\.com/v1/convert},
        sub {
            my $req = shift;

            my $content = decode_json($req->decoded_content);
            is(
                $content->{to_type},
                "test/plain",
                "Target MIME type sent to conversion API correctly"
            );
            is(
                $content->{content},
                encode_base64("this is only a test", ""),
                "Content sent to conversion API correctly"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"content": "dGhpcyBpcyBDT05URU5UCg=="}',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert.example.com/',
    );

    my $result = $dc->convert_scalar(
        destination_type => 'test/plain',
        source           => 'this is only a test',
    );

    is($result, "this is CONTENT\n", "Simple conversion of scalar succeeds");
}

sub test_converter_convert_scalar_good_with_options {
    my $ua = Test::LWP::UserAgent->new();
    $ua->map_response(
        qr{http://convert\.example\.com/v1/convert},
        sub {
            my $req = shift;

            my $content = decode_json($req->decoded_content);
            is(
                $content->{to_type},
                "test/plain",
                "Content sent to conversion API correctly"
            );
            is(
                $content->{content},
                encode_base64("this is only a test", ""),
                "Content sent to conversion API correctly"
            );
            is_deeply(
                $content->{options},
                { height => 200, width => 320 },
                "Options sent to conversion API correctly"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"content": "dGhpcyBpcyBDT05URU5UCg=="}',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert.example.com/',
    );

    my $result = $dc->convert_scalar(
        destination_type => 'test/plain',
        source           => 'this is only a test',
        filter_options   => { width => 320, height => 200 },
    );

    is($result, "this is CONTENT\n", "Simple conversion of scalar succeeds");
}

sub test_converter_convert_scalar_error {
    my $ua = Test::LWP::UserAgent->new();
    $ua->map_response(
        qr{http://convert-error\.example\.com/v1/convert},
        sub {
            my $req = shift;

            return HTTP::Response->new(
                '500' => 'Not OK',
                ['Content-Type' => 'application/json'],
                '{"error": "The world is burning"}',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert-error.example.com/',
    );

    throws_ok(
        sub {
            my $result = $dc->convert_scalar(
                destination_type => 'test/plain',
                source           => 'this is only a test',
            );
        },
        'BTTW::Exception::Base',
        "Converting errors lead to an exception"
    );
}

sub test_converter_convert_scalar_error2 {
    my $ua = Test::LWP::UserAgent->new();
    $ua->map_response(
        qr{http://convert-error\.example\.com/v1/convert},
        sub {
            my $req = shift;

            return HTTP::Response->new(
                '500' => 'Not OK',
                ['Content-Type' => 'text/plain'],
                'It broke',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert-error.example.com/',
    );

    throws_ok(
        sub {
            my $result = $dc->convert_scalar(
                destination_type => 'test/plain',
                source           => 'this is only a test',
            );
        },
        'BTTW::Exception::Base',
        "Converting errors of a different kind leads to an exception"
    );
}

sub test_converter_convert_to_fh_good {
    my $ua = Test::LWP::UserAgent->new();

    $ua->map_response(
        qr{http://convert\.example\.com/v1/convert},
        sub {
            my $req = shift;

            my $content = decode_json($req->decoded_content);
            is(
                $content->{to_type},
                "test/plain",
                "Target MIME type sent to conversion API correctly"
            );
            is(
                $content->{content},
                encode_base64("test data\n", ""),
                "Content sent to conversion API correctly"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"content": "dGhpcyBpcyBDT05URU5UCg=="}',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert.example.com/',
    );

    my $tempfile = File::Temp->new();
    print $tempfile "test data\n";
    close($tempfile);

    my $fh = $dc->convert_to_fh(
        destination_type => 'test/plain',
        source_filename  => $tempfile->filename,
    );

    my $res = <$fh>;
    is($res, "this is CONTENT\n", "Simple conversion to file handle succeeds");
}

sub test_converter_convert_to_fh_good_with_options {
    my $ua = Test::LWP::UserAgent->new();

    $ua->map_response(
        qr{http://convert\.example\.com/v1/convert},
        sub {
            my $req = shift;

            my $content = decode_json($req->decoded_content);
            is(
                $content->{to_type},
                "test/plain",
                "Target MIME type sent to conversion API correctly"
            );
            is(
                $content->{content},
                encode_base64("test data\n", ""),
                "Content sent to conversion API correctly"
            );
            is_deeply(
                $content->{options},
                { height => 200, width => 320 },
                "Options sent to conversion API correctly"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"content": "dGhpcyBpcyBDT05URU5UCg=="}',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert.example.com/',
    );

    my $tempfile = File::Temp->new();
    print $tempfile "test data\n";
    close($tempfile);

    my $fh = $dc->convert_to_fh(
        destination_type => 'test/plain',
        source_filename  => $tempfile->filename,
        filter_options   => { width => 320, height => 200 },
    );

    my $res = <$fh>;
    is($res, "this is CONTENT\n", "Conversion to file handle with options succeeds");
}

sub test_converter_convert_file_good {
    my $ua = Test::LWP::UserAgent->new();

    $ua->map_response(
        qr{http://convert\.example\.com/v1/convert},
        sub {
            my $req = shift;

            my $content = decode_json($req->decoded_content);
            is(
                $content->{to_type},
                "test/plain",
                "Target MIME type sent to conversion API correctly"
            );
            is(
                $content->{content},
                encode_base64("test data\n", ""),
                "Content sent to conversion API correctly"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"content": "dGhpcyBpcyBDT05URU5UCg=="}',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert.example.com/',
    );

    my $tempfile = File::Temp->new();
    print $tempfile "test data\n";
    close($tempfile);

    my $tempfile2 = File::Temp->new();

    $dc->convert_file(
        destination_type     => 'test/plain',
        destination_filename => $tempfile2->filename,
        source_filename      => $tempfile->filename,
    );

    my $res = <$tempfile2>;
    is($res, "this is CONTENT\n", "Simple conversion to file handle succeeds");
}

sub test_converter_convert_file_good_with_options {
    my $ua = Test::LWP::UserAgent->new();

    $ua->map_response(
        qr{http://convert\.example\.com/v1/convert},
        sub {
            my $req = shift;

            my $content = decode_json($req->decoded_content);
            is(
                $content->{to_type},
                "test/plain",
                "Target MIME type sent to conversion API correctly"
            );
            is(
                $content->{content},
                encode_base64("test data\n", ""),
                "Content sent to conversion API correctly"
            );
            is_deeply(
                $content->{options},
                { height => 200, width => 320 },
                "Options sent to conversion API correctly"
            );

            return HTTP::Response->new(
                '200' => 'OK',
                ['Content-Type' => 'application/json'],
                '{"content": "dGhpcyBpcyBDT05URU5UCg=="}',
            );
        },
    );

    my $dc = Zaaksysteem::DocumentConverter->new(
        ua  => $ua,
        uri => 'http://convert.example.com/',
    );

    my $tempfile = File::Temp->new();
    print $tempfile "test data\n";
    close($tempfile);

    my $tempfile2 = File::Temp->new();

    $dc->convert_file(
        destination_type     => 'test/plain',
        source_filename      => $tempfile->filename,
        destination_filename => $tempfile2->filename,
        filter_options       => { width => 320, height => 200 },
    );

    my $res = <$tempfile2>;
    is($res, "this is CONTENT\n", "Conversion to file handle with options succeeds");
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
