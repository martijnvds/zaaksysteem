package Zaaksysteem::Test::Object::Queue::Model::Subject;

use URI;
use Zaaksysteem::Test;

use Zaaksysteem::Backend::Object::Queue::Component;
use Zaaksysteem::Object::Queue::Model;

=head1 NAME

Zaaksysteem::Test::Object::Queue::Model::Subject - Test queue items related to subjects

=head1 DESCRIPTION

Test subject queue items.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Queue::Model::Subject

=head2 test_update_cases_of_deceased

Referenced by: L<Zaaksysteem::Test::BR::Subject::Types::Person#test_run_post_triggers>

=cut

sub test_update_cases_of_deceased {
    my $log = {};

    ### Mock
    my $role  = mock_moose_class({
        roles => ['Zaaksysteem::Object::Queue::Model::Subject', 'MooseX::Log::Log4perl'],
        methods => {
            _retrieve_cases_for_aanvrager => sub {
                $log->{_retrieve_cases_for_aanvrager} = $_[2];

                return [1,2,3];
            },
            _touch_cases => sub {
                $log->{_touch_cases} = $_[1];

                return 1;
            },
            build_resultset => sub {
                return mock_one();
            }
        },
    });

    ### Exceptions
    {
        throws_ok(
            sub {
                $role->update_cases_of_deceased()
            },
            qr/Validation failed for 'Zaaksysteem::Backend::Object::Queue::Component' with value undef/,
            'Exception: no item given'
        );
    }

    my $item = mock_one(natuurlijk_persoon_id => 44);

    ok($role->update_cases_of_deceased($item), 'Got valid return value');
    cmp_deeply($log->{_touch_cases}, [1,2,3], "Got valid input params for _touch_cases");
    cmp_deeply($log->{_retrieve_cases_for_aanvrager}, { natuurlijk_persoon_id => 44 }, "Got valid input params for _touch_cases");
}

sub test_update_cases_of_deceased_via_model {
    my $log = {};

    my $class  = mock_moose_class(
        {
            superclasses => ['Zaaksysteem::Object::Queue::Model'],
            methods => {
                _retrieve_cases_for_aanvrager => sub {
                    $log->{_retrieve_cases_for_aanvrager} = $_[2];

                    return [1,2,3];
                },
                _touch_cases => sub {
                    $log->{_touch_cases} = $_[1];

                    return 1;
                },
            },
        },
        {
            table => mock_one(),
            statsd => mock_one(),
            base_uri => URI->new('http://localhost/'),
            instance_hostname => 'localhost',
            message_queue_factory => sub {},
            message_queue_exchange => 'amq.topic',
            schema => mock_one(),

        }
    );

    my $status = 'pending';
    my $item = mock_one(
        data   => { natuurlijk_persoon_id => 47, },
        label  => 'Update cases of deceased persons',
        type   => 'update_cases_of_deceased',
        status => sub { my $s = shift; $status = $s if defined $s; },
        update => sub {
            return mock_strict(
                discard_changes => mock_strict(
                    date_created => DateTime->now(),
                    date_finished => DateTime->now(),
                    status => 'statussed',
                )
            );
        }
    );

    $item = $class->run($item);
    is($status, 'finished', 'Succesfully finished handler');
    is($log->{_retrieve_cases_for_aanvrager}->{natuurlijk_persoon_id}, 47, "Got correct requests");
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
