package Zaaksysteem::Test::External::ValidSign;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use BTTW::Tools::RandomData qw(generate_uuid_v4);
use WebService::ValidSign::Object::Sender;
use WebService::ValidSign::Object::Document;

use Zaaksysteem::External::ValidSign;

my $sender_uuid = generate_uuid_v4;

sub _get_model {

    return Zaaksysteem::External::ValidSign->new(
        secret    => 'foobar',
        endpoint  => 'https://localhost/api',
        interface => mock_one(id => 42, name => "ValideTekenen"),
        subject   => mock_strict(
            find => sub {
                my $val = pop;
                my $email = $val eq $sender_uuid ? 'shouldwork' : 'noworky';
                return mock_strict(
                    subject => { email_address => "$email\@zaaksysteem.nl" });
            }

        ),
        object_subscription_rs => mock_one(),
        file_rs                => mock_one(),
        case_rs                => mock_one(),

        @_
    );
}

sub test_get_sender_methods {
    my $test = shift;

    # This doesn't work, for some reason or another
    # So we are going to inject something in the mocked function to prove that
    # we called it.
    $test->test_report->plan(6);
    my $random_id = generate_uuid_v4();

    my $model     = _get_model();
    my $override = override('Zaaksysteem::External::ValidSign::_get_sender' => sub {
            my ($self, %params) = @_;
            cmp_deeply(
                \%params,
                { search => 'shouldwork@zaaksysteem.nl' },
                'Search params are correct for _get_sender()'
            );
            return $random_id;
        });

    lives_ok(
        sub {
            my $rv = $model->get_sender($sender_uuid);
            # Because $test->test_report->plan() doesn't do what I think it
            # does we just make sure that this thing actually ran by looking at
            # a random uuid
            is($rv, $random_id, '... and we actually ran $model->_get_sender');
        },
        'model->get_sender() works',
    );

    $override->override(
        'Zaaksysteem::External::ValidSign::_get_sender' => sub {
            my ($self, %params) = @_;
            if ($params{search} eq 'noworky@zaaksysteem.nl') {
                die "It will not work, ever";
            }
            return 1;
        },
    );

    my $bad_sender = generate_uuid_v4;
    throws_ok(
        sub {
            $model->get_sender($bad_sender);
        },
        qr/It will not work, ever/,
        "Default sender missing, unable to get a sender"
    );

    $model = _get_model(default_sender => 'defaultsender@zaaksysteem.nl');

    lives_ok(
        sub {
            $model->get_sender($bad_sender);
        },
        "We were able to get a sender"
    );

}

sub test_get_file {
    my $test = shift;

    my $uuid = generate_uuid_v4;
    my $model = _get_model(
        file_rs => mock_strict(
            find => sub {
                my $id = pop;
                return unless $id == 42;
                return mock_strict(
                    name => $uuid,
                    filestore_id => {
                        uuid     => $uuid,
                        get_path => 'a path',
                    },
                );
            }
        ),
    );

    my $file = $model->get_file(42);
    isa_ok($file, "WebService::ValidSign::Object::Document");
    cmp_deeply(
        $file->TO_JSON,
        {
            id   => $uuid,
            name => $uuid,
        },
        'Document has all the things we are looking for'
    );
    is($file->path, "a path", ".. and path is also correct");

    throws_ok(
        sub {
            $model->get_file(666);
        },
        qr#^validsign/file/not_found#,
        "Dies when no file can be found"
    );
}

sub test_get_case {

    my $uuid = generate_uuid_v4;
    my $model = _get_model(
        case_rs => mock_strict(
            find => sub {
                my $id = pop;
                return unless $id == 42 or $id == 39;
                return mock_strict(
                    id => $id,
                    is_afgehandeld => $id == 42 ? 0 : 1,
                    filestore_id => {
                        uuid     => $uuid,
                        get_path => 'a path',
                    },
                );
            }
        ),
    );

    lives_ok(
        sub {
            $model->get_case(42);
        },
        "Case 42 is found and open",
    );

    throws_ok(
        sub {
            $model->get_case(39);
        },
        qr#validsign/case/closed#,
        "Case 39 is closed"
    );

    throws_ok(
        sub {
            $model->get_case(666);
        },
        qr#validsign/case/not_found#,
        "Case 666 is not found"
    );
}

sub test_create_signging_request {

    my $document_package_id = 101;

    my $model = _get_model(
        interface => mock_one(id => 16, name => "ValideTekenen"),
        object_subscription_rs => mock_strict(
            create => sub {
                my $hash = pop;
                cmp_deeply(
                    $hash,
                    {
                        interface_id => 16,
                        external_id  => 101,
                        local_table  => 'Zaak',
                        local_id     => 42,
                    },
                    "Create for object subscription goes well",
                );
            },
        ),
        validsign => mock_strict(
            package => {
                create => sub {
                    my $pkg = pop;
                    $pkg->id($document_package_id);
                    return $document_package_id;
                },
            },
        ),
    );

    my $override = override(
        "Zaaksysteem::External::ValidSign::get_sender" => sub {
            WebService::ValidSign::Object::Sender->new(
                email      => 'testsuite@zaaksysteem.nl',
                first_name => 'Test',
                last_name  => 'Suite',
            );
        },
    );
    $override->override(
        "Zaaksysteem::External::ValidSign::get_case" => sub {
            return mock_case(id => 42);
        },
    );
    $override->override(
        "Zaaksysteem::External::ValidSign::get_file" => sub {
            return WebService::ValidSign::Object::Document->new(
                name => "testdocument",
            );
        }
    );

    my $pkg = $model->create_signing_request(
        subject_uuid => $sender_uuid,
        case_id      => 42,
        file_id      => 39,
    );

    isa_ok($pkg, "WebService::ValidSign::Object::DocumentPackage");
    is($pkg->id, 101, ".. and has an ID");

    ok($pkg->has_documents, ".. and has documents");
    ok($pkg->has_sender, ".. and a sender");

    is($pkg->language, 'nl', '.. and the language is set to NL');
    is(
        $pkg->name,
        "Zaaksysteem zaak 42 - file testdocument",
        ".. and the name is also correct"
    );
}

sub test_receive_event_cb {

    my $model = _get_model();
    lives_ok(
        sub {
            $model->receive_event_cb(
                sub {
                    return 1;
                }
            );
        },
        "Call succeeds"
    );

    throws_ok(
        sub {
            $model->receive_event_cb(
                sub {
                    die "I died";
                }
            );
        },
        qr/^I died/,
        "Call dies",
    );

}

sub test_receive_event {

    my $model = _get_model();

    my @callbacks = qw(
        PACKAGE_COMPLETE
        PACKAGE_DECLINE
    );

    lives_ok(
        sub {
            isa_ok($model->receive_event($_), "CODE") for @callbacks;
        },
        "All callbacks are implemented"
    );
}

sub test_receive_event_package_complete {

    my $file_id = generate_uuid_v4;

    my $model = _get_model(
        validsign => mock_strict(
             package => {
                find => sub {
                    my $pkg = WebService::ValidSign::Object::DocumentPackage->new(
                        name => 'Foo'
                    );

                    $pkg->add_document(
                        WebService::ValidSign::Object::Document->new(
                            name => 'Name of document',
                        )
                    );
                    return $pkg;
                },
                download_document => { filename => 'a filename' }
            },
        ),
        file_rs => mock_strict(
            file_create => sub {
                my $val = pop;
                cmp_deeply(
                    $val,
                    {
                        name      => 'Name of document (ondertekend).pdf',
                        file_path => 'a filename',
                        disable_logging    => 1,
                        db_params          => { case_id => 42 },
                    },
                    "File create went fine"
                );
                return mock_one(id => $file_id, name => $val->{name});
            },
        ),
    );
    my $cb = $model->receive_event('PACKAGE_COMPLETE');

    my $override = override(
        'Zaaksysteem::External::ValidSign::get_object_subscription_by_external_id' => sub {
            my $search = pop;
            return mock_strict(
                local_id => 42,
                id       => $search,
                delete   => sub { pass("delete called") },
            );
        }
    );
    $override->override(
        'Zaaksysteem::External::ValidSign::get_case' => sub {
            my $id = pop;
            return mock_case(id => $id);
        }
    );

    my $rv = $cb->($model, packageId => 1);
    is($rv->id, 42, "File was created for case 42");
}

sub test_receive_event_package_decline {

    my $model = _get_model(
        validsign => mock_strict(
             package => {
                find => sub {
                    my $pkg = WebService::ValidSign::Object::DocumentPackage->new(
                        name => 'Foo'
                    );

                    $pkg->add_document(
                        WebService::ValidSign::Object::Document->new(
                            name => 'Name of document',
                        )
                    );
                    return $pkg;
                },
            },
        ),
    );

    my $override = override(
        'Zaaksysteem::External::ValidSign::get_object_subscription_by_external_id' => sub {
            my $search = pop;
            return mock_strict(
                local_id => 42,
                id       => $search,
                delete   => sub { pass("delete called") },
            );
        }
    );
    $override->override(
        'Zaaksysteem::External::ValidSign::get_case' => sub {
            my $id = pop;
            return mock_case(id => $id, is_afgehandeld => 0);
        }
    );

    my $cb = $model->receive_event('PACKAGE_DECLINE');

    my $rv = $cb->($model, packageId => 1);
    is($rv, 1, "We have a declined package");

}




__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::External::ValidSign - Tests for the ValidSign model

=head1 DESCRIPTION

Glue layer between Zaaksysteem and ValidSign.

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::External::ValidSign;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
