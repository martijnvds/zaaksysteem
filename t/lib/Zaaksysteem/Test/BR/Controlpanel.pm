package Zaaksysteem::Test::BR::Controlpanel;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::BR::Controlpanel;

sub test__get_full_raw_fqdn {
    my $self = shift;

    my %controlpanel = (customer_type => mock_strict(value => 'government'));
    my $controlpanel = mock_strict(
        get_object_attribute => sub {
            my $val = shift;
            if (exists $controlpanel{$val}) {
                return $controlpanel{$val};
            }
            die "Unable to get_object_attribute '$val' from controlpanel";
        },
    );

    my %instance = (customer_type => mock_one(value => 'commercial'));
    my $instance = mock_strict(
        get_object_attribute => sub {
            my $val = shift;
            if (exists $instance{$val}) {
                return $instance{$val};
            }
            die "Unable to get_object_attribute '$val' from instance";
        },
    );

    throws_ok(
        sub {
            Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(host => undef);
        },
        qr/Validation of profile/,
        "Throws on missing params"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host          => "test1234.zaaksysteem.net",
            customer_type => "commercial",
            controlpanel  => $controlpanel,
        ),
        'test1234.zaaksysteem.net',
        "Check return of valid given FQDN"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host          => "test1234",
            customer_type => "commercial",
            controlpanel  => $controlpanel
        ),
        'test1234.zaaksysteem.net',
        '_get_full_raw_fqdn on commercial type'

    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host          => "test1234",
            customer_type => "government",
            controlpanel  => $controlpanel,
        ),
        'test1234.zaaksysteem.nl',
        '_get_full_raw_fqdn on gov type'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host         => "test1234",
            controlpanel => $controlpanel,
        ),
        'test1234.zaaksysteem.nl',
        '_get_full_raw_fqdn on controlpanel type'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host         => "test1234",
            controlpanel => $controlpanel,
            instance     => $instance,
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from instance settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_get_full_raw_fqdn(
            host          => "test1234",
            customer_type => 'commercial',
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from customer_type settings'
    );
}

sub test__verify_host_or_fqdn {

    my %controlpanel = (customer_type =>  mock_strict(value => 'government'));
    my $controlpanel = mock_strict(
        get_object_attribute => sub {
            my $val = shift;
            if (exists $controlpanel{$val}) {
                return $controlpanel{$val};
            }
            die "Unable to get_object_attribute '$val' from controlpanel";
        },
    );

    my %instance = (customer_type => mock_one(value => 'commercial'));
    my $instance = mock_strict(
        get_object_attribute => sub {
            my $val = shift;
            if (exists $instance{$val}) {
                return $instance{$val};
            }
            die "Unable to get_object_attribute '$val' from instance";
        },
    );

    my $objectmodel = mock_one(count => 0);

    throws_ok(
        sub {
            Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
                host    => undef,
            ),
        },
        qr/Validation of profile/,
        "Throws on missing params"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234.zaaksysteem.net",
            customer_type   => "commercial",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.net',
        "Check return of valid given FQDN"
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            customer_type   => "commercial",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from customer type settings'

    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            customer_type   => "government",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.nl',
        'Check transformation of host to government fqdn from customer type settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.nl',
        'Check transformation of host to government fqdn from controlpanel settings'
    );

    is(
        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test1234",
            controlpanel    => $controlpanel,
            instance        => $instance,
            objectmodel     => $objectmodel,
        ),
        'test1234.zaaksysteem.net',
        'Check transformation of host to commercial fqdn from instance settings'
    );

    ### Check messages
    {
        my $messages = {};
        my $objectmodel = mock_one(count => 1);

        is(
            Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
                host            => "test1234",
                controlpanel    => $controlpanel,
                instance        => $instance,
                objectmodel     => $objectmodel,
                messages        => $messages,
            ),
            undef,
            'Domain already exist'
        );

        like($messages->{fqdn}, qr/domain.*in use/,'Got domain already exist error message');

        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "wiki",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
            messages        => $messages,
        ),

        like($messages->{fqdn}, qr/reserved/,'Reserved hostname checking');

        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "bla.bla",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
            messages        => $messages,
        ),

        like($messages->{fqdn}, qr/does not contain a dot/,'single word hostname check');

        Zaaksysteem::BR::Controlpanel::_verify_host_or_fqdn(
            host            => "test.zaaksysteem.not",
            controlpanel    => $controlpanel,
            objectmodel     => $objectmodel,
            messages        => $messages,
        ),

        like($messages->{fqdn}, qr/does not contain a dot/,'single word hostname check');

    }
}

1;

__END__

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::BR::Controlpanel

=cut
=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
