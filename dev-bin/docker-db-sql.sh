#!/bin/bash

db=$1
directory=$2

[[ -z $db ]] && db=zaaksysteem
[[ -z $directory ]] && directory=/opt/zaaksysteem/db/upgrade/NEXT

for i in $directory/*.sql
do
    echo $i | grep -q rollback
    [ $? -eq 0 ] && continue
    psql -U zaaksysteem $db -f $i
done
