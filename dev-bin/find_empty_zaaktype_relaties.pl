#!/usr/bin/env perl

use strict;
use Data::Dumper;
use DBI;


my $db_user = '';
my $db_pass = '';


sub find_empty_relaties {
    my ($database_name) = @_;

    my $dbh = DBI->connect("DBI:Pg:dbname=" . $database_name,
        $db_user, $db_pass, {'RaiseError' => 1});


    my $has_zaaktype_node = $dbh->selectrow_hashref("select count(*) from pg_class where relname = 'zaaktype_node'");

    return unless $has_zaaktype_node->{count};

    my $relaties = $dbh->selectall_arrayref(
        "select n.zaaktype_id, n.titel, s.fase
        from zaaktype_node n, zaaktype_relatie r, zaaktype_status s, zaaktype z
        where r.relatie_zaaktype_id is null
        and n.id = r.zaaktype_node_id
        and n.id = z.zaaktype_node_id
        and s.id = r.zaaktype_status_id
        and z.id = n.zaaktype_id
        and z.deleted is null
        order by n.zaaktype_id, s.status");

    if (@$relaties) {
        print join "\n", map { "$database_name, " . join ", ", map { $_ || '-' } @$_ } @$relaties;
        print "\n";
    }

}


sub invoke_all_databases {
    my $dbh = DBI->connect("DBI:Pg:dbname=template1",
        $db_user, $db_pass, {'RaiseError' => 1});

    my $database_names = $dbh->selectcol_arrayref("select datname from pg_database");
    print "database, zaaktype_id, titel, fase\n";

    find_empty_relaties($_) for grep !/^(postgres|template)/, @$database_names;
}


invoke_all_databases;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

