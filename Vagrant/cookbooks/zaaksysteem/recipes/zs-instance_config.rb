# Installs zs-instance_config library from git repo

git "#{Chef::Config['file_cache_path']}/zs-instance_config-repo" do
    repository node['zaaksysteem']['instance_config']['repository']
    action :sync
    notifies :run, "bash[zs_instance_config_make_install]", :immediately
end

bash "zs_instance_config_make_install" do
    user "root"
    cwd "#{Chef::Config['file_cache_path']}/zs-instance_config-repo"
    code <<-EOS
        [[ -f Makefile ]] && make clean
        cpanm -i .
    EOS
    action :nothing
end
