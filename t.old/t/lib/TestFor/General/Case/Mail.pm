package TestFor::General::Case::Mail;

# ./zs_prove -v t/lib/TestFor/General/Case/Mail.pm
use base qw(ZSTest);

use TestSetup;

sub mail_action_mock_email_sender {
    my $callback = shift;

    my $mock = Test::MockObject->new();
    $mock->mock('send', sub {
        my $self = shift;
        $callback->(shift);
        return "message";
    });
    return $mock;
}


sub case_mail_action : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

        my $case = $zs->create_case_ok;

        my $second_case = $zs->create_case_ok;

        throws_ok(sub {
            $case->mail_action
        }, qr/Input not a HashRef, unable to assert profile/,
            "Fails when called without arguments");

        throws_ok(sub {
            $case->mail_action({wrong=>'params'})
        }, qr/Validation of profile failed/,
            "Fails when called without all required arguments");

        my $body = "Dit is de body éá#$$%ßßƒƒßƒß";
        my $recipient = 'jw@mintlab.nl';
        my $subject = 'Dit is het subject ™££¢£¢£∞éüî';

SKIP: {
    skip 'AUTUMN2015BREAK: DBIx::Class::Storage::DBI::txn_do(): Missing parameters: from',3;
        my $email_sender = mail_action_mock_email_sender(sub {
            my $email = shift;

            isa_ok $email, 'Email::MIME', 'Email object is an Email::MIME';

            is $email->body, Encode::encode("utf-8", $body), "Body came through encoded";
            is $email->header('To'), $recipient, "Recipient came through";
            is $email->header('Subject'), $subject, "Subject came through";
        });

        my $result = $case->mail_action({
            case => $second_case,
            email_sender => $email_sender,
            action_data => {
                rcpt => 'overig',
                email => $recipient,
                body => $body,
                subject => $subject
            }
        });

        is $result, "E-mail verstuurd", "Says that email has been sent";


}; # END SKIP

    }, 'Unit test Zaak::mail_action input validation');


}


sub case_notification_recipient : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

        my $case = $zs->create_case_ok;

        throws_ok(sub {
            $case->notification_recipient;
        }, qr/Input not a HashRef, unable to assert profile/,
            "Fails when called without arguments");

        throws_ok(sub {
            $case->notification_recipient({kaas => 1});
        }, qr/Validation of profile failed/,
            "Fails when called without all required arguments");

        for my $recipient_type (qw/aanvrager/){ #behandelaar medewerker coordinator overig/) {
            lives_ok(sub {
                $case->notification_recipient({
                    recipient_type => $recipient_type
                })
            }, 'Lives when called with a permitted recipient_type');
        };

        is $case->notification_recipient({ recipient_type => 'aanvrager' }),
            'devnull@zaaksysteem.nl',
            'Correct aanvrager email returned';


        my $subject = $zs->create_subject_ok();


        TODO: {
            local $TODO = 'AUTUMN2015BREAK';

            throws_ok(sub {
                $case->notification_recipient({ recipient_type => 'behandelaar' });
            }, qr/Behandelaar niet ingesteld/, 'Fails when no behandelaar supplied');
        };

        my $medewerker = $zs->create_medewerker_ok();

        my $result = $case->notification_recipient({
            recipient_type => 'behandelaar',
            behandelaar => 'betrokkene-medewerker-' . $medewerker->get_column('subject_id')
        });

        is $result, 'devnull@zaaksysteem.nl', 'Got correct behandelaar address';


        my $email = 'dfoijfgdoijfsdio@mintlab.nl';
        is $case->notification_recipient({
            recipient_type => 'overig',
            email => $email
        }), $email, "When recipient_type is overig input is returned";


        is $case->notification_recipient({
            recipient_type => 'overig',
            email => '[[aanvrager_email]]'
        }), 'devnull@zaaksysteem.nl', "When recipient_type is overig magic_strings are interpolated";


    }, 'Unit test Zaak::notification_recipient');
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

