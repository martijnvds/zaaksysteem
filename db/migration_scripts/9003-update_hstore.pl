#!/usr/bin/perl -w
use strict;

=head1 NAME

=head1 SYNOPSIS

    perl ./db/migration_scripts/9003-update_hstore.pl my.hostname.nl etc/zaaksysteem.conf etc/customer.d test.csv 1

=head1 COMMAND LINE

This script expects command line arguments, unnamed, in the following order:

=over

=item 1. Hostname (used to determine which database to connect to)

=item 2. Location of "zaaksysteem.conf"

=item 3. Location of the "customer.d" config file directory

=item 4. The CSV file to use as a basis for adding attributes

=item 5. "Commit bit". If this is a "true" value (to Perl), . Otherwise, changes will be rolled back.

=back

=head1 CSV FILE FORMAT

The CSV file should meet the following conditions:

=over

=item * There should be 4 fields: "case_id", "attribute_type", "attribute_value" and "attribute_name"

=item * The field separator should be a semicolon (";")

=item * Field values can be escaped with double quotes ("")

=back

=cut

use Moose;
use Data::Dumper;
use JSON;
use Time::HiRes qw(gettimeofday tv_interval);

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw[ConfigLoader];

use Text::CSV_XS;
use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Model::DB;
use Zaaksysteem::Model::LDAP;

use Config::Any;

my $log = Zaaksysteem::Log::CallingLogger->new();

error("USAGE: $0 [hostname] [configfile] [customer.d-dir] [inputfile] [commit]") unless @ARGV >= 4;
my ($hostname, $config_file, $config_dir, $input_file, $commit)    = @ARGV;

my $mig_config                              = {};

my $current_hostname;
sub load_customer_d {
    my $config_root = $config_dir;

    # Fetch configuration files
    opendir CONFIG, $config_root or die "Error reading config root: $!";
    my @confs = grep {/\.conf$/} readdir CONFIG;

    info("Found configuration files: @confs");

    for my $f (@confs) {
        my $config      = _process_config("$config_root/$f");
        my @customers   = $config;

        for my $customer (@customers) {
            for my $config_hostname (keys %$customer) {
                my $config_data     = $customer->{$config_hostname};

                if ($hostname eq $config_hostname ) {
                    info('Upgrading hostname: ' . $hostname);
                    $current_hostname = $hostname;
                    $mig_config->{database_dsn}         = $config_data->{'Model::DB'}->{connect_info}->{dsn};
                    $mig_config->{database_password}    = $config_data->{'Model::DB'}->{connect_info}->{password};
                    $mig_config->{ldap_config_basedn}   = $config_data->{LDAP}->{basedn};
                    $mig_config->{customer_info}        = { gemeente => $config_data->{customer_info} };
                }
            }
        }
    }
}

sub load_zaaksysteem_conf {
    my $config_root = 'etc/customer.d/';
    my $config      = _process_config($config_file);

    $mig_config->{ldap_config_hostname}         = $config->{LDAP}->{hostname};
    $mig_config->{ldap_config_password}         = $config->{LDAP}->{password};
    $mig_config->{ldap_config_user}             = $config->{LDAP}->{admin};
    $mig_config->{ldap_config}                  = $config->{LDAP};
}

sub _process_config {
    my ($config) = @_;
    return Config::Any->load_files({
        files       => [$config],
        use_ext     => 1,
        driver_args => {}
    })->[0]->{$config};
}

sub load_input_file {
    open my $fh, "<:encoding(UTF-8)", $input_file;

    my $csv = Text::CSV_XS->new({ binary => 1, sep_char => ';' });
    $csv->column_names($csv->getline($fh));

    for my $col (qw(case_id attribute_type attribute_value attribute_name)) {
        if (!grep { $_ eq $col } $csv->column_names) {
            die "Input file has no column '$col'\n";
        }
    }

    return $csv->getline_hr_all($fh);
}

load_customer_d();
load_zaaksysteem_conf();
my $csv_data = load_input_file();

error('Cannot find requested hostname: ' . $hostname) unless $current_hostname;

error('Missing one of required config params: ' . Data::Dumper::Dumper($mig_config))
    unless (
        $mig_config->{database_dsn}
    );

my $dbic = database($mig_config->{database_dsn}, $mig_config->{user}, $mig_config->{database_password});
my $ldap = build_ldap();

$dbic->ldap($ldap);
$dbic->betrokkene_model->ldap($ldap);

update_hstore($dbic, $csv_data);

$log->info('All done.');
$log->_flush;

sub build_ldap {
    my $ldap = Net::LDAP->new(
        $mig_config->{ldap_config_hostname},
        version => 3,
    );

    $ldap->bind(
        $mig_config->{ldap_config_user},
        password    => $mig_config->{ldap_config_password}
    ) or die "Could not bind";

    return Zaaksysteem::Backend::LDAP::Model->new(
        ldap       => $ldap,
        base_dn    => $mig_config->{ldap_config_basedn},
        ldapconfig => $mig_config->{ldap_config},
        ldapcache  => {},
    );
}

sub database {
    my ($dsn, $user, $password) = @_;

    my %connect_info = (
        dsn             => $dsn,
        pg_enable_utf8  => 1,
    );

    $connect_info{password}     = $password if $password;
    $connect_info{user}         = $user if $user;

    Zaaksysteem::Model::DB->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => \%connect_info
    );

    my $schema = Zaaksysteem::Model::DB->new->schema;

    $schema->log($log);
    $schema->catalyst_config($mig_config->{customer_info});

    return $schema;
}

sub error {
    my $error = shift;

    $log->error($error);
    $log->_flush;

    die("\n");
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

sub update_hstore {
    my ($dbic, $csv_data) = @_;

    my $starttime = [gettimeofday];
    my $cases_done = 0;

    for my $line (@$csv_data) {
        $cases_done++;

        $dbic->txn_do(sub {
            my $object_data = $dbic->resultset('ObjectData')->search(
                {
                    object_class => 'case',
                    object_id    => $line->{case_id},
                }
            )->single;

            if (!$object_data) {
                info(sprintf("No object_data found for case $line->{case_id}"));
                return;
            }

            $object_data->properties->{values}{ $line->{attribute_name} } =
                Zaaksysteem::Object::Attribute->new(
                    name           => $line->{attribute_name},
                    attribute_type => $line->{attribute_type},
                    value          => $line->{attribute_value},
                );
            $object_data->update();

            info(
                sprintf(
                    "Updated hstore for zaak $line->{case_id} (%d, %.3f/second)",
                    $cases_done,
                    $cases_done / tv_interval($starttime, [gettimeofday]),
                )
            );
        });
    };
}

1;
