
BEGIN;

  CREATE TABLE public.queue_evict ( like queue INCLUDING DEFAULTS);

  INSERT INTO public.queue_evict SELECT * FROM queue;

  TRUNCATE queue;

  INSERT INTO public.queue
    SELECT * FROM queue_evict WHERE
      status NOT IN ('finished', 'failed', 'cancelled')
    EXCEPT
    SELECT * FROM queue_evict
      WHERE parent_id IN (
        SELECT id FROM queue_evict WHERE type = 'run_ordered_item_set'
        AND status in ('finished', 'failed', 'cancelled')
      )
    ;

    DELETE FROM queue WHERE status = 'running' AND date_started < NOW() - interval '1d';

COMMIT;
