BEGIN;

  ALTER TABLE groups ALTER column id SET DEFAULT nextval('groups_id_seq');
  SELECT setval('groups_id_seq', (SELECT MAX(id) FROM groups));

COMMIT;
