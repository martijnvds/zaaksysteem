BEGIN;

ALTER TABLE zaaktype_node
    ADD adres_geojson BOOLEAN DEFAULT FALSE;

COMMIT;
