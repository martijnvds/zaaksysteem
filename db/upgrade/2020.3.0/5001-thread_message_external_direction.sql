BEGIN;
    ALTER TABLE thread_message_external ADD direction TEXT;
    UPDATE thread_message_external SET direction = 'unspecified' WHERE type = 'pip';
    UPDATE thread_message_external SET direction = 'outgoing' WHERE type = 'email';
    ALTER TABLE thread_message_external ALTER direction SET NOT NULL;
    ALTER TABLE thread_message_external ADD CONSTRAINT thread_message_external_direction_constraint CHECK(direction IN ('incoming', 'outgoing', 'unspecified'));
COMMIT;
