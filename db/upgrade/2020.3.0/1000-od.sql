
BEGIN;

  alter table object_data alter column date_created SET default now();

COMMIT;

BEGIN;
  alter table object_data alter column date_created SET NOT NULL;
COMMIT;
