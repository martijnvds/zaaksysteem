BEGIN;

ALTER TABLE thread_message_external ADD COLUMN read_pip TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE thread_message_external ADD COLUMN read_employee TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE thread ADD COLUMN unread_pip_count INTEGER NOT NULL DEFAULT 0;
ALTER TABLE thread ADD COLUMN unread_employee_count INTEGER NOT NULL DEFAULT 0;

UPDATE thread_message_external SET 
    read_pip = (SELECT created FROM thread_message WHERE thread_message_external.id = thread_message.thread_message_external_id)
    ,read_employee = (SELECT created FROM thread_message WHERE thread_message_external.id = thread_message.thread_message_external_id)
;

COMMIT;