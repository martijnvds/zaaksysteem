BEGIN;

ALTER TABLE custom_object_type ADD COLUMN authorization_definition JSONB;

COMMIT;
