BEGIN;

ALTER TABLE thread_message_external ALTER COLUMN participants SET DEFAULT '[]';

COMMIT;
