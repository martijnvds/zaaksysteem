BEGIN;

ALTER TABLE natuurlijk_persoon ALTER COLUMN voorletters TYPE character varying(50);
ALTER TABLE gm_natuurlijk_persoon ALTER COLUMN voorletters TYPE character varying(50);

COMMIT;
