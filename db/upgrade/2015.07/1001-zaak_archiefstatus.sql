BEGIN;

    ALTER TABLE zaak ADD archival_state TEXT;
    ALTER TABLE zaak ADD CONSTRAINT archival_state_value CHECK(archival_state IN ('overdragen', 'vernietigen'));

    UPDATE zaak SET archival_state = 'vernietigen' WHERE status = 'resolved';
    UPDATE zaak SET archival_state = 'overdragen', status = 'resolved' WHERE status = 'overdragen';

    ALTER TABLE zaak ADD _status TEXT;
    UPDATE zaak SET _status = status;
    ALTER TABLE zaak DROP COLUMN status;
    ALTER TABLE zaak RENAME _status TO status;
    ALTER TABLE zaak ALTER status SET NOT NULL;

    ALTER TABLE zaak ADD CONSTRAINT status_value CHECK(status IN ('new', 'open', 'stalled', 'resolved', 'deleted'));

    ALTER TABLE zaaktype_resultaten ADD trigger_archival BOOLEAN NOT NULL DEFAULT TRUE;

COMMIT;
