BEGIN;

    -- Copy possible errors to a second table so we can investigate
    -- later on
    CREATE TABLE object_mutation_in_error AS (
        SELECT * FROM object_mutation WHERE executed = TRUE AND lock_object_uuid IS NOT NULL
    );
    DELETE FROM object_mutation WHERE executed = TRUE AND lock_object_uuid IS NOT NULL;

    INSERT INTO queue (type, label, priority, metadata, data)
        SELECT 'touch_case', 'ZS-161200 on files for cases', 3000, '{"require_object_model":1, "disable_acl": 1, "target":"backend"}',
            '{"case_object_id":"' || uuid || '"}'
            FROM object_data WHERE properties LIKE '%original_name":null%' AND object_class = 'case';

COMMIT;


