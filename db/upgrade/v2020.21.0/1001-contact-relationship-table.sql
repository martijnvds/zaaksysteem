BEGIN;

  DROP VIEW IF EXISTS contact_relationship_view;

  CREATE VIEW contact_relationship_view AS
  SELECT
    contact,
    contact_uuid,
    contact_type,
    relation,
    relation_uuid,
    relation_type
  FROM
    contact_relationship
  UNION ALL
  SELECT
    related_person_id,
    related_uuid,
    'person',
    custom_object_id,
    co.uuid,
    'custom_object'
  FROM custom_object_relationship
  JOIN custom_object co
  ON custom_object_relationship.custom_object_id = co.id
  WHERE related_person_id IS NOT NULL
  UNION ALL
  SELECT
    related_organization_id,
    related_uuid,
    'company',
    custom_object_id,
    co.uuid,
    'custom_object'
  FROM custom_object_relationship
  JOIN custom_object co
  ON custom_object_relationship.custom_object_id = co.id
  WHERE related_organization_id IS NOT NULL
  UNION ALL
  SELECT
    related_employee_id,
    related_uuid,
    'employee',
    custom_object_id,
    co.uuid,
    'custom_object'
  FROM custom_object_relationship
  JOIN custom_object co
  ON custom_object_relationship.custom_object_id = co.id
  WHERE related_employee_id IS NOT NULL
  ;

COMMIT;
