BEGIN;

ALTER TABLE contactmoment ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
CREATE INDEX contactmoment_uuid_idx ON contactmoment (uuid);

COMMIT;
