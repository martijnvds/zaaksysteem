BEGIN;

  ALTER TABLE zaaktype_sjablonen ADD COLUMN display_name TEXT;

COMMIT;

BEGIN;

  ALTER TABLE zaaktype_sjablonen RENAME COLUMN display_name TO label;

COMMIT;
