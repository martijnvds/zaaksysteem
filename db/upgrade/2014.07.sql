-- have the db stop accepting useless types

BEGIN;
ALTER TABLE bibliotheek_kenmerken DROP CONSTRAINT IF EXISTS bibliotheek_kenmerken_value_type_check;
ALTER TABLE bibliotheek_kenmerken ADD CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK (((value_type)::text ~ '^(text_uc|checkbox|richtext|date|file|bag_straat_adres|email|valutaex|bag_openbareruimte|text|bag_openbareruimtes|url|valuta|option|bag_adres|select|valutain6|valutaex6|valutaex21|image_from_url|bag_adressen|valutain|calendar|bag_straat_adressen|googlemaps|numeric|valutain21|textarea|bankaccount)$'::text));

-- Safe because the contents of this table are transient anyway
DROP TABLE IF EXISTS object_type_bibliotheek_entry;

CREATE TABLE object_type_bibliotheek_entry (
    id SERIAL PRIMARY KEY,
    search_term TEXT NOT NULL,
    object_type TEXT NOT NULL DEFAULT 'type',
    bibliotheek_categorie_id INTEGER NOT NULL,
    object_uuid UUID NOT NULL,
    CONSTRAINT bibliotheek_categorie_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie (id) ON DELETE CASCADE,
    CONSTRAINT object_data FOREIGN KEY (object_uuid) REFERENCES object_data (uuid) ON DELETE CASCADE
);

ALTER TABLE object_type_bibliotheek_entry ALTER object_type DROP DEFAULT;
ALTER TABLE object_type_bibliotheek_entry RENAME TO object_bibliotheek_entry;

ALTER TABLE object_relationships ADD object1_type TEXT;
ALTER TABLE object_relationships ADD object2_type TEXT;

UPDATE object_relationships SET 
      object1_type = (SELECT object_class FROM object_data WHERE uuid = object_relationships.object1_uuid)
    , object2_type = (SELECT object_class FROM object_data WHERE uuid = object_relationships.object2_uuid)
;

ALTER TABLE object_relationships ALTER object1_type SET NOT NULL;
ALTER TABLE object_relationships ALTER object2_type SET NOT NULL;

ALTER TABLE object_bibliotheek_entry
    ADD search_index tsvector;
ALTER TABLE object_bibliotheek_entry
    ADD searchable_id INTEGER NOT NULL DEFAULT nextval('searchable_searchable_id_seq');

ALTER TABLE searchable ALTER object_type SET DATA TYPE text;
ALTER TABLE object_bibliotheek_entry inherit searchable;

-- This query fixes a legacy problem with casetype settings.
-- Any subcase that is configured for the registration phase needs
-- to be started automatically. Due to an HTML error this didn't
-- happen - A disabled checkbox indicated it would, but didn't
-- tell the backend.

UPDATE zaaktype_relatie r SET automatisch_behandelen = true
FROM zaaktype_status s
WHERE s.id = r.zaaktype_status_id
AND s.status = 1;

-- to check for success, these should be all true:
-- SELECT r.automatisch_behandelen FROM zaaktype_relatie r
-- JOIN zaaktype_status s ON s.id = r.zaaktype_status_id
-- WHERE s.status = 1;

INSERT INTO
    zaaktype_betrokkenen (zaaktype_node_id, betrokkene_type, created, last_modified)
(
    SELECT
        zaaktype_node_id, 'niet_natuurlijk_persoon', created, last_modified
    FROM
        zaaktype_betrokkenen
    WHERE
        betrokkene_type = 'niet_natuurlijk_persoon_na' and
        zaaktype_node_id NOT IN (
            SELECT
                zaaktype_node_id
            FROM
                zaaktype_betrokkenen
            WHERE betrokkene_type = 'niet_natuurlijk_persoon'
        )
);

ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS geboortegemeente;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS geboortegemeente_omschrijving;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS geboorteregio;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS indicatie_curatele;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS indicatie_gezag;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS aanduiding_verblijfsrecht;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS datum_aanvang_verblijfsrecht;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS datum_einde_verblijfsrecht;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS aanduiding_soort_vreemdeling;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS land_vanwaar_ingeschreven;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS nnp_ts;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS hash;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS email;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS telefoon;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_persoon_ingang;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_persoon_einde;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_persoon_onjuist;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_huwelijk_ingang;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_huwelijk_einde;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_huwelijk_onjuist;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_overlijden_ingang;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_overlijden_einde;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_overlijden_onjuist;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_verblijfplaats_ingang;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_verblijfplaats_einde;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_verblijfplaats_onjuist;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS system_of_record;
ALTER TABLE natuurlijk_persoon DROP COLUMN IF EXISTS system_of_record_id;

ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS geboortegemeente;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS geboortegemeente_omschrijving;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS geboorteregio;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS indicatie_curatele;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS indicatie_gezag;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS aanduiding_verblijfsrecht;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS datum_aanvang_verblijfsrecht;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS datum_einde_verblijfsrecht;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS aanduiding_soort_vreemdeling;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS land_vanwaar_ingeschreven;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS land_waarnaar_vertrokken;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS nnp_ts;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS hash;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_persoon_ingang;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_persoon_einde;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_persoon_onjuist;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_huwelijk_ingang;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_huwelijk_einde;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_huwelijk_onjuist;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_overlijden_ingang;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_overlijden_einde;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_overlijden_onjuist;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_verblijfplaats_ingang;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_verblijfplaats_einde;
ALTER TABLE gm_natuurlijk_persoon DROP COLUMN IF EXISTS onderzoek_verblijfplaats_onjuist;

ALTER TABLE natuurlijk_persoon ADD COLUMN in_gemeente BOOLEAN;

UPDATE natuurlijk_persoon SET in_gemeente = true WHERE authenticated = true;

ALTER TABLE transaction ADD COLUMN error_message TEXT;
ALTER TABLE transaction ADD COLUMN text_vector tsvector;

CREATE INDEX ON transaction USING gist(text_vector);

COMMIT;
