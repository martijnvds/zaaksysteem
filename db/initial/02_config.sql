CREATE OR REPLACE FUNCTION create_default_config(param text, mydefault text)
  RETURNS void AS
$BODY$
BEGIN

    INSERT INTO config (parameter, value) SELECT param, mydefault
    WHERE NOT EXISTS (
            SELECT id FROM config WHERE parameter = param
    );


END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION create_default_config(text, text)
  OWNER TO zaaksysteem;

BEGIN;

    ALTER TABLE config DROP COLUMN definition_id;

    SELECT create_default_config('allocation_notification_template_id', '');
    SELECT create_default_config('allowed_templates', '');
    SELECT create_default_config('app_enabled_meeting', '0');
    SELECT create_default_config('app_enabled_wordapp', '0');
    SELECT create_default_config('case_distributor_group', '');
    SELECT create_default_config('case_distributor_role', '');
    SELECT create_default_config('customer_info_adres', 'H.J.E. Wenckbachweg 90');
    SELECT create_default_config('customer_info_email', 'servicedesk@mintlab.nl');
    SELECT create_default_config('customer_info_faxnummer', '');
    SELECT create_default_config('customer_info_gemeente_id_url', 'https://zaaksysteem.nl');
    SELECT create_default_config('customer_info_gemeente_portal', 'https://zaaksysteem.nl');
    SELECT create_default_config('customer_info_huisnummer', '90');
    SELECT create_default_config('customer_info_latitude', '52.3375433');
    SELECT create_default_config('customer_info_longitude', '4.9259539');
    SELECT create_default_config('customer_info_naam', 'Ontwikkelomgeving');
    SELECT create_default_config('customer_info_naam_kort','dev.zs.nl');
    SELECT create_default_config('customer_info_naam_lang','Ontwikkelomgeving dev.zaaksysteem.nl');
    SELECT create_default_config('customer_info_postbus','');
    SELECT create_default_config('customer_info_postbus_postcode','');
    SELECT create_default_config('customer_info_postcode','1114AD');
    SELECT create_default_config('customer_info_straatnaam','H.J.E. Wenckbachweg');
    SELECT create_default_config('customer_info_telefoonnummer','020 - 737 000 5');
    SELECT create_default_config('customer_info_website','https://zaaksysteem.nl');
    SELECT create_default_config('customer_info_woonplaats','Amsterdam-Duivendrecht');
    SELECT create_default_config('customer_info_zaak_email','no-reply@zaaksysteem.nl');
    SELECT create_default_config('custom_relation_roles','[]');
    SELECT create_default_config('disable_dashboard_customization', '0');
    SELECT create_default_config('document_intake_user', '');
    SELECT create_default_config('enable_mintlab_id', '1');
    SELECT create_default_config('enable_stufzkn_simulator', '1');
    SELECT create_default_config('feedback_email_template_id', '');
    SELECT create_default_config('files_locally_editable', '0');
    SELECT create_default_config('file_username_seperator', '-');
    SELECT create_default_config('first_login_confirmation', '');
    SELECT create_default_config('first_login_intro', '');
    SELECT create_default_config('new_user_template', '');
    SELECT create_default_config('pdf_annotations_public', '0');
    SELECT create_default_config('pip_login_intro', '');
    SELECT create_default_config('public_manpage', '1');
    SELECT create_default_config('requestor_search_extension_active', '');
    SELECT create_default_config('requestor_search_extension_href', '');
    SELECT create_default_config('requestor_search_extension_name', '');
    SELECT create_default_config('signature_upload_role', '');
    SELECT create_default_config('subject_pip_authorization_confirmation_template_id', '');
    SELECT create_default_config('users_can_change_password', '0');
    SELECT create_default_config('bag_spoof_mode', '1');
    SELECT create_default_config('bag_local_only', '0');
    SELECT create_default_config('bag_priority_gemeentes', '[]');
    SELECT create_default_config('edit_document_online', '0');

    ALTER TABLE config ADD COLUMN definition_id UUID UNIQUE;

    UPDATE config SET "value" = CASE
        WHEN "value" = '' THEN '[]'
        ELSE '["' || regexp_replace("value", ',', '","', 'g') || '"]'
    END WHERE "parameter" = 'allowed_templates';

    UPDATE config SET definition_id = CASE
        WHEN (parameter = 'allocation_notification_template_id') THEN '8c3f8850-435d-4b64-8489-4285cdeedf7e'::UUID
        WHEN (parameter = 'allowed_templates') THEN '33aeed61-95bb-4dd7-9022-f2ba5aa2b718'::UUID
        WHEN (parameter = 'app_enabled_meeting') THEN '287fd068-0999-4c44-a504-a9538f19a763'::UUID
        WHEN (parameter = 'app_enabled_wordapp') THEN '2c3ed588-6770-41df-8c95-2987b9bd6c61'::UUID
        WHEN (parameter = 'case_distributor_group') THEN '82f270d2-498e-4ce9-adcd-cb23331daf69'::UUID
        WHEN (parameter = 'case_distributor_role') THEN '7362679d-b8f6-493b-bcb1-b8d9350b443e'::UUID
        WHEN (parameter = 'customer_info') THEN '97971af3-f072-42dc-9ea3-930bd582b101'::UUID
        WHEN (parameter = 'customer_info_adres') THEN '33e32331-e6d6-4031-85c7-59c7eb1163bc'::UUID
        WHEN (parameter = 'customer_info_email') THEN '239bbdee-0811-42c3-8cc2-10d20b8b1e30'::UUID
        WHEN (parameter = 'customer_info_faxnummer') THEN '3499c848-0903-47aa-9d86-f3bc4190e7e5'::UUID
        WHEN (parameter = 'customer_info_gemeente_id_url') THEN '424ff3bc-bcf6-4772-872a-b9a93a2f3c34'::UUID
        WHEN (parameter = 'customer_info_gemeente_portal') THEN '11fa98a7-4fd4-4dbc-a929-c2e6ab25f49b'::UUID
        WHEN (parameter = 'customer_info_huisnummer') THEN '4bd449ae-d82d-43ed-aa84-72fe01b7462d'::UUID
        WHEN (parameter = 'customer_info_latitude') THEN 'd81f8648-2a36-4ea0-814e-2af227a5da8a'::UUID
        WHEN (parameter = 'customer_info_longitude') THEN '165a4192-8c28-4e74-904b-2317b00e2e23'::UUID
        WHEN (parameter = 'customer_info_naam') THEN '5f2fef2a-4833-4847-9b1a-236039700fb0'::UUID
        WHEN (parameter = 'customer_info_naam_kort') THEN 'fadcf16c-4bbe-4157-9771-e156ca0ae205'::UUID
        WHEN (parameter = 'customer_info_naam_lang') THEN '945bd84a-d3cc-412f-a801-e89787deaf29'::UUID
        WHEN (parameter = 'customer_info_postbus') THEN 'eece146f-97e3-4315-a4dd-e6751360059b'::UUID
        WHEN (parameter = 'customer_info_postbus_postcode') THEN '02ec24a4-b511-4596-ad22-1b2a381f26ee'::UUID
        WHEN (parameter = 'customer_info_postcode') THEN 'f3e9df2b-61cc-4d38-9feb-35dda8a5d22a'::UUID
        WHEN (parameter = 'customer_info_straatnaam') THEN '2e35ebfd-e6e0-4124-8e4f-7c28cfffeefd'::UUID
        WHEN (parameter = 'customer_info_telefoonnummer') THEN '4eea2794-28ac-4d24-a07e-e13d75528d1e'::UUID
        WHEN (parameter = 'customer_info_website') THEN 'e9cf0c02-f64f-44a2-b7d7-ac6004218084'::UUID
        WHEN (parameter = 'customer_info_woonplaats') THEN '086512fa-4b43-48c6-94da-7c9d1ccc895e'::UUID
        WHEN (parameter = 'customer_info_zaak_email') THEN 'a21cbc94-b624-4a6a-8e10-3fa73c6148c8'::UUID
        WHEN (parameter = 'custom_relation_roles') THEN 'e73874c6-e81a-49cb-9b87-05deb68e1118'::UUID
        WHEN (parameter = 'disable_dashboard_customization') THEN '1c7b45b7-f101-4b7e-8514-f412063cc0c8'::UUID
        WHEN (parameter = 'document_intake_user') THEN '7561d210-b9be-4e29-9b65-c4f8a2de1fd4'::UUID
        WHEN (parameter = 'enable_mintlab_id') THEN 'e3d6909a-134b-4317-b61f-1b7dca1569b5'::UUID
        WHEN (parameter = 'enable_stufzkn_simulator') THEN '42f9b366-e324-4b79-abcc-a5d699290da6'::UUID
        WHEN (parameter = 'feedback_email_template_id') THEN '1acf71e1-aab1-4951-8e7a-db7cdaaf6a2b'::UUID
        WHEN (parameter = 'files_locally_editable') THEN '92236ee6-eb05-44f3-bd0a-5bca376749b4'::UUID
        WHEN (parameter = 'file_username_seperator') THEN '98a664ba-e652-4ac0-8bd5-a6962389c6c6'::UUID
        WHEN (parameter = 'first_login_confirmation') THEN 'f4ebe619-933c-4ead-b664-506a9c6e1ea0'::UUID
        WHEN (parameter = 'first_login_intro') THEN 'a34e8a0c-6427-4e97-a14b-db0f12783a21'::UUID
        WHEN (parameter = 'new_user_template') THEN '6271cb81-e5fb-40fd-9385-d8544fee75f0'::UUID
        WHEN (parameter = 'pdf_annotations_public') THEN '608f73b1-f15f-4985-8dc6-06ce2d9c257a'::UUID
        WHEN (parameter = 'pip_login_intro') THEN '2f9d7541-8e22-4dfc-9934-1374c55175b4'::UUID
        WHEN (parameter = 'public_manpage') THEN '2ea16761-23b7-4964-9548-2d54861cc78d'::UUID
        WHEN (parameter = 'requestor_search_extension_active') THEN '348a00a6-4163-49f4-9279-eef1ccb19ae6'::UUID
        WHEN (parameter = 'requestor_search_extension_href') THEN '7fcfb2ea-8f46-475c-8f89-e09b2c28c828'::UUID
        WHEN (parameter = 'requestor_search_extension_name') THEN '836c7e4b-a896-4c25-a7a4-6f138f75d41e'::UUID
        WHEN (parameter = 'signature_upload_role') THEN '09c5e460-2fd3-4153-84db-7f9159fe4f07'::UUID
        WHEN (parameter = 'subject_pip_authorization_confirmation_template_id') THEN '2cd00952-d6a0-4096-93c9-7515108c798d'::UUID
        WHEN (parameter = 'users_can_change_password') THEN '49999eee-96b4-46c4-babc-8fa6bb5978cb'::UUID
        WHEN (parameter = 'bag_spoof_mode') THEN '9b0956d2-c9a6-4e84-8c49-0f53d17551a1'::UUID
        WHEN (parameter = 'bag_priority_gemeentes') THEN '55482181-7e7d-4bc2-b653-356762a91ade'::UUID
        WHEN (parameter = 'bag_local_only') THEN '399be534-a816-42b9-bae8-f2899405b7b8'::UUID
        WHEN (parameter = 'edit_document_online') THEN '8d4a83c1-dff9-48d0-8481-10eba0baa129'::UUID
        WHEN (parameter = 'edit_document_msonline') THEN '1bab86db-09d7-4062-a7c6-64e11fdc7959'::UUID
        WHEN (parameter = 'show_object_v1') THEN 'd1532d86-536e-4254-a60a-8f30fcf09903'::UUID
        WHEN (parameter = 'show_object_v2') THEN '96f00720-2db7-416b-96f0-0552851c4012'::UUID
        
    END;

    ALTER TABLE config ALTER COLUMN definition_id SET NOT NULL;

COMMIT;

DROP FUNCTION IF EXISTS create_default_config(text, text);
