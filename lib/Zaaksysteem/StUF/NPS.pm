package Zaaksysteem::StUF::NPS;
use Moose::Role;

use BTTW::Tools;
use Data::FormValidator;
use Zaaksysteem::Constants;
use Zaaksysteem::StUF::Body::Field;
use List::Util qw(first);

use constant NATUURLIJK_PERSOON_MAP => {
    'inp.a-nummer'                    => 'a_nummer',
    'inp.bsn'                         => 'burgerservicenummer',
    'voornamen'                       => 'voornamen',
    'voorletters'                     => 'voorletters',
    'voorvoegselGeslachtsnaam'        => 'voorvoegsel',
    'geslachtsnaam'                   => 'geslachtsnaam',
    'geboortedatum'                   => 'geboortedatum',
    'geslachtsaanduiding'             => 'geslachtsaanduiding',
    'overlijdensdatum'                => 'datum_overlijden',
    'inp.indicatieGeheim'             => 'indicatie_geheim',
    'burgerlijkeStaat'                => 'burgerlijkestaat',
    'aanduidingNaamgebruik'           => 'aanduiding_naamgebruik',
    'geslachtsnaamPartner'            => 'partner_geslachtsnaam',
    'voorvoegselGeslachtsnaamPartner' => 'partner_voorvoegsel',
};

use constant NATUURLIJK_PERSOON_PARTNER_MAP         => {
    'inp.a-nummer'                  => 'partner_a_nummer',
    'inp.bsn'                       => 'partner_burgerservicenummer',
    'voorvoegselGeslachtsnaam'      => 'partner_voorvoegsel',
    'geslachtsnaam'                 => 'partner_geslachtsnaam',
};

use constant NATUURLIJK_PERSOON_ADRES_MAP => {
    'aoa.identificatie'        => 'bag_id',
    'aoa.postcode'             => 'postcode',
    'postcode'                 => 'postcode',
    'wpl.woonplaatsNaam'       => 'woonplaats',
    'gor.straatnaam'           => 'straatnaam',
    'aoa.huisnummer'           => 'huisnummer',
    'aoa.huisletter'           => 'huisletter',
    'aoa.huisnummertoevoeging' => 'huisnummertoevoeging',
    'gemeentecode'             => 'gemeente_code',
    'landcode'                 => 'landcode',
    'sub.adresBuitenland1'     => 'adres_buitenland1',
    'sub.adresBuitenland2'     => 'adres_buitenland2',
    'sub.adresBuitenland3'     => 'adres_buitenland3',
};

=head2 METHODS

=head2 get_params_for_natuurlijk_persoon

Gets a set of params for manipulating natuurlijk_persoon

=cut

sub get_params_for_natuurlijk_persoon {
    my $self            = shift;
    my $index           = shift || 0;

    my $params          = {};
    my $object_params   = (
        UNIVERSAL::isa($self->as_params, 'ARRAY')
            ? $self->as_params->[$index]->{NPS}
            : $self->as_params->{NPS}
    );

    for my $key (keys %{ NATUURLIJK_PERSOON_MAP() }) {
        next unless exists($object_params->{$key});
        $params->{NATUURLIJK_PERSOON_MAP->{$key}} = $object_params->{ $key };
    }

    $self->_get_params_for_natuurlijk_persoon_partner($index, $params, $object_params);

    ### Validate params
    my $valid_params    = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_GBA_PROFILE
    )->valid;

    return $valid_params;
}

=head2 get_params_for_natuurlijk_persoon_adres

Gets a set of params for manipulating the adres entry for a PRS.

=cut

sub get_params_for_natuurlijk_persoon_adres {
    my $self                        = shift;
    my $index                       = shift || 0;
    my $object_params               = shift;
    my ($params, $address)          = ({}, {});

    unless ($object_params) {
        $object_params = (
            UNIVERSAL::isa($self->as_params, 'ARRAY')
            ? $self->as_params->[$index]->{NPS}
            : $self->as_params->{NPS}
        );
    }

    return unless (
        ($object_params->{verblijfsadres} && $object_params->{verblijfsadres}->[0]->{verblijfsadres}->{'gor.straatnaam'}) ||
        $object_params->{'sub.verblijfBuitenland'} ||
        $object_params->{'sub.correspondentieAdres'}
    );

    if ($object_params->{verblijfsadres} && $object_params->{verblijfsadres}->[0]->{verblijfsadres}->{'gor.straatnaam'}) {
        $address                    = $object_params->{verblijfsadres}->[0]->{verblijfsadres};
        $params->{functie_adres}    = 'W';
    } elsif ($object_params->{'sub.verblijfBuitenland'}) {
        $address                    = $object_params->{'sub.verblijfBuitenland'}->[0]->{'verblijfsadres'};
        $params->{functie_adres}    = 'W';
    }

    my $coraddress;
    if ($object_params->{'sub.correspondentieAdres'} && $object_params->{'sub.correspondentieAdres'}->[0]->{'verblijfsadres'}->{'gor.straatnaam'}) {
        $coraddress                 = $object_params->{'sub.correspondentieAdres'}->[0]->{'verblijfsadres'};
        $params->{functie_adres}    = 'B';
    }



    for my $key (keys %{ NATUURLIJK_PERSOON_ADRES_MAP() }) {
        if ($coraddress && exists $coraddress->{$key}) {
            $params->{'correspondentie_' . NATUURLIJK_PERSOON_ADRES_MAP->{$key}} = $coraddress->{ $key };
        }

        if ($address && exists($address->{$key})) {
            $params->{NATUURLIJK_PERSOON_ADRES_MAP->{$key}} = $address->{ $key };
        }
    }

    if (
        $object_params->{extraElementen} &&
        $object_params->{extraElementen}->{gemeenteCode}
    ) {
        $params->{gemeente_code} = $object_params->{extraElementen}->{gemeenteCode};
    }

    # Cleanup migration from country. When switching from a foreign country to
    # The Netherlands, we forget updating the zipcode etc. Clean it manually

    $self->_clear_foreign_params($params, 'correspondentie_') if $coraddress;
    $self->_clear_foreign_params($params) if $address;

    my $valid_params = Data::FormValidator->check(
        $params,
        GEGEVENSMAGAZIJN_GBA_PROFILE
    )->valid;

    return $valid_params;
}


=head2 _clear_foreign_params

Identifies country, and empties the unnecessary fields: on 6030 (Netherlands), it clears adres_buitenlandX. When there is
a different landcode given, we clear postcode huisnummer straatnaam woonplaats huisnummertoevoeging huisletter gemeente_code.

=cut

sub _clear_foreign_params {
    my $self    = shift;
    my $params  = shift;
    my $type    = shift || '';

    if (!$params->{"${type}landcode"} || $params->{"${type}landcode"} eq '6030') {
        $params->{$type . $_}   = undef for qw/adres_buitenland1 adres_buitenland2 adres_buitenland3/;
    } else {
        $params->{$type . $_}   = undef for qw/postcode huisnummer straatnaam woonplaats huisnummertoevoeging huisletter gemeente_code/;
    }
}

=head2 updated_components

    my $updated = $stuf->updated_components

    # {
    #     verblijfsadres    => 1,
    #     buitenland        => 1,
    #     correspondentie   => 1,
    #     partner           => 1,
    # }

Returns a list of changed components in this stuf message. Sometimes, when a "mutatiesoort = W" is given, not everything
is sent to us. This way we know if we have to empty or ignore the change.

When is a component updated?

1) There is an entry in the "new" block (second block) of the StUF "wijziging/correctie" message.
2) There is an entry in the (single) block of the StUF "toevoeging" message.
3) There is an entry in the "old" block (first block) of the StUF "wijziging/correctie" message, but none in the "new" block (second block).

=cut

sub updated_components {
    my $self        = shift;
    my %changed;

    if ($self->mutatiesoort eq 'T') {
        my $things = $self->body->[0]->as_params->{'NPS'};
        return $self->updated_component($things);
    }
    else {
        my $old = $self->body->[0]->as_params->{'NPS'};
        my $new = $self->body->[1]->as_params->{'NPS'};

        $new = $self->updated_component($new);
        $old = $self->updated_component($old) if $old;

        return $new if !$old;

        ## Compare
        %changed = %$new;
        for my $key (keys %$old) {
            if (!$new->{$key}) {
                $changed{$key} = 1;
            }
        }

        return \%changed;
    }
}

=head2 updated_component

    my $updated = $stuf->updated_component($params)

    # {
    #     verblijfsadres    => 1,
    #     buitenland        => 1,
    #     correspondentie   => 1,
    #     partner           => 1,
    # }

Returns a list of changed components from the given list of parameters.

See for more information L<updated_components>

=cut

sub updated_component {
    my $self    = shift;
    my $params  = shift;
    my %changed;

    if ($params->{'verblijfsadres'}) {
        $changed{'verblijfsadres'} = 1;
    } elsif ($params->{'sub.verblijfBuitenland'}) {
        $changed{'buitenland'} = 1;
    }

    if ($params->{'sub.correspondentieAdres'}) {
        $changed{'correspondentie'} = 1;
    }

    if ($params->{geslachtsnaamPartner}) {
        $changed{'partner'} = 1;
    }
    elsif ($params->{'inp.heeftAlsEchtgenootPartner'}) {
        $changed{'partner'} = 1;
    }

    return \%changed;
}


=head2 get_params_for_natuurlijk_persoon_partner

Gets a set of params for manipulating the partner entry for a PRS.

=cut

sub _get_params_for_natuurlijk_persoon_partner {
    my $self                                = shift;
    my ($index, $params, $object_params)    = @_;

    my $partner = $self->get_active_partner($index);
    return unless $partner;

    for my $key (keys %{ NATUURLIJK_PERSOON_PARTNER_MAP() }) {
        next unless exists $partner->{$key};

        # Don't override the value if we already have one (mainly here for
        # parnter geslachtsnaam).
        next if exists $params->{NATUURLIJK_PERSOON_PARTNER_MAP->{$key}};

        $params->{NATUURLIJK_PERSOON_PARTNER_MAP->{$key}} = $partner->{ $key };
    }
}

=head2 get_active_parnter

Grab the active partner from the XML message. This is indicated by the
C<is_active> bit in the XML.  If all the partners are inactive we take the last
partner, based on the C<datumSluiting> bit in the XML. When nothing is found,
this function returns C<undef>.

=cut

sub get_active_partner {
    my $self                        = shift;
    my $index                       = shift || 0;

    my $as_params   = (
        UNIVERSAL::isa($self->as_params, 'ARRAY')
            ? $self->as_params->[$index]
            : $self->as_params
    );

    return unless exists $as_params->{NPS}{'inp.heeftAlsEchtgenootPartner'};
    my @partners = @{$as_params->{NPS}{'inp.heeftAlsEchtgenootPartner'}};

    my $active = List::Util::first { $_->{is_active} } @partners;
    return $active->{NPS} if $active;

    my ($last) = sort { int($b->{datumSluiting}) <=> int($a->{datumSluiting}) }
        @partners;
    return $last->{NPS} if $last;
    return;
}

=head2 Active calls


=head2 search

=cut

define_profile search => (
    required        => [qw/reference_id date/],
    optional        => [qw/
        bsn-nummer
        voornamen
        voorletters
        voorvoegsel
        geslachtsnaam
        geslachtsaanduiding
        geboortedatum
        sleutelGegevensbeheer
    /],
);

sub search {
    my $self                        = shift;
    my $options                     = assert_profile(shift || {})->valid;

    throw(
        'stuf/set_afnemerindicatie/invalid_stuurgegevens',
        'Cannot run this function without stuurgegevens set on object'
    ) unless $self->stuurgegevens;

    my $stufobject      = Zaaksysteem::StUF->new(
        entiteittype    => $self->entiteittype,
        stuurgegevens   => $self->stuurgegevens,
        namespace       => 'http://www.egem.nl/StUF/sector/bg/0310',
        element         => 'npsLv01',
        asynchroon      => 1,
        version         => $self->version,
        #soap_ssl_key    => $self->soap_ssl_key,
        #soap_ssl_crt    => $self->soap_ssl_crt,
    );

    $stufobject->stuurgegevens->berichtcode('Lv01');
    $stufobject->stuurgegevens->entiteittype($self->entiteittype);

    $stufobject->stuurgegevens->referentienummer(
        $options->{reference_id}
    );
    $stufobject->stuurgegevens->tijdstipBericht(
        $options->{date}->strftime('%Y%m%d%H%M%S00')
    );

    my $params          = { map(
        {
            $_ => Zaaksysteem::StUF::Body::Field->new(value => $options->{ $_ })
        } grep(
            {
                $_ ne 'date' && $_ ne 'reference_id' && $options->{ $_ }
            }
            keys %{ $options }
        )
    ) };

    $stufobject->stuurgegevens->vraag({});

    # my $entiteit_class = 'Zaaksysteem::StUF::Body::' . $self->entiteittype;

    # $stufobject->body(
    #     [
    #         $entiteit_class->new(
    #             %$params
    #         ),
    #         $entiteit_class->new(
    #             %$params
    #         ),
    #         $entiteit_class->new(
    #             is_description          => 1,
    #         ),
    #     ]
    # );

    return $stufobject;
}

sub binnen_gemeente {
    my ($self, $gemeentecode) = @_;

    my $object_params = (
        UNIVERSAL::isa($self->as_params, 'ARRAY')
        ? $self->as_params->[0]->{NPS}
        : $self->as_params->{NPS}
    );

    if (!exists $object_params->{"inp.gemeenteVanInschrijving"}) {
        $self->log->trace(
            "Unable to determine if inside or outside municipality, no inp.gemeenteVanInschrijving found!"
        );
        return;
    }

    if(int($object_params->{"inp.gemeenteVanInschrijving"}) == int($gemeentecode)) {
        $self->log->trace("Marking as 'binnengemeentelijk': gemeentecode '$gemeentecode' matches inp.gemeenteVanInschrijving");
        return 1;
    }

    $self->log->trace("Marking as 'buitengemeentelijk': gemeentecode '$gemeentecode' does not match inp.gemeenteVanInschrijving");
    return 0;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_active_partner

TODO: Fix the POD

=cut

=head2 verhuisd_against_bag

TODO: Fix the POD

=cut

