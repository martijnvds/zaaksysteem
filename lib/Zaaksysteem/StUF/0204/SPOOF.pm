package Zaaksysteem::StUF::0204::SPOOF;
use Moose::Role;

use BTTW::Tools;

use JSON::Path;
use String::Random;

use Encode qw/encode_utf8/;

with 'MooseX::Log::Log4perl';

use constant PRS_ENTRY => [
    {
        path    => '$.a-nummer',
        value   => sub { '1' . String::Random->new()->randpattern('nnnnnnnnn'); }
    },
    {
        path    => '$.bsn-nummer',
        value   => sub { '12' . String::Random->new()->randpattern('nnnnnnn'); }
    },
    {
        path    => '$.voornamen',
        value   => sub { return String::Random::random_string('0',[
                'Wim',
                'Piet',
                'Frits',
                'Jasper',
                'Jaap',
                'Gerard',
                'Koos',
                'Jan',
                'Delajah',
                'Marco',
                'Mieke',
                'Else',
                'Jonas',
                'Niels',
                'Pim',
                'Theo',
                'Chris',
                'Bert',
                'Sam',
                'Noah',
                'Max',
                'Sven',
                'Siem',
                'Gijs',
                'Teun'
            ])
        },
    },
    {
        path    => '$.voorletters',
        value   => sub { return String::Random->new()->randpattern('CC') },
    },
    {
        path    => '$.voorvoegselGeslachtsnaam',
        value   => sub { return ('de','van der','van de')[int(rand(3))] },
    },
    {
        path    => '$.geslachtsnaam',
        value   => sub { return  encode_utf8("\N{U+00CB} \N{U+00E6}" . String::Random::random_string('0',[
            'Bakker',
            'Kool',
            'Puree',
            'Fleppenstein',
            'Springintveld',
            'Janssen',
            'Dekker',
            'Brouwer',
            'Muler',
            'Bos',
            'Vos',
            'Smits',
            'Schouten',
            'Jacobs',
            'Kuipers',
            'Post',
            'Visser'
            ]));
        },
    },
    {
        path    => '$.geboortedatum',
        value   => sub { return String::Random->new()->randregex('19\d\d051\d') }

    },
    {
        path    => '$.datumOverlijden',
    },
    {
        path    => '$.indicatieGeheim',
        value   => sub { return ['0', '0', '1', '7']->[int(rand(4))] }, # 50% chance of secret
    },
    {
        path    => '$.burgerlijkeStaat',
    },
    {
        path    => '$.aanduidingNaamgebruik',
        value   => sub { return 'E' },
    },
    {
        path    => '$.geslachtsaanduiding',
        value   => sub { return ['M', 'V']->[int(rand(1))] },
    },
    {
        path    => '$.PRSADRVBL.ADR.straatnaam',
        value   => sub { return ucfirst(lc(String::Random->new()->randpattern('CCCCCCCCCCCCCCCCC'))) }
    },
    {
        path    => '$.PRSADRVBL.ADR.postcode',
        value   => sub { return String::Random->new()->randpattern('nnnnCC') }
    },
    {
        path    => '$.PRSADRVBL.ADR.huisnummer',
        value   => sub { return String::Random->new()->randpattern('nn') }
    },
    {
        path    => '$.PRSADRVBL.ADR.huisletter',
        value   => sub { return uc(String::Random->new()->randpattern('C')) }
    },
    {
        path    => '$.PRSADRVBL.ADR.huisnummertoevoeging',
        value   => sub { return String::Random->new()->randpattern('CCCC') }
    },
    {
        path    => '$.PRSADRVBL.ADR.woonplaatsnaam',
        value   => sub { return ucfirst(lc(String::Random->new()->randpattern('CCCCCCCCC'))) }
    },
    {
        path    => '$.PRSADRVBL.ADR.gemeentecode',
        value   => sub { return '1332' }
    },
    {
        path    => '$.PRSADRVBL.ADR.landcode',
        value   => sub { return '6030' }
    },
    {
        path    => '$.PRSPRSHUW.PRS.a-nummer',
        value   => sub { '1' . String::Random->new()->randpattern('nnnnnnnnn'); }
    },
    {
        path    => '$.PRSPRSHUW.PRS.bsn-nummer',
        value   => sub { '12' . String::Random->new()->randpattern('nnnnnnn'); }
    },
    {
        path    => '$.PRSPRSHUW.PRS.voornamen',
        value   => sub { return String::Random::random_string('0',[
                'Wim',
                'Piet',
                'Frits',
                'Jasper',
                'Jaap',
                'Gerard',
                'Koos',
                'Jan',
                'Delajah',
                'Marco',
                'Mieke',
                'Else',
                'Jonas',
                'Niels',
                'Pim',
                'Theo',
                'Chris',
                'Bert',
                'Sam',
                'Noah',
                'Max',
                'Sven',
                'Siem',
                'Gijs',
                'Teun'
            ])
        },
    },
    {
        path    => '$.PRSPRSHUW.PRS.voorletters',
        value   => sub { return String::Random->new()->randpattern('CC') },
    },
    {
        path    => '$.PRSPRSHUW.PRS.voorvoegselGeslachtsnaam',
        value   => sub { return ('de','van der','van de')[int(rand(3))] },
    },
    {
        path    => '$.PRSPRSHUW.PRS.geslachtsnaam',
        value   => sub { return String::Random::random_string('0',[
            'Bakker',
            'Kool',
            'Puree',
            'Fleppenstein',
            'Springintveld',
            'Janssen',
            'Dekker',
            'Brouwer',
            'Muler',
            'Bos',
            'Vos',
            'Visser',
            'Smits',
            'Schouten',
            'Jacobs',
            'Kuipers',
            'Post',
            'Visser'
            ])
        },
    },
];


=head1 NAME

Zaaksysteem::StUF::0204::SPOOF - Sp00f mode for StUF 0204 calls

=head1 SYNOPSIS

    $instance->spoof_answer_search_natuurlijk_persoon(
    );

=head1 DESCRIPTION

=head1 ATTRIBUTES

See L<Zaaksysteem::XML::Compile::Instance> for a description of Required attributes

=head1 METHODS

=head2 spoof_answer_search_natuurlijkpersoon

=cut

=head2 BSN examples

When a BSN starts with a 9, then it will be used as a "GBA-V" user, e.g.: searching in GBA-V.

When a BSN starts with a 1-8, then it will be used as a "Normal" user, e.g.: searching in CMG.


The second number defines whether the user is a normal, secret of investigation user. see below

=over 4

=item 2: Normal user

=item 3: Geheime user

=item 4: User "briefadres"

=item 5: User "in onderzoek"

subitem:
1) aanduidingGegevensInOnderzoek
2) aanduidingGegevensInOnderzoekOverlijden
3) aanduidingGegevensInOnderzoekVerblijfstitel
4) Adres
5) huwelijkonderzoek

=back

=cut

sub spoof_answer {
    my $self                = shift;
    my $request             = shift;

    my $xml                 = $request->content;

    ### Detect sort of call:
    if ($xml =~ /berichtsoort>lv01/i) {
        return $self->_spoof_lv01($xml);
    }

    if ($xml =~ /berichtsoort>lk01/i) {
        return $self->_spoof_lk01($xml);
    }
}

sub spoof_gba_v_kennisgeving {
    my $self                = shift;
    my $params              = shift;
    my $options             = shift || {};

    my $prs = $self->_load_body_prs($params);

    $prs    = $self->_load_identification($params, $prs, $options);

    $prs->{sleutelOntvangend}       = $options->{local_id};
    $prs->{sleutelVerzendend}       = '234' . String::Random->new()->randpattern('nnnnnnn');

    $prs->{sleutelGegevensbeheer}   = '423' . String::Random->new()->randpattern('nnnnnnn');

    my $perldata            = {
        'body'              => {
            'PRS'   => [ $prs, $prs ],
        },
        'stuurgegevens'     => $self->generate_stuurgegevens(
            {
                messagetype             => 'Lk01',
                entitytype              => 'PRS',
                sender                  => 'CMODIS',
                receiver                => 'ZSNL',
                reference               => 'CMO' . String::Random->new()->randpattern('nnnnnnn'),
                datetime                => '2014050209011458',
                mutation_type           => 'update',
                follow_subscription     => 1,
            },
        ),
    };

    my $xml = $self->handle_message(
        'kennisgevingsbericht',
        $perldata,
        {
            %{ $options },
            translate   => 'WRITER',
        }
    );

    return $self->_xml_to_soap($xml);
}

sub _spoof_lv01 {
    my $self                = shift;
    my $xml                 = shift;

    my ($element, $toomany) = $self->retrieve_xml_element(
        {
            xml         => $xml,
            method      => 'vraagbericht'
        }
    );

    my $perldata            = try {
        $self->vraagbericht('READER', $element);
    } catch {
        $self->log->warn("Spoof: _spoof_lv01: \$perldata failed: $_");
        return;
    };

    my $lv01xml             = try {
        $self->_spoof_lv01_prs($perldata);
    } catch {
        $self->log->warn("Spoof: _spoof_lv01_prs: \$lv01xml failed: $_");
        return;
    };

    return HTTP::Response->new(200, 'answer manually created',
        [ 'Content-Type' => 'text/xml' ], $lv01xml
    );
}

sub _spoof_lk01 {
    my $self                = shift;
    my $xml                 = shift;

    my ($element, $toomany) = $self->retrieve_xml_element(
        {
            xml         => $xml,
            method      => 'kennisgevingsbericht'
        }
    );

    my $perldata            = try {
        $self->kennisgevingsbericht('READER', $element);
    } catch {
        $self->log->warn("Spoof: _spoof_lk01: \$perldata failed: $_");
        return;
    };

    my $lk01xml             = try {
        $self->_spoof_lk01_prs($perldata);
    } catch {
        $self->log->warn("Spoof: _spoof_lk01_prs: \$lv01xml failed: $_");
        return;
    };


    return HTTP::Response->new(200, 'answer manually created',
        [ 'Content-Type' => 'text/xml' ], $lk01xml
    );
}

sub _spoof_lk01_prs {
    my $self                = shift;
    my $perldata            = shift;

    my $stuurgegevens       = $perldata->{stuurgegevens};

    $stuurgegevens->{berichtsoort}   = 'Bv01';
    $stuurgegevens->{ontvanger}     = {
        applicatie  => $stuurgegevens->{zender}->{applicatie}
    };
    $stuurgegevens->{zender}        = {
        applicatie  => $stuurgegevens->{ontvanger}->{applicatie}
    };

    my $response            = try {
        $self->bevestigingsbericht(
            'WRITER',
            {
                stuurgegevens   => $stuurgegevens,
                body            => {}
            }
        );
    } catch {
        $self->log->warn("Spoof: _spoof_lk01_prs: \$response failed: $_");
        return;
    };

    return $self->_xml_to_soap($response);
}

sub _spoof_lv01_prs {
    my $self                = shift;
    my $perldata            = shift;

    my $stuurgegevens       = $perldata->{stuurgegevens};

    $stuurgegevens->{berichtsoort}   = 'La01';
    $stuurgegevens->{ontvanger}     = {
        applicatie  => $stuurgegevens->{zender}->{applicatie}
    };
    $stuurgegevens->{zender}        = {
        applicatie  => $stuurgegevens->{ontvanger}->{applicatie}
    };
    # $stuurgegevens->{kennisgeving}   = {
    #     applicatie  => $stuurgegevens->{ontvanger}->{applicatie}
    # };
    delete($stuurgegevens->{vraag});
    $stuurgegevens->{antwoord}      = {
        indicatorVervolgvraag       => 0,
        crossRefNummer              => sprintf("%d", (rand(2500)+1)*999),
    };

    my $count = 10;

    if (
        $perldata->{body}->{PRS}->[0]->{'bsn-nummer'} ||
        $perldata->{body}->{PRS}->[0]->{'geboortedatum'}
    ) {
        $count = 1;
    }

    if ($perldata->{body}->{PRS}->[0]->{'bsn-nummer'}) {
        my ($scenarioid) = $perldata->{body}->{PRS}->[0]->{'bsn-nummer'}->{_} =~ /^\d(\d)/;

        if ($scenarioid == 6) {
            $count = 0;
        }
    }

    my @prs;
    for (1..$count) {
        next if (!$count);
        my $prs                         = $self->_load_prs_entry($perldata->{body}->{PRS}->[0]);

        ### CMG user
        if ($prs->{'bsn-nummer'} =~ /^[0-8]/) {
            $prs->{sleutelGegevensbeheer}   = '423' . sprintf("%d", rand(2500)+1);
            $prs->{sleutelVerzendend}       = '234' . sprintf("%d", rand(2500)+1);
        }

        ### Briefadres
        my ($scenarioid) = $prs->{'bsn-nummer'} =~ /^\d(\d)/;

        ### Briefadres

        ### Geheim
        if ($scenarioid == 3) {
            $prs->{indicatieGeheim} = 1;
        }

        ### Briefadres
        if ($scenarioid == 4) {
            $prs->{PRSADRCOR} = $prs->{PRSADRVBL};
            delete($prs->{PRSADRVBL});
        }

        ### Onderzoek
        if ($scenarioid == 5) {
            my ($onderzoekid) = $prs->{'bsn-nummer'} =~ /^\d\d(\d)/;
            if ($onderzoekid == 2) {
                $prs->{extraElementen} = {
                    seq_extraElement    => [
                        {
                            extraElement    => {
                                naam    => 'aanduidingGegevensInOnderzoekOverlijden',
                                '_'     => 'J'
                            }
                        }
                    ]
                };
            } elsif ($onderzoekid == 4) {
                my $adronderzoek = (
                    $prs->{PRSADRVBL}
                        ? $prs->{PRSADRVBL}->{ADR}
                        : $prs->{PRSADRCOR}->{ADR}
                );
                $adronderzoek->{extraElementen} = {
                    seq_extraElement    => [
                        {
                            extraElement    => {
                                naam    => 'aanduidingGegevensInOnderzoek',
                                '_'     => 'J'
                            }
                        }
                    ]
                };
            } elsif ($onderzoekid == 5 && $prs->{PRSPRSHUW}) {

                $prs->{PRSPRSHUW}->{extraElementen} = {
                    seq_extraElement    => [
                        {
                            extraElement    => {
                                naam    => 'aanduidingGegevensInOnderzoek',
                                '_'     => 'J'
                            }
                        }
                    ]
                };
            } else {
                $prs->{extraElementen} = {
                    seq_extraElement    => [
                        {
                            extraElement    => {
                                naam    => 'aanduidingGegevensInOnderzoek',
                                '_'     => 'J'
                            }
                        }
                    ]
                };
            }

        }

        ### Use data from extraElementen
        delete($prs->{PRSPRSHUW});


        push(@prs, $prs);
    }

    my $response            = try {
        $self->antwoordbericht(
            'WRITER',
            {
                stuurgegevens   => $stuurgegevens,
                body            => {
                    'PRS'           => \@prs
                }
            }
        );
    } catch {
        $self->log->warn("Spoof: _spoof_lv01_prs: \$response failed: $_");
        return;
    };

    return $self->_xml_to_soap($response);
}

sub _load_prs_entry {
    my $self                = shift;
    my $perldata            = shift;

    my $returnhash          = {};


    for my $entry (@{ PRS_ENTRY() }) {
        my $sourcepath = JSON::Path->new($entry->{path});

        $self->gen_hash_from_dot_notation($entry->{path}, $returnhash);

        if (my $val = $sourcepath->value($perldata)) {
            $sourcepath->set($returnhash, $self->plaintext_value($val));
        } elsif ($entry->{value}) {
            $sourcepath->set($returnhash, $entry->{value}->());
        }
    }

    if ($returnhash->{PRSPRSHUW}) {
        unless ($returnhash->{extraElementen}) {
            $returnhash->{extraElementen} = {
                seq_extraElement => [],
            }
        }

        push(
            @{ $returnhash->{extraElementen}->{seq_extraElement} },
            {
                extraElement => {
                    naam    => 'voorvoegselsGeslachtsnaamEchtgenoot',
                    '_'     => $returnhash->{PRSPRSHUW}->{PRS}->{voorvoegselGeslachtsnaam}
                }
            }
        );

        push(
            @{ $returnhash->{extraElementen}->{seq_extraElement} },
            {
                extraElement => {
                    naam    => 'geslachtsnaamEchtgenoot',
                    '_'     => $returnhash->{PRSPRSHUW}->{PRS}->{geslachtsnaam}
                }
            }
        );
    }

    return $returnhash;

}

sub gen_hash_from_dot_notation {
    my $self                = shift;
    my $notation            = shift;
    my $hash                = shift || {};

    $notation =~ s/^\$\.//;

    my ($exists, $more) = $notation =~ /^(.*?)(\.)/;
    $exists = $notation unless $exists;

    $notation =~ s/^(.*?)\.//;

    if ($more) {
        if (!exists($hash->{$exists})) {
            $hash->{$exists} = {};
        }

        return $self->gen_hash_from_dot_notation($notation,$hash->{$exists});
    } elsif (!exists($hash->{$exists})) {
        $hash->{$exists} = undef;
    }

    return $hash;
}

sub _xml_to_soap {
    my $self                = shift;
    my $xml                 = shift;

    # die($xml);

    my $xmlelement          = XML::LibXML->load_xml(string => $xml);

    my $doc                 = XML::LibXML::Document->new('1.0','utf-8');

    my $element_envelope    = $doc->createElementNS('http://schemas.xmlsoap.org/soap/envelope/','Envelope');
    $doc->setDocumentElement($element_envelope);

    $element_envelope->setNamespace('http://schemas.xmlsoap.org/soap/envelope/', 'soap', 1);
    $element_envelope->setNamespace('http://www.w3.org/2001/XMLSchema', 'xsd', 0);
    $element_envelope->setNamespace('http://www.w3.org/2001/XMLSchema-instance', 'xsi', 0);

    my $element_body        = $doc->createElementNS('http://schemas.xmlsoap.org/soap/envelope/','Body');
    $element_envelope->appendChild($element_body);

    $element_body->addChild($xmlelement->documentElement());

    return $doc->toString(1);
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 PRS_ENTRY

TODO: Fix the POD

=cut

=head2 gen_hash_from_dot_notation

TODO: Fix the POD

=cut

=head2 spoof_answer

TODO: Fix the POD

=cut

=head2 spoof_gba_v_kennisgeving

TODO: Fix the POD

=cut
