use utf8;
package Zaaksysteem::Schema::CustomObjectVersion;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CustomObjectVersion

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<custom_object_version>

=cut

__PACKAGE__->table("custom_object_version");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'custom_object_version_id_seq'

=head2 uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 title

  data_type: 'text'
  is_nullable: 1

=head2 status

  data_type: 'enum'
  default_value: 'active'
  extra: {custom_type_name => "custom_object_version_status",list => ["active","inactive","draft"]}
  is_nullable: 0

=head2 version

  data_type: 'integer'
  is_nullable: 0

=head2 custom_object_version_content_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 custom_object_type_version_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 custom_object_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 external_reference

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "custom_object_version_id_seq",
  },
  "uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "title",
  { data_type => "text", is_nullable => 1 },
  "status",
  {
    data_type => "enum",
    default_value => "active",
    extra => {
      custom_type_name => "custom_object_version_status",
      list => ["active", "inactive", "draft"],
    },
    is_nullable => 0,
  },
  "version",
  { data_type => "integer", is_nullable => 0 },
  "custom_object_version_content_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "custom_object_type_version_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "last_modified",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "custom_object_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "external_reference",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<custom_object_version_key>

=over 4

=item * L</id>

=item * L</custom_object_id>

=back

=cut

__PACKAGE__->add_unique_constraint("custom_object_version_key", ["id", "custom_object_id"]);

=head2 C<custom_object_version_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("custom_object_version_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 custom_object_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObject>

=cut

__PACKAGE__->belongs_to(
  "custom_object_id",
  "Zaaksysteem::Schema::CustomObject",
  { id => "custom_object_id" },
);

=head2 custom_object_relationships

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObjectRelationship>

=cut

__PACKAGE__->has_many(
  "custom_object_relationships",
  "Zaaksysteem::Schema::CustomObjectRelationship",
  { "foreign.custom_object_version_id" => "self.id" },
  undef,
);

=head2 custom_object_type_version_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObjectTypeVersion>

=cut

__PACKAGE__->belongs_to(
  "custom_object_type_version_id",
  "Zaaksysteem::Schema::CustomObjectTypeVersion",
  { id => "custom_object_type_version_id" },
);

=head2 custom_object_version_content_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::CustomObjectVersionContent>

=cut

__PACKAGE__->belongs_to(
  "custom_object_version_content_id",
  "Zaaksysteem::Schema::CustomObjectVersionContent",
  { id => "custom_object_version_content_id" },
);

=head2 custom_objects

Type: has_many

Related object: L<Zaaksysteem::Schema::CustomObject>

=cut

__PACKAGE__->has_many(
  "custom_objects",
  "Zaaksysteem::Schema::CustomObject",
  { "foreign.custom_object_version_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-09-25 08:18:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:VcFJfwa2viZsvrHXQnx8sQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2020, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut