use utf8;
package Zaaksysteem::Schema::ZaaktypeStatus;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeStatus

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_status>

=cut

__PACKAGE__->table("zaaktype_status");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_status_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 status_type

  data_type: 'text'
  is_nullable: 1

=head2 naam

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 ou_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=head2 checklist

  data_type: 'integer'
  is_nullable: 1

=head2 fase

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 role_set

  data_type: 'integer'
  is_nullable: 1

=head2 termijn

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_status_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "status_type",
  { data_type => "text", is_nullable => 1 },
  "naam",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "ou_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
  "checklist",
  { data_type => "integer", is_nullable => 1 },
  "fase",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "role_set",
  { data_type => "integer", is_nullable => 1 },
  "termijn",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_actions

Type: has_many

Related object: L<Zaaksysteem::Schema::CaseAction>

=cut

__PACKAGE__->has_many(
  "case_actions",
  "Zaaksysteem::Schema::CaseAction",
  { "foreign.casetype_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->has_many(
  "zaaktype_kenmerkens",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { "foreign.zaak_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);

=head2 zaaktype_notificaties

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeNotificatie>

=cut

__PACKAGE__->has_many(
  "zaaktype_notificaties",
  "Zaaksysteem::Schema::ZaaktypeNotificatie",
  { "foreign.zaak_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_regels

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeRegel>

=cut

__PACKAGE__->has_many(
  "zaaktype_regels",
  "Zaaksysteem::Schema::ZaaktypeRegel",
  { "foreign.zaak_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_relaties

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeRelatie>

=cut

__PACKAGE__->has_many(
  "zaaktype_relaties",
  "Zaaksysteem::Schema::ZaaktypeRelatie",
  { "foreign.zaaktype_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_resultatens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeResultaten>

=cut

__PACKAGE__->has_many(
  "zaaktype_resultatens",
  "Zaaksysteem::Schema::ZaaktypeResultaten",
  { "foreign.zaaktype_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_sjablonens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeSjablonen>

=cut

__PACKAGE__->has_many(
  "zaaktype_sjablonens",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { "foreign.zaak_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_standaard_betrokkenens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen>

=cut

__PACKAGE__->has_many(
  "zaaktype_standaard_betrokkenens",
  "Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen",
  { "foreign.zaak_status_id" => "self.id" },
  undef,
);

=head2 zaaktype_status_checklist_items

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeStatusChecklistItem>

=cut

__PACKAGE__->has_many(
  "zaaktype_status_checklist_items",
  "Zaaksysteem::Schema::ZaaktypeStatusChecklistItem",
  { "foreign.casetype_status_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-03-13 10:15:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Qwznjuk5s13tlnc/MvROtQ


__PACKAGE__->resultset_class('Zaaksysteem::Zaaktypen::BaseStatus');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeFase",
    __PACKAGE__->load_components()
);

__PACKAGE__->has_many(
  "zaaktype_resultaten",
  "Zaaksysteem::Schema::ZaaktypeResultaten",
  { "foreign.zaaktype_status_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_kenmerken",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { "foreign.zaak_status_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_sjablonen",
  "Zaaksysteem::Schema::ZaaktypeSjablonen",
  { "foreign.zaak_status_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_notificaties",
  "Zaaksysteem::Schema::ZaaktypeNotificatie",
  { "foreign.zaak_status_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_regels",
  "Zaaksysteem::Schema::ZaaktypeRegel",
  { "foreign.zaak_status_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_checklists",
  "Zaaksysteem::Schema::ZaaktypeStatusChecklistItem",
  { "foreign.casetype_status_id" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_subjects",
  "Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen",
  { "foreign.zaak_status_id" => "self.id" },
  {},
);

__PACKAGE__->has_many(
  "zaaktype_standaard_betrokkenen",
  "Zaaksysteem::Schema::ZaaktypeStandaardBetrokkenen",
  { "foreign.zaak_status_id" => "self.id" },
  {},
);



# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

