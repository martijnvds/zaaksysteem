use utf8;
package Zaaksysteem::Schema::ObjectAclEntry;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ObjectAclEntry

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<object_acl_entry>

=cut

__PACKAGE__->table("object_acl_entry");

=head1 ACCESSORS

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 entity_type

  data_type: 'text'
  is_nullable: 0

=head2 entity_id

  data_type: 'text'
  is_nullable: 0

=head2 capability

  data_type: 'text'
  is_nullable: 0

=head2 scope

  data_type: 'text'
  default_value: 'instance'
  is_nullable: 0

=head2 groupname

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "entity_type",
  { data_type => "text", is_nullable => 0 },
  "entity_id",
  { data_type => "text", is_nullable => 0 },
  "capability",
  { data_type => "text", is_nullable => 0 },
  "scope",
  { data_type => "text", default_value => "instance", is_nullable => 0 },
  "groupname",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->set_primary_key("uuid");

=head1 UNIQUE CONSTRAINTS

=head2 C<object_acl_entry_unique_idx>

=over 4

=item * L</object_uuid>

=item * L</entity_type>

=item * L</entity_id>

=item * L</capability>

=item * L</scope>

=item * L</groupname>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "object_acl_entry_unique_idx",
  [
    "object_uuid",
    "entity_type",
    "entity_id",
    "capability",
    "scope",
    "groupname",
  ],
);

=head1 RELATIONS

=head2 object_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "object_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "object_uuid" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-06 08:51:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:3/Vc699evf3peRXJPAN40Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

