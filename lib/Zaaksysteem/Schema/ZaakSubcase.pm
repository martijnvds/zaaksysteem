use utf8;
package Zaaksysteem::Schema::ZaakSubcase;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaakSubcase

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaak_subcase>

=cut

__PACKAGE__->table("zaak_subcase");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaak_subcase_id_seq'

=head2 zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 relation_zaak_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 required

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaak_subcase_id_seq",
  },
  "zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "relation_zaak_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "required",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 relation_zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to(
  "relation_zaak_id",
  "Zaaksysteem::Schema::Zaak",
  { id => "relation_zaak_id" },
);

=head2 zaak_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("zaak_id", "Zaaksysteem::Schema::Zaak", { id => "zaak_id" });


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-03-05 16:06:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:lUBycR/4kILi0Aehv8Grpw

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaakSubcase');





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

