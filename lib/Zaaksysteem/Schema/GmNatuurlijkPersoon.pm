use utf8;
package Zaaksysteem::Schema::GmNatuurlijkPersoon;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::GmNatuurlijkPersoon

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<gm_natuurlijk_persoon>

=cut

__PACKAGE__->table("gm_natuurlijk_persoon");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'gm_natuurlijk_persoon_id_seq'

=head2 gegevens_magazijn_id

  data_type: 'integer'
  is_nullable: 1

=head2 betrokkene_type

  data_type: 'integer'
  is_nullable: 1

=head2 burgerservicenummer

  data_type: 'varchar'
  is_nullable: 1
  size: 9

=head2 a_nummer

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 voorletters

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 voornamen

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 geslachtsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 voorvoegsel

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 geslachtsaanduiding

  data_type: 'varchar'
  is_nullable: 1
  size: 3

=head2 nationaliteitscode1

  data_type: 'smallint'
  is_nullable: 1

=head2 nationaliteitscode2

  data_type: 'smallint'
  is_nullable: 1

=head2 nationaliteitscode3

  data_type: 'smallint'
  is_nullable: 1

=head2 geboorteplaats

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 geboorteland

  data_type: 'varchar'
  is_nullable: 1
  size: 75

=head2 geboortedatum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 aanhef_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 voorletters_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 voornamen_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 naam_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 200

=head2 voorvoegsel_aanschrijving

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 burgerlijke_staat

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 indicatie_geheim

  data_type: 'char'
  is_nullable: 1
  size: 1

=head2 import_datum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 adres_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 authenticatedby

  data_type: 'text'
  is_nullable: 1

=head2 authenticated

  data_type: 'smallint'
  is_nullable: 1

=head2 datum_overlijden

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 verblijfsobject_id

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 aanduiding_naamgebruik

  data_type: 'varchar'
  is_nullable: 1
  size: 1

=head2 onderzoek_persoon

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_huwelijk

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_overlijden

  data_type: 'boolean'
  is_nullable: 1

=head2 onderzoek_verblijfplaats

  data_type: 'boolean'
  is_nullable: 1

=head2 partner_a_nummer

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 partner_burgerservicenummer

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 partner_voorvoegsel

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 partner_geslachtsnaam

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 datum_huwelijk

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 datum_huwelijk_ontbinding

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 landcode

  data_type: 'integer'
  default_value: 6030
  is_nullable: 0

=head2 naamgebruik

  data_type: 'text'
  is_nullable: 1

=head2 adellijke_titel

  data_type: 'text'
  is_nullable: 1

=head2 preferred_contact_channel

  data_type: 'enum'
  default_value: 'pip'
  extra: {custom_type_name => "preferred_contact_channel",list => ["email","mail","pip","phone"]}
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "gm_natuurlijk_persoon_id_seq",
  },
  "gegevens_magazijn_id",
  { data_type => "integer", is_nullable => 1 },
  "betrokkene_type",
  { data_type => "integer", is_nullable => 1 },
  "burgerservicenummer",
  { data_type => "varchar", is_nullable => 1, size => 9 },
  "a_nummer",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "voorletters",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "voornamen",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "geslachtsnaam",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "voorvoegsel",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "geslachtsaanduiding",
  { data_type => "varchar", is_nullable => 1, size => 3 },
  "nationaliteitscode1",
  { data_type => "smallint", is_nullable => 1 },
  "nationaliteitscode2",
  { data_type => "smallint", is_nullable => 1 },
  "nationaliteitscode3",
  { data_type => "smallint", is_nullable => 1 },
  "geboorteplaats",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "geboorteland",
  { data_type => "varchar", is_nullable => 1, size => 75 },
  "geboortedatum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "aanhef_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "voorletters_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "voornamen_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "naam_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 200 },
  "voorvoegsel_aanschrijving",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "burgerlijke_staat",
  { data_type => "char", is_nullable => 1, size => 1 },
  "indicatie_geheim",
  { data_type => "char", is_nullable => 1, size => 1 },
  "import_datum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "adres_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "authenticatedby",
  { data_type => "text", is_nullable => 1 },
  "authenticated",
  { data_type => "smallint", is_nullable => 1 },
  "datum_overlijden",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "verblijfsobject_id",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "aanduiding_naamgebruik",
  { data_type => "varchar", is_nullable => 1, size => 1 },
  "onderzoek_persoon",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_huwelijk",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_overlijden",
  { data_type => "boolean", is_nullable => 1 },
  "onderzoek_verblijfplaats",
  { data_type => "boolean", is_nullable => 1 },
  "partner_a_nummer",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "partner_burgerservicenummer",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "partner_voorvoegsel",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "partner_geslachtsnaam",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "datum_huwelijk",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "datum_huwelijk_ontbinding",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "landcode",
  { data_type => "integer", default_value => 6030, is_nullable => 0 },
  "naamgebruik",
  { data_type => "text", is_nullable => 1 },
  "adellijke_titel",
  { data_type => "text", is_nullable => 1 },
  "preferred_contact_channel",
  {
    data_type => "enum",
    default_value => "pip",
    extra => {
      custom_type_name => "preferred_contact_channel",
      list => ["email", "mail", "pip", "phone"],
    },
    is_nullable => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 adres_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::GmAdres>

=cut

__PACKAGE__->belongs_to(
  "adres_id",
  "Zaaksysteem::Schema::GmAdres",
  { id => "adres_id" },
);

=head2 gm_adres

Type: has_many

Related object: L<Zaaksysteem::Schema::GmAdres>

=cut

__PACKAGE__->has_many(
  "gm_adres",
  "Zaaksysteem::Schema::GmAdres",
  { "foreign.natuurlijk_persoon_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-09-11 10:42:17
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MBsMHJtp9QN4tAEkVbWOkA

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::GmResultSet');


__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::GmNatuurlijkPersoon",
    __PACKAGE__->load_components()
);

__PACKAGE__->has_many(
  "addresses",
  "Zaaksysteem::Schema::GmAdres",
  { "foreign.natuurlijk_persoon_id" => "self.id" },
  {},
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

