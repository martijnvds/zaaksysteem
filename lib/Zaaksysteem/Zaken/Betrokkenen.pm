package Zaaksysteem::Zaken::Betrokkenen;

use Moose::Role;

use BTTW::Tools;

sub betrokkene_set {
    my ($self, $opts, $betrokkene_target, $betrokkene_info) = @_;

    if (UNIVERSAL::isa($betrokkene_info, 'HASH') && $betrokkene_info->{betrokkene}) {
        my $target;
        try {
            $target = $self->_betrokkene_get_handle($betrokkene_info);
        } catch {
            my $message = "Cannot create case: '$betrokkene_target' does not exist: $_";
            $self->log->error($message);
            throw('zaken/betrokkene/set/not_found', $message);
        };

        my $zaakbetrokkene = $self->result_source->schema->resultset('ZaakBetrokkenen')->find($target);
        $zaakbetrokkene->update({ rol => ucfirst($betrokkene_target) });

        $opts->{$betrokkene_target} = $target;
        if ($opts->{$betrokkene_target}) {
            my ($gmid)  = $betrokkene_info->{betrokkene} =~ /(\d+)$/;
            $opts->{$betrokkene_target . '_gm_id'} = $gmid;
        }

        return $opts->{$betrokkene_target};
    } elsif (
        UNIVERSAL::isa($betrokkene_info, 'HASH') &&
        $betrokkene_info->{gegevens_magazijn_id} &&
        $betrokkene_info->{betrokkene_id} &&
        $betrokkene_info->{betrokkene_type} &&
        $betrokkene_info->{verificatie}
    ) {
        $opts->{$betrokkene_target} =
            $self->_manual_betrokkene_create($betrokkene_info);

        if ($opts->{$betrokkene_target}) {
            $opts->{$betrokkene_target . '_gm_id'} =
                $betrokkene_info->{gegevens_magazijn_id};
        }

        return $opts->{$betrokkene_target};
    } elsif (
        UNIVERSAL::isa($betrokkene_info, 'HASH') &&
        $betrokkene_info->{betrokkene_type} &&
        $betrokkene_info->{verificatie} &&
        UNIVERSAL::isa($betrokkene_info->{create}, 'HASH')
    ) {
        my $gmid = $self->_betrokkene_create_nieuw($betrokkene_info);
        my $target = try {
            $self->_betrokkene_get_handle($betrokkene_info);
        } catch {
            my $message = "Cannot create case: '$betrokkene_target' does not exist: $_";
            $self->log->error($message);
            throw('zaken/betrokkene/set/not_found', $message);
        };

        $opts->{$betrokkene_target} = $target;
        ($gmid)  = $betrokkene_info->{betrokkene} =~ /(\d+)$/;
        $opts->{$betrokkene_target . '_gm_id'} = $gmid;

        return $opts->{$betrokkene_target};
    } else {
        throw("ZS/Z/B", 'Only implementing structure: { betrokkene    => "betrokkene-TYPE-ID" }');
    }
}

# OLD BTYPE MAP:
my $OLD_BTYPE_MAP = {
    natuurlijk_persoon  => 'gm_natuurlijk_persoon_id',
    medewerker          => 'medewerker_id',
    bedrijf             => 'gm_bedrijf_id',
    org_eenheid         => 'org_eenheid_id',
};

# TODO: Op basis van het type moeten we echt gaan checken wat noodzakelijk is, nu krijg je echt rare meldeingen
sub _betrokkene_create_nieuw {
    my ($self, $betrokkene_info) = @_;

    my $gmid = $self->z_betrokkene->create(
        $betrokkene_info->{betrokkene_type},
        {
            %{$betrokkene_info->{create}},
            authenticated_by => $betrokkene_info->{verificatie}
        }
    );

    if (!$gmid) {
        throw("Betrokkene/create", "Unable to create new betrokkene!");
    }

    $betrokkene_info->{betrokkene} = sprintf('betrokkene-%s-%d', $betrokkene_info->{betrokkene_type},$gmid);

    $self->log->debug("Zaken/Betrokkenen/_betrokkene_create_nieuw $betrokkene_info->{betrokkene}");

    return $gmid;
}

sub _manual_betrokkene_create {
    my ($self, $betrokkene_info) = @_;

    my ($raw_betrokkene,$old_betrokkene);

    my $colname = $OLD_BTYPE_MAP->{ $betrokkene_info->{betrokkene_type} };

    if ($self->can('dbic')) {
        $old_betrokkene = $self->dbic->resultset('Betrokkene')->find(
            $betrokkene_info->{betrokkene_id},
        );

        return unless $old_betrokkene;

        if (
            $betrokkene_info->{betrokkene_type} eq 'medewerker' ||
            $betrokkene_info->{betrokkene_type} eq 'org_eenheid'
        ) {
            $betrokkene_info->{betrokkene_id} =
                $betrokkene_info->{gegevens_magazijn_id}
        } else {
            $betrokkene_info->{betrokkene_id} =
                $old_betrokkene->$colname->id
        }

        $raw_betrokkene = $self->dbic->resultset('ZaakBetrokkenen')->create(
            $betrokkene_info
        );
    } else {
        $old_betrokkene = $self->result_source->schema->resultset('Betrokkene')->find(
            $betrokkene_info->{betrokkene_id},
        );

        return unless $old_betrokkene;

        if (
            $betrokkene_info->{betrokkene_type} eq 'medewerker' ||
            $betrokkene_info->{betrokkene_type} eq 'org_eenheid'
        ) {
            $betrokkene_info->{betrokkene_id} =
                $betrokkene_info->{gegevens_magazijn_id}
        } else {
            $betrokkene_info->{betrokkene_id} =
                $old_betrokkene->$colname->id
        }

        $raw_betrokkene = $self->result_source->schema->resultset('ZaakBetrokkenen')->create(
            $betrokkene_info
        );
    }

    return unless $raw_betrokkene;

    my ($internextern, $search_id) = (1, $raw_betrokkene->id);

    if (
        my $betrokkene = $self->z_betrokkene->get(
            {
                intern => 1,
                type    => $betrokkene_info->{betrokkene_type},
            }, $raw_betrokkene->id
        )
    ) {
        $raw_betrokkene->naam( $betrokkene->naam );
        $raw_betrokkene->update;
    }

    return $raw_betrokkene->id;
}

sub _betrokkene_get_handle {
    my ($self, $betrokkene_info) = @_;

    ## Set betrokkene-TYPE-ID
    my $betrokkene_ident  = $self->z_betrokkene->set($betrokkene_info->{betrokkene});

    if (!$betrokkene_ident) {
        throw("Case/Betrokkene/get_handle", sprintf("Betrokkene '%s' not found!", $betrokkene_info->{betrokkene}));
    }

    ### Retrieve TYPE
    my ($betrokkene_type) = $betrokkene_info->{betrokkene}
        =~ /betrokkene-([\w_]+)-/;

    ### Retrieve betrokkene ID from ident (GMID-ID)
    my ($betrokkene_id) = $betrokkene_ident =~ /-(\d+)$/;

    ### Retrieve just created betrokkene
    my $betrokkene = $self->z_betrokkene->get(
        {
            intern => 1,
            type    => $betrokkene_type,
        }, $betrokkene_id
    );

    if ($betrokkene) {
        return $betrokkene_id;
    }

    throw("Case/Betrokkene/get_handle", sprintf("Cannot retrieve found betrokkene '%s'", $betrokkene_info->{betrokkene}));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 betrokkene_set

TODO: Fix the POD

=cut

