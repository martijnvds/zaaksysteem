package Zaaksysteem::Zaken::ComponentZaakBetrokkenen;
use Moose;
use Data::UUID;

extends 'DBIx::Class::Row';

with 'MooseX::Log::Log4perl';

use Zaaksysteem::Constants qw(
    LOGGING_COMPONENT_ZAAK
);

=head1 NAME

Zaaksysteem::Zaken::ComponentZaakBetrokkenen

=head1 DESCRIPTION

This package defines additional data and behavior for the
L<ZaakBetrokkenen|Zaaksysteem::Schema::ZaakBetrokkenen> table.

=head1 ATTRIBUTES

=head2 betrokkene_identifier

This attribute holds a stringified 'betrokkene identifier', composed from
our internal data.

B<Warning>: This attribute may be out of date after updates to the
L<betrokkene_type|Zaaksysteem::Schema::ZaakBetrokkenen/betrokkene_type> or
L<gegevens_magazijn_id|Zaaksysteem::Schema::ZaakBetrokkenen/gegevens_magazijn_id>
fields.

=cut

has betrokkene_identifier => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        my $self = shift;

        return sprintf(
            'betrokkene-%s-%s',
            $self->betrokkene_type,
            $self->gegevens_magazijn_id
        );
    }
);

around update => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $orig->($self, @_);

    return $rv if $self->betrokkene_type ne 'medewerker';

    $self->_update_acl_for_employee($self->authorisation);
    return $rv;
};

sub _update_acl_for_employee {
    my ($self, $scope) = @_;

    return if $self->betrokkene_type ne 'medewerker';

    return unless $self->get_column('zaak_id');
    return if ($self->rol eq 'Behandelaar' || $self->rol eq 'Coordinator');

    my $subject = $self->result_source->schema->resultset('Subject')->find(
        $self->gegevens_magazijn_id
    );

    # JS is unable to set a default to an empty string, so we force none and
    # this is what we store inside the DB. But the scope setting none does not
    # exist down low in the Perl code
    my @scope;
    if ($scope && $scope ne 'none') {
        @scope = ($scope);
    }

    $self->zaak_id->update_user_acl(
        {
            capabilities => \@scope,
            entity_id    => $subject->username,
            entity_type  => 'user',
            scope        => 'instance',
        }
    );
    return 1;
}

=head1 METHODS

=head2 delete

When a ZaakBetrokkene is deleted, this should be logged.

=cut

before delete => sub {
    my $self = shift;

    $self->_update_acl_for_employee();

    $self->zaak_id->trigger_logging(
        'case/subject/remove',
        {
            component => LOGGING_COMPONENT_ZAAK,
            data => {
                subject_name => $self->naam,
                case_id      => $self->get_column('zaak_id'),
                role         => $self->rol,
            },
        },
    );

    return;
};

=head2 TO_JSON

This method returns a hashref which can be used by L<JSON/encode_json> to
serialize instances of this class.

=cut

sub TO_JSON {
    my $self = shift;

    return {
        id                    => $self->id,
        name                  => $self->naam,
        role                  => $self->rol,
        pip_authorized        => $self->pip_authorized ? \1 : \0,
        betrokkene_id         => $self->betrokkene_id,
        betrokkene_type       => $self->betrokkene_type,
        betrokkene_identifier => $self->betrokkene_identifier,
        magic_string_prefix   => $self->magic_string_prefix,
        employee_authorisation => $self->authorisation,
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2019 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
