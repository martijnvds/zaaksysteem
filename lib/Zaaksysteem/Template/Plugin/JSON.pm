package Zaaksysteem::Template::Plugin::JSON;

use Moose;
use JSON::XS;

BEGIN { extends 'Template::Plugin'; }

has context => ( is => 'ro', isa => 'Template::Context' );

sub load { shift; }

sub BUILDARGS {
    my $orig = shift;
    my $self = shift;

    my $context = shift;

    return $self->$orig({ context => $context });
}

sub encode {
    my $self = shift;

    my $scalar;

    if(scalar(@_) > 1) { $scalar = \@_; }
    else { $scalar = shift; }

    my $json = JSON::XS->new->canonical->allow_nonref->allow_blessed->convert_blessed->encode($scalar);

    $json =~ s/\\"/\\u0022/g;
    $json =~ s/</\\u003c/g;
    $json =~ s/>/\\u003e/g;
    $json =~ s/&/\\u0026/g;

    return $json;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILDARGS

TODO: Fix the POD

=cut

=head2 encode

TODO: Fix the POD

=cut

=head2 load

TODO: Fix the POD

=cut

