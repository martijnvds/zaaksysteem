package Zaaksysteem::PDFGenerator;
use Moose;
use namespace::autoclean;

use Zaaksysteem::DocumentConverter;
use BTTW::Tools;
use BTTW::Tools::File qw(get_file_extension);
use IO::All;

=head1 NAME

Zaaksysteem::PDFGenerator - PDF generator model

=head1 ATTRIBUTES

=head2 schema

The L<database schema|Zaaksysteem::Schema> instance to look up required/extra data in.

=cut

has schema => (
    is       => 'ro',
    isa      => 'DBIx::Class::Schema',
    required => 1,
);

=head1 METHODS

=head2 generate_pdf

Create a PDF from a specified template and context, and return the PDF data.

Requires two (named) arguments:

=over

=item * template

The L<template|Zaaksysteem::DB::Component::BibliotheekSjablonen> to use to
create the PDF from.

=item * context

The L<context|Zaaksysteem::ZTT::Context::WOZ> context to use to fill in the
template.

=back

=cut

define_profile generate_pdf =>  (
    required => {
        'template' => 'Zaaksysteem::DB::Component::BibliotheekSjablonen',
        'context' => 'Zaaksysteem::ZTT::Context',
    },
);

sub generate_pdf {
    my ($self, %params) = @_;
    my $args = assert_profile(\%params)->valid;

    my $ztt = Zaaksysteem::ZTT->new()->add_context($args->{context});
    my $template = $args->{template}->filestore_id;

    my $tmp_file = $template->save_template_document($ztt);
    my $content = io($tmp_file->filename)->slurp;

    my $extension = get_file_extension($template->original_name);
    my $converter = Zaaksysteem::DocumentConverter->new();

    my $pdf_content = $converter->convert_scalar(
        destination_type => 'pdf',
        source_extension => $extension,
        source           => $content,
    );

    return $pdf_content;
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
