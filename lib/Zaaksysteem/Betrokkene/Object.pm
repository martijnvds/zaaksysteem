package Zaaksysteem::Betrokkene::Object;
use Zaaksysteem::Moose;

with qw[
    Zaaksysteem::ZTT::Context
    MooseX::Clone
];

use Zaaksysteem::Betrokkene::ResultSet;
use Zaaksysteem::Constants qw[RGBZ_LANDCODES ZAAKSYSTEEM_BETROKKENE_SUB];

has 'types' => (
    'is'      => 'ro',
    'isa'     => 'HashRef',
    'lazy'    => 1,
    'default' => sub {
        {
            'medewerker'         => 'Medewerker',
            'natuurlijk_persoon' => 'NatuurlijkPersoon',
        }
    },
);

has prod => (
    weak_ref => 1,
    is       => 'ro',
);

has stash => (
    weak_ref => 1,
    is       => 'ro',
);

has config => (
    weak_ref => 1,
    is       => 'ro',
);

has customer => (
    weak_ref => 1,
    is       => 'ro',
);

has dbic => (
    weak_ref => 1,
    is       => 'ro',
);

has gm_object => (
    is      => 'rw',
    isa     => 'Defined',
    builder => '_build_gm_object',
    clearer => '_clear_gm_object',
    lazy    => 1,
);

has historic => (
    is      => 'ro',
    isa     => 'Defined',
    builder => '_build_historic',
    lazy    => 1,
);

has current => (
    is      => 'ro',
    isa     => 'Defined',
    builder => '_build_current',
    lazy    => 1,
);

sub _build_historic {
    my ($self) = @_;
    my $clone = $self->_clone();
    $clone->_load_intern;
    return $clone;
}

sub _build_current {
    my ($self) = @_;
    my $clone = $self->_clone();
    $clone->_load_extern;
    return $clone;
}

sub _build_gm_object {
    my $self = shift;
    if ($self->can('gm_np')) {
        return $self->gm_np;
    }
    else {
        return $self->gm_bedrijf;
    }
}

sub _clone {
    my $self = shift;
    my $clone = $self->clone();
    $clone->_clear_gm_object();
    return $clone;
}

has preferred_contact_channel => (
    is      => 'rw',
    isa     => 'Str',
    builder => '_build_preferred_contact_channel',
    lazy    => 1,
    trigger => \&_set_preferred_contact_channel,
    clearer => '_clear_preferred_contact_channel',
);

sub _set_preferred_contact_channel {
   my $self = shift;
   my ($new, $old) = @_;

   if ($self->btype eq 'medewerker') {
      return;
   }

   $self->gm_object->update({ preferred_contact_channel => $new });
}

sub _build_preferred_contact_channel {
   my $self = shift;

   if ($self->btype eq 'medewerker') {
      return '';
   }

   return $self->dbic->resultset('DB::Config')->get('contact_channel_enabled') ?
      ( $self->gm_object->preferred_contact_channel // '' ) : ''
}

has '_dispatch_options' => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my $self    = shift;

        my $dispatch = {
            prod        => $self->prod,
            dbic        => $self->dbic,
            stash       => $self->stash,
            config      => $self->config,
            customer    => $self->customer,
        };

        Scalar::Util::weaken($dispatch->{stash});

        return $dispatch;
    }
);


### What was the trigger of the call, get, search etc
has 'trigger'      => (
    'is'    => 'rw'
);

### Type, well, should always be 'natuurlijk_persoon'
has 'type'      => (
    'is'    => 'rw'
);

### TODO ah why not, see other TODO
has 'bo'      => (
    'is'    => 'rw'
);

### There is always a betrokkene id
has 'id'        => (
    'is'    => 'rw',
);

### There is always a betrokkene_type
has 'btype'        => (
    'is'    => 'rw',
);

### And the identifier, when internal
has 'identifier' => (
    'is'    => 'rw',
);

### Authenticated by: can be: medewerker, kvk, gba
### or UNDEF (webform)
has 'authenticated_by' => (
    'is'    => 'rw',
);

### Only with GBA or KVK
has 'authenticated'    => (
    'is'    => 'rw',
);

has 'can_verwijderen'    => (
    'is'    => 'rw',
);

# The BAG location for this betrokkene's address (if it exists)
has 'bag_object'   => (
    'is'      => 'rw',
    'isa'     => 'Maybe[Zaaksysteem::Geo::BAG::Object]',
    'lazy'    => 1,
    'builder' => '_build_bag_object',
);

sub _build_bag_object {
    my ($self) = @_;

    return try {
        return $self->dbic->bag_model->get_exact(
            'postcode'              => $self->postcode || '',
            'huisnummer'            => $self->huisnummer || '',
            'huisletter'            => $self->huisletter || '',
            'huisnummer_toevoeging' => $self->huisnummertoevoeging || '',
        );
    }
    catch {
        $self->log->error("Error retrieving BAG object for betrokkene: $_");
        return;
    };
}

sub rt_setup_identifier {
    my ($self) = @_;

    return 'betrokkene-' . $self->btype
        . '-' . $self->ex_id;
}

has 'betrokkene_identifier' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return shift->rt_setup_identifier;
    }
);

has 'ex_id' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        if ($self->can('ldapid')) {
            return $self->ldapid;
        } elsif ($self->can('gmid')) {
            return $self->gmid;
        }
    }
);

has 'human_type' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        if( $self->btype eq 'natuurlijk_persoon' ) {
            return 'Natuurlijk persoon';
        }

        if( $self->btype eq 'bedrijf' ) {
            return 'Niet natuurlijk persoon';
        }

        if( $self->btype eq 'medewerker' ) {
            return 'Behandelaar';
        }
    }
);

has [qw/is_overleden is_briefadres in_onderzoek/]  => (
    'is'    => 'ro',
);

has 'record'    => (
    'is'        => 'rw',
    'isa'       => 'DBIx::Class::Row'
);

has 'country'      => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self = shift;

        my $landcode;
        if ($self->btype eq 'natuurlijk_persoon') {
            $landcode = $self->landcode;
        } elsif ($self->btype eq 'bedrijf') {
            $landcode = $self->vestiging_landcode;
        }
        return RGBZ_LANDCODES->{$landcode} if $landcode;
    }
);

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        betrokkene_type => $self->type,
        gegevens_magazijn_id => $self->ex_id,
        naam => $self->naam,
        subject_identifier => $self->betrokkene_identifier,

        betrokkene_id => undef,
        deleted => undef,
        uuid => $self->uuid,
        verificatie => undef,
        rol => undef,
        magic_string_prefix => undef
    };
}

=head2 as_hashref

Returns nothing.

Subclasses use this to return a representation of themselves in plain hash form.

=cut

sub as_hashref {
    return;
}

sig log_view => 'Str';

sub log_view {
    my ($self, $user_id) = @_;

    return 1 unless $self->btype eq 'natuurlijk_persoon';

    return $self->dbic->resultset('Logging')->trigger(
        'subject/view',
        {
            component => 'betrokkene',
            betrokkene_id => $user_id,
            data => {
                name        => $self->naam,
                identifier  => $self->burgerservicenummer,
            }
        }
    );
}


sub street_address {
    my ($self) = @_;

    # for other btypes, die of natural causes. let nature be.
    return $self->btype eq 'bedrijf' ? (join ' ', grep { $_ } (
            $self->vestiging_straatnaam,
            $self->vestiging_huisnummer,
            $self->vestiging_huisnummertoevoeging
        )
    ) : join ' ', grep { $_ } (
        $self->straatnaam,
        $self->huisnummer,
        $self->huisnummertoevoeging
    );
}

sub city {
    my ($self) = @_;

    return $self->btype eq 'bedrijf' ? $self->vestiging_woonplaats : $self->woonplaats;
}

sub postal_code {
    my ($self) = @_;

    return $self->btype eq 'bedrijf' ? $self->vestiging_postcode : $self->postcode;
}

=head2 as_object

Return the "Subject" object representing this betrokkene.

=cut

sub as_object {
    my $self = shift;

    my $bridge = Zaaksysteem::BR::Subject->new(
        schema => $self->dbic,
    );
    my $object = $bridge->find($self->uuid);

    if (!$object) {
        my $message = sprintf(
            "No object found for betrokkene '%s' uuid='%s'",
            $self->betrokkene_identifier,
            $self->uuid
        );
        $self->log->info($message);

        throw(
            'betrokkene/object/not_found',
            $message,
        );
    }

    return $object;
}

=head2 get_string_fetchers

Implements magic_string value retrieval logic for L<Zaaksysteem::ZTT::Context>.

=cut

sub get_string_fetchers {
    my $self = shift;

    return sub {
        my $tag = shift;

        return Zaaksysteem::ZTT::Element->new(
            value => ZAAKSYSTEEM_BETROKKENE_SUB->($self, $tag->name) || ''
        );
    };
}

sub create_message {
    my ($self, %opts) = @_;

    my %params = %opts;

    return $self->dbic->resultset('Message')->message_create(
        {
            $params{case_id} ? (case_id    => $params{case_id} ) : (),
            subject_id => $self->betrokkene_identifier,
            event_type => $params{event_type},
            message    => $params{message},
            $params{log} ? (log => $params{log}) : (),
        }
   );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 RGBZ_LANDCODES

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 city

TODO: Fix the POD

=cut

=head2 log_view

TODO: Fix the POD

=cut

=head2 postal_code

TODO: Fix the POD

=cut

=head2 rt_setup_identifier

TODO: Fix the POD

=cut

=head2 street_address

TODO: Fix the POD

=cut

