package Zaaksysteem::Betrokkene::Object::NatuurlijkPersoon;
use Moose;

extends 'Zaaksysteem::Betrokkene::Object';
with 'Zaaksysteem::Betrokkene::Roles::Naw';

use BTTW::Tools;
use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik/;
use Zaaksysteem::Constants qw/FRIENDLY_BETROKKENE_MESSAGES/;
use Zaaksysteem::Types qw(UUID);
use List::Util qw(any first);

my $CLONE_MAP = [qw/
    burgerservicenummer
    a_nummer
    voornamen
    geslachtsnaam
    voorvoegsel
    adellijke_titel
    geslachtsaanduiding
    geboorteplaats
    geboorteland
    geboortedatum
    aanhef_aanschrijving
    voorletters_aanschrijving
    voornamen_aanschrijving
    naam_aanschrijving
    voorvoegsel_aanschrijving
    burgerlijke_staat

    indicatie_geheim
    authenticatedby
    import_datum
    verblijfsobject_id
    datum_huwelijk
    datum_huwelijk_ontbinding

    aanduiding_naamgebruik

    partner_voorvoegsel
    partner_geslachtsnaam
    partner_burgerservicenummer

    preferred_contact_channel
/ ];

my $CONTACT_MAP = {
    telefoonnummer  => 1,
    mobiel          => 1,
    email           => 1,
    note            => 1,
};

my $ADRES_CLONE_MAP = [qw/
    straatnaam
    huisnummer
    woonplaats
    postcode
    huisletter
    huisnummertoevoeging
    functie_adres
    adres_buitenland1
    adres_buitenland2
    adres_buitenland3
    landcode
    gemeente_code
/];

my @SEARCH_MAP = qw(
    straatnaam
    huisnummer
    woonplaats
    postcode
    huisletter
    huisnummertoevoeging
);

my @SEARCH_MAP_NP = qw(
    a_nummer
    aanhef_aanschrijving
    burgerlijke_staat
    burgerservicenummer
    datum_overlijden
    geboortedatum
    geboorteland
    geboorteplaats
    geslachtsaanduiding
    geslachtsnaam
    indicatie_geheim
    naam_aanschrijving
    partner_geslachtsnaam
    partner_voorvoegsel
    uuid
    voorletters_aanschrijving
    voornamen
    voornamen_aanschrijving
    voorvoegsel
    voorvoegsel_aanschrijving
);

=head2 in_onderzoek

Retrieve db field

=cut

sub in_onderzoek {
    my ($self) = @_;

    return $self->gm_extern_np->in_onderzoek;
}

=head2 active

Ask if the Natuurlijk Persoon object is active or not

=cut

sub active {
    my ($self) = @_;

    return $self->gm_extern_np->active;
}


has 'is_briefadres'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;

        if (uc($self->functie_adres || '') eq 'B') {
            return 1;
        }
    }
);

has 'subscription_id'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        return shift->gm_extern_np->subscription_id
    }
);


has uuid => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return shift->gm_extern_np->uuid;
    }
);

has 'gm_adres'  => (
    'is'    => 'rw',
);

has 'gm_np'     => (
    'is'    => 'rw',
);

has 'gm_extern_np'     => (
    'is'    => 'rw',
);

has 'is_overleden' => (
    'is'    => 'rw',
    'lazy'  => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->gm_extern_np->is_overleden(@_);
    }
);

=head2 verblijfsadres

Return: L<Zaaksysteem::Schema::Adres>

=cut

has 'verblijfsadres'        => (
    'is'        => 'rw',
    'lazy'      => 1,
    'builder'   => '_build_verblijfsadres',
    'clearer'   => '_clear_verblijfsadres',
);

sub _build_verblijfsadres {
    my $self        = shift;

    $self->gm_np->_clear_verblijfsadres;
    return $self->gm_np->verblijfsadres;
}

=head2 correspondentieadres

Return: L<Zaaksysteem::Schema::Adres>

=cut

has 'correspondentieadres'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'builder'   => '_build_correspondentieadres',
    'clearer'   => '_clear_correspondentieadres',
);

sub _build_correspondentieadres {
    my $self        = shift;

    $self->gm_np->_clear_correspondentieadres;
    return $self->gm_np->correspondentieadres;
}

=head2 add_address

Arguments: \%ADDRESS_PARAMS

Returns: Adres ROW

=cut

sub add_address {
    my $self    = shift;

    my $row     = $self->gm_np->add_address(@_);

    ### Reload cache
    $self->$_ for qw/_clear_correspondentieadres _clear_verblijfsadres/;
    $self->gm_np->_clear_cached_addresses;
    $self->adres_id($self->gm_np->adres_id);

    return $row;
}

=head2 delete_address_by_function

Arguments: $STRING_FUNCTIE_LETTER (enum: B or W)

Returns: $TRUE_ON_SUCCESS

$np->delete_address_by_function('B');

Removes an address by function

=cut

sub delete_address_by_function {
    my $self    = shift;

    my $row     = $self->gm_np->delete_address_by_function(@_);

    ### Reload cache
    $self->$_ for qw/_clear_correspondentieadres _clear_verblijfsadres/;
    $self->gm_np->_clear_cached_addresses;
    $self->adres_id($self->gm_np->adres_id);

    return $row;
}

=head2 bsn

Convenience method for BSN

=cut

sub bsn {
    my $self = shift;
    return sprintf("%09d", $self->burgerservicenummer);
}


sub is_verhuisd {
    my $self    = shift;

    my $from  = $self->woonplaats // '';
    my $adres = $self->gm_extern_np->adres_id;
    my $to    = $adres ? $adres->woonplaats // '' : '';

    return 1 if ($from ne $to);
    return 0;
}

=head2 datum_overlijden

Retrieve potential passing date

=cut

sub datum_overlijden {
    my ($self) = @_;

    return $self->gm_extern_np->datum_overlijden;
}


has 'gmid'      => (
    'is'    => 'rw',
);

has 'intern'    => (
    'is'    => 'rw',
);

has 'in_zaaksysteem'    => (
    'is'    => 'rw',
);

### Convenience method containing some sort of display_name
### Ai, this one is different from the one we punt in the database as
### display_name. We create a new method display_name to get it even
has 'naam' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        ### Depends on aanduiding naamgebruik.
        return $self->gm_np->display_name;
    },
);

has display_name => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my ($self) = @_;
        return join(" ",
            map  { $self->$_ }
            grep { length($self->$_) } qw(voornamen achternaam));
    },
);

has '_geslacht' => (
    is => 'ro',
    lazy => 1,
    builder => '_get_geslacht',
);

sub _get_geslacht {
    my $self = shift;

    my $geslacht = uc($self->geslachtsaanduiding // '');
    return first { $_ eq $geslacht } qw(M V);
    return '';
}

has is_male => (
    is => 'ro',
    lazy => 1,
    builder => '_is_male',
);

sub _is_male {
    my ($self) = @_;
    return $self->_geslacht eq 'M';
}

has is_female => (
    is => 'ro',
    lazy => 1,
    builder => '_is_female',
);

sub _is_female {
    my ($self) = @_;
    return $self->_geslacht eq 'V';
}


has 'geslacht'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'man' if $self->is_male;
        return 'vrouw' if $self->is_female;
        return;
    }
);

has 'aanhef'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'meneer' if $self->is_male;
        return 'mevrouw' if $self->is_female;
        return;
    }
);

has 'aanhef1'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'heer' if $self->is_male;
        return 'mevrouw' if $self->is_female;
        return;
    }
);

has 'aanhef2'      => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return 'de heer' if $self->is_male;
        return 'mevrouw' if $self->is_female;
        return;
    }
);

has 'volledige_naam' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return join(" ",
            map  { $self->$_ }
            grep { length($self->$_) } qw(voorletters achternaam));
    },
);

has 'achternaam'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->gm_np->achternaam;
    }
);

has 'volledig_huisnummer'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->get_volledig_huisnummer_from_adres($self) if $self;
    }
);

has 'verblijf_volledig_huisnummer'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->get_volledig_huisnummer_from_adres($self->verblijfsadres) if $self;
    }
);

has 'correspondentie_volledig_huisnummer'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my ($self) = @_;

        return $self->get_volledig_huisnummer_from_adres($self->correspondentieadres) if $self;
    }
);

has 'messages'    => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;
        my %rv;

        $rv{'briefadres'}   = FRIENDLY_BETROKKENE_MESSAGES->{'briefadres'}
            if $self->is_briefadres;
        $rv{'onderzoek'}    = FRIENDLY_BETROKKENE_MESSAGES->{'onderzoek'}
            if $self->in_onderzoek;
        $rv{'deceased'}     = FRIENDLY_BETROKKENE_MESSAGES->{'deceased'}
            if $self->is_overleden;
        $rv{'secret'}       = FRIENDLY_BETROKKENE_MESSAGES->{'secret'}
            if $self->indicatie_geheim;
        $rv{'moved'}        = FRIENDLY_BETROKKENE_MESSAGES->{'moved'}
            if $self->is_verhuisd;

        return \%rv;
    }
);

has 'messages_by_urgence' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;
        my (@info, @error);

        my $messages            = $self->messages;

        push(@info, $messages->{briefadres})    if $messages->{briefadres};

        push(@error, $messages->{onderzoek})    if $messages->{onderzoek};
        push(@error, $messages->{deceased})     if $messages->{deceased};
        push(@error, $messages->{secret})       if $messages->{secret};

        push(@info, $messages->{moved})         if $messages->{moved};


        return {
            error   => \@error,
            info    => \@info,
        };
    }
);

has 'messages_as_flash_messages' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;
        my @messages;

        my $aanvrager_messages  = $self->messages_by_urgence;

        push (
            @messages,
            {
                'type'      => 'error',
                'message'   => $_,
            }
        ) for @{ $aanvrager_messages->{error} };

        push (
            @messages,
            {
                'type'      => 'info',
                'message'   => $_,
            }
        ) for @{ $aanvrager_messages->{info} };

        return \@messages;
    }
);

has 'voorletters'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'builder'   => '_build_voorletters',
);

sub _build_voorletters {
    my $self        = shift;

    return $self->gm_np->_build_voorletters(@_);
}

has 'can_verwijderen'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return 1 unless $self->gm_extern_np->authenticated;

        return;
    },
);

my $ORDER_MAP = {
    'geslachtsnaam'     => 'geslachtsnaam',
    'voornamen'         => 'voornamen',
    'voorvoegsel'       => 'voorvoegsel',
    'bsn'               => 'burgerservicenummer',
    'geboortedatum'     => 'geboortedatum',
    'straatnaam'        => 'straatnaam',
    'huisnummer'        => 'huisnummer',
    'authenticated'     => 'authenticated',
};

my $ORDER_DIR = {
    'ASC'       => 'asc',
    'DESC'      => 'desc',
};

sub _map_natuurlijk_persoon_search {
    my ($self, $searchr, $db_key_prefix) = @_;
    my %search;

    my @exact_matches = qw(geboortedatum uuid);
    my @fuzzy         = qw(voorvoegsel geslachtsnaam);
    foreach my $key (@SEARCH_MAP_NP) {
        next unless defined($searchr->{$key});

        my $value  = $searchr->{$key};
        my $db_key = $key;
        $db_key = "$db_key_prefix.$db_key" if $db_key_prefix;

        if ($key eq 'burgerservicenummer') {
            $value  = int($value);
            $db_key = "NULLIF($db_key,'')::integer";
        }
        elsif ($searchr->{EXACT} || any { $_ eq $key } @exact_matches) {
            if (any { $_ eq $key } @fuzzy) {
                $value = { 'ilike' => "%$value%" };
            }
            # value = value
        }
        else {
            $value = { 'ilike' => "%$value%" };
        }
        $search{$db_key} = $value;
    }
    return \%search;
}

sub _map_address_search_options {
    my ($self, $searchr, $db_key_prefix) = @_;

    my %search;
    my @exact_matches = qw(huisnummer);
    foreach my $key (@SEARCH_MAP) {
        next unless defined($searchr->{$key});

        my $value  = $searchr->{$key};
        my $db_key = $key;
        $db_key = "$db_key_prefix.$db_key" if $db_key_prefix;

        if ($key eq 'postcode') {
            $value = uc($value);
            $value =~ s/\s*//g;
            $value = { 'ilike' => "%$value%" };
        }
        elsif ($searchr->{EXACT} || any { $_ eq $key } @exact_matches) {
            # value = value
        }
        else {
            $value = { 'ilike' => "%$value%" };
        }
        $search{$db_key} = $value;
    }
    return \%search;
}

sub _search_via_np {
    my ($self, $schema, $search, $opts) = @_;

    my $address_search = $self->_map_address_search_options($search, 'adres_id');
    my $people_search = $self->_map_natuurlijk_persoon_search($search);

    my $model = 'GmNatuurlijkPersoon';

    my %search = %$people_search;

    if (!delete $opts->{intern}) {
        $model = 'NatuurlijkPersoon';
        $search{'me.deleted_on'} = undef;
    }

    if ($address_search) {
        %search = (%search, %$address_search);
    }

    $opts->{prefetch} = 'adres_id';
    return $schema->resultset($model)->search_rs(\%search, $opts);
}

sub search {
    my $self = shift;

    ### We will return a resultset layer over DBIx::Class containing
    ### data, we will use to populate this class. That's why we cannot
    ### be called when we are the object itself. Should be a future
    ### feature
    die('M::B::NP->search() only possible call = class based')
        unless !ref($self);

    my ($dispatch_options, $opts, $searchr) = @_;
    return unless defined($opts->{intern});

    my ($row_order, $row_order_direction);
    if (exists $dispatch_options->{stash}{order}) {
        $row_order = $ORDER_MAP->{ $dispatch_options->{stash}{order} };
    }
    if (exists $dispatch_options->{stash}{order_direction}) {
        $row_order_direction = $ORDER_DIR->{$dispatch_options->{stash}{order_direction}};
    }
    $row_order           //= 'geslachtsnaam';
    $row_order_direction //= 'asc';


    $dispatch_options->{stash}{paging_rows} ||= 40;
    $dispatch_options->{stash}{paging_page} ||= 1;

    my $row_limit = $opts->{rows_per_page}
        ? $opts->{rows_per_page}
        : $dispatch_options->{stash}{paging_rows};
    $dispatch_options->{stash}{paging_rows} = $row_limit;

    my $resultset = $self->_search_via_np(
        $dispatch_options->{dbic},
        $searchr,
        {
            intern   => $opts->{intern},
            page     => $dispatch_options->{stash}{paging_page},
            rows     => $row_limit,
            order_by => { '-' . $row_order_direction => $row_order }
        }
    );

    $dispatch_options->{stash}{paging_total}
        = $resultset->pager->total_entries;

    $dispatch_options->{stash}{paging_lastpage}
        = $resultset->pager->last_page;

    return Zaaksysteem::Betrokkene::ResultSet->new(
        'class'     => __PACKAGE__,
        'dbic_rs'   => $resultset,
        'opts'      => $opts,
        %{ $dispatch_options },
    );
}


sub BUILD {
    my ($self) = @_;

    ### Nothing to do if we do not know which way we came in
    return unless ($self->trigger && $self->trigger eq 'get' && $self->id);

    ### It depends on the 'intern' option, weather we retrieve
    ### our data from our our snapshot DB, or GM. When there is
    ### no intern defined, we will look at the id for a special string
    if ($self->id =~ /\-/) {
        $self->log->trace('XXX Found special string');

        ### Special string, no intern defined, go to intern default
        if (!defined($self->{intern})) {
            $self->log->trace('XXX Found internal request');
            $self->{intern} = 1;
        }

        my ($gmid, $id) = $self->id =~ /^(\d+)\-(\d+)$/;

        $self->id($id);
        $self->gmid($gmid);
    }

    if (!$self->intern) {
        ### Get id is probably gmid, it is an external request, unless it is
        ### already set of course
        if (!$self->gmid) {
            $self->gmid($self->id);
            $self->id(undef);
        }
    }

    if ($self->{intern}) {
        $self->_load_intern or die('Failed loading internal M::B::NP Object');
    } else {
        $self->_load_extern or die('Failed loading external M::B::NP Object');
    }

    ### Some defaults, should move to Object
    $self->btype('natuurlijk_persoon');

}

sub _load_contact_data {
    my ($self, $gm_id)  = @_;

    return unless $gm_id;

    my $contactdata = $self->dbic->resultset('ContactData')->search({
        gegevens_magazijn_id    => $gm_id,
        betrokkene_type         => 1,
    })->first;

    return unless defined $contactdata;

    for my $key (keys %{ $CONTACT_MAP }) {
        $self->{$key} = $contactdata->$key;
    }
}

sub _load_extern {
    my ($self) = @_;

    my $gm = $self->dbic->resultset('NatuurlijkPersoon')->search(
        { 'me.id' => $self->gmid },
        { prefetch => 'adres_id' }
    )->first;

    unless (defined $gm) {
        $self->log->warn(sprintf(
            'Could not find external GM by id: ',
            $self->gmid
        ));

        return;
    }

    $self->gm_np($gm);

    $self->_load_address_data;
    $self->_load_authenticated_data($gm);
    $self->_load_attributes;
    $self->_load_contact_data($self->gmid);

    $self->gm_extern_np($self->gm_np);

    return 1;
}

sub _load_authenticated_data {
    my ($self, $object) = @_;

    $self->authenticated(1) if $object->authenticated;
    $self->authenticated_by($object->authenticatedby)
        if $object->authenticatedby;
}

sub _load_address_data {
    my $self = shift;

    if (my $address = $self->gm_np->adres_id) {
        $self->gm_adres($address);
        return 1;
    }
    return 0;
}

sub _load_intern {
    my ($self) = @_;

    my $bo = $self->dbic->resultset('ZaakBetrokkenen')->search(
        { 'me.id'  => $self->id },
        { prefetch => 'natuurlijk_persoon' },
    )->first;

    if (!defined $bo) {
        $self->log->warn(sprintf(
            'Could not find internal betrokkene by id (%d)',
            $self->id
        ));
        return;
    }

    ### TODO : NO idea yet if I really need this object
    $self->bo($bo);

    ### Retrieve data from internal GM
    return unless $bo->natuurlijk_persoon;

    my $dbg = $self->dbic->resultset('NatuurlijkPersoon')->search(
        { 'me.id' => $bo->natuurlijk_persoon->get_column('gegevens_magazijn_id') },
        { prefetch => 'adres_id' }
    )->first;

    ### Make sure we have these data for back reference
    $self->gm_np($bo->natuurlijk_persoon);
    $self->identifier($bo->natuurlijk_persoon->id . '-' . $self->id);

    $self->_load_address_data;
    $self->_load_authenticated_data($dbg);
    $self->_load_attributes;

    if (my $data = $bo->natuurlijk_persoon->gegevens_magazijn_id) {
        $self->_load_contact_data($data);
        $self->gmid($data);
    }

    $self->gm_extern_np($dbg);

    return 1;
}

sub _load_attributes {
    my ($self) = @_;

    for my $meth (keys %{ $CONTACT_MAP }) {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            ### On update, add custom field back to RT
            'trigger'   => sub {
                my ($self, $new, $old) = @_;
                my ($external_id);

                $self->log->trace("Trigger called for contactupdate: $meth ($old => $new)");

                return $new if ($old && $new eq $old);

                if ($self->gmid) {
                    $external_id = $self->gmid;
                } else {
                    $external_id = $self->bo->natuurlijk_persoon->gegevens_magazijn_id;
                }

                my $contactdata = $self->dbic->resultset('ContactData')->search({
                    gegevens_magazijn_id    => $external_id,
                    betrokkene_type         => 1,
                })->first;

                if ($contactdata) {
                    $contactdata->$meth($new);
                    $contactdata->update;
                }
                else {
                    $contactdata
                        = $self->dbic->resultset('ContactData')->create(
                        {
                            'gegevens_magazijn_id' => $external_id,
                            'betrokkene_type'      => 1,
                            $meth                  => $new,
                        }
                        );
                }
            },
        );
    }

    for my $meth (@{ $CLONE_MAP }, 'adres_id', 'in_gemeente') {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            ### On update, add custom field back to RT
            'trigger'   => sub {
                my ($self, $new) = @_;

                # And definetly do not update the adres_id
                if ($meth eq 'adres_id') { return; }

                ### Update object
                $self->gm_np->$meth($new);
                $self->gm_np->update;
            },
            ### Load custom fields from RT
            'default'   => sub {
                my ($self) = @_;

                return $self->gm_np->$meth;
            }
        );
    }

    ### Adres?
    for my $meth (@{ $ADRES_CLONE_MAP }) {
        $self->meta->add_attribute($meth,
            'is'        => 'rw',
            'lazy'      => 1,
            'default'   => sub {
                my ($self) = @_;

                my $adres = $self->gm_np->adres_id;
                if ($adres && $adres->can($meth)) {
                    return $adres->$meth;
                }
                return '';

            },
            'trigger'   => sub {
                my ($self, $new, $old) = @_;

                if ($self->correspondentieadres) {
                    $self->correspondentieadres->$meth(($new) || undef);
                    $self->correspondentieadres->update;
                } elsif ($self->verblijfsadres) {
                    $self->verblijfsadres->$meth(($new) || undef);
                    $self->verblijfsadres->update;
                }
            },
        );

        for my $prefix (qw/verblijf correspondentie/) {
            $self->meta->add_attribute($prefix . '_' . $meth,
                'is'        => 'rw',
                'lazy'      => 1,
                'default'   => sub {
                    my ($self) = @_;

                    if ($prefix eq 'verblijf') {
                        return undef unless $self->verblijfsadres;

                        return $self->verblijfsadres->$meth;
                    } elsif ($prefix eq 'correspondentie') {
                        return undef unless $self->correspondentieadres;

                        return $self->correspondentieadres->$meth;
                    }
                },
                'trigger'   => sub {
                    my ($self, $new, $old) = @_;

                    if ($prefix eq 'verblijf') {
                        return $old unless $self->verblijfsadres;

                        $self->verblijfsadres->$meth(($new) || undef);
                        $self->verblijfsadres->update;
                    } elsif ($prefix eq 'correspondentie') {
                        return $old unless $self->correspondentieadres;

                        $self->correspondentieadres->$meth(($new) || undef);
                        $self->correspondentieadres->update;
                    }

                    ### Remove verblijfsobject
                    $self->gm_np->verblijfsobject_id(undef);
                    $self->gm_np->update;
                },
            );
        }
    }
}

sub _make_intern {
    my ($self, $dispatch_options, $gmo) = @_;

    $self->log->trace('M::B::NP->_make_intern called with object: ' . ref($gmo));
    return unless ref($gmo) eq __PACKAGE__;

    (
        $self->log->warn('M::B::NP->set: Not an external GM object'),
        return
    ) if $gmo->intern;

    ### Get NP values
    my %np_values           = map { $_ => $gmo->gm_np->$_ }
        @{ $CLONE_MAP };

    ### Alright, now get the addresses
    my @addresses;
    my $address_rs =$gmo->gm_np->addresses->search_rs;
    while (my $address = $address_rs->next) {
        my %np_adres_values     = map { $_ => $address->$_ }
            @{ $ADRES_CLONE_MAP };

        if ($address->id eq $gmo->gm_np->get_column('adres_id')) {
            $np_adres_values{primary} = 1;
        }

        push(@addresses, \%np_adres_values);
    }

    ### Create the link
    $np_values{'gegevens_magazijn_id'}  = $gmo->gmid;

    my $create = {
        'adressen'  => \@addresses,
        'gm'        => \%np_values,
    };

    my $res = $self->_create_intern(
        $dispatch_options,
        {},
        $create
    );

    return $res;
}

define_profile create => (
    required     => ['np-geslachtsnaam'],
    dependencies => {
        'np-landcode' => sub {
            my $dfv  = shift;
            my $code = shift;

            if (!defined $code || $code eq '6030') {
                if ($dfv->get_input_data->{'np-correspondentie_straatnaam'}) {
                    return [
                        'np-correspondentie_postcode',
                        'np-correspondentie_huisnummer',
                        'np-correspondentie_straatnaam',
                        'np-correspondentie_woonplaats'
                    ];
                }
                else {
                    return [
                        'np-postcode',   'np-huisnummer',
                        'np-straatnaam', 'np-woonplaats'
                    ];
                }
            }
            else {
                return ['np-adres_buitenland1'];
            }
        }
    },
    optional => [
        qw/
            authenticated_by

            np-authenticated

            np-voorletters
            np-voornamen
            np-geslachtsaanduiding
            np-huisnummer
            np-postcode
            np-straatnaam
            np-woonplaats
            np-functie_adres

            np-correspondentie_straatnaam
            np-correspondentie_huisnummer
            np-correspondentie_woonplaats
            np-correspondentie_postcode
            np-correspondentie_huisnummertoevoeging
            np-correspondentie_huisletter
            np-landcode
            uuid
            preferred_contact_channel
            /
    ],
    dependency_groups => {
        'verblijf' => [
            qw/
            np-correspondentie_straatnaam
            np-correspondentie_huisnummer
            np-correspondentie_woonplaats
            np-correspondentie_postcode
            /
        ],
    },
    optional_regexp => qr/npc?-.*/,
    defaults        => {
        'np-voorletters'   => '',
        'np-functie_adres' => 'W',
        'np-landcode'      => '6030',
        'np-authenticated' => 0,
    },
    constraint_methods => {
        'np-postcode' => qr/^\d{4}[A-Z]{2}$/i,
        uuid          => sub {
            my $val = pop;
            return UUID->check($val);
        }
    }
);

sub create {
    my ($self, $dispatch_options, $params) = @_;
    $params = assert_profile($params)->valid;

    my $create = {};

    ### generate data
    $create->{gm} = {
        map {
            my $label = $_;
            $label =~ s/^np-//g;
            $label => $params->{ $_ }
        } grep(/^np-/, keys %{$params})
    };

    foreach (qw(uuid preferred_contact_channel)) {
        $create->{gm}{$_} = $params->{$_} if $params->{$_};
    }

    $create->{gmc} = {
        map {
            my $label = $_;
            $label =~ s/^npc-//g;
            $label => $params->{ $_ }
        } grep(/^npc-/, keys %{ $params })
    };

    $create->{adres} = {};
    for my $adresid (@{$ADRES_CLONE_MAP},
        qw/correspondentie_straatnaam
           correspondentie_huisnummer
           correspondentie_woonplaats
           correspondentie_postcode
           correspondentie_huisnummertoevoeging
           correspondentie_huisletter/
        )
    {
        my $value = delete $create->{gm}{$adresid};
        if (defined $value) {
            $create->{adres}{$adresid} = $value;
        }
    }

    $create->{gm}{authenticatedby} = $params->{authenticated_by}
        if $params->{authenticated_by};

    $self->log->debug("Dumping " . dump_terse($create));

    my $boid = $self->_create_extern($dispatch_options, undef, $create);
    $self->log->trace("Creating natuurlijk persoon with ID $boid");
    return $boid;
}

sub _create_extern {
    my ($self, $dispatch_options, $opts, $create) = @_;

    my ($vbladres, $coradres);

    $coradres = { map({ my $adreskey = $_; $adreskey =~ s/^correspondentie_//; $adreskey => $create->{adres}->{$_} } grep ( { $_ =~ /^correspondentie_/ } keys %{ $create->{adres} })) };
    $coradres->{functie_adres} = 'B';
    $vbladres = { map({ $_ => $create->{adres}->{$_} } grep ( { $_ !~ /^correspondentie_/ } keys %{ $create->{adres} })) };
    $vbladres->{functie_adres} = 'W';

    my ($npacoroo, $npaoo);
    if ($vbladres->{straatnaam} || $vbladres->{adres_buitenland1}) {
        $npaoo = $dispatch_options->{dbic}->resultset('Adres')->create(
            $vbladres
        );
    }

    if ($coradres->{straatnaam} || $coradres->{adres_buitenland1}) {
        $npacoroo = $dispatch_options->{dbic}->resultset('Adres')->create(
            $coradres
        );
    }

    return unless ($npaoo || $npacoroo);

    $self->log->trace(
        'M::B::NP->_create_extern created adres with id ' .
        ($npacoroo ? $npacoroo->id : $npaoo->id)
    );

    ### Copy this ID to our GM
    my $npoo = $dispatch_options->{dbic}->resultset('NatuurlijkPersoon')->create(
        {
            'adres_id'      => ($npacoroo ? $npacoroo->id : $npaoo->id),
            %{ $create->{gm} }
        }
    );

    ### Make sure the links are correctly
    if ($npaoo) {
        $npaoo->natuurlijk_persoon_id($npoo->id);
        $npaoo->update;
    }

    if ($npacoroo) {
        $npacoroo->natuurlijk_persoon_id($npoo->id);
        $npacoroo->update;
    }

    return unless $npoo;

    $self->log->trace('M::B::NP->_create_extern created gm with id: ' . $npoo->id);

    ### Register contact_data
    if (
        $create->{gmc} &&
        %{ $create->{gmc} }
    ) {
        my $npco = $dispatch_options->{dbic}->resultset('ContactData')->create(
            {
                'gegevens_magazijn_id'  => $npoo->id,
                'betrokkene_type'       => 1,
                %{ $create->{gmc} }
            }
        );
    }

    return $npoo->id;
}

sub _create_intern {
    my ($self, $dispatch_options, $opts, $create) = @_;

    $self->log->trace('Create GmNatuurlijkPersoon');
    my $npoo = $dispatch_options->{dbic}->resultset('GmNatuurlijkPersoon')->create(
        $create->{gm}
    );

    my ($adres_id, $adres_id_brief);
    for my $address (@{ $create->{adressen} }) {

        # Legacy behaviour, we now take the B-riefadres as the main address if
        # it is present.
        delete $address->{primary}; # legacy

        my $npaoo = $dispatch_options->{dbic}->resultset('GmAdres')->create(
            {
                %{ $address },
                natuurlijk_persoon_id => $npoo->id,
            }
        );

        # See this is what we really really want
        if ($npaoo->functie_adres eq 'B') {
            $adres_id_brief = $npaoo->id;
        }
        else {
            $adres_id = $npaoo->id;
        }
    }

    # I wanna really, really, really wanna zigazig ahdress / spice girls
    # inspired comments
    $npoo->update({adres_id => $adres_id_brief ? $adres_id_brief : $adres_id});

    $self->gm_np($npoo);

    my $naam = $self->_build_voorletters($npoo->voornamen || '') . ' ' . naamgebruik({
        aanduiding => $npoo->aanduiding_naamgebruik || '',
        partner_voorvoegsel => $npoo->partner_voorvoegsel,
        partner_geslachtsnaam => $npoo->partner_geslachtsnaam,
        voorvoegsel => $npoo->voorvoegsel,
        geslachtsnaam => $npoo->geslachtsnaam,
    });

    $self->log->trace("Created GmNatuurlijkPersoon: $naam");

    if (!$npoo) {
        throw("ZS/B/O/NP", "Unable to create GmNatuurlijkPersoon");
    }
    $self->log->trace('M::B::NP->_create_intern created gm with id: ' . $npoo->id);

    ### Register contact_data
    if (ref $create->{gmc} eq 'HASH' && $create->{gm}{gegevens_magazijn_id} && scalar keys %{$create->{gmc}}) {
        $self->log->trace('Create Contactdata');
        my $npco = $dispatch_options->{dbic}->resultset('ContactData')->create(
            {
                gegevens_magazijn_id  => $create->{gm}->{gegevens_magazijn_id},
                betrokkene_type       => 1,
                %{ $create->{gmc} }
            }
        );
        if (!$npco) {
            throw("ZS/B/O/NP", "Unable to create ContactData");
        }
        $self->log->trace('M::B::NP->_create_intern created ContactData with id: ' . $npco->id);
    }


    ### Create betrokkene
    my $args = {
        betrokkene_type      => 'natuurlijk_persoon',
        betrokkene_id        => $npoo->id,
        gegevens_magazijn_id => $create->{gm}{gegevens_magazijn_id},
        naam                 => $naam,

        # Cheapskate UUID retrieval, save ourselves one DB round-trip by
        # plucking the data from the 'natuurlijk_persoon' table directly
        # during row creation.
        subject_id           => \[
            '(SELECT uuid FROM natuurlijk_persoon WHERE id = ?)',
            [ '' => $create->{ gm }{ gegevens_magazijn_id } ]
        ]
    };
    my $bo = $dispatch_options->{dbic}->resultset('ZaakBetrokkenen')->create($args);

    return unless $bo;
    $self->log->trace('M::B::NP->_create_intern created BO:' . $bo->id);

    return $bo->id;
}

sub set {
    my ($self, $dispatch_options, $external_id) = @_;

    ### We assume id is a GM id, because we cannot set an old betrokkene
    ### 'again'. So, we will load __PACKAGE__ with trigger get and as an
    ### external object. Feed it to our internal baker, and return a classy
    ### string with information;
    my $identifier = $external_id . '-';

    $self->log->trace('M::B::NP->set called with identifier: ' . $identifier);

    # Load external id
    my $gmo = __PACKAGE__->new(
        'trigger'       => 'get',
        'id'            => $external_id,
        'intern'        => 0,
        %{ $dispatch_options },
    );

    if (!$gmo) {
        throw("Betrokkene/Object/NP", "Unable to create Natuurlijk persoon met ID $external_id");
    }

    # Feed it to our baker
    my $bid = $self->_make_intern($dispatch_options, $gmo);
    if (!$bid) {
        $self->log->warn('M::B::NP->set no bid for gmo: ' . $gmo->id);
        return;
    }
    $identifier .= $bid;

    $self->log->trace('M::B::NP->set create identifier ' . $identifier);

    return 'natuurlijk_persoon-' . $identifier;
}

sub verwijder {
    my ($self) = @_;

    $self->gm_extern_np->deleted_on(DateTime->now);
    return $self->gm_extern_np->update;
}

=head2 as_hashref

Returns the betrokkene data as a plain hashref.

=cut

sub as_hashref {
    my $self = shift;

    return {
        id                => $self->betrokkene_identifier,
        name              => $self->display_name,
        street            => join(
            ' ',
            grep { $_ } (
                $self->straatnaam,
                $self->huisnummer,
                $self->huisnummertoevoeging
            )
        ),
        city              => $self->woonplaats,
        postal_code       => $self->postcode,
        telephone_numbers => [ grep { $_ } ($self->telefoonnummer, $self->mobiel) ],
        email_addresses   => [ grep { $_ } ($self->email) ],
        type              => 'natuurlijk_persoon',
        gmid              => $self->gmid,
        uuid              => $self->uuid,
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 FRIENDLY_BETROKKENE_MESSAGES

TODO: Fix the POD

=cut

=head2 create

TODO: Fix the POD

=cut

=head2 is_verhuisd

TODO: Fix the POD

=cut

=head2 search

TODO: Fix the POD

=cut

=head2 set

TODO: Fix the POD

=cut

=head2 verwijder

TODO: Fix the POD

=cut

