package Zaaksysteem::Event::P5::ExternalMessageCreated;
use Moose;

with 'Zaaksysteem::Event::EventInterface', 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Event::P5::ExternalMessageCreated - External message created event handler

=cut

sub event_type { 'ExternalMessageCreated' }

sub emit_event {
    my $self = shift;

    $self->queue->emit_case_event({
        case => $self->case,
        event_name => 'CaseThreadUpdated',
        description => 'Communicatie thread aangepast',
        changes => $self->event->{ changes }
    });

    my $external_message_type = $self->extract_new_value_from_event('external_message_type');

    if ($external_message_type ne 'pip') {
        my $msg = "ExternalMessageCreated event processed: " .
            "not a PIP message (but $external_message_type)";
        $self->log->info($msg);
        return $self->new_ack_message($msg);
    }

    my $subject   = $self->subject->find($self->event->{user_uuid});
    my $requestor = $self->case->aanvrager_object->as_object;
    my $assignee  = $self->case->behandelaar_object;

    $assignee = $assignee->as_object if $assignee;

    if (!defined $requestor && !defined $assignee) {
        my $msg = 'ExternalMessageCreated event processed: no recipients';
        $self->log->info($msg);
        return $self->new_ack_message($msg);
    }

    if ($subject->id eq $requestor->id) {
        $self->queue->emit_case_event(
            {
                case       => $self->case,
                event_name => 'CaseFeedbackCreated',
            }
        );
        return $self->new_ack_message('Event processed');
    }

    if ($subject->id eq $assignee->id) {
        my $tpl = $self->get_template_by_name(
            'new_int_pip_message_notification_template_id'
        );

        return $self->new_ack_message('Event processed: no template') if !$tpl;

        my $recipient = $requestor->subject->email_address;
        if (!$recipient) {
            return $self->new_ack_message('Event processed: no requestor mail');
        }

        $self->case->mailer->send_case_notification(
            {
                notification => $tpl,
                recipient    => $recipient
            }
        );
        return $self->new_ack_message('Event processed');
    }

    return $self->new_ack_message('Event processed: no intended recipients');
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
