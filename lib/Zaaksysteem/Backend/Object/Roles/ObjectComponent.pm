package Zaaksysteem::Backend::Object::Roles::ObjectComponent;

use Moose::Role;
use DateTime;
use DateTime::Format::ISO8601;
use BTTW::Tools;

use constant DEFAULT_COLUMNS => [qw[
    object.uuid
    object.id
    object.date_created
    object.date_modified
]];

has '_is_objectcomponent' => (
    'is'        => 'ro',
    'default'   => 1,
);

has 'object_describe' => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self                    = shift;

        my $rv                      = {
            class       => 'case',
            description => 'Zaaksysteem.nl case',
            label       => 'case',
            attributes  => []
        };

        if (
            $self->_zql_options->{requested_attributes} &&
            @{ $self->_zql_options->{requested_attributes} }
        ) {
            for my $requested_attribute (@{ $self->_zql_options->{requested_attributes} }) {
                my ($attribute) = grep (
                    { $requested_attribute eq $_->name }
                    @{ $self->object_attributes }
                );

                next unless $attribute;

                push @{ $rv->{attributes} }, $attribute->_json_data;
            }
        } else {
            for my $attribute (@{ $self->object_attributes }) {
                push @{ $rv->{attributes} }, $attribute->_json_data;
            }
        }

        return $rv;
    }
);

=head2 _zql_options

Options set by our ZQL handling, like notifying we would like to describe our object etc.

=cut

has '_zql_options'         => (
    'is'            => 'rw',
    'lazy'          => 1,
    'default'       => sub { {}; },
    'isa'           => 'HashRef'
);

has 'requested_columns'    => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self            = shift;

        if (
            $self->_zql_options->{requested_attributes} &&
            scalar @{ $self->_zql_options->{requested_attributes} }
        ) {
            return $self->_zql_options->{requested_attributes};
        }

        return [ keys %{ $self->properties->{values} } ];
    },
    'isa'           => 'ArrayRef'
);

=head2 $row->human_values

isa: ArrayRef

    my $zql     = Zaaksysteem::Search::ZQL->new('SELECT case.id,zaaktype.titel FROM case');

    my $row     = $zql->apply_to_resultset($objects)->first;

    print join(',', @{ $row->human_values });
    # Prints: 1, "Testzaaktype"

Returns an ArrayRef with the humanvalues of this row, as requested by the attribute list
given in, e.g, ZQL. See example

=cut


has 'human_values'          => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self            = shift;
        my @values;

        for my $col (@{ $self->requested_columns }) {
            push(@values, $self->properties->{values}->{$col}->{human_value});
        }

        return \@values;
    },
    'isa'           => 'ArrayRef'
);

=head2 blacklisted_columns

Set an array ref of strings which define the blacklisted columns. The
attributes found will display a message telling the user that they
aren't allowed to view the attributes

=cut

has blacklisted_columns => (
    is        => 'rw',
    isa       => 'ArrayRef[Str]',
    traits    => [qw(Array)],
    handles   => { _is_blacklisted => 'first' },
    default   => sub { [] },
    lazy      => 1,
);

=head2 $row->csv_data

isa: ArrayRef

    my $zql     = Zaaksysteem::Search::ZQL->new('SELECT case.id,zaaktype.titel FROM case');

    my $row     = $zql->apply_to_resultset($objects)->first;

    print join(',', @{ $row->csv_data });
    # Prints: 1, "Testzaaktype"

Returns an ArrayRef with the csv values (alias for humanvalues) of this row, as requested by the attribute list
given in, e.g, ZQL. See example

=cut

has 'csv_data' => (
    'is'      => 'ro',
    'isa'     => 'ArrayRef',
    'lazy'    => 1,
    'builder' => 'build_csv_data',
);

sub build_csv_data {
    my $self            = shift;

    my $columns = DEFAULT_COLUMNS();

    if ($self->_zql_options->{ requested_attributes } && scalar(@{ $self->_zql_options->{ requested_attributes }})) {
        $columns = $self->requested_columns;
    }

    my %ok;
    if ($self->can('get_search_attributes')) {
        %ok = map { $_ => 1 } $self->get_search_attributes;
    }

    my @data;
    for my $column (@{ $columns }) {

        my $value;
        if (%ok && !$ok{$column}) {
            $value = "";
        }
        elsif ($self->is_blacklisted($column)) {
            $value = "Geen rechten";
        }
        elsif ($self->properties->{values}->{$column}{dynamic_class}) {
            $value = $self->get_object_attribute($column)->value;
        }
        else {
            my $human_value = $self->properties->{values}->{$column}->{human_value};

            if (ref $human_value eq 'ARRAY') {
                $value = join ', ', @{ $human_value };
            } else {
                ### For non array values, format cells
                $value = $self->_csv_formatted_value($self->properties->{values}->{$column});
            }
        }

        push @data, $value;
    }
    return \@data;
}

=head2 _csv_formatted_value

Arguments: \%JSON_ATTRIBUTE

    $val = $self->_csv_formatted_value(
        {
            value           => '2014-10-23T11:46:13'
            attribute_type  => 'timestamp',
        }
    );

    # val = '2014-10-23'

Will format a time for CSV export. It will turn the input into UTC when the time
does not match '00:00:00', and will reoutput it in 'Europe/Amsterdam' time.

=cut

sub _csv_formatted_value {
    my $self            = shift;
    my $attribute       = shift;

    my $value           = $attribute->{value};
    my $type            = $attribute->{attribute_type};

    return $value unless $type;

    if ($value && ($type eq 'timestamp' || $type eq 'timestamp_or_text')) {
        try {
            # Add a Z when not already exists...ZULU time.
            my $dtvalue = DateTime::Format::ISO8601->parse_datetime(
                $value . ($value =~ /Z/ ? "" : 'Z')
            );

            ### Alright: dates are incorrect in our database, because we do not append
            ### them with a Z(ZULU), although they are in UTC.
            ### That would not be a problem, until we saved dates with a 00:00:00 time
            ### which are not UTC...so...automagic donkey detection implemented:
            if ($value =~ /00:00:00/) {
                $value = $dtvalue->strftime('%Y-%m-%d');
            } else {
                $dtvalue->set_time_zone('Europe/Amsterdam');
                $value = $dtvalue->strftime('%Y-%m-%d %H:%M:%S');
            }
        }
        catch {
            $self->log->info(
                "Tried to convert timestamp into readable format, but failed for value: "
                . $value . " / Error: $_"
            );
        };

    }
    elsif (($attribute->{bibliotheek_kenmerken_type} // '') eq 'file') {
        $value = join(", ", map { $_->{filename} } @{$value});
    }
    elsif(ref($value)) {
        $value = $attribute->{human_value};
    }

    return $value;
}


=head2 $row->csv_header

isa: ArrayRef

    my $zql     = Zaaksysteem::Search::ZQL->new('SELECT case.id,zaaktype.titel FROM case');

    my $row     = $zql->apply_to_resultset($objects)->first;

    print join(',', @{ $row->csv_header });
    # Prints: case.id,zaaktype.titel

Returns an ArrayRef with the csv header of this row, could be an alias for whatever you would
call the listing in an SQL statement before the WHERE keyword, e.g. C<< SELECT case.id, zaaktype.titel >>

=cut

has 'csv_header'             => (
    'is'            => 'ro',
    'lazy'          => 1,
    'default'       => sub {
        my $self            = shift;

        my $columns = DEFAULT_COLUMNS();

        if ($self->_zql_options->{requested_attributes} && scalar(@{ $self->_zql_options->{requested_attributes} })) {
            $columns = $self->requested_columns;
        }

        my @retval;

        for my $key (@{ $columns }) {
            push (@retval,
                {
                    label           => $self->properties->{ values }->{ $key }->{ human_label },
                    attribute_type  => $self->properties->{ values }->{ $key }->{ attribute_type },
                    attribute_name  => $key
                }
            );
        }

        return \@retval;
    },
    'isa'           => 'ArrayRef'
);


sub _initialize_zql {
    my $self                    = shift;
    $self->_zql_options(shift);
}

sub TO_JSON {
    my $self                    = shift;

    my $rv                      = {
        object_type         => $self->object_class,
        id                  => $self->id,
        object_id           => $self->object_id,
        values              => {},
    };

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub {shift->iso8601};

    for my $col (@{ $self->requested_columns }) {
        if (exists $self->properties->{values}{$col}{dynamic_class}) {
            $rv->{values}->{$col} = $self->get_object_attribute($col)->value;
        }
        else {
            if (exists $self->properties->{values}{$col}{value}) {
                $rv->{values}->{$col} = $self->properties->{values}{$col}{value};
            }
        }
    };

    if ($self->_zql_options->{describe_rows}) {
        $rv->{describe} = $self->object_describe;
    }

    # $rv->{complete}             = $self->next::method(@_) if $self->next::can;

    return $rv;
}


=head2 is_blacklisted

Wrapper around _is_blacklisted handle for blacklisted_columns

=cut

sub is_blacklisted {
    my ($self, $term) = @_;
    return $self->_is_blacklisted(sub { $_ eq $term });
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 DEFAULT_COLUMNS

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

