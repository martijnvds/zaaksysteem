package Zaaksysteem::Backend::Sysin::STUFDCR::Model;
use Moose;
use namespace::autoclean;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::StUF::0310::Common
);

use DateTime;
use MIME::Base64 qw(decode_base64);
use XML::LibXML;
use Zaaksysteem::DocumentValidator;
use Zaaksysteem::SOAP::Client;
use BTTW::Tools;
use Zaaksysteem::XML::Generator::StUF0310;

=head1 NAME

Zaaksysteem::Backend::Sysin::STUFDCR::Model - Model for StUF DCV/DCA interactions

=head1 DESCRIPTION

StUF Documentcreatie is a way to generate documents from templates using a standard
API. This module implements that API on the Zaaksysteem side, in the "DCV" role
("Documentcreatieverzoeker").

=head1 CONSTANTS

=head2 %DISPATCH_TABLE

Mapping of XML element names (namespace-prefixed) to methods of this class.

Used to determine how to parse StUF-DCR XML messages.

=cut

my %DISPATCH_TABLE = (
    "{http://schemas.xmlsoap.org/soap/envelope/}Fault" => "handle_error",
    "{http://www.kinggemeenten.nl/StUF/sector/dcr/0310}leverDocumentcreatieResultaatDu02" => "parse_dcr_resultaat",
    "{http://www.kinggemeenten.nl/StUF/sector/dcr/0310}verstrekDocumentcreatieResultaatDi01" => "parse_dcr_resultaat",
);

=head2 $SOAP_NS

=head2 $XLINK_NS

=head2 $STUF_NS

=head2 $STUF_BG_NS

=head2 $STUF_DCR_NS

XML namespace URLs.

=cut

my $SOAP_NS     = 'http://schemas.xmlsoap.org/soap/envelope/';
my $XLINK_NS    = 'http://www.w3.org/1999/xlink';
my $STUF_NS     = 'http://www.egem.nl/StUF/StUF0301';
my $STUF_BG_NS  = 'http://www.egem.nl/StUF/sector/bg/0310';
my $STUF_ZKN_NS = 'http://www.egem.nl/StUF/sector/zkn/0310';
my $STUF_DCR_NS = 'http://www.kinggemeenten.nl/StUF/sector/dcr/0310';

=head1 ATTRIBUTES

=head2 recipient_application

Recipient application name to put in the outgoing StUF message.

=cut

has recipient_application => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 soap_client

The L<Zaaksysteem::SOAP::Client> instance to use to connect to the DCA. Should
be pre-loaded with the endpoint and certificates.

=cut

has soap_client => (
    is       => 'ro',
    isa      => 'Zaaksysteem::SOAP::Client',
    required => 1,
);

=head2 xml_backend

The L<Zaaksysteem::XML::Compile> instance to use. Defaults to a new
C<Zaaksysteem::XML::Compile> instance, with the StUF-DCR bits loaded, which
should be fine for most use cases.

=cut

has xml_backend => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $xml_backend = Zaaksysteem::XML::Compile->xml_compile;
        $xml_backend->add_class('Zaaksysteem::XML::Generator::StUF0310');
        return $xml_backend;
    },
);

=head2 sender_organisation

The sending organisation

=cut

has sender_organisation => (
    is  => 'ro',
    isa => 'Str',
);

=head2 sender_administration

The sending administration

=cut

has sender_administration => (
    is  => 'ro',
    isa => 'Str',
);


=head2 sender_user

The sending user

=cut

has sender_user => (
    is  => 'ro',
    isa => 'Str',
);


=head2 sender_application

The sending application

=cut

has sender_application => (
    is  => 'ro',
    isa => 'Str',
);

=head1 METHODS

=head2 create_document_from_template

Sends a request to create a new document to the StUF DCA (Document Creating
Application) provider, based on the specified case and template.

=cut

sub create_document_from_template {
    my $self = shift;
    my %args = @_;

    my $instance = $self->xml_backend->stuf0310;
    my $job_id = Data::UUID->new->create_str();
    my $document_id = Data::UUID->new->create_str();


    my $xml_out = $instance->start_document_creation(
        'writer',
        {
            # We can't use generate_stuurgegevens here, as it requires a
            # document to take the recipient from.
            stuurgegevens => {
                user      => $args{username},
                recipient => $self->recipient_application,
                zender    => {
                    organisatie   => $self->sender_organisation,
                    gebruiker     => $self->sender_user,
                    applicatie    => $self->sender_application,
                    administratie => $self->sender_administration,
                },
                transaction_id => $args{transaction} ? $args{transaction}->id : undef,
                timestamp => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
            },

            parameters => {
                template_name => $args{template_external_name},
                job_id        => $job_id,
            },

            # typing "documentspecificatie" into the template a ton of times: nope.
            ds => {
                document_id => $document_id,
                zaak        => $self->format_case($args{case}),
                kenmerken   => $self->_build_kenmerken($args{case}),
            },
        }
    );

    $self->log->trace("Request XML: $xml_out");

    my $soap_client = $self->soap_client;

    my $soap_data = $soap_client->call(
        'http://www.kinggemeenten.nl/StUF/sector/dcr/0310/startDocumentcreatieDi02',
        $xml_out,
    );

    my $response = $soap_data->{response}->decoded_content;
    $self->log->trace("Response XML: $response");

    my ($xc, $root) = $self->parse_incoming_xml($response);

    my $result = $self->dispatch($xc, $root);

    return {
        request => $soap_data->{request}->decoded_content,
        response => $soap_data->{response}->decoded_content,
        result => $result,
    };
}

=head2 handle_error

Handle a SOAP error returned by the StUF DCR service.

=cut

sub handle_error {
    my $self = shift;
    my ($xc, $root) = @_;

    my $error = "Received an error. XML = " . $root->toString();
    $self->log->error($error);

    return {
        error => $error,
        status => 'soap-error',
    };
}

=head2 parse_dcr_resultaat

Process the C<leverDocumentcreatieResultaatDu02> or
C<verstrekDocumentcreatieResultaatDi01> message received in response to a
document creation request.

=cut

sub parse_dcr_resultaat {
    my $self = shift;
    my ($xc, $root) = @_;

    my $job_id = $xc->findvalue('DCr:parameters/DCr:jobidentificatie', $root);
    my $status = $xc->findvalue('DCr:parameters/DCr:creatieJobStatus', $root);

    my ($stuurgegevens_node) = $xc->findnodes('DCr:stuurgegevens', $root);
    my %stuurgegevens = $self->parse_stuurgegevens($xc, $stuurgegevens_node);

    my %rv = (
        status        => $status,
        job_id        => $job_id,
        stuurgegevens => \%stuurgegevens,
    );

    if ($status eq 'klaar') {
        my ($content) = $xc->findnodes('DCr:document/ZKN:inhoud', $root);

        my $filename = $content->getAttributeNS($STUF_NS, 'bestandsnaam');

        if (not defined $content) {
            throw('stufdcr/no_content', 'StUF-DCR bericht heeft status "klaar" maar geen documentinhoud');
        }

        my $tmp = File::Temp->new(UNLINK => 1);
        print $tmp decode_base64($content->textContent);
        $tmp->close();

        $rv{document} = {
            filename => $filename,
            content => $tmp,
        };
    }
    elsif (my $url = $xc->findvalue('DCr:parameters/DCr:gebruikersinteractieUrl', $root)) {
        $rv{resume_url} = $url;
    }

    return \%rv;
}

=head2 parse_incoming_xml

Parse an incoming StUF DCR XML message (reply to an outgoing SOAP call, or an
incoming asynchronous SOAP call). The return value of this can be fed to L</dispatch> to
actually process the message's contents.

=cut

sub parse_incoming_xml {
    my $self = shift;
    my ($xml) = @_;

    my $failed;
    my $doc = try {
        XML::LibXML->load_xml(string => $xml);
    } catch {
        $self->log->error("Error parsing XML: $_");
        throw("stufdcr/parse_response", "Could not parse StUF DCR response: $_");
    };

    my $xc = XML::LibXML::XPathContext->new($doc);
    $xc->registerNs('SOAP',  $SOAP_NS);
    $xc->registerNs('xlink', $XLINK_NS);
    $xc->registerNs('StUF',  $STUF_NS);
    $xc->registerNs('BG',    $STUF_BG_NS);
    $xc->registerNs('ZKN',   $STUF_ZKN_NS);
    $xc->registerNs('DCr',   $STUF_DCR_NS);

    # Get the children of the SOAP:Body tag
    my @nodes = $xc->findnodes('//SOAP:Body/*[1]');
    my $result;

    if (@nodes) {
        return ($xc, $nodes[0]);
    }

    $self->log->error(sprintf(
        "Received XML was not a SOAP message, but {%s}%s",
        $doc->documentElement->namespaceURI // '',
        $doc->documentElement->localname,
    ));
    throw("stufdcr/not_soap", "Received a non-SOAP response");
}

=head2 dispatch

Process a parsed message using the handler defined in C<%DISPATCH_TABLE>.

=cut

sub dispatch {
    my $self = shift;
    my ($xc, $root) = @_;

    my $name = sprintf('{%s}%s', $root->namespaceURI // '', $root->localname);

    if (exists $DISPATCH_TABLE{$name}) {
        my $method = $DISPATCH_TABLE{$name};
        return $self->$method($xc, $root);
    }

    throw("stufdcr/unknown_message", "Received an unknown message - element '$name' is not known");
}

sub _build_kenmerken {
    my $self = shift;
    my ($case) = @_;

    my @attributes;

    for my $attr (@{ $case->object_data->object_attributes }) {
        my $value = $attr->value;
        my $type = 'string';

        next if $attr->name !~ /^attribute\./;

        my $name = $self->_filter_kenmerk_name($attr->name);

        if (blessed($value) && $value->isa('DateTime')) {
            $value = $attr->value->clone->set_time_zone('UTC')->iso8601 . 'Z';
            $type = 'dateTime';
        }
        else {
            $value = $attr->human_value;

            if (ref($value) eq 'ARRAY') {
                $value = join(", ", @$value);
            }
        }

        push @attributes, {
            naam  => $name,
            type  => $type,
            value => $value,
        };
    }

    return \@attributes;
}

sub _filter_kenmerk_name {
    my $self = shift;
    my $name = shift;

    # Remove our prefix
    $name =~ s/^attribute\.//;

    # Make sure the name now starts with a valid "start" character
    $name =~ s/^[^a-zA-Z_]/_/;

    # Specification StUF-DCR § 5.1.3 
    # "Letters (zowel upper- als lowercase), underscore, punt, koppelteken, nummers (0-9)."
    $name =~ s/[^a-zA-Z0-9._-]//g;

    return $name;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
