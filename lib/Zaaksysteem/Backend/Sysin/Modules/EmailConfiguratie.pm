package Zaaksysteem::Backend::Sysin::Modules::EmailConfiguratie;
use Moose;

use Mail::Track;
use Try::Tiny;

use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::Email;
use IO::All;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Roles::Email
/;


=head1 INTERFACE CONSTANTS

=head2 INTERFACE_ID

=head2 INTERFACE_CONFIG_FIELDS

=head2 MODULE_SETTINGS

=cut

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

my $DEFAULT_HTML_TEMPLATE = <<'EOH';
<html>
  <head>
    <style>
      h1 { color: red; }
      div { color: green; }
      p { color: blue; }
    </style>
  </head>
  <body>
    <h1>Koptekst</h1>
    <div>{{message}}</div>
    <p>Voettekst</p>
  </body>
</html>
EOH

my $INTERFACE_ID = 'emailconfiguration';
my @INTERFACE_CONFIG_FIELDS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_user',
        type        => 'text',
        label       => 'Standaard verzendadres',
        required    => 1,
        description => '<p>E-mailadres dat Zaaksysteem standaard gebruikt als afzenderadres van e-mailberichten.</p><p>Deze waarde wordt niet gebruikt als er een e-mailsjabloon verstuurd wordt waarop afzender-gegevens ingesteld zijn.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sender_name',
        type        => 'text',
        label       => 'Verzendnaam',
        required    => 0,
        description => '<p>Naam van de verzender die in combinatie met het bovenstaande e-mailadres gebruikt wordt als afzender: &quot;Directie ZS &lt;info@zaaksysteem.nl&gt;&quot;</p><p>Deze waarde wordt niet gebruikt als er een e-mailsjabloon verstuurd wordt waarop afzender-gegevens ingesteld zijn.</p>',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_subject',
        type        => 'text',
        label       => 'Onderwerpprefix',
        required    => 1,
        description => <<'EOT',
<p>Een prefix voor het onderwerp dat aan mails wordt toegevoegd indien ze afkomstig zijn vanuit Zaaksysteem.</p>
<p>Dit wordt gebruikt om een uniek identificeerbaar onderwerp te krijgen waardoor antwoorden op vanuit een zaak gestuurde e-mail automatisch in de zaak toegevoegd kunnen worden.</p>
EOT
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_max_size',
        type        => 'text',
        label       => 'Maximale emailgrootte (MB)',
        required    => 1,
        default     => '10',
        description => 'De maximale grootte van de e-mails (in megabytes) die verstuurd mogen worden. Mails die groter zijn dan hier opgegeven worden niet verstuurd.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_use_smarthost',
        type        => 'checkbox',
        label       => 'Externe mailserver gebruiken',
        required    => 0,
        description => 'Gebruik de opgegeven (SMTP) mailserver voor uitgaande email, in plaats van de standaard-server die Zaaksysteem biedt.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_smarthost_hostname',
        type        => 'text',
        label       => 'Hostname van externe mailserver',
        required    => 0,
        data        => { placeholder => 'smtp.example.com' },
        when        => 'interface_use_smarthost === true',
        description => 'De hostname van de SMTP-server die Zaaksysteem zal gebruiken voor het verzenden van email. Zaaksysteem gebruikt hiervoor het Submission-protocol (RFC 6409).',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_smarthost_port',
        type        => 'text',
        label       => 'Poort van externe mailserver',
        required    => 0,
        default     => '587',
        when        => 'interface_use_smarthost === true',
        description => 'De poort waarop de SMTP-server bereikt kan worden',
        data        => { pattern => '^[0-9]+$', placeholder => '587' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_smarthost_username',
        type        => 'text',
        label       => 'Gebruikersnaam voor externe mailserver',
        required    => 0,
        description => 'De gebruikersnaam die Zaaksysteem zal gebruiken om aan te melden bij de externe mailserver',
        when        => 'interface_use_smarthost === true',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_smarthost_password',
        type        => 'password',
        label       => 'Wachtwoord voor externe mailserver',
        required    => 0,
        description => 'Het wachtwoord dat Zaaksysteem zal gebruiken om aan te melden bij de externe mailserver',
        when        => 'interface_use_smarthost === true',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_use_dkim',
        type        => 'checkbox',
        label       => 'DKIM-signatures genereren',
        required    => 0,
        when        => 'interface_use_smarthost !== true',
        description => 'Laat Zaaksysteem uitgaande email-headers voor <abbr title="DomainKeys Identified Mail">DKIM</abbr> toevoegen, zodat ontvangers kunnen verifi&euml;n dat de email uit een vertrouwde bron komt.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_dkim_domain',
        type        => 'text',
        label       => 'DKIM Afzenderdomein',
        required    => 0,
        when        => 'interface_use_smarthost !== true && interface_use_dkim === true',
        description => 'Afzenderdomein voor DKIM-ondertekening. Wordt opgenomen in de DKIM-signature en door de ontvangende partij gebruikt foor verificatie van deze signature.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_custom_dkim',
        type        => 'checkbox',
        label       => 'Eigen DKIM-sleutel gebruiken',
        required    => 0,
        when        => 'interface_use_smarthost !== true && interface_use_dkim === true',
        description => 'Gebruik een eigen private key en &quot;selector&quot; voor het genereren van een DKIM-signature. Als deze optie niet geactiveerd is, wordt een ingebouwde set keys gebruikt voor het genereren van DKIM-signatures.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_dkim_key',
        type        => 'file',
        label       => 'DKIM Private key',
        required    => 0,
        when        => 'interface_use_smarthost !== true && interface_use_dkim === true && interface_custom_dkim === true',
        description => '<p>Private key die gebruikt wordt om DKIM-signatures te genereren, in Base64 (PEM) formaat (&quot;-----BEGIN RSA PRIVATE KEY-----&quot;).</p>',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_dkim_selector',
        type        => 'text',
        label       => 'DKIM Selector',
        required    => 0,
        when        => 'interface_use_smarthost !== true && interface_use_dkim === true && interface_custom_dkim === true',
        description => '<p>De selector wordt samen met de domeinnaam opgeslagen in de DKIM-signature en wordt door de ontvangende partij gebruikt om de publieke sleutel op te vragen om de signature te verifi&euml;ren.</p><p>Als het veld <em>DKIM Private key</em> leeg is, wordt deze waarde niet gebruikt.</p>',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_dkim_algorithm',
        type        => 'select',
        label       => 'Algoritme voor DKIM-signature',
        data        => {
            options     => [
                {
                    value => 'rsa-sha256',
                    label => 'rsa-sha256',
                },
            ],
        },
        default     => 'rsa-sha256',
        required    => 0,
        when        => 'interface_use_smarthost !== true && interface_use_dkim === true && interface_custom_dkim === true',
        description => 'Algoritme dat gebruikt wordt om de signature te genereren. Zaaksysteem gebruikt altijd &quot;rsa-sha256&quot;.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_dkim_canonicalization',
        type        => 'select',
        label       => 'Methode voor DKIM-canonicalisatie',
        data        => {
            options     => [
                {
                    value    => 'relaxed',
                    label    => 'relaxed',
                },
            ],
        },
        default     => 'relaxed',
        required    => 0,
        when        => 'interface_use_smarthost !== true && interface_use_dkim === true && interface_custom_dkim === true',
        description => 'Methode van &quot;canonicalisatie&quot; van de email voor het genereren van de signature. Zaaksysteem gebruikt altijd de methode &quot;relaxed&quot;.'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_rich_email',
        type        => 'checkbox',
        label       => 'Opgemaakte email versturen',
        required    => 0,
        description => 'Verstuur alle email met een HTML-opmaak.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_rich_email_templates',
        type => 'multiple',
        label => 'Opmaaksjablonen',
        description => 'Lijst van HTML-opmaaksjablonen die gebruikt kunnen worden voor het verzenden van e-mail. Als er in een zaak(type) geen opmaaksjabloon ingesteld is, zal de bovenste gebruikt worden.',
        when => 'interface_rich_email === true',
        data => {
            fields => [
                {
                    name => 'label',
                    label => 'Naam',
                    type => 'text',
                    required => 1,
                },
                {
                    name => 'template',
                    label => 'Opmaaksjabloon',
                    description =>
                        "<p>HTML-sjabloon waar het bericht in verwerkt wordt.</p>" .
                        "<p>Waar <code>{{message}}</code> staat wordt de berichttekst door het systeem ingevuld.</p>" .
                        "<p>Waar <code>{{image_url}}</code> staat wordt een speciale URL voor de geconfigureerde afbeelding ingevuld.</p>" .
                        "<p><strong>Let op</strong>: e-mailprogramma's ondersteunen een beperkte subset van HTML en CSS.</p>",
                    type => 'textarea',
                    default => $DEFAULT_HTML_TEMPLATE,
                    required => 1,
                },
                {
                    name => 'image',
                    label => 'Afbeelding',
                    type => 'file',
                }
            ]
        }
    )
);

my $INTERFACE_DESCRIPTION = <<'EOD';
<p>
    Met deze koppeling kunt u de manier waarop Zaaksysteem e-mail verstuurt instellen.
</p>
<p>
    Voor meer informatie kunt u terecht op de
    <a target="_blank" href="http://wiki.zaaksysteem.nl/wiki/Koppelprofiel_Email#Uitgaande_mailconfiguratie">
        Zaaksysteem Wiki
    </a>
</p>
EOD

my %MODULE_SETTINGS = (
    name                            => $INTERFACE_ID,
    description                     => $INTERFACE_DESCRIPTION,
    label                           => 'Uitgaande mailconfiguratie',
    interface_config                => \@INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    has_attributes                  => 0,
    attribute_list                  => [],
    retry_on_error                  => 1,
    trigger_definition  => {},
);

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %MODULE_SETTINGS );
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
