package Zaaksysteem::Backend::Sysin::Modules::Roles::StUFADRAOA;

use Moose::Role;

use Zaaksysteem::StUF;
use BTTW::Tools;

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::STUFPRSNPS - STUFPRSNPS engine for StUF PRS/NPS related queries

=head1 SYNOPSIS

    # See testfile:
    # t/170-sysin-modules-stufprs.t

=head1 DESCRIPTION

STUFPRS engine for StUF PRS related queries

=head1 TRIGGERS

=head2 search($params)

=cut

sub stuf_create_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $stuf_params         = $object->get_params_for_adr;

    my $org_rs              = $record
                            ->result_source
                            ->schema
                            ->resultset('BagWoonplaats');


    my ($mutations, $preview, $entry);

    try {
        ($mutations, $preview, $entry) = $org_rs->bag_create_or_update(
            $stuf_params,
            {
                "return_entry" => 'BagNummeraanduiding'
            }
        );
    } catch {
        throw(
            'stuf/adraoa/failure',
            'Error: ' . (ref($_) ? $_->message : $_)
        );
    };

    $self->process_stash->{row}->{mutations} = $mutations;

    $record->preview_string(
        $preview->to_string
    );

    return $entry;
}

=head2 UPDATE SUBJECT

=head2 $module->stuf_update_entry($record, $object)

Updates a PRS entry in our database

=cut


sub stuf_update_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $entry               = $self->get_entry_from_subscription(@_, 'BagNummeraanduiding');

    $self->_update_stuf_entry(
        @_, $entry
    );


    return $entry;
}

sub _update_stuf_entry {
    my $self                                = shift;
    my ($record, $object, $entry)           = @_;
    my ($mutations, $preview);

    my $stuf_params         = $object->get_params_for_adr;

    try {
        ($mutations, $preview, $entry)    = $entry
                            ->result_source
                            ->schema
                            ->resultset('BagWoonplaats')
                            ->bag_create_or_update(
                                $stuf_params,
                                {
                                    "return_entry" => 'BagNummeraanduiding'
                                }
                            );
    } catch {
        throw(
            'stuf/adraoa/failure',
            'Error: ' . (ref($_) ? $_->message : $_)
        );
    };

    $self->process_stash->{row}->{mutations} = $mutations;

    $record->preview_string(
        $preview->to_string
    );

    return $entry;
}

=head2 DELETE SUBJECT

=head2 stuf_delete_entry

=cut

sub stuf_delete_entry {
    my $self                                = shift;
    my ($record, $object, $subscription)    = @_;


    my $entry                               = $self->get_entry_from_subscription(
                                                $record,
                                                $object,
                                                'BagNummeraanduiding',
                                                $subscription
                                            );

    $record->preview_string(
        $entry->to_string
    );

    $entry->einddatum(DateTime->now->strftime('%Y%m%d'));
    $entry->update;

    $self->_remove_subscription_from_entry($record, $object, $entry);


    return $entry;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 stuf_create_entry

TODO: Fix the POD

=cut

