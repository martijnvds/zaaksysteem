package Zaaksysteem::Backend::Sysin::Modules::Postex;

use Zaaksysteem::Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with 'Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams';

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Postex - Postex integration module

=head1 DESCRIPTION

This sysin module declares the Zaaksysteem interface for interactions with
the L<www.postex.com|http://www.postex.com> REST API.

=cut

use URI;
use Zaaksysteem::Constants::Users qw(:all);
use Zaaksysteem::External::Postex;
use Zaaksysteem::ZAPI::Error;
use Zaaksysteem::ZAPI::Form::Field;

=head1 CONSTANTS

=head2 INTERFACE_CONFIG_FIELDS

Collection of L<Zaaksysteem::ZAPI::Form> objects used in building the UI for
this module.

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_endpoint',
        type => 'text',
        label => 'Postex API Endpoint',
        description => 'API Endpoint URL van de Postex service',
        required => 1,
        data => { placeholder => 'https://api.demo.postex.com/rest/data/v1/generation/raw/12345' }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_key',
        type => 'text',
        label => 'Postex API Key',
        description => 'API Key van de Postex service',
        required => 1,
    ),
];

=head2 INTERFACE_DESCRIPTION

Module description / pointers for the UI.

=cut

use constant INTERFACE_DESCRIPTION => <<'EOD';
<p>
    <a href="https://www.postex.com/" hreflang="nl-NL" target="_blank">
    Postex</a> is een service waarmee documenten verzonden kunnen worden.
</p>

EOD

=head2 INTERFACE_TRIGGERS

Set of triggers available in the module.

=cut

use constant INTERFACE_TRIGGERS => [
    { method => 'receive_event', update => 0, public => 1 },
    {
        method            => 'send_message',
        update            => 1,
        api               => 1,
        api_is            => 'ro',
        api_allowed_users => REGULAR,
    },
];

=head2 MODULE_SETTINGS

Sysin module parameters, used during instantiation to configure the module.

=cut

use constant MODULE_SETTINGS => {
    name                          => 'postex',
    label                         => 'Postex',
    description                   => INTERFACE_DESCRIPTION,
    interface_config              => INTERFACE_CONFIG_FIELDS,
    is_multiple                   => 0,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    trigger_definition            => {
        map { $_->{method} => $_ } @{ INTERFACE_TRIGGERS() }
    },
    sensitive_config_fields => ['api_key'],
};

=head1 METHODS

=cut

around BUILDARGS => sub {
    my ($orig, $class) = @_;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

=head2 receive_event

    $module->receive_event({ parameters => 'value' }, $interface);

Receive event trigger of the interface, processes the JSON send to the
interface endpoint

=cut

sub receive_event {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process(
        {
            processor_params => {
                processor => '_process_receive_event',
                %$params,
            },
            direction               => 'incoming',
            external_transaction_id => $params->{packageId} // 'unknown',
            input_data =>
                JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );

    if ($transaction->error_count) {
        return Zaaksysteem::ZAPI::Error->new(
            type => 'general/error',
            messages => [
                "Error in transaction: " . $transaction->id,
                $transaction->preview_data->[0]{preview_string},
            ]
        );
    }
    return [
        {
            transaction_id => $transaction->id,
            message        => $transaction->preview_data->[0]{preview_string},
        }
    ];
}


=head2 send_message

    $module->send_message({ parameters => 'value' }, $interface);

Send message trigger of the interface, processes the JSON send to the
interface endpoint

=cut

sub send_message {
    my ($self, $params, $interface) = @_;

    my $req_params = $params->{request_params};
    my $json_params = JSON::XS->new->utf8(0)->canonical->pretty->encode(
        $req_params
    );

    my $transaction = $interface->process(
        {
            processor_params => {
                processor => '_process_send_message',
                %$req_params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => $json_params,
        }
    );

    my $message = 'Bericht aangeleverd aan Postex';
    if (! $transaction->success_count) {
        $message = 'Fout tijdens versturen naar Postex';
    }

    return {
        type      => 'result',
        reference => $transaction->uuid,
        preview   => $message,
        instance  => {
            ok      => $transaction->success_count ? \1 : \0,
            message => $message,
            type    => 'sysin/postex/send_message',
        },
    };
}
=head1 PRIVATE METHODS

=head2 _load_values_into_form_object

Hooks into interface initialization routine to inject default values for
specified config fields.

=cut

around _load_values_into_form_object => sub {
    my $orig = shift;
    my $self = shift;
    my $opts = $_[1]; # don't clobber @_ further than original args

    my $form = $self->$orig(@_);
    my $interface = $opts->{ entry };

    $form->load_values({
        interface_callback_endpoint => sprintf(
            '%ssysin/interface/%d/trigger/receive_event',
            $opts->{ base_url },
            $interface->id,
        )
    });

    return $form;
};

=head2 _process_send_message

Send document to Postex
Triggers case logging mechanism for case/send_postex

=cut

sub _process_send_message {
    my ($self, $record) = @_;
    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);

    my $schema = $interface->result_source->schema;

    my $case_id = delete $params->{case_id};
    my $result;
    try {

        my $case = $schema->resultset('Zaak')->get_open_case_by_id($case_id);
        $result = $interface->model->send_case_document(
            case => $case,
            %$params,
        );

    }
    catch {
        $record->is_error(1);
        throw(
            'sysin/modules/postex/communication_problem',
            "Bericht kon niet worden aangeleverd: $_",
            {
                fatal => 1
            }
        );
    }
    finally {

        my @msg = ("Postex bericht voor zaak $case_id");
        if ($record->is_error) {
            push(@msg, "is niet correct verwerkt")
        }
        else {
            push(@msg, "is succesvol afgeleverd");
        }

        $transaction->preview_data({ preview_string => join(" ", @msg) });
        $record->preview_string(join(" ", @msg));
    };

    $record->output(qq{Zaak ID: $case_id
==============
Subject: $params->{subject}
Bericht: $params->{body}});

    return $transaction;
}

sub _get_model {
    my ($self, $opts) = @_;

    my $config = $opts->{interface}->get_interface_config;

    return Zaaksysteem::External::Postex->new(
        secret   => $config->{api_key},
        endpoint => URI->new($config->{api_endpoint}),
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
