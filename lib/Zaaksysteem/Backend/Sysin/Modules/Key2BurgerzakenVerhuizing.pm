package Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenVerhuizing;

use Moose;

use XML::LibXML::Simple;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::SOAP;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_OPTIONS];
use Zaaksysteem::Object::ConstantTables qw/MUNICIPALITY_TABLE/;

use BTTW::Tools;
use List::Util qw(first);

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenVerhuizing - Key2Burgerzaken Verhuizingen interface API

=head1 SYNOPSIS

    my $transaction = $interface->process_trigger(
        'request_verhuizing',
        {
            case        => $CASE_ROW,
        }
    );


=head1 DESCRIPTION

This is the Key2Burgerzaken module, which will handle all transactions between zaaksysteem
and a Centric Key2Burgerzaken application regarding BinnenVerhuizingen.

=head1 CONSTANTS

=head2 INTERFACE_ID

Static interface type string (C<key2burgerzakenverhuizing>)

=cut

use constant INTERFACE_ID => 'key2burgerzakenverhuizing';

=head2 INTERFACE_CONFIG_FIELDS

Defines the form fields used to view/edit interface instances of this type.

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(

        name        => 'interface_endpoint',
        type        => 'text',
        label       => 'URL Key2Burgerzaken',
        required    => 1,
        data        => { pattern => '^https:\/\/.+' },
        description => <<'EOD',
Vanuit Centric zal er een zogenoemde SOAP url worden opgegeven. Deze URL
gebruikt zaaksysteem.nl om mee te "praten". Voorbeeld:
https://HOST.TLD/BOKoppelvlakPIV/PROD/Versie2_0/Service.asmx
EOD

    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_request_type',
        type        => 'select',
        label       => 'Verhuizingsoort',
        required    => 1,
        default     => 'BinnenVerhuisaanvraagRequest',
        data        => {
            options => [
                {
                    label => 'Binnengemeentelijk',
                    value => 'BinnenVerhuisaanvraagRequest'
                },
                {
                    label => 'Buitengemeentelijk',
                    value => 'BuitenVerhuisaanvraagRequest'
                }
            ]
        },
        description => <<'EOD'
Geef hier aan welke soort verhuizing door deze koppeling verwerkt dient te
worden
EOD
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_action_on_processed',
        type        => 'select',
        label       => 'Actie op verwerkte aanvraag',
        required    => 1,
        default     => 'nop',
        data        => {
            options     => [
                {
                    value    => 'nop',
                    label    => 'Geen actie',
                },
                {
                    value    => 'close',
                    label    => 'Zaak afhandelen',
                }
            ],
        },
        description => <<'EOD'
Wanneer de status "aanvraag verwerkt" wordt doorgegeven, welke actie moet er
plaatsvinden op de aangemaakte zaak? Afhandelen betekent dat een zaak wordt
afgehandeld en het resultaat wordt gezet op onderstaand zaakresultaat.
EOD
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_result_on_processed',
        type        => 'select',
        label       => 'Zaakresultaat op verwerkte aanvraag',
        required    => 1,
        data        => {
            options     => [
                map {
                    { value => $_, label => ucfirst($_) }
                } @{ ZAAKSYSTEEM_OPTIONS->{ RESULTAATTYPEN } }
            ]
        },
        description => <<'EOD'
Wanneer de status "aanvraag verwerkt" wordt doorgegeven, wordt onderstaand
resultaat gezet. Zorg ervoor dat u het resultaat overneemt zoals u deze heeft
opgegeven in uw zaaktype.
EOD
    ),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_use_bag',
        type        => 'checkbox',
        label       => 'Gebruik BAG voor adres verhuizing',
        description => <<'EOD'
Hier kunt u aangeven of de BAG gebruikt wordt voor het bepalend van het nieuwe
adres
EOD
    )
];

=head2 MODULE_SETTINGS

Defines the base arguments for instances of this module type.

=cut

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Key2Burgerzaken (Verhuizing)',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => ['text'],
    module_type                     => [INTERFACE_ID, 'soapapi'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 1,
    is_casetype_interface           => 1,
    has_attributes                  => 1,
    attribute_list                  => [
        {
            external_name   => 'Verhuisdatum',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'IndicatieInwonend',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Meeverhuizers.BurgerservicenummerMeeverhuizer',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Meeverhuizers.OmschrijvingAangifte',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Statuscode',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Statusomschrijving',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'IndicatieMeldenAanBurger',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'BackOfficeID',
            attribute_type  => 'magic_string'
        },
        {
            external_name   => 'Adresgegevens.Postcode',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.Huisnummer',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.Huisletter',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.Huisaanduiding',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.Huistoevoeging',
            attribute_type  => 'magic_string',
            no_bag          => 1,
        },
        {
            external_name   => 'Adresgegevens.FunctieAdres',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'Contactgegevens.Emailadres',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'Contactgegevens.TelefoonnummerPrive',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'Contactgegevens.TelefoonnummerMobiel',
            attribute_type  => 'magic_string',
        },
        {
            external_name   => 'BagAdres',
            attribute_type  => 'magic_string',
            use_bag         => 1,
        },
    ],
    trigger_definition  => {
        request_verhuizing   => {
            method  => 'request_verhuizing',
            #update  => 1,
        },
    },
    test_interface                  => 1,
    test_definition                 => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests       => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'key2buz_test_connection',
                description => 'Test verbinding naar profiel URL'
            }
        ],
    },
};

use constant MAP_STATUS_PREFIX_TO_TYPE => {
    'BVH'   => 'BinnenVerhuisaanvraagRequest',
    'BUV'   => 'BuitenVerhuisaanvraagRequest'
};

use constant VALID_SOAP_ACTIONS => [
    'urn:Centric/ITS/GBA/v1.0/StatusUpdate/StatusUpdate'
];

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 ATTRIBUTES

=head2 request_response_type_map

Declares a list of request types to their respective expected response types.

=cut

has request_response_type_map => (
    is => 'ro',
    isa => 'HashRef[Str]',
    traits => [qw[Hash]],
    builder => '_build_response_type_map',
    handles => {
        response_type_for => 'get'
    }
);

sub _build_response_type_map {
    return {
        UittrekselaanvraagRequest    => 'UittrekselaanvraagResponse',
        NaamgebruikaanvraagRequest   => 'NaamgebruikaanvraagResponse',
        BinnenVerhuisaanvraagRequest => 'BinnenVerhuisaanvraagResponse',
        BuitenVerhuisaanvraagRequest => 'BuitenVerhuisaanvraagResponse',
        EmigreatieaanvraagRequest    => 'EmigratieaanvraagResponse',
        GeheimhoudingaanvraagRequest => 'GeheimhoudingaanvraagResponse'
    };
}

=head1 TRIGGERS

=head2 request_verhuizing

Triggers a "Verhuizing" to Key2Burgerzaken. Creates a transaction in our database,
and mutation information in mutation tables.

    my $txn = $interface->process_trigger(request_verhuizing => {
        case => $my_case
    });

=head3 Parameters

=over 4

=item case_id

ID of case to use for sending information to Key2Burgerzaken.

=item case

Instead of an id, a L<Zaaksysteem::Schema::Zaak> instance can be used for
sending information to Key2Burgerzaken.

=back

=cut

Params::Profile->register_profile(
    method      => 'request_verhuizing',
    profile     => {
        required        => ['case'],
        optional        => ['case_id'],
        defaults    => {
            case        => sub {
                my ($dfv)       = @_;

                if ($dfv->get_filtered_data->{'case_id'}) {
                    return $dfv->get_input_data
                                ->{schema}
                                ->resultset('Zaak')
                                ->find($dfv->get_filtered_data->{'case_id'});
                }

                return;
            },
        }
    }
);

sub request_verhuizing {
    my $self                        = shift;
    my $raw_params                  = shift || {};
    my $interface                   = shift;

    my $params                      = assert_profile(
        {
            %{ $raw_params },
            schema  => $interface->result_source->schema
        }
    )->valid;

    my $xml_params = $self->build_xml_data($interface, $params->{ case });

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor   => '_process_verhuizing',
                xml_params  => $xml_params,
                case_id     => $params->{ case }->id,
            },
            external_transaction_id => 'unknown',
            input_data              => sprintf(
                "Key2Burgerzaken verhuis aanvraag via zaak %d.\n%s",
                $params->{ case }->id,
                dump_terse($xml_params)
            )
        }
    );

    return $transaction;
}

=head1 METHODS

=head2 is_valid_soap_api

Returns true when this interface is a valid interface for this soap action.

=cut

define_profile is_valid_soap_api => (
    required    => {
        xml         => 'Str',
        soapaction  => 'Str',
        interface   => 'Zaaksysteem::Backend::Sysin::Interface::Component',
    },
);

sub is_valid_soap_api {
    my $self        = shift;
    my $params      = assert_profile({ @_ })->valid;
    my $interface   = $params->{interface};

    return unless grep { $_ eq $params->{soapaction} } @{ VALID_SOAP_ACTIONS() };

    my $xml_hash    = $self->_get_status_response_body($params->{xml});

    my $status_code = $xml_hash->{ Status }{ Statuscode };
    my $prefix      = substr($status_code, 0, 3);
    my $module_type = MAP_STATUS_PREFIX_TO_TYPE->{$prefix};

    throw('api/soap/key2burgerzakenverhuizing/unsupported_status', 'Unsupported status code: ' . $status_code) unless $module_type;

    if ($interface->get_interface_config->{request_type} eq $module_type) {
        return $interface;
    }

    $self->log->trace(
        sprintf(
            "No active interface configured for module: '%s' / interface: %d",
            $module_type,
            $interface->id
        )
    );

    return;
}

=head2 build_xml_data

Aggregates data for the request.

Returns a nested hashref to be used for XML generation.

=cut

sig build_xml_data => 'Zaaksysteem::Schema::Interface, Zaaksysteem::Schema::Zaak';

sub build_xml_data {
    my $self = shift;
    my $interface = shift;
    my $case = shift;

    my $params = $self->_collect_params_from_case($interface, $case);

    $params->{ Zaakgegevens } = {
        ZaakID => sprintf('Zaak#%s', $case->id),
        DatumAanvraag => $case->registratiedatum->ymd
    };

    return $params;
}

=head2 stuf_test_connection

When called, this method will attempt to connect to the configured SOAP
endpoint to verify it can be reached. Will fail if no endpoint value has been
set or the host is unreachable.

=cut

sub key2buz_test_connection {
    my $self                        = shift;
    my $interface                   = shift;

    my $endpoint = $interface->jpath('$.endpoint');

    unless ($endpoint) {
        throw(
            'sysin/modules/key2burgerzaken/no_endpoint_defined',
            'De koppeling vereist een URL (endpoint) naar een Key2Burgerzaken server'
        );
    }

    $self->test_host_port($endpoint);

    return 1;
}

=head2 get_attribute_mapping

=cut

sub get_attribute_mapping {
    my $self        = shift;
    my ($interface) = @_;

    my $config      = $self->next::method(@_);

    my $check_key = $interface->jpath('$.use_bag') ? 'no_bag' : 'use_bag';

    $config->{ attributes } = [
        grep { !exists($_->{ $check_key }) || !$_->{ $check_key } }
             @{ $config->{ attributes } }
    ];

    return $config;
}

=head2 _process_verhuizing($transaction_record)

Return value: $STRING_OUTPUT

Do not use this function directly, instead let it play by trigger:
L<Zaaksysteem::Backend::Sysin::Modules::Key2BurgerzakenVerhuizing#request_verhuizing>.

Sends a C<BinnenVerhuisaanvraag> to a Centric Key2Burgerzaken application. Received
information will populate a case with the the options which can be configured
in the attribute map of this integration module.

B<Options>

=over 4

=item BackOfficeID

The backoffice ID received from Centric

=item Statuscode

The status code received from centric, e.g. C<BVH07>

=item StatusOmschrijving

A human readable description of the status code received from centric

=back

=cut

sub _process_verhuizing {
    my $self = shift;
    my $record = shift;
    my $mutations;

    my $transaction = $self->process_stash->{ transaction };
    my $params = $transaction->get_processor_params;
    my $interface = $transaction->interface;

    ### Dispatch
    my $soap = Zaaksysteem::SOAP->new(
        wsdl_file       => 'key2burgerzaken/koppelvlak_v2.wsdl',
        soap_action     => 'urn:Centric/ITS/GBA/v1.0/BOAanvraag/Aanvraag',
        soap_service    => 'Service',
        soap_port       => 'ServiceSoap',
        soap_endpoint   => $interface->jpath('$.endpoint')
    );

    my $request_type = $interface->jpath('$.request_type');

    my ($answer, $trace) = @_;
    try {
        ($answer, $trace) = $soap->dispatch(Aanvraag => {
            $request_type => $params->{ xml_params }
        });
    } catch {
        if (blessed($_) && $_->isa('BTTW::Exception::Base')) {
            if ($_->type eq 'stuf/soap/dispatch_error') {
                $transaction->input_data($record->input($_->object->request->content));
            }
        }

        throw('sysin/modules/key2burgerzakenverhuizing/invalid_response', "$_");
    };

    $transaction->input_data($record->input($trace->request->content));

    my $response_type = $self->response_type_for($request_type);

    if ($answer) {
        $self->_populate_case($record, {
            response_type => $response_type,
            xml_hash => $answer->{ AanvraagResult }{ $response_type },
            case_id => $params->{ case_id },
        });
    }

    $record->output($trace->response->content);
}

=head2 _strip_xmlns_from_keys

Strip the an XML namespace from the keys of a hash (recursively).

This is done because L<XML::Simple>'s "NsExpand" option leads to very verbose hash key names.

=cut

sub _strip_xmlns_from_keys {
    my ($xmlns, $hash) = @_;

    my %rv;
    for my $key (keys %$hash) {
        (my $filtered_key = $key) =~ s/^\{$xmlns\}//;

        if (ref($hash->{$key}) eq 'HASH') {
            $rv{$filtered_key} = _strip_xmlns_from_keys($xmlns, $hash->{$key});
        }
        elsif (ref($hash->{$key}) eq 'ARRAY') {
            $rv{$filtered_key} = [
                map { _strip_xmlns_from_keys($xmlns, $_) } @{ $hash->{$key} }
            ];
        }
        else {
            $rv{$filtered_key} = $hash->{$key};
        }
    }

    return \%rv;
}

=head2 _process_row

The default processor for incoming StatusUpdateResponses from Centric. It will use
the Statuscode and Statusomschrijving to update a case. See C<_process_verhuizing>
for more information about status info.

=cut

sub _process_row {
    my $self                = shift;
    my ($record, $object)   = shift;

    my $transaction         = $record->transaction_id;
    $transaction->direction('incoming');
    $transaction->update;

    my $xml_hash = $self->_get_status_response_body($transaction->input_data);

    my $case_id  = $xml_hash->{ Zaakgegevens }{ ZaakID };

    # Strip optional Zaak# prefix, not all clients serialize the
    # case_id as such... *sigh*
    ($case_id) = $case_id =~ m[(\d+)];

    unless ($case_id) {
        throw('sysin/key2burgerzakenverhuizing/case_id_not_found', sprintf(
            'Could not decode ZaakID field, got "%s"', $xml_hash->{ Zaakgegevens }{ ZaakID }
        ));
    }

    $self->_populate_case(
        $record,
        {
            xml_hash    => $xml_hash,
            case_id     => $case_id
        }
    );

    $record->output($record->input);
}

sub _get_status_response_body {
    my ($self, $xml) = @_;

    my $xs                  = XML::LibXML::Simple->new(NsExpand => 1);
    my $data                = $xs->XMLin(
        $xml,
        ForceArray => 0,
        KeyAttr    => '{urn:Centric/ITS/GBA/v1.0/StatusUpdate}statusUpdateRequest',
    );

    my $body = $data->{ '{http://www.w3.org/2003/05/soap-envelope}Body' } || $data->{ '{http://schemas.xmlsoap.org/soap/envelope/}Body' };

    unless (ref $body eq 'HASH') {
        throw('sysin/key2burgerzakenverhuizing/soap_body_not_found', sprintf(
            'Could not dereference SOAP body element (possibly namespace naming convention issues)'
        ));
    }

    my $request = $body->{ '{urn:Centric/ITS/GBA/v1.0/StatusUpdate}statusUpdateRequest' };

    return _strip_xmlns_from_keys("urn:Centric/ITS/GBA/v1.0/BOAanvraag", $request);
}

=head2 HELPERS

=head2 _collect_params_from_case($interface, $case)

Collects information for a BinnenVerhuisRequest from the given case, with this hash
the C<_process_verhuizing> function is able to construct a valid XML message.

=cut

sub _collect_params_from_case {
    my $self                        = shift;
    my ($interface, $case)          = @_;

    my $xml_params                  = {};

    $self->_collect_params_from_case_requestor(@_, $xml_params);

    my $attributes                  = {};
    for my $attribute (@{ $interface->get_interface_config->{attribute_mapping} }) {
        my $external_name           = $attribute->{external_name};
        my $internal_name           = $attribute->{internal_name};

        my $value = '';
        if ($internal_name) {
            $value = $case->value_by_magic_string(
                {
                    magic_string    => $internal_name->{searchable_object_id},
                    plain           => 1
                }
            );
        }

        if ($external_name eq 'Verhuisdatum') {
            $value      =~ s/^(\d{2})-(\d{2})-(\d{4})$/$3-$2-$1/;
        }

        if (
            $interface->get_interface_config->{use_bag}
        ) {
            if ($external_name eq 'BagAdres') {
                my $bag_model = $interface->result_source->schema->bag_model;

                my ($type, $id) = split('-', $value);

                my $bag_object = $bag_model->get($type => $id);
                next if not $bag_object;

                my $address = $bag_object->nummeraanduiding;
                next if not $address;

                $attributes->{'Adresgegevens.Postcode'}         = $address->{postcode};
                $attributes->{'Adresgegevens.Huisnummer'}       = $address->{huisnummer};
                $attributes->{'Adresgegevens.Huisletter'}       = $address->{huisletter};
                $attributes->{'Adresgegevens.Huistoevoeging'}   = $address->{huisnummertoevoeging};

                next;
            # When Bag is selected, skip over adresgegevens data (except functieadres)
            } elsif ($external_name =~ /^Adresgegevens\./ && $external_name ne 'Adresgegevens.FunctieAdres') {
                next;
            }
        } else {
            if ($external_name eq 'BagAdres') {
                next;
            }
        }

        # get kenmerk value by magic string - functionality not ready yet
        $attributes->{ $external_name } = $value;
    }

    ### Params collected, create hash
    $xml_params->{Aanvraaggegevens}->{$_} = $attributes->{$_}
        for qw/
            Verhuisdatum
            IndicatieInwonend
        /;

    if ($interface->get_interface_config->{request_type} eq 'BuitenVerhuisaanvraagRequest') {
        $xml_params->{'Aanvraaggegevens'}->{'HuidigeAdresgegevens'} = {
            FunctieAdres    => uc($case->aanvrager_object->functie_adres),
            Straatnaam      => $case->aanvrager_object->straatnaam,
            Postcode        => $case->aanvrager_object->postcode,
            Huisnummer      => $case->aanvrager_object->huisnummer,
            ($case->aanvrager_object->huisletter            ? (Huisletter      => $case->aanvrager_object->huisletter) : ()),
            ($case->aanvrager_object->huisnummertoevoeging  ? (Huistoevoeging  => $case->aanvrager_object->huisnummertoevoeging) : ()),
        };

        my $municipality = $self->_assert_municipality_code($case->aanvrager_object);
        $xml_params->{'Aanvraaggegevens'}->{'GemeentecodeAanvrager'} = $municipality->{dutch_code};
    }

    for my $adresitem (grep(/^Adresgegevens\./, keys %{ $attributes })) {
        my $looseprefix = $adresitem;
        $looseprefix  =~ s/^Adresgegevens\.//;

        next unless $attributes->{$adresitem};

        my $value       = '';
        $value          = $attributes->{$adresitem} if $attributes->{$adresitem};

        ### Fix postcode, make sure it is [1-9][0-9]{3}[A-Z]{2}
        if ($looseprefix eq 'Postcode') {
            $value          =~ s/\s//;
            $value          = uc($value);
        }

        $xml_params->{Aanvraaggegevens}->{'Adresgegevens'}->{$looseprefix} = $value;
    }

    for my $adresitem (grep(/^Contactgegevens\./, keys %{ $attributes })) {
        my $looseprefix = $adresitem;
        $looseprefix  =~ s/^Contactgegevens\.//;

        next unless $attributes->{$adresitem};

        my $value       = '';
        $value          = $attributes->{$adresitem} if $attributes->{$adresitem};

        ### Fix postcode, make sure it is [1-9][0-9]{3}[A-Z]{2}
        if ($looseprefix eq 'Postcode') {
            $value          =~ s/\s//;
            $value          = uc($value);
        }

        $xml_params->{Contactgegevens}->{$looseprefix} = $value;
    }

    if ($attributes->{'Meeverhuizers.BurgerservicenummerMeeverhuizer'}) {
        my @bsns = split(
            ',',
            $attributes->{'Meeverhuizers.BurgerservicenummerMeeverhuizer'},
        );

        ### Field contains list of magic strings. Replace it with value from case
        for (my $i = 0; $i < scalar(@bsns); $i++) {
            if ($bsns[$i] =~ /\[\[/) {
                my $string = $bsns[$i];
                $string    =~ s/[\[\]]//g;

                $bsns[$i]  = $case->value_by_magic_string(
                    {
                        magic_string    => $string,
                        plain           => 1
                    }
                );
            }
        }

        if ($interface->get_interface_config->{request_type} eq 'BinnenVerhuisaanvraagRequest') {
            $xml_params->{Aanvraaggegevens}->{Meeverhuizers} = {
                'Meeverhuizer' => [],
            };

            for my $bsn (@bsns) {
                next unless $bsn =~ /^\d+$/;

                push(
                    @{ $xml_params->{Aanvraaggegevens}->{Meeverhuizers}->{Meeverhuizer} },
                    {
                        BurgerservicenummerMeeverhuizer     => $bsn,
                    }
                );
            }
        }
        else {
            my @omschrijvingen = split(
                ',',
                $attributes->{'Meeverhuizers.OmschrijvingAangifte'},
            );

            my @meeverhuizers;

            for (my $i = 0; $i <= $#bsns ; $i++) {
                my $bsn = $bsns[$i] or next;
                my $oms = $omschrijvingen[$i] or next;

                push @meeverhuizers, {
                    BurgerservicenummerMeeverhuizer => $bsn,
                    OmschrijvingAangifte            => $oms,
                };
            }

            $xml_params->{Aanvraaggegevens}{BuitengemeentelijkeMeeverhuizers} = {
                'Meeverhuizer' => \@meeverhuizers,
            };
        }
    }

    return $xml_params;
}

=head2 _collect_params_from_case_requestor($interface, $case, $xml_params)

Fills C<$xml_params> with information about the case_requestor of this case

=cut

sub _collect_params_from_case_requestor {
    my $self                        = shift;
    my ($interface, $case, $xml_params)  = @_;

    $xml_params->{Aanvraaggegevens}->{BurgerservicenummerAanvrager}  =
        $case->systeemkenmerk(
            'aanvrager_burgerservicenummer'
        );
}

=head2 _assert_municipality_code

Check if the case requestor has a municipality code based on the C<gemeente_code>

=cut

sub _assert_municipality_code {
    my ($self, $aanvrager) = @_;

    if (my $code = $aanvrager->gemeente_code) {
        my $municipality = first { int($_->{dutch_code}) ==  int($code) } @{ MUNICIPALITY_TABLE() };
        return $municipality if $municipality;
        throw("key2burgerzaken/municipality_code/not_found", "Municipality code '$code' cannot be found");

    }
    elsif (my $woonplaats = $aanvrager->woonplaats) {
        my $municipality = first { lc($_->{label}) eq lc($woonplaats) } @{ MUNICIPALITY_TABLE() };
        return $municipality if $municipality;
        throw("key2burgerzaken/municipality/not_found", "Municipality '$woonplaats' cannot be found");
    }
    throw("key2burgerzaken/municipality_code/missing", "Unable to determine municipality code for case requestor");

}

=head2 _populate_case(\%options)

Populates a case with information from a xml_hash which is a C<HashRef> representation
of the given XML message from Centric.

B<Options>

=over 4

=item xml_hash

A C<HashRef> representation of the xml message from Centric

=item case_id

The case_id to work on

=back


B<Populates>

=over 4

=item BackOfficeID

The backoffice ID received from Centric

=item Statuscode

The status code received from centric, e.g. C<BVH07>

=item StatusOmschrijving

A human readable description of the status code received from centric

=back

=cut

Params::Profile->register_profile(
    method      => '_populate_case',
    profile     => {
        required        => [qw/xml_hash case_id/],
    }
);

sub _populate_case {
    my $self                        = shift;
    my $record                      = shift;
    my $options                     = assert_profile(shift || {})->valid;

    my $xml_hash                    = $options->{xml_hash};

    my $case                        = $record
                                    ->result_source
                                    ->schema
                                    ->resultset('Zaak')
                                    ->find($options->{case_id});

    my $interface_attrs             = $record
                                    ->transaction_id
                                    ->interface
                                    ->get_interface_config->{attribute_mapping};

    my $mapped_interface_attrs      = {
        map {
            $_->{external_name} => $_->{internal_name}->{searchable_object_id}
        } grep { ref($_->{internal_name}) } @{ $interface_attrs }
    };

    ### Collect all magic strings
    my $zt_kenmerken   = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => [ values %{ $mapped_interface_attrs }]
        },
        {
            join        => 'bibliotheek_kenmerken_id',
        }
    );



    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                        ->new(
                            table       => 'Zaak',
                            table_id    => $case->id,
                            create      => 1
                        );

    my $kenmerken_map   = {};
    while (my $zt_kenmerk = $zt_kenmerken->next) {
        $kenmerken_map->{ $zt_kenmerk->bibliotheek_kenmerken_id->magic_string } =
            $zt_kenmerk->get_column('bibliotheek_kenmerken_id');
    }

    my $update_fields   = {};
    if ($xml_hash->{Backofficegegevens}) {

        if ($mapped_interface_attrs->{BackOfficeID}) {
            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{BackOfficeID} }
            } = $xml_hash->{Backofficegegevens}->{BackOfficeID};

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{BackOfficeID},
                    new_value   => $xml_hash->{Backofficegegevens}->{BackOfficeID},
                }
            );
        }
    }

    if ($xml_hash->{Status}) {
        if ($mapped_interface_attrs->{Statuscode}) {
            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{Statuscode} }
            } = $xml_hash->{Status}->{Statuscode};

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{Statuscode},
                    new_value   => $xml_hash->{Status}->{Statuscode},
                }
            );
        }

        if ($mapped_interface_attrs->{Statusomschrijving}) {
            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{Statusomschrijving} }
            } = $xml_hash->{Status}->{StatusOmschrijving};

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{Statusomschrijving},
                    new_value   => $xml_hash->{Status}->{StatusOmschrijving},
                }
            );
        }

        if (
            $mapped_interface_attrs->{IndicatieMeldenAanBurger}
        ) {
            my $new_value = (
                $xml_hash->{Status}->{IndicatieMeldenAanBurger} eq 'true'
                    ? 'Melden aan burger'
                    : 'Niet melden aan burger'
            );

            $update_fields->{
                $kenmerken_map->{ $mapped_interface_attrs->{IndicatieMeldenAanBurger} }
            } = $new_value;

            $mutation_record->add_mutation(
                {
                    column      => 'kenmerk.' . $mapped_interface_attrs->{IndicatieMeldenAanBurger},
                    new_value   => $new_value,
                }
            );
        }
    }

    $case->zaak_kenmerken->update_fields(
        {
            new_values => $update_fields,
            zaak       => $case,
        }
    );
    $case->touch();

    $record->preview_string('Zaak: ' . $case->id . ': ' . $case->onderwerp);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    my $interface = $record->transaction_id->interface;

    # Nothing to do if no action configured.
    return unless $interface->jpath('$.action_on_processed') eq 'close';

    my $status_code = $xml_hash->{ Status }{ Statuscode };

    # BVH07 and BUV08 are 'request succesfully processed' status indicators
    unless ($status_code eq 'BVH07' || $status_code eq 'BUV08') {
        $self->log->error(sprintf(
            'Configured to close case %d, but received non-success status ("%s")',
            $case->id,
            $status_code
        ));

        return;
    }

    try {
        $case->resultaat($interface->jpath('$.result_on_processed'));
        $case->set_gesloten(DateTime->now());
    } catch {
        $self->log->error('Failed to close case: %s', $_);

        return;
    };

    $case->logging->trigger('case/early_settle', {
        component => 'zaak',
        data => {
            case_id => $case->id,
            case_result => $case->resultaat,
            reason => sprintf(
                'Gesloten door koppeling "%s", transactie ID: %s',
                $self->interface->name,
                $self->process_stash->{transaction}->id,
            )
        }
    });

    return;
}

1;

__END__

=head1 EXAMPLES

=head2 BinnenVerhuisaanvraagRequest

    <?xml version="1.0" encoding="UTF-8"?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
        <SOAP-ENV:Body>
            <tns:AanvraagRequest xmlns:tns="urn:Centric/ITS/GBA/v1.0/BOAanvraag" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <tns:BinnenVerhuisaanvraagRequest>
                    <tns:Zaakgegevens>
                        <tns:ZaakID>90</tns:ZaakID>
                        <tns:DatumAanvraag>2013-08-10</tns:DatumAanvraag>
                    </tns:Zaakgegevens>
                    <tns:Aanvraaggegevens>
                        <tns:BurgerservicenummerAanvrager>987654321</tns:BurgerservicenummerAanvrager>
                        <tns:Verhuisdatum>1970-01-01</tns:Verhuisdatum>
                        <tns:IndicatieInwonend>0</tns:IndicatieInwonend>
                        <tns:Adresgegevens>
                            <tns:Postcode>1051JL</tns:Postcode>
                            <tns:Huisnummer>7</tns:Huisnummer>
                            <tns:Huistoevoeging>521</tns:Huistoevoeging>
                        </tns:Adresgegevens>
                    </tns:Aanvraaggegevens>
                    <tns:Contactgegevens>
                        <tns:Emailadres>michiel@example.com</tns:Emailadres>
                        <tns:TelefoonnummerPrive>0612345678</tns:TelefoonnummerPrive>
                        <tns:TelefoonnummerMobiel>0612345678</tns:TelefoonnummerMobiel>
                    </tns:Contactgegevens>
                </tns:BinnenVerhuisaanvraagRequest>
            </tns:AanvraagRequest>
        </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>

=head2 BinnenVerhuisaanvraagResponse

    <?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soap:Body>
            <AanvraagResponse xmlns="urn:Centric/ITS/GBA/v1.0/BOAanvraag">
                <BinnenVerhuisaanvraagResponse>
                    <Zaakgegevens>
                        <ZaakID>90</ZaakID>
                    </Zaakgegevens>
                    <Backofficegegevens>
                        <ApplicatieID>PIV</ApplicatieID>
                        <BackOfficeID>5583</BackOfficeID>
                    </Backofficegegevens>
                    <Status>
                        <Datum>2013-08-13</Datum>
                        <Statuscode>BVH01</Statuscode>
                        <StatusOmschrijving>De aanvraag is ter beoordeling aangeboden aan een ambtenaar.</StatusOmschrijving>
                        <IndicatieMeldenAanBurger>true</IndicatieMeldenAanBurger>
                    </Status>
                </BinnenVerhuisaanvraagResponse>
            </AanvraagResponse>
        </soap:Body>
    </soap:Envelope>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
