package Zaaksysteem::DB::Component::Logging::Sysin::Interface::Deactivate;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Commponent::Logging::Sysin::Interface::Update - Handles
C</sysin/interface/deactivate> event logs

=head1 METHODS

=head2 onderwerp

Overrides default event subject line generator.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        'Koppeling "%s" gedeactiveerd',
        $self->data->{ interface_name }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
