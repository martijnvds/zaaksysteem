package Zaaksysteem::DB::Component::Logging::Case::Note::Create;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use Data::Dumper;

sub onderwerp {
    'Notitie toegevoegd'
}

sub event_category { 'note' }

sub creator_from_betrokkene {
    my $self = shift;

    return '' unless($self->betrokkene_id);

    my $betrokkene  = $self
                    ->result_source
                    ->schema
                    ->betrokkene_model
                    ->get({}, $self->betrokkene_id);

    return 'Onbekend' unless $betrokkene;

    return $betrokkene->naam;
}

around 'creator' => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig(@_) || $self->creator_from_betrokkene;
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 creator_from_betrokkene

TODO: Fix the POD

=cut

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

