package Zaaksysteem::DB::Component::Logging::Case::Update::Allocation;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use JSON;

sub onderwerp {
    my $self = shift;

    if ($self->data->{subject}) {
        return sprintf(
            'Toewijzing gewijzigd naar %s (%s/%s)"',
            $self->data->{subject},
            $self->data->{ou_name},
            $self->data->{role_name}
        );
    }
    else {
        return sprintf(
            'Toewijzing gewijzigd naar afdeling "%s" en rol "%s"',
            $self->data->{ ou_name },
            $self->data->{ role_name }
        );
    }
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    if ($data->{comment}) {
        $data->{content} = $data->{comment};
    }
    $data->{expanded} = JSON::false;
    return $data;
};

=head2 event_category

Type type of event category for this logging item.

=cut

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

