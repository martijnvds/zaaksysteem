package Zaaksysteem::DB::Component::Logging::Case::Update::Result;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    if($self->data->{ old_result }) {
        return sprintf(
            'Zaakresultaat gewijzigd van %s naar %s',
            $self->data->{ old_result },
            $self->pretty_label(),
        );
    } else {
        return sprintf(
            'Zaakresultaat ingesteld op %s',
            $self->pretty_label()
        );
    }
}

sub event_category { 'case-mutation'; }

sub pretty_label {
    my $self = shift;

    my $result_name  = $self->data->{ result } // '';
    my $result_label = $self->data->{ result_label };

    my $pretty_label = $result_name . ( defined $result_label ? " ($result_label)" : '' );

    return $pretty_label;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

