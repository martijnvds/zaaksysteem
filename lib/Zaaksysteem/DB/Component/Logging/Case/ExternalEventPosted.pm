package Zaaksysteem::DB::Component::Logging::Case::ExternalEventPosted;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::ExternalEventPosted

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $data = shift->data;

    return sprintf("Notificatie van gebeurtenis '%s' verstuurd naar %s",
        $data->{ event_name },
        $data->{ interface_name },
    );
}

=head2 event_category

defines the category, used by the frontend in the timeline, to display the right
icons etc.

=cut

sub event_category { 'case-mutation' }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
