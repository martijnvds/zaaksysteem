package Zaaksysteem::DB::Component::Adres;
use Zaaksysteem::Moose;
extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::DB::Component::Adres - Database component for "adres" rows

=head1 METHODS

=head2 update

When an address is updates, poke all referring C<natuurlijk_persoon> entries to
update their search_term.

=cut

sub insert {
    my $self   = shift;

    $self->_update_bag_params($self->{_column_data});

    return $self->next::method(@_);
}

sub _update_bag_params {
    my ($self, $params) = @_;

    if (uc($params->{functie_adres}) eq 'B') {
        $params->{bag_id}       = undef;
        $params->{geo_lat_long} = undef;
        return;
    }

    return unless $params->{bag_id};

    my $schema = $self->result_source->schema;
    my $bag_object = $schema->bag_model->get(
        nummeraanduiding => $params->{bag_id}
    );

    $params->{geo_lat_long} = $bag_object ? $bag_object->coordinates : undef;
}

sub update {
    my $self   = shift;
    my $params = shift;

    $self->_update_bag_params($params);

    my $rv = $self->next::method($params, @_);

    for my $np ($self->natuurlijk_persoons->search) {
        $np->update();
    }

    return $rv;
}

=head2 TO_JSON

Returns all data required to create a JSON representation of this address.

Excludes the linked "natuurlijk_persoon_id", to prevent loops.

=cut

sub TO_JSON {
    my $self = shift;

    # Filter out natuurlijk_persoon, so we don't create an infinite loop
    # (natuurlijk_persoon -> adres -> natuurlijk_persoon -> adres, etc.)
    my @serializable
        = grep { $_ ne 'natuurlijk_persoon_id' } $self->result_source->columns;

    return { map { $_ => $self->get_column($_) } @serializable };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
