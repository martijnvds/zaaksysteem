package Zaaksysteem::API::v1::Serializer::Encoder::JSON;

use Moose;
use JSON::XS qw();
use Carp;
use BTTW::Tools;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Encoder::JSON - JSON encoding implementation

=head1 SYNOPSIS

    my $encoder = Zaaksysteem::API::v1::Serializer::Encoder::JSON->new;

    my $data = $encoder->encode({ ... });

=head1 DESCRIPTION

This encoder implements a simple JSON producer.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastrucure.

=head1 ATTRIBUTES

=head2 encoder

This attribute holds a reference to an instance of L<JSON>. The default
instance is configured to produce pretty-printed B<UTF-8 byte> strings

=cut

has encoder => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub {
        return JSON::XS->new->utf8->allow_blessed(1);
    }
);

=head1 METHODS

=head2 grok

Implements the interface required by L<Zaaksysteem::API::v1::Serializer>. It
returns true for the formatstrings C<JSON> and C<application/json>
(case-insensitive).

=cut

sub grok {
    my $fmt = lc pop;

    return grep { $_ eq $fmt } qw[
        json
        application/json
    ];
}

=head2 encode

This method calls the configured L</encoder> instance with the data provided.

=cut

sub encode {
    my ($self, $serializer, $data) = @_;

    my $rv;
    try {
        $rv = $self->encoder->encode($data);
    }
    catch {
        $self->log->fatal(Carp::longmess("$_"));
        die $_;
    };
    return $rv;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
