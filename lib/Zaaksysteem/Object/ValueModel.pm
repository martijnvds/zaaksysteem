package Zaaksysteem::Object::ValueModel;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Object::ValueModel - Object ValueType metamodel

=head1 DESCRIPTION

This package wraps around the known Object ValueType packages, and creates an
index for each specified type.

This package functions as a singleton, and should be used using package
functions, not methods.

=head1 SYNOPSIS

    package My::Fancy::Package;

    use Zaaksysteem::Object::ValueModel;

    # Build a new Zaaksysteem::Object::ValueType::String instance with
    # the content 'abc'
    my $value = Zaaksysteem::Object::ValueModel->new_value(string => 'abc');

=cut

use Moose::Util::TypeConstraints qw[find_type_constraint];

use Zaaksysteem::Object::Value;

use Zaaksysteem::Object::ValueType::Boolean;
use Zaaksysteem::Object::ValueType::Datetime;
use Zaaksysteem::Object::ValueType::ObjectRef;
use Zaaksysteem::Object::ValueType::String;
use Zaaksysteem::Object::ValueType::Text;

use Zaaksysteem::Types qw[ValueType];

# Singleton container
our $_INSTANCE;

=head1 ATTRIBUTES

=head2 value_types

Contains the initialized L<Zaaksysteem::Interface::ValueType> instances.

=cut

has value_types => (
    is => 'rw',
    isa => find_type_constraint('HashRef')->parameterize(ValueType),
    traits => [qw[Hash]],
    builder => '_build_value_types'
);

=head1 FUNCTIONS

=head2 preload

If called, this method will ensure a instance of this package has been created
as a singleton, and is initialized.

=cut

sub preload {
    my $class = shift;

    return defined $class->get_instance;
}

=head2 new_value

Construct a new L<Zaaksysteem::Object::Value> object of a known
L<Zaaksysteem::Interface::ValueType>

    my $value = Zaaksysteem::Object::ValueModel->new_value(boolean => 1);

=cut

sub new_value {
    my $self = shift->get_instance;
    my $type = shift;
    my $value = shift;

    return Zaaksysteem::Object::Value->new(
        type => $self->value_types->{ $type },
        value => $value
    );
}

=head2 get_instance

Returns an instance of this class. Will set the C<$_INSTANCE> package variable
with the instance if the variable is undefined.

    my $model = Zaaksysteem::Object::ValueModel->get_instance;

=cut

sub get_instance {
    my $this = shift;

    return $this if blessed $this;

    unless (blessed $this::_INSTANCE) {
        $this::_INSTANCE = $this->new;
    }

    return $this::_INSTANCE;
}

sub _build_value_types {
    my %map = map {
        $_->name => $_->new
    } map {
        sprintf('Zaaksysteem::Object::ValueType::%s', $_)
    } qw[
        Boolean
        Datetime
        ObjectRef
        String
        Text
    ];

    return \%map;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
