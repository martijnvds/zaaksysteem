=head1 OBJECT EXPORTS (overdrachtsbestanden)

Export files are POSIX "tar" files that can be used for archiving purposes, and
contain a JSON representation of the object(s) and all attached files.

An export tar file with two objects, one with two attached files and another
with one attached file, contains the following file/directory structure:

    [-] archive.tar
     |  meta.json
     |  README.txt
     ` [-] data
        |  uuid-of-object-1.json
        | [-] uuid-of-object-1
        |  |  uuid-of-file-1.blob
        |  |  uuid-of-file-1.json
        |  |  uuid-of-file-2.blob
        |  `  uuid-of-file-2.json
        |  uuid-of-object-2.json
        ` [-] uuid-of-object-2
           |  uuid-of-file-1.blob
           `  uuid-of-file-1.json

JSON files can be opened/read with any text editor that supports UNIX line
endings and unicode character points.

=head2 meta.json

This file contains some metadata about the export, in JSON format.
For version 1, it looks like:

    {
        // File format revision
        "version": 1,

        // Note, this is UTC
        "time": "2014-06-26T11:29:23Z",

        // [1]
        "export_type": "query",

        // [1]
        "query": "SELECT {} FROM case",

        // Version of Zaaksysteem used to create the file
        "zs_version": "3.18.0",

        // Environment the export is made from (think "gemeente")
        "environment": "mintlab",

        // Username of the user doing the export
        "user": "g.bruiker"
    }

[1] The value of "export_type" can be either "single" for a single-object
export, or "query" for an export of multiple objects, based on a ZQL query. If
"export_type" is "query", the query used will be in the "query" property. If
"export_type" is "single", the "query" key will not be present.

=head2 Object data

The object data is stored in JSON files (one per object) in the "data/"
subdirectory. These files are named "uuid.json", where "uuid" is the object's
UUID.

These JSON files contain the complete object (all attributes), but exclude
attachments.

=head2 Documents (object attachments)

If an object has file attachments, those are found in another subdirectory,
"data/uuid" (again, with "uuid" replaced by the uuid of the object the
attachments belong to).

For each attachment, this directory, contains two files. The files
with the ".blob" extension contain the attachments' raw data, the corresponding
".json" files contain associated metadata, like the file path, file name, etc.

To open/check an attachment in the appropriate program, you can change the
extension of the ".blob" file to the extension specified in the JSON file.

=head1 Example file

An example export file, made on the 17th of July, 2014 could have the following contents:

    [-] zs-2014-07-17T09-52-10-b233b9.tar
     |  meta.json
     |  README.txt
     ` [-] data
        |  72150a05-3642-4863-a5a7-77a08016efb8.json
        ` [-] 72150a05-3642-4863-a5a7-77a08016efb8
           |  f43d6dd3-d221-4898-b1b9-5ad6bad92d9d.blob
           `  f43d6dd3-d221-4898-b1b9-5ad6bad92d9d.json

Translated to something more "human-readable", using the metadata from the JSON files, it could look something like:

    [-] your-search-query.tar
     |  search-metadata.json
     |  README.txt
     ` [-] data
        |  case-3242.json
        ` [-] case-3242
           |  document-1.pdf
           `  document-1.json

Files aren't named like this in the export file, because they could conflict
(more than one document with the same name can be attached to a single case).

=head1 ABOUT ARCHIVING GOVERNMENT DATA

Zaaksysteem.nl is used for archiving government data which means that different
laws and regulations are applicable on this export file. To reconstruct the
situation of export, the following additional information may be needed:

=over 4

=item Actieve Selectielijst

Gemeentelijke Selectielijst 1996, vastgesteld door de VNG

=item Einddatum Selectielijst

Onbekend (planning 01-01-2017)

=item Locatie data

Equinix Datacenter Amsterdam, serviced by Cyso Hosting B.V.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
