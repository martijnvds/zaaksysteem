package Zaaksysteem::Object::Attribute;

use Moose;
use namespace::autoclean;

with qw(MooseX::Log::Log4perl);

use BTTW::Tools;
use List::Util qw(any);
use Moose::Util qw/ensure_all_roles/;
use Moose::Util::TypeConstraints qw[enum];
use Zaaksysteem::Object::Constants qw[ATTRIBUTE_TYPES];

=head1 NAME

Zaaksysteem::Object::Attribute - Defines a attribute, compatible with object JSON

=head1 ATTRIBUTES

=head2 name

The name for this attribute

=cut

has name => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

=head2 attribute_type
Defines the type of this attribute. Valid values documented in
L<Zaaksysteem::Object::Constants/ATTRIBUTE_TYPES>.

=cut

has attribute_type => (
    is => 'ro',
    isa => enum([ keys %{ ATTRIBUTE_TYPES() } ]),
    required => 1
);

=head2 label

This attribute defines a human readable label for the object attribute.

=cut

has label => (
    is => 'rw',
    isa => 'Str'
);

=head2 bwcompat_name

This attribute holds a string or ARRAY of strings that are considered
the old "magic string" equivalent of the attribute. Remember "aanvrager_naam"?

=cut

has bwcompat_name => (
    is       => 'ro',
    required => 0,
    isa      => 'Str | ArrayRef[Str]',
);

=head2 bwcompat_type

This attribute should be set to the 'bibliotheek kenmerk value type'.

=cut

has bwcompat_type => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_bwcompat_type'
);

=head2 _json_data

Defines the contents of TO_JSON

=cut

has _json_data => (
    is            => 'ro',
    lazy          => 1,
    builder       => '_build_json_data',
    clearer       => '_clear_json_data',
);

sub _build_json_data {
    my $self = shift;

    my %json = (
        name => $self->name,
        attribute_type => $self->attribute_type,
        human_label => $self->label,
    );

    if ($self->has_bwcompat_type) {
        $json{ bibliotheek_kenmerken_type } = $self->bwcompat_type;
    }

    if ($self->has_value) {
        my $value = $self->value;
        my $human_value;

        # CodeRef values are special in that their evaluation hits an
        # external system
        if (defined $value && ref $value eq 'CODE') {
            $value = undef;
        } else {
            $human_value = $self->human_value;
        }

        $json{ value } = $value;
        $json{ human_value } = $human_value;
    }

    if ($self->has_pending_change) {
        $json{ pending_change } = $self->pending_change;
    }

    return \%json;
}

=head2 parent_object

Object the attribute is attached to. A weak reference.

=cut

has parent_object => (
    is       => 'rw',
    weak_ref => 1,
);

=head2 dynamic_class

The name of the role implementing the value of this attribute, if this is a
"dynamic" attribute.

If this is set, the specified role (prefixed with
L<Zaaksysteem::Object::AttributeRole>) will be applied to the
attribute.

=cut

has dynamic_class => (
    is  => 'ro',
    isa => 'Maybe[Str]',
);

=head2 object_row

(Optional) reference to the DBIx::Class Component for this value

=cut

has object_row => (
    is => 'rw',
    isa => 'DBIx::Class::Row',
    predicate => 'has_object_row'
);

=head2 value

Machine representational value for this attribute. For a human readable value,
see below.

=cut

has value => (
    is => 'rw',
    clearer => '_clear_value',
    trigger => sub {
        my $self = shift;

        $self->_clear_human_value;
        $self->_clear_json_data;
    }
);

=head2 grouping

This stores a 'grouping' incidation of the attribute and is set for the
purpose of unsplitting Zaaksysteem's split-brain.

If multiple attribute instances share the same group name, the property names
will be aggregated and will together produce a singular 'v0' style value for
the group.

This helps in cases such as the C<case.casetype.name> attribute, which we
want to aggregate on the C<casetype>, forming a map of casetype fields
(C<name> in this case), stored as a singular property.

=cut

has grouping => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_grouping',
);

=head2 property_name

Used along with L</grouping> to set the state of the attribute's 'property'
name (as used in the C<case_property> table).

=cut

has property_name => (
    is => 'rw',
    isa => 'Str',
    predicate => 'has_property_name',
);

=head2 pending_change

Allows the constructing code of this attribute to set the 'pending change'
state, implying there is an user interaction waiting for the attribute.

=cut

has pending_change => (
    is => 'rw',
    isa => 'HashRef',
    predicate => 'has_pending_change',
);

# On value call, if not a set-call, and not already initialized, and we're
# a systeemkenmerk with an object_row to use, implement our own default sub
# that does not interfere with has_value for non-systeemkenmerk attributes.
before value => sub {
    my $self = shift;

    # Ignore setters
    return if scalar @_;

    # We want to do this initialization only once
    return if $self->value_built_from_systeemkenmerk;
    $self->value_built_from_systeemkenmerk(1);

    if ($self->is_systeemkenmerk && $self->has_object_row) {
        $self->meta->find_attribute_by_name('value')->set_raw_value(
            $self,
            $self->build_systeemkenmerk_value($self->object_row)
        );
    }
};

=head2 human_value

The human readable value for this attribute, in case of the bag identifier, this could be
C<woonplaats>

=cut

has human_value => (
    is => 'rw',
    lazy => 1,
    builder => '_build_human_value',
    clearer => '_clear_human_value',
);

# Default human_value builder should be the plain value
sub _build_human_value {
    my $self = shift;
    my $value = shift // $self->value;
    my $ref = ref $value;

    return '' unless defined $value;
    return "$value" unless $ref;

    if (eval { $value->isa('Zaaksysteem::Types::MappedString') }) {
        return $value->mapped;
    }

    if ($ref eq 'ARRAY') {
        # Undefined entries in a list are a thing once people change the
        # attribute values in ZTB and the value has been selected by an open
        # case. This causes undef to be in the list and becomes a deep
        # recursion due to the nature of this sub (see $value // $self->value
        $self->log->info(
            "_build_human_value has an undefined value in " . $self->name
        ) if any { !defined $_ } @{$value};

        return join(', ',
            map  { $self->_build_human_value($_) } grep { defined $_ } @{$value}
        );
    }
    elsif ($ref eq 'CODE') {
        return $value->();
    }

    return '';
}

=head2 format

Format string that can be used by roles to format the value for human consumption.

=cut

has format => (
    is => 'rw',
    isa => 'Defined'
);

=head2 is_filter

This boolean meta-attribute defines if the attributes can be used in a
L<ZQL|Zaaksysteem::Search::ZQL> query.

Defaults to C<false>.

=cut

has is_filter => (
    is => 'ro',
    isa => 'Bool',
    default => 0
);

=head2 systeemkenmerk_reference

This attribute can be supplied with a code reference that gets the value of
the attribute using the old pass-zaak-and-pray style.

    my $attr = Zaaksysteem::Object::Attribute->new(
        ...,
        systeemkenmerk_reference => sub {
            my $zaak = shift;

            return $zaak->retrieve_some_string
        }
    );

=cut

has systeemkenmerk_reference => (
    is  => 'ro',
    isa => 'CodeRef',
    predicate => 'is_systeemkenmerk',
    traits => [qw[Code]],
    handles => {
        build_systeemkenmerk_value => 'execute'
    }
);

=head2 value_built_from_systeemkenmerk

This attribute holds a state flag for systeemkenmerk value building.

=cut

has value_built_from_systeemkenmerk => (
    is => 'rw',
    isa => 'Bool',
    default => 0
);

=head2 unique

This attribute is an indication to the model that objects with this
object-attribute should have a unique constraint enforced on the value.

=cut

has unique => (
    is  => 'ro',
    isa => 'Bool',
    default => 0
);

=head2 is_sensitive

Mark an item as sensitive for GDPR requirements

=cut

has is_sensitive => (
    is  => 'ro',
    isa => 'Bool',
    default => 0
);

=head1 METHODS

=head2 get_bwcompat_name

This accessor method will return a list of bw-compat attribute aliasses.

    my @aliases = $attribute->get_bwcompat_name;

=cut

sub get_bwcompat_name {
    my $self = shift;

    return unless $self->bwcompat_name;
    return ref $self->bwcompat_name ? @{ $self->bwcompat_name } : $self->bwcompat_name;
}

=head2 BUILD

=cut

sub BUILD {
    my $self = shift;

    my @roles = @{ ATTRIBUTE_TYPES->{ $self->attribute_type } };

    if(scalar @roles) {
        ensure_all_roles($self, map { sprintf('Zaaksysteem::Object::Attribute::%s', $_) } @roles);
    }

    if ($self->dynamic_class) {
        ensure_all_roles(
            $self,
            "Zaaksysteem::Object::AttributeRole::" . $self->dynamic_class
        );
    }

    $self->_prepare_attribute_object;
}

=head2 TO_JSON

Stringifies this object to JSON when called with the JSON module

=cut

sub TO_JSON {
    my $self = shift;

    $self->_clear_json_data;
    return $self->_json_data;
}

=head2 vectorize_value

This generic method implements some logic required for clean free-form text
vectors. It flattens values if embedded in an ARRAYREF, lower-cases the string
and filters anything that is not a word-like character, whitespace, or a
literal '.' or '@'.

=cut

sub vectorize_value {
    my $self = shift;

    my $str = $self->human_value;

    return unless defined $str;

    if(ref $str eq 'ARRAY') {
        $str = join ' ', @{ $str }
    }

    $str = lc($str);

    $str =~ s/[^\w\s\.\@]//g;

    return $str;
}

=head2 index_value

Returns the "index" value for this attribute (for hstore/search queries).

Returns an array of index values if the attribute is multi-valued.

=cut

sub index_value {
    my $self = shift;
    my $value = $self->value;

    if (ref($value) eq 'ARRAY') {
        return [
            map { $self->_build_index_value($_) }
                @{$value}
        ];
    }

    return $self->_build_index_value($value);
}

=head2 has_value

Replaces L</value>'s default C<predicate>.

=cut

sub has_value {
    my $self = shift;
    my $attr = $self->meta->find_attribute_by_name('value');

    return $attr->has_value($self) || ($self->is_systeemkenmerk && $self->has_object_row);
}

=head2 init_system_value

Initialize the value based on the provided case.

=cut

sig init_system_value => "Zaaksysteem::Schema::Zaak";

sub init_system_value {
    my ($self, $case) = @_;
    return unless $self->is_systeemkenmerk;

    $self->value($self->systeemkenmerk_reference->($case));
    $self->_clear_human_value;
    $self->human_value;
    return;
}

=head2 as_hash_entry

Return a hash key + value for this attribute.

=cut

sub as_hash_entry {
    my $self = shift;

    return ($self->name, $self);
}

=head1 INTERNAL METHODS

=head2 _prepare_attribute_object

Called directly after applying the C<Attribute::*> roles. Here you could write your own
BUILD like methods

=cut

sub _prepare_attribute_object { }

=head2 _build_index_value

This transforms a single value to an indexable value.

This is called once for each value of the attribute.

=cut

sub _build_index_value {
    my $self = shift;
    my $value = shift;

    return $value unless blessed $value && $value->isa('Zaaksysteem::Types::MappedString');

    return $value->original;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
