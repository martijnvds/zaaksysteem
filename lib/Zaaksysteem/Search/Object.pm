package Zaaksysteem::Search::Object;
use Moose;

=head1 NAME

Zaaksysteem::Search::Object - A search model for objects with ZQL

=head1 SYNOPSIS

    use Zaaksysteem::Search::Object;

    my $search = Zaaksysteem::Search::Object->new(
        user => $c->user,
        schema => $c->model('DB')->schema,
        object_model => $c->model('Object'),
        query => 'SELECT {}  FROM case',
    );

    # Be aware that results can either be:
    # 1) An arrayref
    # 2) A DBIx::Class resultset
    my $results = $search->search;

    # Your code here

=head1 DESCRIPTION

A model that was build to move code from fat controllers and making them a
little bit slimmer. Its main aim was to have code that lived in
L<Zaaksysteem::Controller::API::Object> to be reused for using with the
L<Zaaksysteem::Export::Model>.

Perhaps more work is needed to make it really spiffy, abeit the first iteration
seems to do its job.

=cut

use Carp qw(croak confess);
use BTTW::Tools;
use Zaaksysteem::Search::ZQL;

has user => (
    is => 'ro',
    isa => 'Zaaksysteem::Schema::Subject',
    required => 1,
);

has schema => (
    is => 'ro',
    isa => 'Zaaksysteem::Schema',
    required => 1,
);

has object_model => (
    is => 'ro',
    isa => 'Defined',
    required => 1,
);

has query => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_query',
);

has zql => (
    is => 'ro',
    isa => 'Zaaksysteem::Search::ZQL',
    lazy => 1,
    builder => '_build_zql_parser',
    init_arg => undef,
);

has base_rs => (
    is => 'ro',
    isa => 'Defined',
    predicate => 'has_base_rs',
);

has resultset => (
    is => 'ro',
    isa => 'Defined',
    lazy => 1,
    builder => '_build_resultset',
    init_arg => undef,
);

has case_rs => (
    is => 'ro',
    isa => 'Defined',
    lazy => 1,
    builder => '_build_case_rs',
    init_arg => undef,
);

has library_attribute_rs => (
    is => 'ro',
    isa => 'Defined',
    lazy => 1,
    builder => '_build_library_attribute_rs',
    init_arg => undef,
);

sub _build_case_rs {
    my $self = shift;
    return $self->schema->resultset('Zaak');
}

sub _build_library_attribute_rs {
    my $self = shift;
    return $self->schema->resultset('BibliotheekKenmerken');
}

sub _build_resultset {
    my $self = shift;
    return $self->zql->apply_to_resultset(
        $self->has_base_rs ? $self->base_rs : $self->object_model->acl_rs
    );
}

sub _build_zql_parser {
    my $self = shift;
    return Zaaksysteem::Search::ZQL->new($self->query)
}

has is_intake => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    builder => '_build_is_intake',
);

sub _build_is_intake {
    my $self = shift;
    return $self->zql->cmd->can('is_intake') && $self->zql->cmd->is_intake;
}


sub search {
    my $self = shift;

    if ($self->zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Count')) {
        return $self->search_count();
    }
    elsif ($self->zql->cmd->isa('Zaaksysteem::Search::ZQL::Command::Select')) {
        return $self->search_select();
    }
    croak("Unable to search on ZQL cmd " . $self->zql->cmd);
}


sub search_documents {
    my $self = shift;

    my $object_rs = $self->object_model->zql_search($self->query)->rs;

    $object_rs = $object_rs->search_rs(undef,
        { select => 'object_id', group_by => 'object_id' });

    return $self->case_rs->search_rs(
        {
            'me.id' =>
                { -in => $object_rs->get_column('object_id')->as_query },
        },
    );
}

sub assert_zql_command_for {
    my ($self, $type) = @_;

    return 1 if $self->check_zql_command_for($type);

    my $zql = $self->zql->cmd->from->value;
    croak("zql from '$zql' does not support '$type'");
}

sub check_zql_command_for {
    my ($self, $type) = @_;
    return $type eq $self->zql->cmd->from->value;
}

sub search_intake {
    my $self = shift;
    my $resultset = shift // $self->resultset;

    return $resultset unless $self->check_zql_command_for('case');

    return $resultset unless $self->is_intake;

    my @seeers = ();
    my $mine = { 'me.status' => [ 'new' ] };
    $mine->{'me.behandelaar_gm_id'} = $self->user->uidnumber;
    push @seeers, $mine;

    my $ou_id = $self->user->primary_group->id;

    my @role_ids = map { $_->id } @{ $self->user->roles };

    push @seeers, {
        '-and' => [
            { '-and' => [
                    { '-or'       => { route_role => \@role_ids } },
                    { '-or'       => [
                            { 'route_ou'  => $ou_id },
                            { 'route_ou'  => undef },
                        ],
                    }
                ],
            },
            { 'behandelaar' => undef },
        ],
    };

    # Special case: divver, they can see all zaken without a complete role.
    if ($self->user->has_legacy_permission('zaak_route_default')) {
        push @seeers, { 'me.route_role' => undef };
    };

    my %where = (
        '-or'                 => \@seeers,
        'me.deleted'          => undef,
        'me.registratiedatum' => { '<' => \'NOW()' },
        'me.status'           => 'new',
    );

    my $in_query = $self->case_rs->search(\%where, { column => 'me.id' })
        ->get_column('id')->as_query;

    return $resultset->search_rs(
        {
            'me.object_class' => 'case',
            'me.object_id'    => { -in => $in_query }
        }
    );
}

has blacklisting => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy => 1,
    builder => '_build_blacklisted_attributes',
);

# TODO: Move to different function in bibliotheek-kenmerken-resultset
sub _build_blacklisted_attributes {
    my $self = shift;

    my $rs     = $self->library_attribute_rs;
    my $search = "@> '{\"sensitive_field\":\"on\"}'::jsonb";
    $rs = $rs->search_rs(
        { deleted => undef, 'properties::jsonb' => \"$search" },
        { select  => [qw(id magic_string)] }
    );

    my %blacklist;

    my $prefix = 'attribute.';
    while (my $found = $rs->next) {
        $blacklist{$prefix . $found->magic_string} = $found;
    }

    # Also hide keys found in zs::attributes
    my @attrs = grep { $_->is_sensitive } Zaaksysteem::Attributes->predefined_case_attributes();
    foreach (@attrs) {
        $blacklist{$_->name} = $_;
    }
    return \%blacklist;
}


has is_search_distinct => (
    is => 'ro',
    isa => 'Bool',
    lazy => 1,
    builder => '_is_search_distinct',
);

sub _is_search_distinct {
    my $self = shift;
    return $self->zql->cmd->distinct ? 1 : 0;
}

sub search_distinct {
    my ($self, $resultset) = @_;
    $resultset //= $self->resultset;

    if(!$self->is_search_distinct) {
        croak("Cannot search distinct on something that isn't distinct");
    }

    my $opts = $self->zql->cmd->dbixify_opts;
    my @results;
    my $tr = sub { my $val = shift; $val =~ s/\$/./g; return $val };
    while (my $object = $resultset->next) {
        push @results, {
            count => int($object->get_column('count')),
            map { $tr->($_) => $object->get_column($_) } @{ $opts->{ as } }
        };
    }
    return \@results;
}

sub search_select {
    my $self = shift;

    # Make sure intake is special
    my $zql = $self->zql;
    my $rs = $self->search_intake;

    if($self->check_zql_command_for('case')) {
        $self->blacklisting;
        return $rs unless $self->is_search_distinct;
        return $self->search_distinct;
    }
    else {
        return Zaaksysteem::Object::Iterator->new(
            rs       => $rs,
            inflator => sub { $self->object_model->inflate_from_row(shift) }
        );
    }

}

sub search_count {
    my $self = shift;
}



__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
