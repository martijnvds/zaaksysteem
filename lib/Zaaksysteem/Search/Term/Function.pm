package Zaaksysteem::Search::Term::Function;

use Moose;

extends 'Zaaksysteem::Search::Term';

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Search::Term::Function - "Function call" search term

=head1 DESCRIPTION

This represents a function called in a ZQL search
(C<SELECT {} FROM foo WHERE function() = "value">). It implements the
L<Zaaksysteem::Search::Term> role.

It contains a dispatch table, split out by object class.

=cut

# Mapping between "urgency" and the percentage of (allowed) time that has
# passed for a case.
my %URGENTIE = (
    normal => {min => undef, max => 79},
    medium => {min => 80,    max => 89},
    high   => {min => 90,    max => 99},
    late   => {min => 100,   max => undef},
);

my %DISPATCH_BY_CLASS = (
    case => {
        urgency => sub {
            my $completion   = "CAST(index_hstore->'case.date_of_completion' AS timestamp)";
            my $registration = "CAST(index_hstore->'case.date_of_registration' AS timestamp)";
            # +1day required because these timestamps are actually datestamps
            my $target       = "(CAST(index_hstore->'case.date_target' AS timestamp) + '1 day')";

            return qq{
                CASE
                    WHEN
                        ABS(ROUND(
                            100 * (
                                date_part('epoch', COALESCE($completion, NOW()) - $registration)
                                / GREATEST(date_part('epoch', $target - $registration), 1)
                            )
                        )) <= $URGENTIE{normal}{max}
                    THEN 'normal'
                    WHEN
                        ABS(ROUND(
                            100 * (
                                date_part('epoch', COALESCE($completion, NOW()) - $registration)
                                / GREATEST(date_part('epoch', $target - $registration), 1)
                            )
                        )) BETWEEN $URGENTIE{medium}{min} AND $URGENTIE{medium}{max}
                    THEN 'medium'
                    WHEN
                        ABS(ROUND(
                            100 * (
                                date_part('epoch', COALESCE($completion, NOW()) - $registration)
                                / GREATEST(date_part('epoch', $target - $registration), 1)
                            )
                        )) BETWEEN $URGENTIE{high}{min} AND $URGENTIE{high}{max}
                    THEN 'high'
                    WHEN
                        ABS(ROUND(
                            100 * (
                                date_part('epoch', COALESCE($completion, NOW()) - $registration)
                                / GREATEST(date_part('epoch', $target - $registration), 1)
                            )
                        )) >= $URGENTIE{late}{min}
                    THEN 'late'
                END
            };
        },
    },
);

has object_type => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has function => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has arguments => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Search::Term::Set',
    required => 1
);

=head1 METHODS

=head2 evaluate

Evaluate the function.

=cut

override 'evaluate' => sub {
    my $self = shift;
    my ($resultset, $conditional) = @_;

    if (exists $DISPATCH_BY_CLASS{ $self->object_type }{ $self->function }) {
        my ($value, @placeholders) = $DISPATCH_BY_CLASS{ $self->object_type }{ $self->function }->();

        return ($value, map { [ dummy => $_ ] } @placeholders);
    }

    throw(
        'not_found/search_function',
        sprintf("Search function '%s' not found", $self->function),
    );
};

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

