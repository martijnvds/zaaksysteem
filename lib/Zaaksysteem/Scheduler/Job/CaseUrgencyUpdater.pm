package Zaaksysteem::Scheduler::Job::CaseUrgencyUpdater;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Scheduler::Job::CaseUrgencyHighWatcher - Watcher to monitor and
publish events for cases where the urgency is high

=head1 DESCRIPTION

This job recalculates the urgency of all open/new cases in the system and sets
the case's C<urgency> field accordingly.

=cut

use BTTW::Tools;
use Zaaksysteem::Types qw[CaseUrgency];

=head1 METHODS

=head2 run

Implements the scheduler invocation method.

=cut

sub run {
    my ($self, $c) = @_;

    my $base_rs = $c->model('DB::Zaak')->search({
        status => [qw[open new]]
    });

    for my $level (@{ CaseUrgency->values }) {
        my $cases = $base_rs->search_extended(
            {
                _urgency => $level,  # This is a special 'virtual' field, caught by search_extended
                -or => [
                    urgency => { '!=' => $level }, # exclude cases that already have the expected urgency value
                    urgency => undef
                ]
            },
            {
                rows => 50
            }
        );

        my $queue;

        # Full inflate/deflate required for update triggers
        while (my $case = $cases->next) {
            my $old_urgency = $case->urgency;

            $case->update({ urgency => $level });

            if ($level eq 'high') {
                $queue //= $c->model('Queue');

                $queue->emit_case_event({
                    case => $case,
                    event_name => 'CaseTermExceeded',
                    description => 'Zaak termijn overschrijding',
                    changes => [
                        { key => 'urgency', old => $old_urgency, new => $level }
                    ],
                    data => {
                        date_target => $case->streefafhandeldatum
                    }
                });
            }
        }
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
