package Zaaksysteem::Controller::ExportQueue;

use Moose;
use namespace::autoclean;
use Try::Tiny;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('exportqueue') : CaptureArgs(0) {
    my ( $self, $c ) = @_;

    $c->stash->{queue} = $c->model('ExportQueue');
    $c->stash->{template} = 'export_queue.tt';
}

sub list : Chained('base') : PathPart('') : Args(0) {
     my ($self, $c) = @_;
}

sub download_file : Chained('base') : PathPart('download') : Args(1) {
    my ($self, $c, $token) = @_;

    try {
        my $entry = $c->stash->{queue}->get_export_item_by_token($token);
        $c->serve_filestore_streaming($entry->filestore_id);
        $entry->update({downloaded => $entry->downloaded + 1  });
        return;
    }
    catch {
        $c->log->info("$_");
        $c->stash->{error} = "$_";
        return;
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
