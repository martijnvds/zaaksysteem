package Zaaksysteem::Controller::Zaak::Destroy;

use Moose;

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 bulk_update_destroy

there are three outs in this sub. it's tricky to separate these in different subs
and still take advantage of the listaction js and validation system.
1: validation. returns JSON
2: destroy. return JSON
3: show form + warnings/errors. Returns HTML

=cut

define_profile bulk_update_destroy => (
    required => [qw/selection selected_case_ids/],
    optional => [qw/commit confirm_destroy do_validation zql/],
    constraint_methods => {
        selection => qr/^(?:selected_cases|zql)$/,
        selected_case_ids => sub {
            my $value = shift;
            my @case_ids = split /,/, $value;
            my $fault = grep { $_ !~ m/^\d+$/ } @case_ids;
            return !$fault;
        }
    },
);

sub bulk_update_destroy : Chained('/') : PathPart('bulk/update/destroy') {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin', 'beheerder', 'zaak_beheer');

    my $params = assert_profile($c->req->params)->valid;

    my $in_query;

    # Decide which in_query style we're working with (complete resultset or
    # selection).
    if ($params->{ selection } eq 'selected_cases') {
        # Selected cases are presented as a single, comma-seperated parameter
        # Ordered set, so the destruction checks are presented well-ordered.
        $in_query = [
            sort  { $a <=> $b }
            split /,/,
                  $params->{ selected_case_ids }
        ];
    } else {
        my $zql = Zaaksysteem::Search::ZQL->new($params->{ zql });

        my $object_rs = $zql->apply_to_resultset($c->model('Object')->acl_rs);

        $in_query = $object_rs->search(undef, {
            columns => 'me.object_id'
        })->as_query;
    }

    my $resultset = $c->model('DB::Zaak')->search({
        'me.id' => { in => $in_query }
    });

    # $params will contain the user response made in the dialog. the
    # warnings need to be answered with a confirm.
    # a confirm will result in confirm_destroy=on in the $params hashref:
    my $destroy_report = $resultset->check_destroy($params);
    $c->stash->{destroy_report} = $destroy_report;

    if ($destroy_report->has_errors) {
        $c->stash->{message} = 'Selectie kan niet worden vernietigd omdat er fouten zijn geconstateerd.';
    } elsif ($destroy_report->has_warnings && !$params->{confirm_destroy}) {
        $c->stash->{need_confirmation} = 1;
        $c->stash->{message} .= 'Er zijn waarschuwingen, bevestig de wijzigingen om door te gaan.'
    }

    if ($params->{do_validation}) {
        if (my $dv = $c->zvalidate) {

            my @valid = grep { $_ ne 'selection' } $dv->valid;
            my $json = {
                success     => $dv->success,
                missing     => [ $dv->missing ],
                invalid     => [ $dv->invalid ],
                unknown     => [ $dv->unknown ],
                valid       => [ @valid ],
                msgs        => $dv->msgs,
            };

            if ($c->stash->{message}) {
                $json->{msgs}->{selection_extra} = $c->stash->{message};
                push @{$json->{invalid}}, 'error';
                $json->{success} = 0;
                $json->{msgs}->{error} = $c->stash->{message};
            }
            $c->zcvalidate($json);
        }
        $c->detach;
    }

    $c->detach('destroy', [$resultset]) if !$c->stash->{message} && $params->{commit};

    my @case_ids = map { $_->id } $resultset->all;
    $c->stash->{case_ids} = \@case_ids;
    $c->stash->{action} = 'destroy';
    $c->stash->{selection} = $params->{selection};
    $c->stash->{selected_case_ids} = join ",", @case_ids;
    $c->stash->{template} = 'zaak/widgets/management.tt';
    $c->stash->{nowrapper} = 1;
}


sub destroy : Private {
    my ($self, $c, $resultset) = @_;

    # actual deletion is permitted if there are no errors,
    # and no warnings unless they are confirmed.
    $resultset->destroy_cases;

    $c->stash->{ json }{ success } = 1;
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 destroy

TODO: Fix the POD

=cut

