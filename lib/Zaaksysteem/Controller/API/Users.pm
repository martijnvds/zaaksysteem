package Zaaksysteem::Controller::API::Users;

use Moose;

use XML::Dumper;
use Encode qw/from_to/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub dispatch : Chained('/') : PathPart('api/users'): Args(0) {
    my ($self, $c)  = @_;

    unless ($self->_verify_api_key($c, $c->req->params->{api_key})) {
        $c->res->status('401');
        $c->detach('/forbidden')
    }

    ### Convert imput from XML to HASH
    my $xs      = XML::Dumper->new;

    my $data    = $xs->xml2pl($c->req->params->{message});

    my $ld          = $c->model('Users');

    my @components  = @{ $ld->components };
    my $component   = $c->req->params->{component};

    unless (grep({ $_ eq $component } @components)) {
        $c->res->status('500');
        $c->res->body('Component not found');
        $c->detach;
    }

    if ($ld->$component($data, $c->model('DB'))) {
        $c->res->status('200');
        $c->res->body('OK');
        $c->detach;
    }

    $c->res->status('500');
    $c->res->body('NOK');
    $c->detach;
}

=head2 session

Returns when a SAML session will reach its timeout.

=head3 URL

=over

=item C</api/users/session>

Displays the session timeout

=back

=head3 RETURNS

Returns in milliseconds when the timeout will be reached.

=cut

sub session : Chained('/') : PathPart('api/users/session'): Args(0) {
    my ($self, $c) = @_;

    if ($c->check_saml_session) {
        $c->stash->{zapi} = [ {
            session => {
                type => 'saml',
                ttl  => $c->get_saml_session_timeout(),
            },
        }, ];
    }
    else {
        $c->stash->{zapi} = [ {
            session => {
                type => 'zs',
                ttl  => $c->get_session_timeout(),
            },
        }, ];

    }
}

=head2 extend

Extends the SAML session timeout

=head3 URL

=over

=item C</api/users/session/extend>

Updates and displays the session timeout

=back

=head3 RETURNS

Returns in milliseconds when the timeout will be reached.

=cut

sub extend : Chained('/') : PathPart('api/users/session/extend'): Args(0) {
    my ($self, $c) = @_;

    $c->extend_saml_session_timeout();
    $c->forward('/api/users/session');
}


sub _verify_api_key {
    my ($self, $c, $api_key)    = @_;

    my $authinterface           = $c->model('DB::Interface')->search_active({module => 'authldap'})->first;

    unless ($authinterface && $authinterface->get_interface_config->{ad_api_key}) {
        $c->log->warn('API Key for AD: Cannot find interface "authldap" or no ad_api_key filled');
        return;
    }


    my $config_key  = $authinterface->get_interface_config->{ad_api_key};

    unless ($api_key =~ /^AD/) {
        $c->log->warn('API Key: incorrect identifier');
        return;
    }

    $api_key    =~ s/^AD-//;

    unless ($api_key eq $config_key) {
        $c->log->warn('API Key: incorrect');
        return;
    }

    return 1;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 dispatch

TODO: Fix the POD

=cut
