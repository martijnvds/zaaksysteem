package Zaaksysteem::Controller::API::Bulk::File;
use Moose;
use JSON;

use BTTW::Tools;
use Zaaksysteem::Types qw(UUID);

BEGIN {extends 'Zaaksysteem::General::ZAPIController'}

=head1 NAME

Zaaksysteem::Controller::API::Bulk::File - Controller that performs file actions in bulk

=cut

use constant FILE_PROPERTIES => [qw/
    accepted
    case_document_ids
    case_id
    deleted
    destroyed
    directory_id
    document_status
    metadata
    name
    pip_owner
    publish_pip
    publish_website
    rejection_reason
    root_file_id
    version
    rejected
/];

=head1 METHODS

=head2 base

Base controller for file bulk actions.

=cut

sub base : Chained('/api/base') : PathPart('bulk/file') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{zapi_no_pager} = 1;
    $c->stash->{files} = $c->req->param('files');
}

=head2 update

Perform a bulk update of file properties.

=head3 URL

POST /api/bulk/file/update

=head3 Input

This controller expects a JSON data structure that looks like this:

    {
        files: {
            123: {
                action: 'update_properties',
                data: {
                    name: 'foobar'
                }
            },
            124: {
                action: 'update_file',
                data: {
                    existing_file_id: 1337
                }
            }
        }
    }

The allowed actions are:

=over

=item * update_properties

Updates a file's properties. Properties in the "data" key will be updated,
properties that aren't in there will not be changed.

=item * update_file

Replace a file with a new one.

Only works for accepting new versions of existing file, using the
C<existing_file_id> argument.

=item * revert_to

Reverts a file to an older version.

=item * move_case_document

=back

=head3 Output

Returns a ZAPI response, with the following values:

    [
        { file_id: 123, action: 'update_properties', result: 'success', data: {file object} },
        { file_id: 124, action: 'update_file', result: 'error', data: {error object} }
    ]

=cut

sub update : Chained('base') : PathPart('update') : ZAPI {
    my ($self, $c) = @_;
    my $files = $c->stash->{'files'};

    my @rv;
    my %base_params = (
        subject => $c->user->as_object
    );

    for my $file_id (keys %$files) {
        my %file = %{ $files->{$file_id} };
        my %params = %base_params;

        try {

            # HACK hack sad hack
            # Because we don't have time now for a api/v1/{file,filestore}
            # controller with all the logic we do a bit of a icky hack.
            # The frontend only gets the UUID of the filestore object and
            # not the file. When they want to update the properties they
            # only have our UUID. Even if they use the content from
            # api/v1/case they get a UUID from the filestore. So: UUID ==
            # filestore object, otherwise it is considered a file id.
            my $file_obj;
            if (UUID->check($file_id)) {
                $file_obj = $c->model('DB::File')->search({ 'filestore_id.uuid' => $file_id }, { join =>  'filestore_id' })->first;
            }
            else {
                $file_obj = $c->model('DB::File')->find($file_id);
            }

            throw('/file/update/file_not_found', "File with ID $file_id not found") if !$file_obj;

            my $updated;

            # Wrap everything in a transaction so the case for-update select
            # can succeed.
            $c->model('DB')->txn_do(sub {
                my $case;

                # Prevent double access on file interactions from within the
                # same case (this can cause files to go 'missing' (actually
                # just hidden by partially updating several rows at once,
                # from different request contexts)).
                if ($file_obj->get_column('case_id')) {
                    $case = $c->model('DB::Zaak')->search_rs(
                        { 'me.id' => $file_obj->get_column('case_id') },
                        { for => \'share' }
                    )->first;
                }

                if ($file{action} eq 'update_properties') {
                    for my $prop (@{ FILE_PROPERTIES() }) {
                        $params{$prop} = $file{data}{$prop} if exists $file{data}{$prop};
                    }

                    if ($params{destroyed}) {
                        $params{user_has_zaak_beheer_permissions} =
                            $c->check_any_given_zaak_permission($file_obj->case_id, 'zaak_beheer');
                    }

                    # This is a hack because fiddling with .tt files is giving
                    # us headaches. The uploaded file is some kind of weird
                    # datastructure that doesn't match either a filestore/file
                    # object. To prevent additional DB lookups for files we
                    # apply the logic here. The rejected bit is new, so old
                    # behaviour is still using accepted = false instead of the
                    # more explicit rejected = true.
                    # The new 'zaakdossier' project will have a better API
                    # without .tt, some angular, maybe some ezra.js code.
                    if ($params{rejected} && !$file_obj->is_rejectable) {
                        $params{deleted} = delete $params{rejected};
                    }

                    $updated = $file_obj->update_properties(\%params);

                    if ($params{accepted}) {
                        $file_obj->trigger('accept');
                    }
                }
                elsif ($file{action} eq 'update_file') {
                    $params{existing_file_id} = $file{data}{existing_file_id};
                    $updated = $file_obj->update_existing(\%params);
                }
                elsif ($file{action} eq 'revert_to') {
                    $updated = $file_obj->make_leading($params{subject});
                }
                elsif ($file{action} eq 'move_case_document') {
                    $params{from}             = $file{data}{from};
                    $params{case_document_id} = $file{data}{case_document_id};

                    $updated = $file_obj->move_case_document(\%params);
                }
                else {
                    throw('/api/file/bulk/unknown_action', "Unknown action for file with id " . $file_id);
                }
            });

            push @rv, {
                file_id => $file_id,
                action  => $file{action},
                result  => "success",
                data    => $updated,
            };
        } catch {
            $c->log->warn($_);

            push @rv, {
                file_id => $file_id,
                action  => $file{action},
                result  => "error",
                data    => $_,
            };
        };
    }

    $c->stash->{zapi} = \@rv;
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
