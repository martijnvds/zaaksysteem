package Zaaksysteem::Controller::API::v1::File;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::File - Low-level file access API for Zaaksysteem

=head1 DESCRIPTION

This API provides low-level file access for Zaaksysteem.

The base path for this API is C</api/v1/file>.

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::Message::Ack;
use Zaaksysteem::StatsD;
use Zaaksysteem::Types qw[UUID];

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern', 'extern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/file> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('file') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ filestore_rs } = $c->model('DB::Filestore');
}

=head2 instance_base

Reserves the C</api/v1/file/[FILE_UUID]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    unless (UUID->check($uuid)) {
        throw('api/v1/file/file_id_invalid', sprintf(
            'Supplied File identifier is not a valid UUID: "%s"',
            $uuid
        ));
    }


    my $filestore_rs = $c->stash->{ filestore_rs }->search({ uuid => $uuid });
    my $filestore_row = $filestore_rs->first;
    unless (defined $filestore_row) {
        throw('api/v1/file/file_not_found', sprintf(
            'No file found with identifier "%s"',
            $uuid
        ), { http_code => 404 });
    }

    my $case_id;

    my ($file, @rest);
    if ($c->req->params->{case_id}) {

        $case_id = $c->req->params->{case_id};
        my ($file, @rest) = $filestore_row->files->search_rs(
            { case_id => $case_id }
        )->all;

        if (!$file) {
            throw('api/v1/file/case/file_not_found', sprintf(
                'No file found in case %d  with identifier "%s"',
                $case_id,
                $uuid
            ), { http_code => 404 });
        }
        $c->stash->{ file_object } = $file;
    }


    $c->stash->{ filestore_row } = $filestore_row;
    $c->stash->{ file } = $filestore_row->as_object;
}

=head1 lock_base

Reserves the C</api/v1/file/[FILE_UUID]/lock> routing namespace.

=cut

sub lock_base : Chained('instance_base') : PathPart('lock') : CaptureArgs(0) : RO {
    my ($self, $c) = @_;

    my $filestore_row = $c->stash->{ filestore_row };

    my $case_id;
    if ($c->req->params->{case_id}) {
        $case_id = $c->req->params->{case_id};
    }
    my ($file, @rest) = $filestore_row->files->search_rs(
        $case_id ? { case_id => $case_id } : undef
    )->all;

    if ((not defined $file) || scalar @rest > 0) {
        throw('api/v1/file/lock/singular_lock_indeterminate', sprintf(
            'The file "%s" has internal state incompatible with locks',
            $filestore_row->original_name
        ), { http_code => 500 });
    }

    $c->stash->{ file_row } = $file->get_last_version;
}

=head2 lock_instance_base

Reserves the lock object C</api/v1/file/[FILE_UUID]/lock> routing namespace.

=cut

sub lock_instance_base : Chained('lock_base') : PathPart('') : CaptureArgs(0) : RO {
    my ($self, $c) = @_;

    my $file_row = $c->stash->{ file_row };

    unless ($file_row->is_locked) {
        throw('api/v1/file/lock/lock_not_found', sprintf(
            'The file "%s" is currently not locked',
            $c->stash->{ filestore_row }->original_name
        ), { http_code => 404 });
    }
}

=head2 get

Retrieve given L<C<file>|Zaaksysteem::Object::Types::File> object.

=head3 URL Path

C</api/v1/file/[FILE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ file };
}

=head2 download

Download the content of the referenced file.

=head3 URL Path

C</api/v1/file/[FILE_UUID]/download>

=cut

sub download : Chained('instance_base') : PathPart('download') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ api_unauthorized_response } = 1;

    $c->res->header(
        'Cache-Control'       => 'must-revalidate',
        'Pragma'              => 'private',
        'Content-Disposition' => sprintf(
            'attachment; filename="%s"',
            $c->stash->{file_object} ? $c->stash->{file_object}->filename :
            $c->stash->{filestore_row}->original_name
        )
    );

    $c->serve_filestore($c->stash->{ filestore_row });

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    Zaaksysteem::StatsD->statsd->end('serve_file.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('serve_file.count', 1)
}

=head2 upload

Upload a new version of a file.

This action will actually add to the set of
L<C<files>|Zaaksysteem::Object::Types::File> in Zaaksysteem and update associated
documents entries.

=head3 URL Path

C</api/v1/file/[FILE_UUID]/upload>

=cut

sub upload : Chained('instance_base') : PathPart('upload') : Args(0) : RW {
    my ($self, $c) = @_;

    my @uploads = map {
        ref $_ eq 'ARRAY' ? @{ $_ } : $_
    } values %{ $c->req->uploads };

    unless (scalar @uploads == 1) {
        throw(
            'api/v1/file/upload_missing',
            'Upload controller expects exactly one upload'
        );
    }

    my $upload = shift @uploads;

    $self->log->debug(sprintf('Processing file upload "%s"', $upload->filename));

    my $case_id;
    if ($c->req->params->{case_id}) {
        $case_id = $c->req->params->{case_id};
    }

    my $rs = $c->stash->{ filestore_row }->files->search_rs(
        $case_id ? { case_id => $case_id } : undef
    );

    while (my $file = $rs->next) {
        $self->log->debug(sprintf('Updating file "%s"', $file->name));

        $file->update_file({
            subject => $c->user->as_object,
            new_file_path => $upload->tempname,
            original_name => $upload->filename
        });
    }

    $c->stash->{ result } = Zaaksysteem::API::v1::Message::Ack->new(
        message => 'Upload succesfully processed'
    );
}

=head2 virus_scan_status_update

take the C<viruses>-list from the body-params. If this list is empty, apparently
the Virus Scan Service has not found any viruses and the file is OK.

When the VSS has detected any viruses, we will remove the C<File>

NOTE: we will still need to build functionality that will tell the C<Filestore>
to actually delete those files, we currently only have marked them as
contaminated. and C<< Resultset('filestore')->search >> will only return
non-infected files.

=head3 URL Path

C</api/v1/file/[FILE_UUID]/virus_scan_status>

=cut

define_profile virus_scan_status_update => (
    required => { # required would fail if the list of viruses is empty
        viruses => 'Str',
    },
    defaults => {
        viruses => [],
    }
);

sub virus_scan_status_update : Chained('instance_base') : PathPart('virus_scan_status') : Args(0) : RW {
    my ($self, $c) = @_;

    my $params      = assert_profile($c->req->params)->valid;
    $params->{ viruses } = [] unless defined $params->{ viruses };
    my @viruses = ref($params->{ viruses }) ? @{$params->{ viruses }} : ( $params->{ viruses } );

    my $filestore_row = $c->stash->{ filestore_row };
    $filestore_row->update_status_from_viruses( \@viruses );

    if (scalar @viruses) {
        for my $file ($c->stash->{ filestore_row }->files) {
            $self->log->debug(sprintf('Deleting file "%s"', $file->name));
            $file->update_properties({
                subject => $c->user->as_object, # will this work, which user ?
                deleted => 1,
            });
        }
    }

    $c->stash->{ result } = Zaaksysteem::API::v1::Message::Ack->new(
        message => 'Update of virus-scan-status processed'
    );

}

=head2 lock_get

Retrieve the L<C<file/lock>|Zaaksysteem::Object::Types::File::Lock> object on a given
L<C<file>|Zaaksysteem::Object::Types::File> object.

If no lock exists, will respond with a C<404> status and exception.

C</api/v1/file/[FILE_UUID]/lock>

=cut

sub lock_get : Chained('lock_instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ file_row }->lock_as_object;
}

=head2 lock_acquire

Acquire a L<C<file/lock>|Zaaksysteem::Object::Types::File::Lock> on a
L<C<file>|Zaaksysteem::Object::Types::File> object, if one does not already
exist.

Responds with the newly created C<file/lock> object when succesful.

C</api/v1/file/[FILE_UUID]/lock/acquire>

=cut

sub lock_acquire : Chained('lock_base') : PathPart('acquire') : Args(0) : RW {
    my ($self, $c) = @_;

    if ($c->stash->{ file_row }->is_locked) {
        throw('api/v1/file/lock/acquire/file_lock_conflict', sprintf(
            'The file "%s" is already locked',
            $c->stash->{ file }->name
        ), { http_code => 409 });
    }

    $c->stash->{ file_row }->acquire_lock($c->user->as_object, 30);

    $c->detach('lock_get');
}

=head2 lock_extend

Extends the expiration timestamp of a
L<C<file/lock>|Zaaksysteem::Object::Types::File::Lock> object, if one exists.

If no lock exists, will respond with a C<404> status and exception.

C</api/v1/file/[FILE_UUID]/lock/extend>

=cut

sub lock_extend : Chained('lock_instance_base') : PathPart('extend') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->stash->{ file_row }->extend_lock($c->user->as_object, 30);

    $c->detach('lock_get');
}

=head2 lock_release

Releases the L<C<file/lock>|Zaaksysteem::Object::Types::File::Lock> on a file
if one currently exists and is owned by the user making the request.

If no lock exists, will respond with a C<404> status and exception.

C</api/v1/file/[FILE_UUID]/lock/release>

=cut

sub lock_release : Chained('lock_instance_base') : PathPart('release') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->stash->{ file_row }->release_lock($c->user->as_object);

    $c->stash->{ result } = Zaaksysteem::API::v1::Message::Ack->new(
        message => 'File lock released'
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
