package Zaaksysteem::Controller::Sysin::TransactionRecordToObject;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

use Zaaksysteem::ZAPI::CRUD::Interface;
use Zaaksysteem::ZAPI::CRUD::Interface::Column;
use Zaaksysteem::ZAPI::CRUD::Interface::Action;
use Zaaksysteem::ZAPI::CRUD::Interface::Filter;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

use constant ZAPI_CRUD => {
    'records' => Zaaksysteem::ZAPI::CRUD::Interface->new(
        options     => {
            select      => 'none',
        },
        actions => [],
        columns => [
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'transaction_id',
                label       => 'transaction id',
                resolve     => 'transaction_id',
            ),
        ],
    ),
    'read'  => Zaaksysteem::ZAPI::CRUD::Interface->new(
        options     => {
            select      => 'none',
        },
        style       => {
            classes     =>  {
            }
        },
        actions         => [
        ],
        columns         => [
            # Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            #     id      => 'id',
            #     label   => 'ID',
            #     resolve => 'id'
            # ),
            # Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            #     id          => 'object_subscription',
            #     label       => 'Afnemerindicatie',
            #     template    => "<[item.object_subscription]>",
            # ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'transaction_record_id',
                label       => 'Record ID',
                template    => '<a href="/beheer/sysin/transactions/<[item.transaction_record_id.transaction_id.id]>/records/<[item.transaction_record_id.id]>"><[item.transaction_record_id.id]></a>'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'local_id',
                label       => 'Tabel ID',
                template    => '<a href="/beheer/object/search/<[item.local_table]>/<[item.local_id]>"><[item.local_id]></a>'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'local_table',
                label       => 'Tabel',
                resolve     => 'local_table',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'mutation_type',
                label       => 'Mutatie type',
                resolve     => 'mutation_type',
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'mutations',
                label       => 'Mutaties',
                resolve     => 'mutations',
                template    => '<div class="record-mutation-table" data-ng-show="getRecordMutations(item).length"><div class="record-mutation-table-header"><div class="record-mutation-table-header-cell record-mutation-table-header-cell-<[cell]>" data-ng-repeat="cell in [ \'Kolom\', \'Nieuw\', \'Oud\' ]"><[cell]></div></div><div class="record-mutation-table-row" data-ng-repeat="mutation in getRecordMutations(item)"><div class="record-mutation-table-cell" data-ng-repeat="cell in [ \'column\', \'new\', \'old\' ]"><[mutation[cell]]></div></div></div>'
            )
        ],
    ),
};

=head1 NAME

Zaaksysteem::Controller::Sysin::TransactionRecordToObject - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Sysin>

=head1 DESCRIPTION

Zaaksysteem API Controller for System Integration module TransactionRecordToObject.

=head1 METHODS

=head2 /sysin/transaction_record_to_object [GET READ]

 /sysin/transaction_record_to_object?zapi_crud=1

 /sysin/transaction_record_to_object


Returns a list of transaction_record_to_object rows.

B<Special Query Params>

=over 4

=item C<zapi_crud=1>

Use the special query parameter C<zapi_crud=1> for getting a technical CRUD
description.

=back

beheer/sysin/transactions/75/records/71/mutation/

=cut

sub records : Regex('^sysin/transaction/(\d+)/record/(\d+)/mutation') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    my ($transaction_id, $transaction_record_id) = @{$c->req->captures};

    if (exists $c->req->params->{zapi_crud}) {
        $c->stash->{zapi} = [ ZAPI_CRUD->{read}->from_catalyst($c) ];
        $c->detach;
    }

    $c->stash->{zapi}   = $c->model('DB::TransactionRecordToObject')->search_filtered(
        {
            transaction_record_id => $transaction_record_id,
        }
    );

}

sub index
    : Chained('/')
    : PathPart('sysin/transaction_record_to_object')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    if (exists $c->req->params->{zapi_crud}) {
        $c->stash->{zapi} = [ ZAPI_CRUD->{read}->from_catalyst($c) ];
        $c->detach;
    }

    $c->stash->{zapi}   = $c->model('DB::TransactionRecordToObject')->search_filtered(
        $c->req->params
    );
}


sub base
    : Chained('/')
    : PathPart('sysin/transaction_record_to_object')
    : CaptureArgs(1)
{
    my ($self, $c, $id) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{entry}  = $c->model('DB::TransactionRecordToObject')->find($id);
}

=head2 /sysin/transaction_record_to_object/ID [GET READ]

Reads interface information from Sysin by ID

B<Options>: none

=cut

sub read
    : Chained('base')
    : PathPart('')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->stash->{zapi}   = $c->stash->{entry} || [];
}


=head2 /sysin/transaction_to_object/object_subscription_delete [POST DELETE]

Marks the related object_subscription as deleted.

=cut

sub object_subscription_delete
    : Chained('/')
    : PathPart('sysin/transaction_record_to_object/object_subscription_delete')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', []) unless
        lc($c->req->method) eq 'post';

    my $rs      = $c->model('DB::TransactionRecordToObject')->search_filtered(
        $c->req->params
    );

    $rs->action_for_selection(
        'object_subscription_delete',
        $c->req->params,
    );

    $c->stash->{zapi} = [];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAPI_CRUD

TODO: Fix the POD

=cut

=head2 base

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 read

TODO: Fix the POD

=cut

=head2 records

TODO: Fix the POD

=cut

