=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Case::ACL - Type definition for case/acl objects

=head1 DESCRIPTION

This page documents the serialization of C<case> objects.

=head1 JSON

=begin javascript

{
   "instance" : {
      "capabilities" : [
         "read",
         "write"
      ],
      "date_created" : "2016-10-19T08:01:31Z",
      "date_modified" : "2016-10-19T08:01:31Z",
      "entity_id" : "10050|20002",
      "entity_type" : "position",
      "read_only" : true,
      "scope" : "instance"
   },
   "reference" : null,
   "type" : "case/acl"
},

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 capabilities E<raquo> C<enum(read, write, manage, search)>

Capabilities this ACL entry has.

=head2 entity_id E<raquo> L<C<[group:table_id]|[role:table_id]>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Format: C<group_id>C<|>C<role_id>

The entity identifier for this ACL entry.

=head2 entity_type E<raquo> C<enum(position)>

Format: C<group_id>|<role_id>

The entity type for this ACL entry.

=head2 scope E<raquo> C<enum(type,instance)>

The scope of this entry. Where C<instance> points to an ACL which is directly added to the case, and C<type> points
to the type of the case or "casetype"

=head2 date_created E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object creation timestamp.

=head2 date_modified E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object modification timestamp.


=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
