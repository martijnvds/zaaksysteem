=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Casetype - Type definition for casetypes
objects

=head1 DESCRIPTION

This page documents the serialization for C<casetype> objects.

=head1 JSON

=begin javascript

{
    "reference": null,
    "type": "casetype",
    "instance": {
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
