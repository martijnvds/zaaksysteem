=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Dashboard - Dashboard API reference
documentation

=head1 NAMESPACE URL

    /api/v1/dashboard

=head1 DESCRIPTION

This page documents the endpoints within the C<dashboard> API namespace.

=head1 ENDPOINTS

=head2 Favourite endpoints

=head3 C<GET /favourite/[favourite:reference_type]>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<favourite|Zaaksysteem::Manual::API::V1::Types::Favourite> instances.

=head3 C<GET /favourite/[favourite:reference_type]/[favourite:id]>

Returns a L<favourite|Zaaksysteem::Manual::API::V1::Types::Favourite>
instance.

=head3 C<POST /favourite/[favourite:reference_type]>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<favourite|Zaaksysteem::Manual::API::V1::Types::Favourite> instances.

=head3 C<POST /favourite/[favourite:reference_type]/[favourite:id]/update>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<favourite|Zaaksysteem::Manual::API::V1::Types::Favourite> instances.

=head3 C<POST /favourite/[favourite:reference_type]/bulk_update>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<favourite|Zaaksysteem::Manual::API::V1::Types::Favourite> instances.

=head3 C<POST /favourite/[favourite:reference_type]/delete>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<favourite|Zaaksysteem::Manual::API::V1::Types::Favourite> instances.

=head2 Widget endpoints

=head3 C<GET /widget>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<widget|Zaaksysteem::Manual::API::V1::Types::Widget> instances.

=head3 C<GET /widget/[widget:id]>

Returns a L<widget|Zaaksysteem::Manual::API::V1::Types::Widget> instance.

=head3 C<POST /widget/create>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<widget|Zaaksysteem::Manual::API::V1::Types::Widget> instances.

=head3 C<POST /widget/[widget:id]/update>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<widget|Zaaksysteem::Manual::API::V1::Types::Widget> instances.

=head3 C<POST /widget/bulk_update>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<widget|Zaaksysteem::Manual::API::V1::Types::Widget> instances.

=head3 C<POST /widget/set_default>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<widget|Zaaksysteem::Manual::API::V1::Types::Widget> instances.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
