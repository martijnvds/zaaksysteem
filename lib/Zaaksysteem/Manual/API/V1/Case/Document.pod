=head1 NAME

Zaaksysteem::Manual::API::V1::Case::Document - Retrieval of documents belonging to a case

=head1 Description

This API-document describes the usage of our JSON Case/Document API. Via the Document API it is possible
to retrieve file metadata or download the file itself.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head1 Retrieve data

=head2 get

   /api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/document/001af15e-d779-461c-862d-16e63b5ebca8

Retrieving (meta-)information about a document belonging to a certain case, is as simple as combining
the two uuids, where the first uuid contains the reference to the case, and the second a reference
to the document.

B<Example call>

  curl --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/document/5aead1cc-68d0-41c9-a30f-b4d762ad13a8

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-cf3848-16f6ff",
   "development" : false,
   "result" : {
      "instance" : {
         "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
         "created" : "2015-03-04T11:01:05Z",
         "id" : "8f03c9a6-2d21-4186-9aee-25a13ba1bcd8",
         "last_modified" : "2015-03-04T11:01:08Z",
         "metadata" : {},
         "mimetype" : "text/plain",
         "name" : "FilestoreTest.txt",
         "size" : 2387,
         "title" : "FilestoreTest"
      },
      "reference" : "8f03c9a6-2d21-4186-9aee-25a13ba1bcd8",
      "type" : "file"
   },
   "status_code" : 200
}

=end javascript

=head2 list

   /api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/document

Retrieving multiple document metadata objects from our database is as
simple as calling same URL as the get call, but without the second uuid.
It will return a C<set> type object containing all the accepted case
documents of a case. Zaaksysteem has a concept of I<case documents>.
Case documents are documents that are associated to attributes. Other
documents may also be present in a case. If you want to retrieve these
files you will need to look at the
L<Zaaksysteem::Manual::API::V1::Case::Document/all> call.

B<Example call>

 curl --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/document

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-4d3835-939326",
   "development" : false,
   "result" : {
      "instance" : {
         "rows" : [
            {
               "instance" : {
                  "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
                  "created" : "2015-03-04T10:59:02Z",
                  "id" : "1180d4f8-d07a-467d-a4b6-213905749b12",
                  "last_modified" : "2015-03-04T10:59:05Z",
                  "metadata" : {},
                  "mimetype" : "text/plain",
                  "name" : "FilestoreTest.txt",
                  "size" : 2387,
                  "title" : "FilestoreTest"
               },
               "reference" : "1180d4f8-d07a-467d-a4b6-213905749b12",
               "type" : "file"
            },
            {
               "instance" : {
                  "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
                  "created" : "2015-03-04T10:59:02Z",
                  "id" : "6df8d923-1425-47b3-b966-0d7a6506e068",
                  "last_modified" : "2015-03-04T10:59:06Z",
                  "metadata" : {},
                  "mimetype" : "text/plain",
                  "name" : "wAsE3ldPML.txt",
                  "size" : 2387,
                  "title" : "wAsE3ldPML"
               },
               "reference" : "6df8d923-1425-47b3-b966-0d7a6506e068",
               "type" : "file"
            }
         ]
      },
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 all

   /api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5/document/all

This call will return all documents associated with a case. For more
information about documents have a look at
L<Zaaksysteem::Manual::API::V1::Case::Document/list> call.

B<Example call>

 curl --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/document/all

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "dev-4f065f-bb086d",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "case" : {
                     "instance" : null,
                     "preview" : "case(...0ae4a7)",
                     "reference" : "9b15dd47-c208-44a3-baa1-28a0b20ae4a7",
                     "type" : "case"
                  },
                  "date_created" : "2018-04-05T11:43:31Z",
                  "date_modified" : "2018-04-05T11:43:31Z",
                  "file" : {
                     "instance" : {
                        "archivable" : true,
                        "date_created" : "2018-04-05T08:24:56Z",
                        "date_modified" : "2018-04-05T11:43:31Z",
                        "md5" : "44f64a242b326f3b61dc95b4e192ff90",
                        "mimetype" : "video/x-matroska",
                        "name" : "example-change.mkv",
                        "size" : 648270,
                        "virus_scan_status" : "ok"
                     },
                     "preview" : "example-change.mkv",
                     "reference" : "aeac7a43-71c4-4fdc-a382-57fc0e8a48b8",
                     "type" : "file"
                  },
                  "filename" : "example-change.mkv",
                  "metadata" : null,
                  "name" : "example-change",
                  "number" : 2,
                  "version" : 1
               },
               "preview" : "document(unsynched)",
               "reference" : null,
               "type" : "document"
            },
            {
               "instance" : {
                  "case" : {
                     "instance" : null,
                     "preview" : "case(...0ae4a7)",
                     "reference" : "9b15dd47-c208-44a3-baa1-28a0b20ae4a7",
                     "type" : "case"
                  },
                  "date_created" : "2018-04-05T11:43:31Z",
                  "date_modified" : "2018-04-05T11:43:31Z",
                  "file" : {
                     "instance" : {
                        "archivable" : true,
                        "date_created" : "2018-04-05T08:24:56Z",
                        "date_modified" : "2018-04-05T11:43:31Z",
                        "md5" : "380957255575eb228c09bfe84b0e7efb",
                        "mimetype" : "video/x-matroska",
                        "name" : "example.mkv",
                        "size" : 2743190,
                        "virus_scan_status" : "ok"
                     },
                     "preview" : "example.mkv",
                     "reference" : "4e87fd9b-ba04-42ee-820b-d1bdbb637850",
                     "type" : "file"
                  },
                  "filename" : "example.mkv",
                  "metadata" : null,
                  "name" : "example",
                  "number" : 1,
                  "version" : 1
               },
               "preview" : "document(unsynched)",
               "reference" : null,
               "type" : "document"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 download file

When you would like to download a file from zaaksysteem, it is possible to accomplish this by calling
a specially formatted URL, which contains the uuid of the case and the uuid of the file.

B<Example download>

 curl --anyauth -k --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/case/32da49c7-d6d9-449e-9de7-18203049f181/document/5aead1cc-68d0-41c9-a30f-b4d762ad13a8/download

B<Response>

You will get a download of your file

=head1 Objects

=head2 File

Most of the calls in this document return an instance of type C<file>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item id

B<TYPE>: UUID

The unique identifier of this file. You can use this identifier to request metadata about this file or download
the file itself.

=item name

B<TYPE>: String

The name of the file, with extension.

=item title

B<TYPE>: String

The name of the file, without extension.

=item size

B<TYPE>: Number

Returns the size of the file in bytes

=item checksum

B<TYPE>: String

A string starting with C<md5:> containing the md5 sum of a file.

=item last_modified

B<TYPE>: ISO8601 Date representation in UTC

The date this file has been modified

=item created

B<TYPE>: ISO8601 Date representation in UTC

The date this file has been created.

=item metadata

B<TYPE>: Object

Contains the contents of the metadata for this file

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Case::Document>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
