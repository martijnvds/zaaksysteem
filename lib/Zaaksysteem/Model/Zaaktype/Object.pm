package Zaaksysteem::Model::Zaaktype::Object;

use strict;
use warnings;

use Moose;

use constant    ZTNO_MAP    => {
    'code'              => { naam => 'code'},
    'titel'             => { naam => 'titel'},
    'trigger'           => { naam => 'trigger'},
    'toelichting'       => { naam => 'toelichting'},
    'versie'            => { naam => 'version'},
    'version'           => { naam => 'version'},
    'actief'            => { naam => 'active'},
    'webform_toegang'   => { naam => 'webform_toegang'},
    'automatisch_behandelen'    => { naam => 'automatisch_behandelen'},
    'webform_authenticatie'     => { naam => 'webform_authenticatie'},
    'toewijzing_zaakintake'     => { naam => 'toewijzing_zaakintake'},
    'bedrijfid_wijzigen'        => { naam => 'bedrijfid_wijzigen' },
    'adres_relatie'     => { naam => 'adres_relatie' },
    'rt_queue_naam'     => { naam => 'zaaktype_rt_queue' },
    'hergebruik'        => { naam => 'aanvrager_hergebruik' },
    'is_public'         => { naam => 'is_public' },
    'online_betaling'   => { naam => 'online_betaling' },
    'adres_aanvrager'   => { naam => 'adres_aanvrager' },
};


### BASICS

has 'c' => (
    'is'        => 'rw',
    'weak_ref'  =>  1,
);

has ['extraopts']  => (
    'is'        => 'rw',
);

has ['nid', 'ztno', 'id']     => (
    'is'        => 'rw',
);

has 'code'          => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->code;
    }
);

has 'ztc'     => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_attributens;
    }
);

has 'kenmerken'     => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_kenmerkens;
    }
);

has 'documenten'     => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_ztc_documentens;
    }
);

has 'resultaten'     => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_resultatens;
    }
);

has 'authorisatie'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_id->zaaktype_authorisations;
    }
);

has 'notificatie'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_notificaties;
    }
);

has 'categorie'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        if ($self->ztno->zaaktype_id->bibliotheek_categorie_id) {
            return $self->ztno->zaaktype_id->bibliotheek_categorie_id;
        }
        return $self->ztno->zaaktype_id->zaaktype_categorie_id;
    }
);

has 'status'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_statuses->search(
            {},
            {
                order_by    => 'status'
            }
        );
    }
);

has 'sjablonen'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_sjablonens;
    }
);

has 'checklist'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->c->model('DB::Checklist')->search(
            'casetype_node_id'   => $self->ztno->id
        );
    }
);

has 'definitie'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->zaaktype_definitie_id;
    }
);

has 'is_current_versie'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        return $self->ztno->is_huidige_versie;
    }
);

sub BUILD {
    my $self    = shift;

    if (!$self->c) {
        die('ZTNO: Zaaktype Object created without catalyst context object');
    }

    if ( !$self->nid || $self->nid !~ /^\d+$/ ) {
        $self->c->log->error(
            'ZTNO: Zaaktype object created without ZT node id'
        );

        return;
    }

    my $ztno = $self->c->model('DB::ZaaktypeNode')->find($self->nid);

    unless (defined $ztno) {
        $self->c->log->warn(sprintf(
            'Zaaktype model: zaaktype_node with id "%s" not found',
            $self->nid
        ));

        return;
    }

    # Zaaktype node object
    $self->ztno($ztno);

    # Zaaktype id
    $self->id($ztno->get_column('zaaktype_id'));

    my $ztno_map = ZTNO_MAP;
    while (my ($key, $value) = each(%{ $ztno_map })) {
        my $dbnaam = $value->{naam};
        $self->meta->add_attribute(
            $key,
            'is'        => 'rw',
            'lazy'      => 1,
            'default'   => sub {
                my $self    = shift;

                return $self->ztno->$dbnaam;
            }
        );
    }
}

sub setup_datums {
    my ($self, $zaak) = @_;

    $self->c->model('Zaaktype')->_calculate_dates($zaak);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 ZTNO_MAP

TODO: Fix the POD

=cut

=head2 setup_datums

TODO: Fix the POD

=cut

