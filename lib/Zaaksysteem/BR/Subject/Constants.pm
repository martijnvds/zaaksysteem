package Zaaksysteem::BR::Subject::Constants;

use utf8;
use strict;
use warnings;

use DateTime;
use Exporter qw[import];
use Scalar::Util qw/blessed/;

use Zaaksysteem::Object::Process;
use BTTW::Tools;

our @EXPORT_OK = qw[
    SUBJECT_CONFIGURATION
    SUBJECT_MAPPING
    TYPE_TO_SCHEMA
    REMOTE_SEARCH_MODULE_NAME_KVKAPI
    REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO
    REMOTE_SEARCH_MODULE_NAME_STUFNP
];

our %EXPORT_TAGS = (
    remote_search_module_names => [
        'REMOTE_SEARCH_MODULE_NAME_KVKAPI',
        'REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO',
        'REMOTE_SEARCH_MODULE_NAME_STUFNP',
    ]
);
=head1 NAME

Zaaksysteem::BR::Subject::Constants - Constants for the Subjects.

=head1 CONSTANTS

=head2 FILTER_TIME

=cut

use constant FILTER_TIME => sub {
    my $value   = shift;

    return unless (
        $value &&
        ( blessed($value) && $value->isa('DateTime') ) ||
        $value =~ /^\d{4}-\d{2}-\d{2}$/
    );

    my ($y,$m,$d) = $value =~ /^(\d{4})-(\d{2})-(\d{2})$/;

    return DateTime->new(year => $y, month => $m, day => $d);
};


=head2 SUBJECT_CONFIGURATION

The mapping of the different subjects. english => dutch

=cut

use constant SUBJECT_CONFIGURATION => {
    person  => {
        table_prefix    => 'natuurlijk_persoon',
        mapping         => {
            burgerservicenummer     => 'personal_number',
            a_nummer                => 'personal_number_a',

            voorletters             => 'initials',
            voornamen               => 'first_names',
            geslachtsnaam           => 'family_name',
            voorvoegsel             => 'prefix',
            geslachtsaanduiding     => 'gender',
            geboortedatum           => 'date_of_birth',
            indicatie_geheim        => 'is_secret',
            in_gemeente             => 'is_local_resident',
            datum_overlijden        => 'date_of_death',
            aanduiding_naamgebruik  => 'use_of_name',
            naamgebruik             => 'surname',
            adellijke_titel         => 'noble_title',
        },
        field_filters   => {
            date_of_death           => sub { FILTER_TIME()->(shift) }, # Watch out for 1942 (timeflux)
            date_of_birth           => sub { FILTER_TIME()->(shift) },
        },
        address_mapping => {
            straatnaam              => 'street',
            huisnummer              => 'street_number',
            huisnummertoevoeging    => 'street_number_suffix',
            huisletter              => 'street_number_letter',
            postcode                => 'zipcode',
            woonplaats              => 'city',
            adres_buitenland1       => 'foreign_address_line1',
            adres_buitenland2       => 'foreign_address_line2',
            adres_buitenland3       => 'foreign_address_line3',
            landcode                => 'country',
            gemeente_code           => 'municipality',
            bag_id                  => 'bag_id',
        },
        mapper          => {
            'subject.address_residence'         => 'address_mapping',
            'subject.address_correspondence'    => 'address_mapping',
            'subject'                           => 'mapping',
        },
        partner_mapping => {
            'partner_a_nummer'                  => 'personal_number_a',
            'partner_burgerservicenummer'       => 'personal_number',
            'partner_voorvoegsel'               => 'prefix',
            'partner_geslachtsnaam'             => 'family_name',
        },
        search_filter   => {
            'subject.geslachtsnaam'                             => 'ilike',
            'subject.voornamen'                                 => 'ilike',
            'subject.address_residence.straatnaam'              => 'ilike',
            'subject.address_residence.postcode'                => sub { my $z = shift; $z =~ s/\s+//g; return uc($z) }, # drop spaces from zipcode
            'subject.address_residence.adres_buitenland1'       => 'like',
            'subject.address_residence.adres_buitenland2'       => 'like',
            'subject.address_residence.adres_buitenland3'       => 'like',
            'subject.address_correspondence.straatnaam'         => 'ilike',
            'subject.address_correspondence.postcode'           => sub { my $z = shift; $z =~ s/\s+//g; return uc($z) }, # drop spaces from zipcode
            'subject.address_correspondence.adres_buitenland1'  => 'like',
            'subject.address_correspondence.adres_buitenland2'  => 'like',
            'subject.address_correspondence.adres_buitenland3'  => 'like',

        },
        profiles        => {
            save    => {
                required    => {
                    family_name         => 'Defined',
                },
                # Initials are optional
                optional => {
                    initials      => 'Defined',
                    gender        => 'Defined',
                    date_of_birth => 'Defined',
                },
                msgs    => sub {
                    my $dfv     = shift;
                    my $rv      = {};

                    return $rv unless $dfv->{_custom_messages};

                    for my $key (keys %{ $dfv->{_custom_messages} }) {
                        $rv->{$key} = $dfv->{_custom_messages}->{$key};
                    }

                    return $rv;
                }
            }
        }
    },
    company => {
        table_prefix    => 'bedrijf',
        mapping         => {
            dossiernummer           => 'coc_number',
            vestigingsnummer        => 'coc_location_number',
            # TODO:
            rsin                    => 'rsin',
            oin                     => 'oin',

            date_registration       => 'date_registration',
            date_founded            => 'date_founded',
            date_ceased             => 'date_ceased',

            handelsnaam             => 'company',
            rechtsvorm              => 'company_type',
        },
        address_mapping => {
            straatnaam              => 'street',
            huisnummer              => 'street_number',
            huisnummertoevoeging    => 'street_number_suffix',
            huisletter              => 'street_number_letter',
            postcode                => 'zipcode',
            woonplaats              => 'city',
            adres_buitenland1       => 'foreign_address_line1',
            adres_buitenland2       => 'foreign_address_line2',
            adres_buitenland3       => 'foreign_address_line3',
            landcode                => 'country',
        },
        mapper          => {
            'subject.address_residence'         => 'address_mapping',
            'subject.address_correspondence'    => 'address_mapping',
            'subject'                           => 'mapping',
        },
        search_filter   => {
            'subject.handelsnaam'                               => 'ilike',
            'subject.address_residence.straatnaam'              => 'ilike',
            'subject.address_residence.adres_buitenland1'       => 'ilike',
            'subject.address_residence.adres_buitenland2'       => 'ilike',
            'subject.address_residence.adres_buitenland3'       => 'ilike',
            'subject.address_correspondence.straatnaam'         => 'ilike',
            'subject.address_correspondence.adres_buitenland1'  => 'ilike',
            'subject.address_correspondence.adres_buitenland2'  => 'ilike',
            'subject.address_correspondence.adres_buitenland3'  => 'ilike',
        },
        profiles        => {
            save    => {
                required    => {
                    coc_number => sub {
                        my ($dfv, $val) = @_;

                        # TODO: elfproef it
                        return unless $val =~ /^\d{7,9}$/;

                        return 1;

                    },
                    company => 'Str',
                },
                optional => {
                    'coc_location_number' => 'Int',
                },
                msgs    => sub {
                    my $dfv     = shift;
                    my $rv      = {};

                    return $rv unless $dfv->{_custom_messages};

                    for my $key (keys %{ $dfv->{_custom_messages} }) {
                        $rv->{$key} = $dfv->{_custom_messages}->{$key};
                    }

                    return $rv;
                }
            }
        }
    },
    employee => {
        table_prefix    => 'subject',
        mapping         => {
            initials                => 'initials',
            sn                      => 'surname',
            displayname             => 'display_name',
            givenname               => 'first_names',
            username                => 'username',
        },
    },
};

=head2 SUBJECT_MAPPING

    {
        natuurlijk_persoon      => 'person',
        ...
    }

Maps table names to object types.

=cut

use constant SUBJECT_MAPPING => {
    natuurlijk_persoon      => 'person',
    bedrijf                 => 'company',
    subject                 => 'employee',
};

=head2 TYPE_TO_SCHEMA

    {
        person  => 'NatuurlijkPersoon'
        ...
    }
    ...

Maps object types to DBIx::Class Schema's.

=cut


use constant TYPE_TO_SCHEMA => {
    person                  => 'NatuurlijkPersoon',
    company                 => 'Bedrijf',
    employee                => 'Subject',
};

=head2 REMOTE_SEARCH_MODULE_NAME_KVKAPI

Enum constant for KvK API remote searches

=cut

use constant REMOTE_SEARCH_MODULE_NAME_KVKAPI => 'kvkapi';

=head2 REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO

Enum constant for Overheid IO remote searches

=cut

use constant REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO => 'overheidio';

=head2 REMOTE_SEARCH_MODULE_NAME_STUFNP

Enum constant for StUF NP remote searches

=cut

use constant REMOTE_SEARCH_MODULE_NAME_STUFNP => 'stufconfig';

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2019 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

