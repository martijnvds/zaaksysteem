package Zaaksysteem::Filestore::Engine::UStore;
use Moose;
use namespace::autoclean;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Filestore::Engine
);

use File::UStore;

=head1 NAME

Zaaksysteem::Filestore::Engine::UStore - UStore local file store backend

=head1 DESCRIPTION

This file store engine writes and reads files on the local disk, separated into
several levels of subdirectories using L<File::UStore>.

=head1 ATTRIBUTES

=head2 local_storage_path

The "root" of the file storage. L<File::UStore> will create subdirectories here as needed.

=cut

has local_storage_path => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 accelerated_download_root

Location where the web server looks for files when doing accelerated downloads.

The Nginx configuration should look something like this:

    location /download/ustore {
        internal;
        root /whatever/you/set/this/to;
    }

This means it should be set to a directory that contains L</local_storage_path>
(several levels deeper).

=cut

has accelerated_download_root => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 ustore

A L<File::UStore> instance to use. Supplying this is not required, as it's
built lazily from L<local_storage_path> as needed.

=cut

has ustore => (
    is      => 'ro',
    lazy    => 1,
    builder => '_build_ustore',
);

=head1 METHODS

=head2 download_url(uuid)

Returns an URL that can be put in the C<X-Accel-Redirect> header, which is used
by Nginx to offload the actual file transfer (so it doesn't take up a backend
process).

=cut

sub download_url {
    my $self = shift;
    my $uuid = shift;

    my $ustore_path = $self->ustore->getPath(lc($uuid));
    my $accel_root  = $self->accelerated_download_root;

    $ustore_path =~ s/^\Q$accel_root\E//;

    return "/download/ustore/$ustore_path";
}

=head2 write($uuid, $fh)

Write a new file to storage, using the supplied UUID as a key, and the supplied
file handle to get file contents.

=cut

sub write {
    my $self = shift;
    my ($uuid, $fh) = @_;

    {
        # A bit of an ugly hack, because File::UStore insists on generating its own UUID.
        no warnings 'redefine';
        local *UUID::unparse = sub {
            $_[1] = lc($uuid);
            return;
        };

        $self->log->trace("Adding file with UUID = $uuid");

        my $added = $self->ustore->add($fh);

        $self->log->trace("UUID after adding = $added");
    }

    return $uuid;
}

=head2 erase($uuid)

Erases a file from the FileStore or Bucket

=cut

sub erase {
    my $self = shift;
    my ($uuid) = @_;

    $self->log->info(sprintf(
        "Erasing object '%s' from UStore",
        $uuid,
    ));

    return $self->ustore->remove(lc($uuid));

}

=head2 get_fh($id)

Returns an open file handle to the stored file.

=cut

sub get_fh {
    my $self = shift;
    my $uuid = shift;

    return $self->ustore->get($uuid);
}

=head2 get_path($id)

Returns the location of the stored file on the local file system.

=cut

sub get_path {
    my $self = shift;
    my $uuid = shift;

    return $self->ustore->getPath(lc($uuid));
}

=head2 _build_ustore

Build a new L<File::UStore> instance based on the L<local_storage_path>.

=cut

sub _build_ustore {
    my $self = shift;
    return File::UStore->new(
        path   => $self->local_storage_path,
        prefix => 'zs_',
        depth  => 5,
    );
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
