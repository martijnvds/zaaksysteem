// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.dom.getParents', function () {
    return function (element) {
      var parents = [];
      while (element) {
        parents.push(element.parentElement);
        element = element.parentElement;
      }

      return parents;
    };
  });
})();
