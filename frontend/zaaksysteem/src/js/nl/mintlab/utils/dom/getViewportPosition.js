// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.dom.getViewportPosition', function () {
    return function (element) {
      var clientRect, rect;
      if (!element) {
        return { x: 0, y: 0 };
      }
      clientRect = element.getBoundingClientRect();

      rect = {
        x: clientRect.left,
        y: clientRect.top,
        top: clientRect.top,
        left: clientRect.left,
        bottom: clientRect.bottom,
        width: clientRect.width,
        height: clientRect.height,
      };

      if (rect.width === undefined) {
        rect.width = element.clientWidth;
        rect.height = element.clientHeight;
      }

      rect.right = rect.left + rect.width;

      return rect;
    };
  });
})();
