// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  window.zsDefine('nl.mintlab.utils.buildUrl', function () {
    var parseUrlParams = window.zsFetch('nl.mintlab.utils.parseUrlParams');

    return function (url, params) {
      var parts, val;

      if (!params) {
        return url;
      }

      parts = [];
      params = _.merge(parseUrlParams(url), params);

      url = url.replace(/\?(.*)$/, '');

      for (var key in params) {
        val = params[key];
        if (_.isArray(val)) {
          _.each(val, function (v) {
            parts.push(key + '[]=' + encodeURIComponent(v));
          });
        } else {
          parts.push(key + '=' + encodeURIComponent(params[key]));
        }
      }

      if (parts.length) {
        url += '?' + parts.join('&');
      }

      return url;
    };
  });
})();
