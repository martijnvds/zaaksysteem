// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsAlwaysVisibleReference', [
    function () {
      return {
        require: '^zsAlwaysVisibleParent',
        link: function (scope, element, attrs, parent) {
          parent.setElement(element);

          scope.$watch(function () {
            parent.trigger();
          });

          scope.$on('$destroy', function () {
            if (parent.getElement() === element) {
              parent.setElement(null);
            }
          });
        },
      };
    },
  ]);
})();
