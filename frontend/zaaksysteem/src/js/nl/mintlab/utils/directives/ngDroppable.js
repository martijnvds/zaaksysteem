// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.directives').directive('ngDroppable', [
    '$document',
    'dropManager',
    function ($document, dropManager) {
      var element = $document[0].createElement('div'),
        hasPartialDragNDropSupport,
        hasFullDragNDropSupport,
        defaultMimetype = 'text/json',
        indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf'),
        link;

      hasFullDragNDropSupport = 'draggable' in element;
      hasPartialDragNDropSupport =
        !hasFullDragNDropSupport && !!element.dragDrop;

      if (hasFullDragNDropSupport) {
        link = function (scope, element, attrs) {
          var _enabled = false,
            dragSource;

          function evaluateDirective() {
            var shouldBeEnabled =
              attrs.ngDroppable !== '' ? scope.$eval(attrs.ngDroppable) : true;

            if (shouldBeEnabled === undefined) {
              shouldBeEnabled = true;
            }

            if (shouldBeEnabled !== _enabled) {
              if (shouldBeEnabled) {
                enable();
              } else {
                disable();
              }
            }
          }

          function enable() {
            _enabled = true;
            element.bind('dragenter', onDragEnter);
            element.bind('dragover', onDragOver);
            element.bind('dragleave', onDragLeave);
            element.bind('drop', onDrop);
          }

          function disable() {
            _enabled = false;
            element.unbind('dragenter', onDragEnter);
            element.unbind('dragover', onDragOver);
            element.unbind('dragleave', onDragLeave);
            element.unbind('drop', onDrop);
          }

          function onDragEnter(event) {
            dragSource = event.target;

            if (isValidDrag(event)) {
              enterDragMode(event);
              return false;
            }
          }

          function onDragOver(event) {
            if (event.target !== dragSource) {
              return;
            }

            if (isValidDrag(event)) {
              enterDragMode(event);
              return false;
            }
          }

          function onDragLeave(event) {
            if (event.target !== dragSource) {
              return;
            }

            dragSource = null;

            exitDragMode(event);
          }

          function enterDragMode(event) {
            event.preventDefault();
            event.stopPropagation();

            element.addClass('drag-over');
          }

          function exitDragMode(/*event*/) {
            element.removeClass('drag-over');
          }

          function isValidDrag(event) {
            var mimetype = attrs.ngDropMimetype || defaultMimetype,
              dataTransfer = event.dataTransfer,
              isValidMimetype = false,
              drop = dropManager.getCurrentDrop();

            if (mimetype === '*' && hasFullDragNDropSupport) {
              isValidMimetype = true;
            } else if (drop && hasFullDragNDropSupport) {
              isValidMimetype = drop.mimetype === mimetype;
            } else if (hasFullDragNDropSupport) {
              if (angular.isArray(dataTransfer.types)) {
                isValidMimetype = indexOf(dataTransfer.types, mimetype) !== -1;
              } else if ('contains' in dataTransfer.types) {
                isValidMimetype = dataTransfer.types.contains(mimetype);
              }
            }

            return isValidMimetype;
          }

          function onDrop(event) {
            var mimetype = attrs.ngDropMimetype || defaultMimetype,
              data;

            if (!isValidDrag(event)) {
              return;
            }

            if (mimetype === 'Files') {
              data = event.dataTransfer;
            } else if (
              mimetype === '*' &&
              event.dataTransfer.types.includes(['Files'])
            ) {
              data = event.dataTransfer;
              mimetype = 'Files';
            } else {
              data = dropManager.getCurrentDrop().data;
            }

            scope.$emit('drop', data, mimetype);

            exitDragMode();

            event.preventDefault();
            event.stopPropagation();
          }

          scope.$watch('ngDroppable', function () {
            evaluateDirective();
          });
        };
      } else {
        link = function (scope, element, attrs) {
          var _enabled;

          function evaluateDirective() {
            var shouldBeEnabled =
              attrs.ngDroppable !== '' ? scope.$eval(attrs.ngDroppable) : true;
            if (shouldBeEnabled === undefined) {
              shouldBeEnabled = true;
            }

            if (shouldBeEnabled !== _enabled) {
              if (shouldBeEnabled) {
                enable();
              } else {
                disable();
              }
            }
          }

          function enable() {
            _enabled = true;
            dropManager.register(element);
          }

          function disable() {
            _enabled = false;
            dropManager.unregister(element);
          }

          scope.performDrop = function (data, mimetype) {
            data.fromDroppable = true;
            scope.$emit('drop', data, mimetype);
          };

          scope.$watch('ngDroppable', function () {
            evaluateDirective();
          });

          scope.$on('$destroy', function () {
            disable();
          });
        };
      }

      return link;
    },
  ]);
})();
