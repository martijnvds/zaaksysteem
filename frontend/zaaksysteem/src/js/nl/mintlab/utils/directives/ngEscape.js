// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem').directive('ngEscape', function () {
    return function (scope, element, attrs) {
      element.bind('keyup', function (event) {
        if (event.keyCode === 27) {
          scope.$eval(attrs.ngEscape);
        }
      });
    };
  });
})();
