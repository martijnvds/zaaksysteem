// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.widget.search')
    .controller('nl.mintlab.widget.search.SearchWidgetSaveController', [
      '$scope',
      '$window',
      '$timeout',
      '$q',
      'searchService',
      'systemMessageService',
      'smartHttp',
      'translationService',
      function (
        $scope,
        $window,
        $timeout,
        $q,
        searchService,
        systemMessageService,
        smartHttp,
        translationService
      ) {
        var defaultFilters = [],
          loadedFilters = [],
          loadingFilter = null,
          allFiltersLoaded = false;

        $scope.filters = [];
        $scope.loaded = false;
        ($scope.hasMoreFilters = false),
          ($scope.collapsed = false),
          ($scope.loading = false),
          ($scope.showAllSearches = false);

        function initialize() {
          var lastFilter = null,
            filterId = $scope.searchId || $window.location.hash,
            selected,
            allFilters;

          defaultFilters = _.map($scope.filters, function (filter) {
            var search = getDefaultSearch();

            if (typeof filter.values.query === 'string') {
              filter.values = JSON.parse(filter.values.query);
            }

            _.merge(search, filter);

            search.values.predefined = true;

            return search;
          });

          allFilters = $scope.getFilters();

          $scope.loaded = true;

          $scope.toggleSearches = function () {
            $scope.showAllSearches = !$scope.showAllSearches;
          };

          if (filterId) {
            selected = _.find(allFilters, { id: filterId.replace('#', '') });
            if (selected && (!lastFilter || lastFilter.id !== selected.id)) {
              lastFilter = null;
            }
          }

          if (!selected && lastFilter) {
            selected = _.find(allFilters, { id: lastFilter.id });
          }

          if (!selected) {
            lastFilter = null;
            selected = allFilters[0];
          }

          $scope.setSearch(selected).then(function () {
            if (lastFilter) {
              $scope.updateColumns(lastFilter.values.columns);

              $scope.setActiveValues(lastFilter.values.values);
              $scope.setActiveColumns(lastFilter.values.columns);
              $scope.setActiveSort(lastFilter.values.options.sort);
              $scope.setLocationVisibility(
                lastFilter.values.options.showLocation
              );
              $scope.setActiveObjectType(lastFilter.values.objectType);
            }
          });
        }

        function getDefaultSearch() {
          var search = $scope.search.createFilter();
          search.values.label = translationService.get('Nieuwe zoekopdracht');
          search.security_rules.length = 0;

          return search;
        }

        function getSavedSearches() {
          var limit = $scope.getLimit();
          var amount =
            typeof limit !== 'undefined'
              ? limit - $scope.filters.length
              : undefined;
          return $scope.search.getSavedFilters(
            { baseUrl: $scope.getApiBaseUrl() + '/search' },
            amount
          );
        }

        function getSavedSearchByUuid() {
          return $scope.search.getSavedFilterByUuid(
            { baseUrl: $scope.getApiBaseUrl() + '/search' },
            $scope.searchId
          );
        }

        $scope.getFilters = function () {
          return defaultFilters.concat(loadedFilters);
        };

        $scope.getLimit = function () {
          return !$scope.showAllSearches ? 8 : undefined;
        };

        $scope.createSearch = function () {
          var search = getDefaultSearch();

          $scope.showAllSearches = true;

          $scope.addSearch(search);
          $scope.setSearch(search);

          return search;
        };

        $scope.addSearch = function (search) {
          $scope.search.addFilter(search);
        };

        $scope.removeSearch = function (search) {
          $scope.search.deleteFilter(search);
          if (search === $scope.activeSearch) {
            $scope.setSearch($scope.getFilters()[0]);
          }
        };

        $scope.handleFilterClick = function (filter) {
          loadingFilter = filter.id;
          $scope.searchId = filter.id;

          // Replace the url with the active search in order to allow the
          // user to refresh the page without losing the active search
          if (window.history.replaceState) {
            window.history.replaceState(
              null,
              document.title,
              '/search/' + $scope.searchId
            );
          }

          // use a $timeout to allow the browser to visually update elements
          // before processing heavy js ops
          function setFilter() {
            $timeout(function () {
              $scope.setSearch(filter).then(function () {
                loadingFilter = null;
              });
            }, 100);
          }

          if (
            !angular.equals(filter.values.objectType, $scope.getObjectType()) &&
            filter.values.objectType
          ) {
            searchService
              .getConfig(filter.values.objectType.object_type)
              .then(function () {
                setFilter();
              });
          } else {
            setFilter();
          }
        };

        $scope.isFilterLoading = function (filter) {
          return loadingFilter === filter.id;
        };

        $scope.handleUnload = function (/*$event*/) {
          // disable saving of current filter to localStorage,
          // as it was doing more harm than good
        };

        $scope.loadFilters = function () {
          var searchFunction =
            !$scope.showAllSearches && $scope.searchId !== ''
              ? getSavedSearchByUuid
              : getSavedSearches;

          if (allFiltersLoaded) {
            $scope.hasMoreFilters = true;
            return $q.when([]);
          }

          $scope.loading = true;

          return (!$scope.isPublic()
            ? searchFunction()['catch'](function () {
                systemMessageService.emitLoadError('de zoekopdrachten');
                return $q.reject();
              })
            : $q.when([])
          ).then(function (result) {
            loadedFilters = result.filters || [];
            $scope.loading = false;
            allFiltersLoaded = $scope.showAllSearches === true;
            $scope.hasMoreFilters = result.hasNext;
          });
        };

        // Load filters after default filters have been set from zs-scope-data block.
        // Otherwise we don't know the amount of default searches
        $scope.$watch('filters', function () {
          $scope.loadFilters().then(initialize);
        });

        $scope.$watch('showAllSearches', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            $scope.loadFilters();
          }
        });
      },
    ]);
})();
