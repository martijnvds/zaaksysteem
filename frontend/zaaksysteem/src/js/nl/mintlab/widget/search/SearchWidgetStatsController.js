// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.widget.search')
    .controller('nl.mintlab.widget.search.SearchWidgetStatsController', [
      '$scope',
      '$http',
      'systemMessageService',
      function ($scope, $http, systemMessageService) {
        var ctrl = this,
          loading = true,
          count = NaN,
          where;

        ctrl.isLoading = function () {
          return loading;
        };

        ctrl.getCount = function () {
          var label = count;
          if (isNaN(count)) {
            label = '-';
          }
          return label;
        };

        where = $scope.getZqlWhere() || '';
        if (where) {
          where = ' WHERE ' + where;
        }

        $http({
          method: 'GET',
          url: '/api/object/search',
          params: {
            zql: 'COUNT ' + $scope.getObjectType().object_type + where,
          },
        })
          .success(function (response) {
            count = response.result[0].count;
          })
          .error(function (/*response*/) {
            systemMessageService.emitLoadError('statistieken');
          })
          ['finally'](function () {
            loading = false;
          });
      },
    ]);
})();
