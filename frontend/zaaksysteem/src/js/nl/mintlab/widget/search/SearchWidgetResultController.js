// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.widget.search')
    .controller('nl.mintlab.widget.search.SearchWidgetResultController', [
      '$scope',
      '$parse',
      '$interpolate',
      'userService',
      'searchService',
      'formService',
      'smartHttp',
      function (
        $scope,
        $parse,
        $interpolate,
        userService,
        searchService,
        formService,
        smartHttp
      ) {
        var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
          crud = null,
          numRows = 20,
          crudItems = [],
          count = 0,
          maxCount = searchService.getMaxResults();

        numRows = userService.getSetting('search.numPerPage') || numRows;

        if (/maponly=1/.test(window.location.href)) {
          $scope.displayMode = 'map';
          var interval = setInterval(function () {
            document.querySelector('i.mdi-map').click();
            if (document.querySelector('.ezra_map-initialized')) {
              clearInterval(interval);
            }
          }, 2500);

          style = document.createElement('style');
          style.type = 'text/css';
          style.appendChild(
            document.createTextNode(
              '.top-bar{display:none}.widget-search-filter-component{display:none}.widget-search-result-crud{display:none}.widget-search-result-header-wrapper{display:none}#contextual-action-menu{display:none}.widget-search-result{padding:0}#contentwrapper{margin-top:0}.ezra_map-viewport{height:100vh!important}.widget-search-result-display{padding:0}.widget-search-result{height: 100%}'
            )
          );

          document.head.appendChild(style);
        } else {
          $scope.displayMode = 'list';
        }

        $scope.$on('zs.pagination.numrows.change', function (event, rows) {
          userService.setSetting('search.numPerPage', rows);
        });

        var mapIframe = document.createElement('iframe');
        var mapConfigGeojson;
        var getGeoFeaturesCall;

        smartHttp
          .connect({
            method: 'GET',
            url: '/api/v1/map/ol_settings',
          })
          .success(function (response) {
            const map_center = response.result.instance.map_center;
            const wms_layers = response.result.instance.wms_layers;
            const map_application_url =
              response.result.instance.map_application_url;
            const map_application = response.result.instance.map_application;
            const mapConfig = {
              center: map_center.split(',').map(Number),
              appUrl:
                map_application === 'external'
                  ? map_application_url
                  : window.location.origin +
                    '/external-components/index.html?component=map',
              wmsLayers: wms_layers
                .filter(function (layer) {
                  return layer.instance.active;
                })
                .map(function (layer) {
                  return {
                    url: layer.instance.url,
                    layers: layer.instance.layer_name,
                    label: layer.instance.label,
                  };
                }),
            };

            mapIframe.src = mapConfig.appUrl;
            mapIframe.style.width = '100%';
            mapIframe.style.height = '450px';
            mapIframe.title = 'advancedSearchMap';
            mapIframe.id = 'advancedSearchMap';
            mapIframe.frameBorder = '0';
            mapIframe.allow = 'fullscreen';
            mapIframe.allowFullscreen = true;
            mapIframe.addEventListener('load', function () {
              const postInit = function () {
                mapIframe.contentWindow.postMessage(
                  {
                    type: 'init',
                    name: 'advancedSearchMap',
                    version: 5,
                    value: {
                      initialFeature: mapConfigGeojson,
                      center: mapConfig.center,
                      wmsLayers: mapConfig.wmsLayers,
                      canDrawFeatures: false,
                      canSelectLayers: true,
                    },
                  },
                  '*'
                );
              };
              mapConfigGeojson
                ? postInit()
                : getGeoFeaturesCall.success(function () {
                    setTimeout(postInit, 0);
                  });
            });
          });

        function getPublicColumns(cols) {
          return cols
            .filter(function (column) {
              return (
                ['actions', 'case.status', 'notifications'].indexOf(
                  column.id
                ) === -1
              );
            })
            .map(function (column) {
              var template = column.template;

              switch (column.id) {
                case 'case.number':
                case 'case.subject':
                  template = undefined;
                  break;
              }

              switch (column.attributeType) {
                case 'file':
                  template =
                    '<ul class="search-column-type-list-list">' +
                    '<li class="search-column-type-list-list-item" data-ng-repeat="file in item.values[column.id]">' +
                    '<a data-ng-href="' +
                    $scope.getApiBaseUrl() +
                    '/file/<[file.uuid]>"><[file.filename||file.original_name]></a>' +
                    '</li>' +
                    '</ul>';
                  break;
              }

              if (template) {
                column.templateUrl = null;
              }

              return _.assign(column, {
                locked: true,
                sort: false,
                template: template,
              });
            });
        }

        function rebuildCrud() {
          var form = formService.get('searchWidgetForm'),
            columns = $scope.getActiveColumns();

          if (!form || !$scope.activeSearch) {
            return null;
          }

          crud = {
            url: $scope.getCrudUrl(),
            options: {
              select: $scope.isPublic() ? 'none' : 'all',
              link: $scope.getItemLink(),
              resolve: $scope.getItemResolve(),
              sort: $scope.getActiveSort(),
            },
            actions: $scope.getActions() || [],
            columns: $scope.isPublic() ? getPublicColumns(columns) : columns,
            style: $scope.getItemStyle(),
            numrows: numRows,
          };
        }

        function hasOpenGroup() {
          return !!_.find($scope.groups, function (group) {
            return group.value === $scope.openGroup;
          });
        }

        $scope.getCrud = function (group) {
          if (group.value !== $scope.openGroup) {
            return null;
          }
          return crud;
        };

        $scope.onGroupClick = function (group) {
          if ($scope.openGroup === group.value) {
            $scope.setOpenGroup(null);
          } else {
            $scope.setOpenGroup(group.value);
          }
        };

        $scope.setDisplayMode = function (mode) {
          $scope.displayMode = mode;
        };

        $scope.getLocations = function () {
          var locations = [],
            mapConfig = $scope.map || {},
            locationResolve = mapConfig.resolve,
            parser = $parse(locationResolve),
            location;

          if (!locationResolve) {
            return locations;
          }

          _.each(crudItems, function (crudItem) {
            var lat,
              lng,
              data = {},
              mapping = mapConfig.mapping || {};

            location = parser(crudItem);

            if (location) {
              lat = parseFloat(location.replace('(', '').split(',')[0]);
              lng = parseFloat(location.replace(')', '').split(',')[1]);

              for (var key in mapping) {
                data[key] = $parse(mapping[key])(crudItem);
              }

              locations.push({
                item: crudItem,
                marker: {
                  latitude: lat,
                  longitude: lng,
                  popup_data: data,
                  no_popup: 1,
                },
              });
            }
          });

          return locations;
        };

        $scope.handleCrudDataChange = function ($response) {
          count = $response.num_rows;
          const mapPlaceholder = document.querySelector(
            '.widget-search-result-map'
          );
          const mapVisible = Boolean(
            document.querySelector('#advancedSearchMap')
          );

          if (mapPlaceholder || mapVisible) {
            getGeoFeaturesCall = smartHttp
              .connect({
                method: 'GET',
                url: '/api/v2/geo/get_geo_features',
                params: {
                  uuid: $response.result
                    .map(function (el) {
                      return el.id;
                    })
                    .join(','),
                },
              })
              .success(function (res) {
                const legacyPoints = [].concat.apply(
                  [],
                  $response.result
                    .map(function (result) {
                      return _.values(result.values)
                        .map(function (val) {
                          const address = _.get(val, 'address_data');

                          if (!address) return undefined;

                          const title = {
                            text: _.get(result.values, 'case.number'),
                            href:
                              '/intern/zaak/' +
                              _.get(result.values, 'case.number'),
                          };
                          const rows = [
                            {
                              title: 'Zaaktype',
                              text: _.get(result.values, 'case.casetype.name'),
                            },
                            {
                              title: 'Adres',
                              text: _.get(val, 'human_identifier'),
                            },
                            {
                              title: 'Aanvrager',
                              text: _.get(result.values, 'case.requestor.name'),
                            },
                            {
                              title: 'Behandelaar',
                              text: _.get(result.values, 'case.assignee', '-'),
                            },
                          ];

                          const geometry = {
                            type: 'Point',
                            coordinates: _.get(address, 'gps_lat_lon')
                              .split(',')
                              .map(Number)
                              .reverse(),
                          };

                          const feature = {
                            type: 'Feature',
                            properties: { popup: { title, rows } },
                            geometry: geometry,
                          };

                          return feature;
                        })
                        .filter(Boolean);
                    })
                    .filter(Boolean)
                );

                const v2Features =
                  res && res.data
                    ? res.data.reduce(function (acc, v2GeoFeature) {
                        const features =
                          v2GeoFeature.attributes.geo_json.features;

                        features.forEach(function (feature) {
                          const origin = feature.properties.zaaksysteem.origin;
                          if (origin.family === 'case') {
                            feature.properties.popup = {
                              title: {
                                text: origin.title,
                                href: '/intern/zaak' + origin.title,
                              },
                              rows: [
                                { title: 'Zaaktype', text: origin.type },
                                { title: 'Status', text: origin.status },
                                {
                                  title: 'Extra informatie',
                                  text: origin.subtitle,
                                },
                                {
                                  title: 'Aanvrager',
                                  text: origin.assignee,
                                },
                                {
                                  title: 'Behandelaar',
                                  text: origin.reqestor || '-',
                                },
                              ],
                            };
                          } else if (origin.family === 'object') {
                            feature.properties.popup = {
                              title: {
                                text: origin.title,
                                href: '/main/object/' + origin.identifier,
                              },
                              rows: [
                                { title: 'Objecttype', text: origin.type },
                                { title: 'Status', text: origin.status },
                              ],
                            };
                          }
                        });
                        return acc.concat(features);
                      }, [])
                    : [];

                const geojson = {
                  type: 'FeatureCollection',
                  features: v2Features.concat(legacyPoints),
                };

                if (mapPlaceholder) {
                  mapConfigGeojson = geojson;
                  mapPlaceholder.parentNode.replaceChild(
                    mapIframe,
                    mapPlaceholder
                  );
                } else {
                  mapIframe.contentWindow.postMessage(
                    {
                      type: 'setFeature',
                      name: 'advancedSearchMap',
                      version: 5,
                      value: geojson,
                    },
                    '*'
                  );
                }
              });
          }
        };

        $scope.getNumResults = function () {
          var countLabel = count;

          if (count > maxCount) {
            countLabel = maxCount + '+';
          }

          return countLabel;
        };

        $scope.hasResults = function () {
          return !$scope.isLoading() && !isNaN(count) && hasOpenGroup();
        };

        this.exceedsLimit = function () {
          return searchService.getMaxResults() < count;
        };

        $scope.$on('activeSearch.change', function () {
          $scope.displayMode = 'list';
        });

        $scope.$watch(
          'groups',
          function onGroupChange() {
            if (!hasOpenGroup()) {
              $scope.setOpenGroup(
                $scope.groups.length === 1 ? $scope.groups[0].value : null
              );
            }
          },
          true
        );

        $scope.$watch(function onChange() {
          rebuildCrud();
        });

        $scope.$on('zs.pagination.numrows.change', function onNumRowsChange(
          event,
          nr
        ) {
          safeApply($scope, function () {
            numRows = nr;
          });
        });

        $scope.$on('zs.crud.item.change', function (event, items) {
          count = NaN;
          crudItems = items;
        });
      },
    ]);
})();
