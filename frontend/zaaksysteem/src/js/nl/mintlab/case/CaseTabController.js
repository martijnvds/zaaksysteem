// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.CaseTabController', [
      '$scope',
      function ($scope) {
        $scope.$watch('notificationCount', function () {
          if ($scope.notificationCount) {
            $scope.setNotificationCount(
              $scope.phaseId,
              $scope.notificationCount
            );
          }
        });
      },
    ]);
})();
