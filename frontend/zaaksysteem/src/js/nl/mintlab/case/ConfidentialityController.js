// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.ConfidentialityController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        function parseOptions(opts) {
          var options = [];
          for (var key in opts) {
            options.push({
              value: key,
              label: opts[key],
            });
          }
          $scope.options = options;
        }

        function save() {
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/case/' + $scope.caseId + '/confidentiality',
              data: {
                confidentiality: $scope.confidentiality,
              },
            })
            .success(function (/* response */) {
              // when confidentiality changes, rules may be attached to it.
              // this reloads every webform (phaseform) to look for
              // potential changes. we could implement an invalidator
              // function per weboform, that would trigger the reload
              // only when the form is visible. furthermore it could
              // ask the server if something changes, if not, no reload
              // would be necessary.
              // i vote to save this for a more thorough overhaul where
              // the fields are shown using angular.
              //
              // See: https://mintlab.atlassian.net/browse/ZS-2073
              //
              $('form.webform').each(function (index, form) {
                loadWebform({ form: $(form) });
              });
            })
            .error(function (/*response*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
              });
            });
        }

        $scope.getLabel = function () {
          var option,
            label = '';

          option = _.find($scope.options, function (opt) {
            return opt.value === $scope.confidentiality;
          });

          if (option) {
            label = option.label;
          }

          return label;
        };

        $scope.isConfidential = function () {
          return $scope.confidentiality === 'confidential';
        };

        $scope.$watch('constants', function (nwVal /*, oldVal, scope*/) {
          parseOptions(nwVal);
        });

        $scope.$watch('confidentiality', function (nwVal, oldVal /*, scope*/) {
          if (oldVal && nwVal !== oldVal) {
            save();
          }
        });
      },
    ]);
})();
