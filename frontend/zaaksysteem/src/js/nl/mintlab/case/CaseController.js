// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.CaseController', [
      '$scope',
      'smartHttp',
      'translationService',
      function ($scope, smartHttp, translationService) {
        var notificationCountByPhaseId = {};

        $scope.templates = [];

        function loadTemplates() {
          var caseId = $scope.caseId;

          smartHttp
            .connect({
              url: 'zaak/' + caseId + '/get_sjablonen',
              method: 'GET',
            })
            .success(function onSuccess(data) {
              $scope.templates = data.result;
            })
            .error(function onError(/*data*/) {
              $scope.$emit('systemMessage', {
                content: translationService.get(
                  'Fout bij het laden van de sjablonen'
                ),
                type: 'error',
              });
            });
        }

        $scope.init = function (caseId) {
          $scope.caseId = caseId;
          loadTemplates();
        };

        $scope.setNotificationCount = function (phaseId, count) {
          notificationCountByPhaseId[phaseId] = count;
        };

        $scope.getNotificationCount = function (phaseId) {
          return notificationCountByPhaseId[phaseId] || 0;
        };
      },
    ]);
})();
