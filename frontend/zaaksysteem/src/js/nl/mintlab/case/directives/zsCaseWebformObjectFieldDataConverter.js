// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.case')
    .directive('zsCaseWebformObjectFieldDataConverter', [
      function () {
        return {
          require: [
            'zsCaseWebformObjectFieldDataConverter',
            'zsCaseWebformObjectField',
          ],
          controller: [
            function () {
              var ctrl = this,
                zsCaseWebformObjectField,
                converted;

              ctrl.setControls = function () {
                zsCaseWebformObjectField = arguments[0];
                if (converted) {
                  zsCaseWebformObjectField.setList(converted);
                }
              };

              ctrl.setObjects = function (objects) {
                converted = _.map(objects, function (obj) {
                  return {
                    bag_id: obj.id,
                    human_identifier: obj.label,
                    address_data: obj,
                  };
                });

                if (zsCaseWebformObjectField) {
                  zsCaseWebformObjectField.setList(converted);
                }
              };

              return ctrl;
            },
          ],
          controllerAs: 'caseWebformObjectFieldDataConverter',
          link: function (scope, element, attrs, controllers) {
            controllers[0].setControls.apply(
              controllers[0],
              controllers.slice(1)
            );
          },
        };
      },
    ]);
})();
