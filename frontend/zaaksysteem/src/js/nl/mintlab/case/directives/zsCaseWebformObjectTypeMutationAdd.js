// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.case')
    .directive('zsCaseWebformObjectTypeMutationAdd', [
      '$parse',
      '$interpolate',
      'objectMutationService',
      function ($parse, $interpolate, objectMutationService) {
        var cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent');

        return {
          scope: true,
          require: [
            'zsCaseWebformObjectTypeMutationAdd',
            '^zsCaseWebformObjectTypeField',
          ],
          controller: [
            '$scope',
            '$attrs',
            function ($scope, $attrs) {
              var ctrl = this,
                zsCaseWebformObjectTypeField,
                options = angular.copy(
                  objectMutationService.getMutationOptions()
                ),
                form;

              ctrl.cancel = function () {
                ctrl.mutationType = null;
              };

              ctrl.confirm = function ($event) {
                var mutation = angular.copy(ctrl.mutation),
                  titleTemplate = zsCaseWebformObjectTypeField.getObjectType()
                    .values.title_template,
                  label;

                mutation.type = ctrl.mutationType;
                mutation.values = {};

                cancelEvent($event);

                switch (ctrl.mutationType) {
                  case 'create':
                  case 'update':
                    mutation.values = form.getValues();
                    break;
                }

                if (titleTemplate) {
                  titleTemplate = titleTemplate.replace(
                    /\[\[(.*?)\]\]/,
                    '<[values["attribute.$1"]]>'
                  );
                  label = $interpolate(titleTemplate)({
                    values: mutation.values,
                  });
                  mutation.label = label;
                }

                mutation.values = _.pick(mutation.values, _.identity);

                $parse($attrs.zsCaseWebformObjectTypeMutationAdd)($scope, {
                  $mutation: mutation,
                });

                ctrl.mutationType = null;
                ctrl.mutation = {};
              };

              ctrl.isValid = function () {
                var isValid = false;
                switch (ctrl.mutationType) {
                  case 'relate':
                  case 'delete':
                    isValid = !!ctrl.mutation.object_uuid;
                    break;

                  case 'update':
                    isValid = !!ctrl.mutation.object_uuid;
                    break;

                  case 'create':
                    isValid = true;
                    break;
                }
                return isValid;
              };

              ctrl.getSpotEnlighterParams = function () {
                return {
                  object_type: $scope.$eval($attrs.objectTypePrefix),
                };
              };

              ctrl.handleSpotEnlighterSelect = function ($object) {
                ctrl.mutation.object_uuid = $object.id;
                ctrl.mutation.label = $object.label;
                ctrl.mutation.values = {};

                _.each($object.object.values, function (value, key) {
                  ctrl.mutation.values['attribute.' + key] = value;
                });

                ctrl.newObject = null;
              };

              ctrl.link = function (controllers) {
                var metadata;

                zsCaseWebformObjectTypeField = controllers[0];

                metadata = zsCaseWebformObjectTypeField.getMetadata();

                _.each(options, function (option) {
                  var label = metadata[option.value + '_object_action_label'];

                  if (label) {
                    option.label = label;
                  }
                });
              };

              ctrl.getOptions = function () {
                return options;
              };

              ctrl.setForm = function (f) {
                form = f;
              };

              ctrl.getMutationValues = function () {
                return ctrl.mutation.values;
              };

              ctrl.handleOptionClick = function (option) {
                ctrl.mutation = {};
                if (ctrl.mutationType === option.value) {
                  ctrl.cancel();
                } else {
                  ctrl.mutationType = option.value;
                }
              };

              ctrl.isObjectVisible = function () {
                return ctrl.mutationType && ctrl.mutationType !== 'create';
              };

              ctrl.isFormVisible = function () {
                return (
                  (ctrl.mutationType === 'update' &&
                    ctrl.mutation.object_uuid) ||
                  ctrl.mutationType === 'create'
                );
              };

              ctrl.isSpotEnlighterVisible = function () {
                return (
                  ctrl.mutationType &&
                  ctrl.mutationType !== 'create' &&
                  !ctrl.mutation.object_uuid
                );
              };

              ctrl.isActionsVisible = function () {
                return !!ctrl.mutationType;
              };

              ctrl.removeObject = function () {
                ctrl.mutation.values = {};
                delete ctrl.mutation.object_uuid;
                delete ctrl.mutation.label;
              };

              ctrl.getVisibleOptions = function (filter) {
                return _.filter(options, function (option) {
                  return filter(option.value);
                });
              };

              ctrl.getSelectedOptionLabel = function () {
                var option = _.find(options, { value: ctrl.mutationType }),
                  label = '';

                if (option) {
                  label = option.label;
                }

                return label;
              };

              ctrl.mutationType = null;
              ctrl.mutation = {};

              return ctrl;
            },
          ],
          controllerAs: 'caseWebformObjectTypeMutationAdd',
          link: function (scope, element, attrs, controllers) {
            controllers[0].link(controllers.slice(1));
          },
        };
      },
    ]);
})();
