// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.locale').service('translationService', [
    function () {
      var labels = {};

      return {
        get: function (id) {
          var label = labels[id] !== undefined ? labels[id] : id,
            args = Array.prototype.slice.call(arguments, 1),
            ses = args.concat();

          if (args.length) {
            label = label.replace(/%(s|\d)/g, function (match, operator) {
              var r;
              if (operator === 's') {
                r = ses.shift();
              } else {
                r = args[match];
              }
              return r;
            });
          }

          return label;
        },
        set: function (id, value) {
          labels[id] = value;
        },
      };
    },
  ]);
})();
