// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.object.view')
    .directive('zsObjectActionListCasetype', [
      'plannedCaseService',
      function (plannedCaseService) {
        return {
          require: ['zsObjectActionListCasetype', '^zsObjectRelationList'],
          controller: [
            function () {
              var ctrl = this,
                zsObjectRelationList;

              function getCases() {
                return _.pluck(
                  zsObjectRelationList.getRelatedObjects({
                    id: 'case',
                  }),
                  'related_object'
                );
              }

              ctrl.link = function (controllers) {
                zsObjectRelationList = controllers[0];
              };

              ctrl.getCasetypes = function () {
                var casetypes;

                casetypes = _.pluck(
                  zsObjectRelationList.getRelatedObjects({
                    id: 'casetype',
                  }),
                  'related_object'
                );

                return casetypes;
              };

              ctrl.handlePlannedCaseSubmit = function ($values) {
                var caseId = $values['case'],
                  casetype = $values.casetype,
                  params = _.pick(
                    $values,
                    'interval_period',
                    'next_run',
                    'interval_value',
                    'runs_left',
                    'copy_relations'
                  ),
                  caseObj;

                caseObj = _.find(getCases(), { id: caseId });

                plannedCaseService.addPlannedCase(caseObj, casetype, params);
              };

              ctrl.isInherited = function (casetype) {
                var relations,
                  relation,
                  isInherited = false;

                if (zsObjectRelationList) {
                  relations = zsObjectRelationList.getRelatedObjects({
                    id: 'casetype',
                  });
                  relation = _.find(relations, {
                    related_object_id: casetype.id,
                    related_object_type: 'casetype',
                  });
                  if (relation) {
                    isInherited = relation.is_inherited;
                  }
                }

                return isInherited;
              };

              ctrl.canDelete = function (casetype) {
                var canDelete = !ctrl.isInherited(casetype);

                return canDelete;
              };

              ctrl.canAdd = function () {
                var canAdd = false;

                if (zsObjectRelationList) {
                  canAdd = zsObjectRelationList.isRelatableType('casetype');
                }

                return canAdd;
              };

              ctrl.removeRelation = function (casetype) {
                zsObjectRelationList.removeRelation(casetype);
              };

              ctrl.handleSelect = function ($object) {
                zsObjectRelationList.addRelation($object.object, 'casetype');
                ctrl.newObject = null;
              };

              ctrl.hasRelatedCases = function () {
                return ctrl.getCases().length > 0;
              };

              ctrl.getCases = function () {
                return getCases();
              };

              return ctrl;
            },
          ],
          link: function (scope, element, attrs, controllers) {
            controllers[0].link(controllers.slice(1));
          },
          controllerAs: 'objectActionListCasetype',
        };
      },
    ]);
})();
