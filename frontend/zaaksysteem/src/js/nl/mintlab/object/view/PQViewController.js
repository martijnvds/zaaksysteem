// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.object.view')
    .controller('nl.mintlab.object.view.PQViewController', [
      '$scope',
      '$parse',
      '$sce',
      'objectService',
      'translationService',
      'urlencodeFilter',
      function (
        $scope,
        $parse,
        $sce,
        objectService,
        translationService,
        urlencodeFilter
      ) {
        var ctrl = this,
          object = $scope.getObject(),
          activeTab,
          typesConfig,
          relatableTypes;

        ctrl.tabs = [];

        typesConfig = {
          faq: {
            label: 'Vragen',
            objectLabel: 'values.question',
            restrict: '',
            params: {
              object_type: 'faq',
            },
          },
          product: {
            label: 'Producten',
            objectLabel: 'values.name',
            restrict: '',
            params: {
              object_type: 'product',
            },
          },
          casetype: {
            label: 'Zaaktypen',
            objectLabel: 'zaaktype_node_id.titel',
          },
        };

        function addTab(id, label, fields) {
          ctrl.tabs.push({
            id: id,
            label: translationService.get(label),
            fields: fields,
          });
        }

        ctrl.getObjectTitle = function () {
          var title = '';

          switch (object.type) {
            case 'product':
              title = object.values.name;
              break;

            case 'faq':
              title = object.values.question;
              break;
          }

          return title;
        };

        ctrl.isActiveTab = function (id) {
          return id === activeTab;
        };

        ctrl.selectTab = function (id) {
          activeTab = id;
        };

        ctrl.getObjectRelationTypes = function () {
          return relatableTypes;
        };

        ctrl.getSpotEnlighterRestrict = function (type) {
          return type.restrict;
        };

        ctrl.getSpotEnlighterParams = function (type) {
          return type.params;
        };

        ctrl.getSpotEnlighterLabel = function (type) {
          return type.spotEnlighterLabel;
        };

        ctrl.getRelatedObjects = function (type) {
          var relations = _.filter(objectService.getRelations(object), {
              related_object_type: type.id,
            }),
            objects = _.pluck(relations, 'related_object');

          return objects;
        };

        ctrl.getRelatedObjectLabel = function (object, type) {
          return $parse(type.objectLabel)(object);
        };

        ctrl.getRelatedObjectLink = function (object, type) {
          var url;

          switch (type.id) {
            case 'casetype':
              url =
                '/zaak/create/?prefill_zaaktype_id=' +
                object.zaaktype_node_id.zaaktype_id +
                '&prefill_zaaktype_name=' +
                urlencodeFilter(object.zaaktype_node_id.titel) +
                '&aanvraag_trigger=' +
                object.zaaktype_node_id.trigger +
                '&related_object=' +
                object.id;
              break;

            default:
              url = '/object/' + object.id;
              break;
          }

          return url;
        };

        ctrl.getFields = function () {
          var activeTabObj = _.find(ctrl.tabs, { id: activeTab });
          return activeTabObj ? activeTabObj.fields : null;
        };

        switch (object.type) {
          case 'product':
            addTab('description', 'Omschrijving', [
              {
                label: translationService.get('Omschrijving'),
                resolve: 'description',
              },
              {
                label: translationService.get('Interne toelichting'),
                resolve: 'internal_explanation',
              },
            ]);

            addTab('requirements', 'Voorwaarden', [
              {
                label: translationService.get('Voorwaarden'),
                resolve: 'terms',
              },
            ]);
            addTab('approach', 'Aanpak', [
              {
                label: translationService.get('Aanpak'),
                resolve: 'approach',
              },
            ]);
            addTab('costs', 'Kosten', [
              {
                label: translationService.get('Kosten'),
                resolve: 'costs',
              },
            ]);
            break;

          case 'faq':
            addTab('question', 'Vraag', [
              {
                label: translationService.get('Vraag'),
                resolve: 'question',
              },
              {
                label: translationService.get('Antwoord'),
                resolve: 'answer',
              },
              {
                label: translationService.get('Interne toelichting'),
                resolve: 'internal_explanation',
              },
            ]);
            break;
        }

        ctrl.selectTab(ctrl.tabs[0].id);

        relatableTypes = _.map(
          (object.relatable_types || []).concat('casetype'),
          function (type) {
            var obj = {
                id: type,
                label: type,
                restrict: type,
              },
              config = typesConfig[type];

            obj = _.merge(obj, config);

            return obj;
          }
        );

        $scope.object = object;

        $scope.getTrustedHtml = function (property) {
          var html = $sce.trustAsHtml(object.values[property]);
          return html;
        };

        return ctrl;
      },
    ]);
})();
