// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.admin.instances').factory('instancesService', [
    '$q',
    '$http',
    'objectService',
    'zsApi',
    'zqlEscapeFilter',
    function ($q, $http, objectService, api, zqlEscapeFilter) {
      var instancesService = new EventEmitter2({
          wildcard: true,
        }),
        originTypes = [
          {
            type: 'development',
            label: 'Ontwikkeling',
          },
          {
            type: 'testing',
            label: 'Test',
          },
          {
            type: 'accept',
            label: 'Acceptatie',
          },
          {
            type: 'production',
            label: 'Productie',
          },
        ],
        softwareVersions = [
          {
            type: 'master',
            label: 'Productie',
          },
        ],
        configs = {},
        controlPanelForm;

      function getOriginOptions() {
        return _.map(instancesService.getOriginTypes(), function (type) {
          return {
            value: type.type,
            label: type.label,
          };
        });
      }

      function getSoftwareVersions() {
        return _.map(instancesService.getSoftwareVersions(), function (type) {
          return {
            value: type.type,
            label: type.label,
          };
        });
      }

      instancesService.loadCustomerConfig = function (customerId) {
        var deferred = configs[customerId];

        if (deferred) {
          return deferred.promise;
        }

        deferred = configs[customerId] = $q.defer();

        $http({
          method: 'GET',
          url: '/api/v1/controlpanel',
          params: {
            zql:
              'SELECT {} FROM controlpanel WHERE owner = ' +
              zqlEscapeFilter(customerId),
          },
        })
          .success(function (response) {
            var customerConfig = response.result.instance.rows[0];
            deferred.resolve(customerConfig);
          })
          .error(function (response) {
            deferred.reject(response.result[0]);
          });

        return deferred.promise;
      };

      instancesService.createControlPanel = function (customerId, type) {
        return api
          .post({
            url: '/api/v1/controlpanel/create',
            data: {
              owner: customerId,
              customer_type: type,
            },
          })
          .then(function (response) {
            return response.data[0];
          });
      };

      instancesService.loadInstances = function (controlPanelId) {
        var data = [];

        return api
          .get({
            url: '/api/v1/controlpanel/' + controlPanelId + '/instance',
          })
          .then(function (response) {
            data = data.concat(response.data);

            if (response.pager && response.pager.hasNext()) {
              return response.pager.next();
            }
          })
          .then(function (response) {
            if (response) {
              data = data.concat(response.data);
            }

            return data;
          });
      };

      instancesService.createInstance = function (controlPanelId, values) {
        return api
          .post({
            url: '/api/v1/controlpanel/' + controlPanelId + '/instance/create',
            data: values,
          })
          .then(function (response) {
            instancesService.emit('instance.add', response.data[0]);
          });
      };

      instancesService.updateInstance = function (
        controlPanelId,
        instance,
        params
      ) {
        var oldVals = angular.copy(instance.instance);

        _.extend(instance.instance, params);

        // Make sure network_acl is undef when no entries exists
        if (_.isArray(params.network_acl) && params.network_acl.length === 0) {
          params.network_acl = null;
        }

        return api
          .post({
            url:
              '/api/v1/controlpanel/' +
              controlPanelId +
              '/instance/' +
              instance.reference +
              '/update',
            data: params,
          })
          .then(function (response) {
            var updatedInstance = response.data[0];

            _.extend(instance.instance, updatedInstance.instance);

            return instance;
          })
          ['catch'](function (error) {
            instance.instance = oldVals;

            throw error;
          });
      };

      instancesService.getOriginTypes = function () {
        return originTypes;
      };

      instancesService.getSoftwareVersions = function () {
        return softwareVersions;
      };

      instancesService.getControlPanelForm = function () {
        return controlPanelForm;
      };

      instancesService.getOriginOptions = getOriginOptions;

      controlPanelForm = {
        name: 'instance-form',
        fields: [
          {
            name: 'label',
            label: 'Titel',
            type: 'text',
            required: true,
          },
          {
            name: 'fqdn',
            label: 'Webadres',
            type: 'text',
            required: true,
            data: {
              pattern: /^[-A-Za-z0-9.]{3,255}$/,
            },
            template: '/html/admin/instances/instances.html#url',
          },
          {
            name: 'customer_type',
            label: 'Type',
            type: 'select',
            data: {
              options: [
                {
                  value: 'commercial',
                  label: 'Commercieel',
                },
                {
                  value: 'government',
                  label: 'Overheid',
                },
                {
                  value: 'lab',
                  label: 'Lab',
                },
                {
                  value: 'development',
                  label: 'Development',
                },
                {
                  value: 'staging',
                  label: 'Staging',
                },
                {
                  value: 'acceptance',
                  label: 'Acceptatie',
                },
                {
                  value: 'preprod',
                  label: 'Preproductie',
                },
                {
                  value: 'testing',
                  label: 'Testing',
                },
              ],
            },
          },
          {
            name: 'otap',
            label: 'Gebruiksdoel',
            type: 'select',
            data: {
              options: getOriginOptions(),
            },
            default: getOriginOptions()[0].value,
          },
          {
            name: 'software_version',
            label: 'Softwareversie',
            type: 'select',
            data: {
              options: getSoftwareVersions(),
            },
            default: getSoftwareVersions()[0].value,
          },
          {
            name: 'template',
            label: 'Template',
            type: 'select',
            data: {
              options: [],
            },
          },
          {
            name: 'password',
            label: 'Wachtwoord',
            type: 'password',
            required: true,
            data: {
              showPasswordStrength: true,
            },
          },
        ],
        actions: [
          {
            id: 'submit',
            type: 'submit',
            label: 'Aanmaken',
          },
        ],
      };

      return instancesService;
    },
  ]);
})();
