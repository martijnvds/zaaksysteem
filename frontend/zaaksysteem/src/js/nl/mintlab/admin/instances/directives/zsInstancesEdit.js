// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.admin').directive('zsInstancesEdit', [
    '$q',
    '$interpolate',
    '$parse',
    'zsApi',
    'instancesService',
    'formService',
    'systemMessageService',
    function (
      $q,
      $interpolate,
      $parse,
      api,
      instancesService,
      formService,
      systemMessageService
    ) {
      return {
        restrict: 'E',
        scope: {
          instance: '&',
          controlpanelId: '&',
          domain: '@',
          customerConfig: '&',
          onComplete: '&',
          showTemplateOption: '&',
          showSoftwareversion: '&',
        },
        template:
          '<div' +
          ' data-zs-form-template-parser="<[getFormConfig()]>"' +
          ' data-zs-form-submit="updateInstance($values);closePopup()"' +
          '</div>',
        controller: [
          '$scope',
          function ($scope) {
            var config,
              instance = $scope.instance();

            (function () {
              config = angular.copy(instancesService.getControlPanelForm());

              if ($scope.showTemplateOption()) {
                _.find(config.fields, {
                  name: 'template',
                }).data.options = _.map(
                  $scope.customerConfig().instance.available_templates,
                  function (tplId) {
                    return {
                      value: tplId,
                      label: tplId,
                    };
                  }
                );
              } else {
                _.remove(config.fields, { name: 'template' });
              }

              /* Editing these leave us in a state we do not want to live in */
              _.remove(config.fields, { name: 'software_version' });
              _.remove(config.fields, { name: 'customer_type' });

              config.actions = [
                {
                  id: 'submit',
                  type: 'submit',
                  label: 'Opslaan',
                },
              ];

              _.each(config.fields, function (field) {
                var value;
                switch (field.name) {
                  case 'fqdn':
                    value = instance.instance.fqdn.replace(
                      /\.(?:zaaksysteem\.nl|zaaksysteem\.net|(.+?)\.zaaksysteem\.app)$/,
                      ''
                    );
                    break;

                  case 'password':
                    value = undefined;
                    break;

                  default:
                    value = instance.instance[field.name];
                    break;
                }

                if (value !== undefined) {
                  field.value = value;
                }
              });
            })();

            $scope.getFormConfig = function () {
              return config;
            };

            $scope.updateInstance = function ($values) {
              var values = angular.copy($values);

              values = _.pick(values, _.identity);

              instancesService
                .updateInstance($scope.controlpanelId(), instance, values)
                .then(function () {
                  systemMessageService.emitSave();
                  $scope.onComplete({ $instance: instance });
                })
                ['catch'](function (error) {
                  var form = formService.get(config.name);

                  if (error && error.type === 'validationexception') {
                    form.setValidity(
                      api.getLegacyFormValidations(config.fields, error)
                    );
                    systemMessageService.emitValidationError();
                  } else {
                    systemMessageService.emitSaveError();
                  }
                });
            };
          },
        ],
      };
    },
  ]);
})();
