// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin')
    .controller('nl.mintlab.admin.EditAppointmentKenmerkController', [
      '$scope',
      'smartHttp',
      'translationService',
      function ($scope, smartHttp, translationService) {
        var interface_url;

        $scope.products = [];
        $scope.locations = [];

        $scope.$watch('current_location', function () {
          $scope.reloadProducts();
        });

        $scope.init = function (interfaceUuid) {
          $scope.interfaceUuid = interfaceUuid;
          interface_url = 'api/v1/sysin/interface/' + $scope.interfaceUuid;
          $scope.reloadLocations();
        };

        $scope.reloadProducts = function () {
          smartHttp
            .connect({
              method: 'GET',
              url:
                interface_url +
                '/trigger/get_product_list?location_id=' +
                $scope.current_location.id,
            })
            .success(function onSuccess(data) {
              $scope.products = data.result.instance.data;

              $scope.current_product = $scope.products.filter(function (x) {
                return x.id == $scope.properties.product_id;
              })[0];
            })
            .error(function onError(/*data*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Kalenderproducten konden niet worden geladen'
                ),
              });
            });
        };

        $scope.reloadLocations = function () {
          smartHttp
            .connect({
              method: 'GET',
              url: interface_url + '/trigger/get_location_list',
            })
            .success(function onSuccess(data) {
              $scope.locations = data.result.instance.data;

              $scope.current_location = $scope.locations.filter(function (x) {
                return x.id == $scope.properties.location_id;
              })[0];
            })
            .error(function onError(/*data*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Locaties voor kalenderproduct konden niet worden geladen'
                ),
              });
            });
        };
      },
    ]);
})();
