// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.objecttype')
    .controller('nl.mintlab.admin.objecttype.ObjectTypeAttributeController', [
      '$scope',
      'formService',
      function ($scope, formService) {
        function getAttr() {
          return $scope.object;
        }

        $scope.$on('popupclose', function () {
          var form = formService.get('attribute_edit_form'),
            values = form.getValues(),
            attr = getAttr();

          for (var key in values) {
            attr[key] = values[key];
          }
        });

        $scope.$on('zs.scope.data.apply', function (event, parentScope, data) {
          var attr = getAttr();

          _.each(data.form.fields, function (field) {
            field.value = attr[field.name];
          });
        });
      },
    ]);
})();
