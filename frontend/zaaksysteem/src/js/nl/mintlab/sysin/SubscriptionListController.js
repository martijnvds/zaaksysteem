// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.sysin')
    .controller('nl.mintlab.sysin.SubscriptionListController', [
      '$scope',
      'smartHttp',
      'translationService',
      function ($scope, smartHttp, translationService) {
        var interfaceOptions = {};

        $scope.interfaces = [];

        function createInterfaceOptions() {
          var options = [],
            interfaces = $scope.interfaces || [],
            item,
            i,
            l,
            allId = translationService.get('Alle koppelingen');

          options.push({
            name: allId,
            value: '',
            label: allId,
          });

          for (i = 0, l = interfaces.length; i < l; ++i) {
            item = interfaces[i];
            options.push({
              name: item.name,
              value: item.id,
              label: item.name,
            });
          }

          return options;
        }

        $scope.getInterfaceOptions = function () {
          return interfaceOptions;
        };

        smartHttp
          .connect({
            method: 'GET',
            url: '/sysin/interface',
          })
          .success(function (data) {
            $scope.interfaces = data.result;
            interfaceOptions = createInterfaceOptions();
          })
          .error(function () {
            $scope.$emit('systemMessage', {
              type: 'error',
              content: translationService.get(
                'Koppelingen konden niet worden opgehaald'
              ),
            });
          });
      },
    ]);
})();
