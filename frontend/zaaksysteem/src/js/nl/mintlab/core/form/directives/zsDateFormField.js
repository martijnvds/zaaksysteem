// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  var dateItems = [
    { label: 'Vandaag', dateSymbol: '[{TODAY}]' },
    { label: 'Volgende week', dateSymbol: '[{WEEK}]' },
    { label: 'Volgende maand', dateSymbol: '[{MONTH}]' },
  ];

  var isDateItemSymbol = function (string) {
    return /\[\{.+\}\]/.test(string);
  };

  angular.module('Zaaksysteem.form').directive('zsDateFormField', [
    '$document',
    'dateFilter',
    'translationService',
    function ($document, dateFilter, translationService) {
      var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

      return {
        scope: true,
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          scope.from = NaN;
          scope.to = NaN;

          function set(from, to) {
            var obj = {
              from: from,
              to: to,
            };

            scope.from = from;
            scope.to = to;

            if (isDateItemSymbol(from)) {
              scope.to = scope.from;
            } else if (isNaN(from) || isNaN(to)) {
              obj = null;
            }

            ngModel.$setViewValue(obj);
          }

          scope.getFrom = function () {
            return scope.from;
          };

          scope.getTo = function () {
            return scope.to;
          };

          scope.getMin = function () {
            return scope.min;
          };

          scope.getMax = function () {
            return scope.max;
          };

          scope.getDateLabel = function () {
            var label,
              from = scope.from,
              dateFrom;

            if (isDateItemSymbol(from)) {
              label = 'Of kies een datum';
            } else if (isNaN(new Date(from).getTime())) {
              label = translationService.get('Kies een datum');
            } else {
              dateFrom = new Date(scope.from);
              label = dateFilter(dateFrom.getTime(), 'mediumDate');
            }

            return label;
          };

          scope.isDynamicDateSelected = function (dateItem) {
            return Boolean(scope.from && scope.from === dateItem.dateSymbol);
          };

          scope.isStaticDateSelected = function () {
            return Boolean(scope.from && typeof scope.from === 'number');
          };

          scope.getDateItems = function () {
            return dateItems;
          };

          scope.showDynamicDates = Boolean(
            $('fieldset.form-fieldset-name-kenmerken').has(element)[0]
          );

          scope.$on('date.range.select', function (event, from, to) {
            safeApply(scope, function () {
              set(from, to);
              scope.closePopup();
            });
          });

          ngModel.$isEmpty = function (value) {
            return (
              !value ||
              (isNaN(new Date(value).getTime()) &&
                (isNaN(value.from) || isNaN(value.to)))
            );
          };

          ngModel.$formatters.push(function (val) {
            var date,
              from = NaN,
              to = NaN;

            if (angular.isObject(val)) {
              from = val.from;
              to = val.to;
            } else if (val || val === 0) {
              date = new Date(val);
              if (!isNaN(date.getTime())) {
                from = new Date(
                  date.getFullYear(),
                  date.getMonth(),
                  date.getDate()
                ).getTime();
                to =
                  new Date(
                    date.getFullYear(),
                    date.getMonth(),
                    date.getDate()
                  ).getTime() - 1;
              }
            }

            set(from, to);

            return val;
          });
        },
      };
    },
  ]);
})();
