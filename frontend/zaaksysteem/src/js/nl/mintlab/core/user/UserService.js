// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.core.user').factory('userService', [
    '$parse',
    '$http',
    '$rootScope',
    'dataStore',
    function ($parse, $http, $rootScope, dataStore) {
      var user = dataStore.read('user'),
        userService = {};

      userService.getCurrentUser = function () {
        return user;
      };

      userService.getCurrentUuid = function () {
        return user ? user.betrokkene_identifier : undefined;
      };

      userService.getCurrentUsername = function () {
        return user ? user.username : undefined;
      };

      userService.isAdmin = function () {
        return user && user.is_admin;
      };

      userService.getSetting = function (name) {
        if (!user) {
          return undefined;
        }

        return $parse(name)(user.settings);
      };

      userService.setSetting = function (name, value) {
        var currentValue = userService.getSetting(name),
          data;

        if (!user || currentValue === value) {
          return;
        }

        data = {};

        $parse(name).assign(user.settings, value);
        $parse(name).assign(data, value);

        $http({
          method: 'POST',
          url: '/api/user/settings',
          data: data,
        });
      };

      $rootScope.$on('zs.scope.data.apply', function (
        scope,
        parentScope,
        data
      ) {
        if (!user && data && data.user) {
          user = data.user;
        }
      });

      return userService;
    },
  ]);
})();
