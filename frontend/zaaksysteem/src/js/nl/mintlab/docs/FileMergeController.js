// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.FileMergeController', [
    '$scope',
    'smartHttp',
    'vormValidator',
    'snackbarService',
    function ($scope, smartHttp, vormValidator, snackbarService) {
      var fields = [
        {
          name: 'document_name',
          label: 'Documentnaam',
          template: {
            inherits: 'text',
            control: function (el) {
              return angular.element(
                '<div class="vorm-input-wrapper">' +
                  el[0].outerHTML +
                  '<span>.pdf</span>' +
                  '</div>'
              );
            },
          },
          required: true,
        },
      ];

      var values = {
        document_name: '',
      };

      $scope.isLoading = false;

      $scope.getFileList = function () {
        return _.chain($scope.selectedFiles)
          .filter(function (file) {
            return file.getEntityType() === 'file';
          })
          .value();
      };

      $scope.getFields = function () {
        return fields;
      };

      $scope.getValidity = function () {
        return vormValidator(fields, values, {});
      };

      $scope.isValid = function () {
        return $scope.getValidity().valid;
      };

      $scope.canSubmit = function () {
        return !$scope.isLoading && $scope.isValid;
      };

      $scope.values = function () {
        return values;
      };

      $scope.handleChange = function (field, value) {
        values[field] = value;
      };

      $scope.startMerge = function () {
        if (!$scope.selectedFiles || $scope.selectedFiles.length == 0) {
          throw new Error('No files found to merge.');
        }

        $scope.isLoading = true;

        smartHttp
          .connect({
            url: '/api/case/' + $scope.caseId + '/file/merge_documents',
            method: 'POST',
            data: {
              filename: values.document_name,
              case_uuid: $scope.caseUuid,
              file_uuids: _.map($scope.getFileList(), function (file) {
                return file.uuid;
              }),
            },
            blocking: false,
          })
          .success(function () {
            snackbarService.info('De documenten worden samengevoegd. Herlaad de pagina')
          })
          .error(function (error) {
            var errorMessage = 'Er is een fout opgetreden bij het samenvoegen.';
            snackbarService.error(errorMessage);
          })
          .finally(function () {
            $scope.isLoading = false;
            $scope.closePopup();
          });
      };
    },
  ]);
