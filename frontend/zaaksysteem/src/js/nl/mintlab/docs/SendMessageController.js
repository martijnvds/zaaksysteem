// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.SendMessageController', [
    '$scope',
    '$q',
    '$http',
    'smartHttp',
    'translationService',
    'systemMessageService',
    'snackbarService',
    'messageBoxService',
    'messagePostexService',
    function (
      $scope,
      $q,
      $http,
      smartHttp,
      translationService,
      systemMessageService,
      snackbarService,
      messageBoxService,
      messagePostexService
    ) {
      var ctrl = this;
      var activeTab = null;
      var errorMessages = {
        tab: [],
        files: [],
      };

      $scope.init = function () {

        $scope.postexUnavailable = true;

        $scope.loading = false;
        $scope.subscriptionActive = false;
        $scope.requestor = _.get($scope.case, 'instance.requestor.instance');
        $scope.interfaceUuid = null;

        $scope.showTab('email');

        if (!$scope.context || $scope.context !== 'docs') return;

        /*
        Checks if there is a valid interface configured, and if so, if there is a subscription
        active for the subject, based on the UUID.

        The chain can be interrupted in several ways:

        1) A reject without parameters. No error message is shown.
        2) A reject with an object with a type and message. 

            If type is 'tab', the message will be shown on hover in the MijnOverheid tab
            If type is 'snackbar', a snackbar will be shown

        3) A reject with a general / backend error. A snackbar will be shown with the error message
        */

        messagePostexService.getInterfaces()
          .then(function (interfaces) {

            if (!interfaces || interfaces.length === 0 || interfaces.length > 1) {
              return true;
            }


            if (!$scope.requestor || !$scope.requestor.uuid) {
              return $q.reject({ type: 'snackbar', message: 'Geen requestor-UUID gevonden.' });
            }

            $scope.postexInterfaceUUID = _.get(interfaces[0], 'reference');
            if (!$scope.postexInterfaceUUID) {
              return $q.reject({ type: 'snackbar', message: 'De configuratie bevat geen UUID. Neem contact op met uw systeembeheerder.' });
            }

            $scope.loading = true;
            $scope.postexUnavailable = false;

            return true;

          })
          .then(function (response) {
            if (!response) {
              return $q.reject({ type: 'tab', message: 'Dit bericht kan niet worden verzonden naar de Postex dienst.' });
            }
          })
          .catch(function (error) {
            if (error) {
              if (error.type === 'tab') {
                $scope.addError(error.type, error.message);
              } else if (error.type === 'snackbar') {
                showSnackbar(error.message);
              } else {

                var message;

                if ((_.get(error, 'data.result.instance.message'))) {
                  message = (_.get(error, 'data.result.instance.message'));
                } else if ((_.get(error, 'data.result.preview'))) {
                  message = (_.get(error, 'data.result.preview'));
                } else {
                  message = error;
                }

                showSnackbar(message);
              }
            }
          })
          .finally(function () {
            $scope.loading = false;
          });

        messageBoxService.getInterfaces()
          .then(function (interfaces) {
            if (!interfaces || interfaces.length === 0) {
              return $q.reject({
                type: 'tab',
                message:
                  'Dit bericht kan niet worden verzonden naar de MijnOverheid Berichtenbox.',
              });
            }

            if (interfaces.length > 1) {
              return $q.reject({
                type: 'tab',
                message:
                  'Er is meer dan één MijnOverheid configuratie gevonden. Neem contact op met uw systeembeheerder.',
              });
            }

            if (!$scope.requestor || !$scope.requestor.uuid) {
              return $q.reject({
                type: 'snackbar',
                message: 'Geen requestor-UUID gevonden.',
              });
            }

            $scope.interfaceUuid = _.get(interfaces[0], 'reference');
            if (!$scope.interfaceUuid) {
              return $q.reject({
                type: 'snackbar',
                message:
                  'De configuratie bevat geen UUID. Neem contact op met uw systeembeheerder.',
              });
            }

            $scope.loading = true;
            return messageBoxService.isSubscribed($scope.interfaceUuid, $scope.requestor.uuid);

            $scope.loading = true;
            return messageBoxService.isSubscribed(
              $scope.interfaceUuid,
              $scope.requestor.uuid
            );
          })
          .then(function (response) {
            if (!response) {
              return $q.reject({
                type: 'tab',
                message:
                  'Dit bericht kan niet worden verzonden naar de MijnOverheid Berichtenbox.',
              });
            }
            $scope.subscriptionActive = true;
          })
          .catch(function (error) {
            if (error) {
              if (error.type === 'tab') {
                $scope.addError(error.type, error.message);
              } else if (error.type === 'snackbar') {
                showSnackbar(error.message);
              } else {
                var message;

                if (_.get(error, 'data.result.instance.message')) {
                  message = _.get(error, 'data.result.instance.message');
                } else if (_.get(error, 'data.result.preview')) {
                  message = _.get(error, 'data.result.preview');
                } else {
                  message = error;
                }

                showSnackbar(message);
              }
            }
          })
          .finally(function () {
            $scope.loading = false;
          });
      };

      $scope.showTab = function (tab) {
        if (activeTab === tab) return;
        activeTab = tab;
      };

      $scope.tabIsActive = function (tab) {
        return activeTab === tab;
      };

      $scope.getName = function (attachment) {
        var context = $scope.context,
          name = '';

        if (context === 'actions' || attachment.naam !== undefined) {
          name = attachment.naam;
        } else if (context === 'docs') {
          name = attachment.name + attachment.extension;
        }
        return name;
      };

      $scope.isMessageBoxAllowed = function () {
        return $scope.subscriptionActive;
      };

      $scope.isPostexUnavailable = function () {
        return $scope.postexUnavailable;
      }

      $scope.addError = function (type, message) {
        errorMessages[type].push(message);
      };

      $scope.getErrors = function (type, joined) {
        if (joined) {
          return errorMessages[type].join('<br />');
        }
        return errorMessages[type];
      };

      $scope.hasFileErrors = function () {
        return $scope.getErrors('files').length > 0;
      };

      function showSnackbar(errormessage) {
        snackbarService.error(
          'Er is een fout opgetreden. Details: ' + errormessage
        );
      }
    },
  ]);
