// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.docs')
    .controller('nl.mintlab.docs.FolderController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        $scope.collapsed = true;
        $scope.folderName = '';
        $scope.fileName = '';
        $scope.filtered = [];
        $scope.preview = false;
        $scope.editName = false;

        function createFolder(name) {
          var entity = $scope.entity;

          entity.name = name;

          entity.updating = true;

          smartHttp
            .connect({
              url: 'directory/create/',
              method: 'POST',
              data: {
                case_id: $scope.caseId,
                name: name,
              },
            })
            .success(function (data) {
              var folderData = data.result[0];
              $scope.entity.updateWith(folderData);
              $scope.editName = false;
              entity.updating = false;
            })
            .error(function () {
              $scope.editName = true;
              entity.updating = false;
            });
        }

        function updateFolder(name) {
          var entity = $scope.entity,
            tmp = entity.name;

          entity.name = name;

          entity.updating = true;

          smartHttp
            .connect({
              url: 'directory/update/',
              method: 'POST',
              data: {
                directory_id: entity.id,
                name: name,
              },
            })
            .success(function () {})
            .error(function () {
              entity.name = tmp;
            })
            .then(function () {
              entity.updating = false;
            });
        }

        $scope.toggleCollapse = function (event) {
          $scope.collapsed = !$scope.collapsed;

          if (event) {
            event.stopPropagation();
          }
        };

        $scope.onFolderNameClick = function (event) {
          $scope.toggleCollapse();
        };

        $scope.saveName = function (name) {
          updateFolder(name);
        };

        $scope.editSave = function (name) {
          if ($scope.entity.id === -1) {
            createFolder(name);
          } else {
            updateFolder(name);
          }
        };

        $scope.$on('editsave', function (event /*, key, value*/) {
          event.stopPropagation();
          $scope.editName = false;
        });

        $scope.$on('editcancel', function (event /*, key, value*/) {
          if ($scope.entity.id === -1) {
            $scope.removeEntity([$scope.entity]);
          }
          event.stopPropagation();
          $scope.editName = false;
        });

        $scope.$on('drop', function (event /*, data, mimetype*/) {
          if (event.currentScope === event.targetScope) {
            $scope.collapsed = false;
          }
        });

        $scope.$watch('entity', function () {
          if ($scope.entity) {
            $scope.collapsed =
              $scope.entity !== $scope.root && $scope.entity.isCollapsed();

            $scope.update = $scope.entity.clone();
            $scope.editName = $scope.entity.id === -1;

            if ($scope.editName) {
              $scope.folderName = $scope.entity.name;
            }

            $scope.entity.getCollapsedInScope = function () {
              return $scope.collapsed;
            };

            $scope.entity.toggleCollapsedInScope = function () {
              return $scope.toggleCollapse();
            };
          }
        });

        $scope.$watch('filterQuery', function () {
          if ($scope.filterQuery) {
            $scope.collapsed = false;
          }
        });
      },
    ]);
})();
