// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

angular.module('Zaaksysteem.net').provider('smartHttp', function () {
  var inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
    EventDispatcher = window.zsFetch('nl.mintlab.events.EventDispatcher'),
    indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf');

  return {
    defaults: {
      prefix: '/',
    },
    $inject: ['$http', '$rootScope'],
    $get: [
      '$http',
      '$rootScope',
      'translationService',
      function ($http, $rootScope, translationService) {
        var defaults = this.defaults;

        function SmartHttp() {
          this._requests = [];
          this._blockingRequests = [];
          this.defaults = defaults;
        }

        inherit(SmartHttp, EventDispatcher);

        SmartHttp.prototype.connect = function (config) {
          var method,
            promise,
            that = this;

          if (arguments.length > 1) {
            throw new Error('Connect called with legacy arguments');
          }

          if (config.method === undefined) {
            config.method = 'GET';
          }

          method = config.method.toLowerCase();

          if (config.blocking === undefined && method === 'post') {
            config.blocking = true;
          }

          if (config.blocking) {
            this._blockingRequests.push(config);
          }

          if (
            config.url.indexOf('/') !== 0 &&
            !config.url.match(/^(http|ftp|https):\/\//)
          ) {
            config.url = this.defaults.prefix + config.url;
          }

          this._requests.push(config);
          this.publish('connect', config);

          promise = $http(config);

          function onComplete() {
            var index;

            index = indexOf(that._blockingRequests, config);

            if (index !== -1) {
              that._blockingRequests.splice(index, 1);
            }
            index = indexOf(that._requests, config);
            if (index !== -1) {
              that._requests.splice(index, 1);
              that.publish('close', config);
            }
          }

          promise.then(onComplete, onComplete);

          return promise;
        };

        SmartHttp.prototype.getRequests = function () {
          return this._requests;
        };

        SmartHttp.prototype.getBlockingRequests = function () {
          return this._blockingRequests;
        };

        SmartHttp.prototype.getUrl = function (url) {
          return this.defaults.prefix + url;
        };

        SmartHttp.prototype.emitDefaultError = function () {
          $rootScope.$broadcast('systemMessage', {
            type: 'error',
            content: translationService.get(
              'Er ging iets fout. Probeer het later opnieuw.'
            ),
          });
        };

        return new SmartHttp();
      },
    ],
  };
});
