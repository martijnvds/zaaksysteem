// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import caseDetailViewModule from './caseDetailView';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import snackbarServiceModule from '../../shared/ui/zsSnackbar/snackbarService';
// import auxiliaryRouteModule from '../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from '../../shared/util/route/onRouteActivate';
import zsModalModule from '../../shared/ui/zsModal';
import viewTitleModule from '../../shared/util/route/viewTitle';
import template from './index.html';
import seamlessImmutable from 'seamless-immutable';
import first from 'lodash/first';
import sortBy from 'lodash/sortBy';
import filter from 'lodash/filter';
import './styles.scss';

export default {
  moduleName: angular.module('Zaaksysteem.officeaddins.word.caseDetail', [
    resourceModule,
    caseDetailViewModule,
    snackbarServiceModule,
    // auxiliaryRouteModule,
    onRouteActivateModule,
    zsModalModule,
    viewTitleModule,
    composedReducerModule,
  ]).name,
  config: [
    {
      route: {
        url: '/zaak/:caseId/',
        resolve: {
          caseItem: [
            '$rootScope',
            '$stateParams',
            '$q',
            'resource',
            'snackbarService',
            ($rootScope, $stateParams, $q, resource, snackbarService) => {
              let caseItemResource = resource(
                { url: `/api/v1/case/${$stateParams.caseId}` },
                { scope: $rootScope }
              ).reduce((requestOptions, data) => {
                return first(data) || seamlessImmutable({});
              });

              return caseItemResource
                .asPromise()
                .then(() => caseItemResource)
                .catch((err) => {
                  snackbarService.error(
                    'De zaak kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.'
                  );

                  return $q.reject(err);
                });
            },
          ],
          caseDocuments: [
            '$rootScope',
            '$stateParams',
            '$q',
            'resource',
            'snackbarService',
            'caseItem',
            (
              $rootScope,
              $stateParams,
              $q,
              resource,
              snackbarService,
              caseItem
            ) => {
              let caseDocumentsResource = resource(
                {
                  url: `/api/case/${
                    caseItem.data().instance.number
                  }/directory/tree`,
                },
                { scope: $rootScope }
              ).reduce((requestOptions, data) => {
                return sortBy(
                  filter(
                    first(data).files,
                    (file) =>
                      file.deleted_by === null &&
                      file.accepted &&
                      (file.extension === '.docx' || file.extension === '.doc')
                  ),
                  (file) => file.name.toLowerCase()
                );
              });

              return caseDocumentsResource
                .asPromise()
                .then(() => caseDocumentsResource)
                .catch((err) => {
                  snackbarService.error(
                    'De documenten van deze zaak konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.'
                  );

                  return $q.reject(err);
                });
            },
          ],
        },
        auxiliary: true,
        onActivate: [
          '$state',
          '$timeout',
          '$rootScope',
          '$window',
          '$document',
          '$compile',
          'viewTitle',
          '$stateParams',
          'zsModal',
          'caseItem',
          'caseDocuments',
          (
            $state,
            $timeout,
            $rootScope,
            $window,
            $document,
            $compile,
            viewTitle,
            $stateParams,
            zsModal,
            caseItem,
            caseDocuments
          ) => {
            let scrollEl = angular.element(
              $document[0].querySelector('.body-scroll-container')
            );

            scrollEl.css({ overflow: 'hidden' });

            let openModal = () => {
              let modal,
                unregister,
                scope = $rootScope.$new(true);

              scope.caseItem = caseItem;
              scope.caseDocuments = caseDocuments;

              modal = zsModal({
                el: $compile(angular.element(template))(scope),
                title: viewTitle.get(),
                classes: 'detail-modal',
              });

              modal.open();

              modal.onClose(() => {
                $window.history.back();
                return true;
              });

              scope.reloadCaseDocuments = () => caseDocuments.reload();

              unregister = $rootScope.$on('$stateChangeStart', () => {
                $window.requestAnimationFrame(() => {
                  $rootScope.$evalAsync(() => {
                    if ($state.current.name === 'caseSearch') {
                      modal.close().then(() => {
                        scope.$destroy();

                        scrollEl.css({
                          overflow: '',
                          height: '',
                        });
                      });

                      unregister();
                    }
                  });
                });
              });
            };

            $window.requestAnimationFrame(() => {
              $rootScope.$evalAsync(openModal);
            });
          },
        ],
      },
      state: 'caseDetail',
    },
  ],
};
