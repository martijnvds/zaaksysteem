// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import routing from './routing';
import wordAppModule from './shared/wordApp';
import resourceModule from './../shared/api/resource';
import zsSnackbarModule from './../shared/ui/zsSnackbar';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import serviceworkerModule from './../shared/util/serviceworker';
import sessionServiceModule from '../shared/user/sessionService';
import get from 'lodash/get';
import loadIconFont from '../shared/util/loadIconFont';

export default angular
  .module('Zaaksysteem.officeaddins.word', [
    routing,
    resourceModule,
    zsSnackbarModule,
    ngAnimate,
    serviceworkerModule,
    wordAppModule,
    sessionServiceModule,
  ])
  .config([
    '$provide',
    ($provide) => {
      // make sure sourcemaps work
      // from https://github.com/angular/angular.js/issues/5217
      $provide.decorator('$exceptionHandler', [
        '$delegate',
        ($delegate) => (exception, cause) => {
          $delegate(exception, cause);
          setTimeout(() => {
            console.error(exception.stack);
          });
        },
      ]);
    },
  ])
  .config([
    '$httpProvider',
    ($httpProvider) => {
      $httpProvider.defaults.withCredentials = true;
      $httpProvider.defaults.headers.common['X-Client-Type'] = 'web';
      $httpProvider.useApplyAsync(true);

      $httpProvider.interceptors.push([
        'zsStorage',
        '$window',
        '$q',
        (zsStorage, $window, $q) => ({
          response: (response) => {
            if (response.config.url === '/api/v1/session/current') {
              if (!response.data.result.instance.logged_in_user) {
                zsStorage.clear();
                $window.location.href = `/auth/login?referer=${$window.location.pathname}`;

                return $q.reject(response);
              }
            }

            return $q.resolve(response);
          },
          responseError: (rejection) => {
            if (
              rejection.status === 401 ||
              // configuration_incomplete implies the API is not configured for public accces,
              // which means we don't have a logged in user
              get(rejection, 'data.result.instance.type') ===
                'api/v1/configuration_incomplete' ||
              get(rejection, 'data.result.instance.type') ===
                'api/v1/error/api_interface_id'
            ) {
              zsStorage.clear();
              $window.location.href = `/auth/login?referer=${$window.location.pathname}`;
            }

            return $q.reject(rejection);
          },
        }),
      ]);
    },
  ])
  .config([
    'resourceProvider',
    (resourceProvider) => {
      zsResourceConfiguration(resourceProvider.configure);
    },
  ])
  .run(loadIconFont)
  .run([
    '$animate',
    ($animate) => {
      $animate.enabled(true);
    },
  ])
  .run([
    '$window',
    'serviceWorker',
    ($window, serviceWorker) => {
      let enable = ENV.USE_SERVICE_WORKERS;
      let worker = serviceWorker();

      if (worker.isSupported()) {
        worker.isEnabled().then((enabled) => {
          if (enabled !== enable) {
            return enable
              ? worker.enable()
              : worker.disable().then(() => {
                  if (enabled) {
                    $window.location.reload();
                  }
                });
          }
        });
      }
    },
  ]).name;
