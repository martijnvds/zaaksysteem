// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

export default angular.module('officeService', []).factory('officeService', [
  '$window',
  ($window) => {
    return $window.Office;
  },
]).name;
