// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const getRelations = (returnCase) => {
  return returnCase ?
    {
      zaak_id: returnCase,
      relation: 'deelzaak',
    } : null;
};

export default getRelations;
