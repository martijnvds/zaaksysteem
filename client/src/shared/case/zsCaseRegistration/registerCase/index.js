// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import pickBy from 'lodash/pickBy';
import getCaseCreateSnack from '../../../../intern/getCaseCreateSnack';
import getRequestInfoV2 from './getRequestInfoV2';
import getRequestInfoV0 from './getRequestInfoV0';
import getApiValues from './getApiValues';
import getEligibleForV2 from './helpersV2/getEligibleForV2';

const registerCase = (
  $http,
  $state,
  ctrl,
  intakeShowContactInfo,
  allocationData,
  fieldReducer,
  snackbarService,
  resetTouched,
  setSubmitting,
  returnCase
) => {
  const vals = ctrl.getValues();
  const fields = fieldReducer.data();

  const eligibleForV2 = getEligibleForV2(
    ctrl.casetypeV2,
    ctrl.recipient,
    fields,
    vals
  );

  setSubmitting(true);

  // when type='me' and data.me is 'true' or 'undefined' it should redirect to the case (see getAssignement.js)
  const shouldRedirectToCase =
    get(vals, '$allocation.type') === 'me' &&
    get(vals, '$allocation.data.me') !== false;
  const apiValues = getApiValues(
    pickBy(vals, (value, key) => !!fields[key]),
    fields
  );

  const requestInfo = eligibleForV2
    ? getRequestInfoV2(
        ctrl,
        vals,
        intakeShowContactInfo,
        allocationData,
        apiValues
      )
    : getRequestInfoV0(
        ctrl,
        vals,
        intakeShowContactInfo,
        apiValues,
        shouldRedirectToCase,
        returnCase
      );

  // v2 only returns the caseUuid, but we need the caseId
  // therefore we do a followup request to get it
  const registerRequest = $http(requestInfo).then((response) => {
    if (!eligibleForV2) {
      return response;
    }

    return $http({
      url: '/api/v2/cm/case/get_case',
      method: 'GET',
      params: {
        case_uuid: get(response, 'data.data.id'),
      },
    });
  });

  const register = (responseHandler, catchHandler) => {
    snackbarService.wait('Uw zaak wordt geregistreerd.', {
      // v0: '/api/v0/case/create'
      // v1: '/api/v1/case/create_delayed'
      // v2: '/api/v2/cm/case/create_case'
      promise: registerRequest,
      then: responseHandler,
      catch: catchHandler,
    });
  };

  const responseHandler = (response) => {
    resetTouched();

    const pathToCaseNumber = eligibleForV2
      ? 'data.data.attributes.number'
      : shouldRedirectToCase
      ? 'data.result[0].number'
      : 'data.result.instance.data.case_id';
    const caseId = Number(get(response, pathToCaseNumber));
    const status = shouldRedirectToCase ? 'open' : 'new';
    const actions =
      eligibleForV2 && !shouldRedirectToCase
        ? [
            {
              type: 'link',
              label: `Zaak ${caseId} openen`,
              link: $state.href('case', { caseId }),
            },
          ]
        : [];

    if (returnCase) {
      $state.go('case', { caseId: returnCase });
    } else if (shouldRedirectToCase) {
      $state.go('case', { caseId });
    } else {
      $state.go('home');
    }

    return getCaseCreateSnack({ caseId, status }, actions);
  };

  const catchHandler = (response) => {
    setSubmitting(false);

    const req_id = response.headers()['zs-req-id'];

    const pathToError = eligibleForV2
      ? 'unknown'
      : shouldRedirectToCase
      ? 'data.result[0].type'
      : 'data.result.instance.type';

    const error_type = get(response, pathToError);

    if (error_type === 'node/allocation/permission') {
      return [
        'Zaak kon niet worden aangemaakt.',
        'De betreffende afdeling/rol heeft onvoldoende rechten.',
        'Neem contact op met uw beheerder voor meer informatie.',
      ].join(' ');
    }

    return [
      'Zaak kon niet worden aangemaakt.',
      'Neem contact op met uw beheerder voor meer informatie.',
      req_id !== undefined
        ? `<p><small>(Foutcode "${req_id}")</small></p>`
        : '',
      error_type,
    ].join(' ');
  };

  register(responseHandler, catchHandler);
};

export default registerCase;
