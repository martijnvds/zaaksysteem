// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsModalModule from './../../../../ui/zsModal';
import createModalModule from './../../../../ui/zsModal/createModal';
import vormFieldsetModule from './../../../../vorm/vormFieldset';
import resourceModule from './../../../../api/resource';
import composedReducerModule from './../../../../api/resource/composedReducer';
import vormValidatorModule from './../../../../vorm/util/vormValidator';
import form from './../../../../../intern/views/case/zsCaseAddSubject/form';
import createMagicStringResource from './../../../../../intern/views/case/zsCaseAddSubject/createMagicStringResource';
import createRoleResource from './../../../../../intern/views/case/zsCaseAddSubject/createRoleResource';
import seamlessImmutable from 'seamless-immutable';
import isArray from 'lodash/isArray';
import getRoleValue from './../../../../../intern/views/case/zsCaseAddSubject/getRoleValue';
import identity from 'lodash/identity';
import get from 'lodash/get';
import './styles.scss';

export default angular
  .module('vormRelatedSubject', [
    zsModalModule,
    createModalModule,
    vormFieldsetModule,
    resourceModule,
    composedReducerModule,
    vormValidatorModule,
  ])
  .component('vormRelatedSubjectControl', {
    bindings: {
      relatedSubject: '<',
      disabled: '<',
    },
    require: {
      ngModel: 'ngModel',
    },
    template: `<button data-name="related-subject-button" type="button" ng-click="$ctrl.handleClick()" ng-disabled="$ctrl.disabled">
					<zs-icon icon-type="{{$ctrl.icon}}"></zs-icon>
					<span class="vorm-related-subject-display-label">
						{{$ctrl.label}}
					</span>
					<span class="vorm-related-subject-display-description">
						{{$ctrl.description}}
					</span>
				</button>`,
    controller: [
      '$scope',
      'createModal',
      function (scope, createModal) {
        let ctrl = this;

        ctrl.$onInit = () => {
          scope.$watch(
            () => ctrl.ngModel.$modelValue,
            () => {
              let relatedSubject = ctrl.ngModel.$modelValue;

              if (relatedSubject) {
                ctrl.label = relatedSubject.related_subject.label;
                ctrl.description = getRoleValue(relatedSubject);
                ctrl.icon =
                  relatedSubject.related_subject.type === 'natuurlijk_persoon'
                    ? 'account'
                    : 'domain';
              } else {
                ctrl.label = ctrl.description = ctrl.icon = '';
              }
            }
          );
        };

        ctrl.handleClick = () => {
          let modal = createModal({
            scope,
            title: 'Betrokkene wijzigen',
            template: `<zs-vorm-related-subject-form
									data-values="$ctrl.values"
									on-submit="$ctrl.onSubmit($values)"
								>
								</zs-vorm-related-subject-form>`,
            controller: {
              onSubmit: (values) => {
                ctrl.ngModel.$setViewValue(values);

                modal.close();
              },
              values: ctrl.relatedSubject,
            },
          });
        };
      },
    ],
  })
  .component('zsVormRelatedSubjectForm', {
    bindings: {
      values: '<',
      onSubmit: '&',
    },
    template: `<form
					ng-submit="$ctrl.onSubmit({ $values: $ctrl.getValues() })"
				>
					<vorm-fieldset
						data-values="$ctrl.getValues()"
						data-fields="$ctrl.getFields()"
						data-validat="$ctrl.getValidity()"
						on-change="$ctrl.handleChange($name, $value)"
					>
					</vorm-fieldset>
					<div class="form-actions">
						<button
							type="submit"
							ng-disabled="$ctrl.isDisabled()"
							class="btn btn-flat"
						>
							Opslaan
						</button>
					</div>
				</form>`,
    controller: [
      '$scope',
      'resource',
      'composedReducer',
      'vormValidator',
      function (scope, resource, composedReducer, vormValidator) {
        let ctrl = this,
          values = seamlessImmutable({
            relation_type: 'natuurlijk_persoon',
          }).merge(ctrl.values || {}),
          magicStringResource = createMagicStringResource(
            resource,
            scope,
            () => {
              return { role: getRoleValue(values) };
            }
          ),
          roleResource = createRoleResource(resource, scope),
          formCtrl = form(magicStringResource, roleResource.data()),
          fields = formCtrl.fields,
          validityReducer;

        magicStringResource.onUpdate(() => {
          values = values.merge({
            magic_string_prefix: magicStringResource.data(),
          });
        });

        roleResource.onUpdate(() => {
          formCtrl = form(magicStringResource, roleResource.data());
          fields = formCtrl.fields;
        });

        validityReducer = composedReducer(
          { scope },
          fields,
          () => values
        ).reduce((formFields, vals) => {
          return vormValidator(formFields, vals);
        });

        ctrl.getFields = () => fields;

        ctrl.getValues = () => values;

        ctrl.handleChange = (name, value) => {
          values = values.merge({ [name]: value });
        };

        ctrl.isValid = () => get(validityReducer.data(), 'valid');

        ctrl.isDisabled = () =>
          !ctrl.isValid() || magicStringResource.state() !== 'resolved';

        ctrl.getValidity = () => get(validityReducer.data(), 'validations');
      },
    ],
  })
  .component('zsVormRelatedSubjectAdd', {
    bindings: {
      value: '<',
      onAdd: '&',
    },
    template: `<button data-name="related-subject-add" type="button" ng-click="$ctrl.handleClick()" class="btn btn-secondary">
					Betrokkene toevoegen
				</button>`,
    controller: [
      '$scope',
      '$compile',
      'zsModal',
      function ($scope, $compile, zsModal) {
        let ctrl = this,
          modal,
          scope;

        let closeModal = () => {
          if (modal) {
            scope.$destroy();

            modal.close();

            modal = null;
          }
        };

        ctrl.handleClick = () => {
          scope = $scope.$new();

          scope.handleSubmit = (values) => {
            let value = ctrl.value,
              newValue = (isArray(value) ? value : [value])
                .concat([values])
                .filter(identity);

            ctrl.onAdd({ $value: newValue });

            closeModal();
          };

          modal = zsModal({
            el: $compile(
              `<zs-vorm-related-subject-form
								on-submit="handleSubmit($values)""
							>
							</zs-vorm-related-subject-form>`
            )(scope),
            title: 'Betrokkene toevoegen',
          });

          modal.open();

          modal.onClose(() => {
            closeModal();

            return false;
          });
        };

        ctrl.$onDestroy = () => {
          closeModal();
        };
      },
    ],
  })
  .factory('vormRelatedSubject', [
    () => {
      return {
        wrapper: (el) => {
          angular
            .element(el[1].querySelector('.vorm-field-add-button'))
            .remove();

          angular.element(el[1]).append(
            `<zs-vorm-related-subject-add
							data-value="vm.value()"
							on-add="vm.onChange({ $value: $value })"
						>
						</zs-vorm-related-subject-add>`
          );

          el.find('vorm-control').attr('ng-if', '!delegate.empty');

          return el;
        },
        control: angular.element(
          `<vorm-related-subject-control
						class="object-list-item"
						related-subject="delegate.value"
						ng-model
					></vorm-related-subject-control>`
        ),
        display: angular.element(
          `<vorm-related-subject-control
						class="object-list-item"
						related-subject="delegate.value"
						data-disabled="true"
					></vorm-related-subject-control>`
        ),
      };
    },
  ]).name;
