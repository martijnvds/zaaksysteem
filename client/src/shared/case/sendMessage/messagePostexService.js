import angular from 'angular';

export default angular
  .module('messagePostexService', [])
  .factory('messagePostexService', [
    '$q', '$rootScope', 'smartHttp',
    function messagePostexService($q, $rootScope, smartHttp) {

      return {

        getInterfaces: () => {

          const startUrl = '/api/v1/sysin/interface/get_all?rows_per_page=250';
          const moduleName = 'postex';
          const defer = $q.defer();
          let allRows = [];

          function processPage(url) {
            smartHttp.connect({
              method: 'GET',
              url
            }).then(response => {
              const { data: { result: { instance } } } = response;
              const { rows, pager } = instance;

              allRows = allRows.concat(rows);

              if (pager.next) {
                processPage(pager.next);
              } else {
                defer.resolve(allRows.filter(row => {
                  const { instance } = row;
                  return (instance.module === moduleName);
                }));
              }
            }).catch(error => {
              defer.reject(error);
            });
          }

          processPage(startUrl);
          return defer.promise;
        },

        submit: (interfaceUuid, subjectUuid, data) => {
          return smartHttp.connect({
            method: 'POST',
            url: `api/v1/sysin/interface/${interfaceUuid}/trigger/send_message`,
            data
          }).then((response) => {
            const { data: { result: { instance } } } = response;
            if (instance.ok !== true) {
              return $q.reject(instance.message);
            }
          });
        }

      };

    }]).name;
