// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import {
  transformAllocationParams,
  getComment,
  getCaseDistributors,
  setCaseAllocation,
} from './allocation';

import ZaaksysteemException from '../../exceptionHandling/ZaaksysteemException';

describe('transformAllocationParams', () => {
  const groupValues = {
    allocation_type: 'org-unit',
    send_email: true,
    'org-unit': { unit: 42, role: 666 },
    assignee: { id: 999 },
  };
  const user = {
    id: 21,
  };

  test('Allocation to group', () => {
    const group = transformAllocationParams(groupValues, user);
    expect(group).toMatchObject({
      orgUnitId: 42,
      roleId: 666,
      notify: false,
    });
  });

  const assigneeValues = {
    allocation_type: 'assignee',
    send_email: true,
    'org-unit': { unit: 42, role: 666 },
    assignee: { id: 999 },
  };

  test('Allocation to assignee', () => {
    const assignee = transformAllocationParams(assigneeValues, user);
    expect(assignee).toMatchObject({
      assigneeId: 999,
      changeDepartment: false,
      notify: true,
    });
  });

  const selfValues = {
    allocation_type: 'self',
    send_email: true,
    'org-unit': { unit: 42, role: 666 },
    self: { id: 999 },
    change_department: true,
  };

  test('Allocation to self', () => {
    const self = transformAllocationParams(selfValues, user);
    expect(self).toMatchObject({
      assigneeId: 21,
      changeDepartment: true,
      notify: false,
    });
  });
});

describe('getComment', () => {
  test('has no comment', () => {
    expect(getComment({ foo: 'bar' })).toMatchObject({});
  });

  test('has comment', () => {
    expect(getComment({ comment: 'my comment' })).toMatchObject({
      comment: 'my comment',
    });
  });
});

describe('setCaseAllocation', () => {
  const groupData = {
    caseId: 42,
    notify: true,
    orgUnitId: 6,
    roleId: 66,
    comment: 'my pretty remark',
    allocationType: 'org-unit',
  };

  test('Allocation to group', () => {
    const group = setCaseAllocation(groupData);
    expect(group).toMatchObject({
      url: `/zaak/${groupData.caseId}/update/allocation`,
      params: {
        change_allocation: 'group',
        ou_id: 6,
        role_id: 66,
        comment: 'my pretty remark',
      },
      method: 'POST',
    });
  });

  const assigneeData = {
    caseId: 42,
    notify: true,
    allocationType: 'does not matter',
    changeDepartment: true,
    assigneeId: 5,
  };

  test('Allocation to assignee', () => {
    const assignee = setCaseAllocation(assigneeData);
    expect(assignee).toMatchObject({
      url: `/zaak/${assigneeData.caseId}/update/allocation`,
      params: {
        change_allocation: 'behandelaar',
        change_department: 1,
        betrokkene_id: 'betrokkene-medewerker-5',
        notify: 1,
      },
      method: 'POST',
    });
  });

  const changeDeptFalse = {
    caseId: 42,
    notify: false,
    allocationType: 'does not matter',
    changeDepartment: false,
    assigneeId: 5,
  };

  test('Allocation to assignee without dept', () => {
    const dept = setCaseAllocation(changeDeptFalse);
    expect(dept).toMatchObject({
      url: `/zaak/${changeDeptFalse.caseId}/update/allocation`,
      params: {
        change_allocation: 'behandelaar',
        change_department: 0,
        betrokkene_id: 'betrokkene-medewerker-5',
        notify: 0,
      },
      method: 'POST',
    });
  });

  const changeDeptLacking = {
    caseId: 42,
    notify: false,
    allocationType: 'does not matter',
    assigneeId: 5,
  };

  test('Allocation to assignee without dept used', () => {
    const deptLacking = setCaseAllocation(changeDeptLacking);
    expect(deptLacking).toMatchObject({
      url: `/zaak/${changeDeptFalse.caseId}/update/allocation`,
      params: {
        change_allocation: 'behandelaar',
        change_department: 0,
        betrokkene_id: 'betrokkene-medewerker-5',
        notify: 0,
      },
      method: 'POST',
    });
  });
});

describe('getCaseDistributors', () => {
  const settings = [
    {
      reference: 'case_distributor_group',
      instance: { value: { instance: 'insert group object here' } },
    },
    {
      reference: 'case_distributor_role',
      instance: { value: { instance: 'insert role  object here' } },
    },
  ];

  test('Group/role defined in settings', () => {
    expect(getCaseDistributors(settings)).toMatchObject({
      group: 'insert group object here',
      role: 'insert role  object here',
    });
  });

  test('Group/role not defined in settings', () => {
    expect(() => {
      getCaseDistributors([]);
    }).toThrowError(ZaaksysteemException);
  });
});
