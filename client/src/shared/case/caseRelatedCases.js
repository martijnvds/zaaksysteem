// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceModule from '../api/resource';
import first from 'lodash/head';

export default angular
  .module('caseRelatedCases', [resourceModule])
  .factory('caseRelatedCases', [
    '$q',
    '$rootScope',
    'resource',
    ($q, $rootScope, resource) => {
      return {
        getRelatedCases: (caseId) => {
          if (!caseId) {
            throw new Error('No case ID provided.');
          }

          // Get the target case data so we can fetch the related case uuid's
          return resource(() => `/api/case/${caseId}`, {
            scope: $rootScope,
            cache: { disabled: true },
          })
            .asPromise()
            .then((result) => {
              // Create an array of promises to fetch each related case data
              let promises = first(result)
                .related_objects.filter(
                  (related_object) =>
                    related_object.related_object_type.toLowerCase() === 'case'
                )
                .map((related_object) => {
                  return resource(
                    () => `/api/v1/case/${related_object.related_object_id}`,
                    {
                      scope: $rootScope,
                      cache: { disabled: true },
                    }
                  )
                    .asPromise()
                    .catch((error) => {
                      return $q.reject(error);
                    });
                });

              return $q.all(promises);
            })
            .then((results) => {
              // Return an array of objects with the relevant information for each related case
              return results.map((result) => {
                let instance = first(result).instance;

                return {
                  name: `(${instance.number}) ${
                    instance.subject ? instance.subject.concat(' - ') : ''
                  } ${instance.casetype.instance.name}`,
                  number: instance.number,
                  id: instance.id,
                };
              });
            });
        },
      };
    },
  ]).name;
