// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import BaseReducer from './BaseReducer';

export default (scope, eventName) => {
  let reducer = new BaseReducer(),
    unwatcher;

  unwatcher = scope.$on(eventName, (event, ...rest) => {
    reducer.setSrc(rest);

    reducer.$setState('resolved');
  });

  reducer.onDestroy(() => {
    unwatcher();
  });

  return reducer;
};
