// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import endsWith from 'lodash/endsWith';
import first from 'lodash/head';
import getFields from './fields.js';
import get from 'lodash/get';
import includes from 'lodash/includes';
import keyBy from 'lodash/keyBy';
import keys from 'lodash/keys';
import mapKeys from 'lodash/mapKeys';
import seamlessImmutable from 'seamless-immutable';

export default class CreateContactController {
  static get $inject() {
    return [
      '$scope',
      '$rootScope',
      '$http',
      '$timeout',
      '$httpParamSerializerJQLike',
      '$q',
      '$compile',
      'resource',
      'composedReducer',
      'vormValidator',
      'snackbarService',
      'sessionService',
    ];
  }

  constructor(
    scope,
    $rootScope,
    $http,
    $timeout,
    $httpParamSerializerJQLike,
    $q,
    $compile,
    resource,
    composedReducer,
    vormValidator,
    snackbarService,
    sessionService
  ) {
    const ctrl = this;
    let pristine = true;
    let unlockTimeout;
    let defaultValues = seamlessImmutable({
      natuurlijk_persoon: {
        betrokkene_type: 'natuurlijk_persoon',
        'np-landcode': '6030',
      },
      bedrijf: {
        betrokkene_type: 'bedrijf',
        vestiging_landcode: '6030',
        rechtsvorm: '01',
      },
    });
    let values;
    let userResource = sessionService.createResource($rootScope, {
      cache: { every: 15 * 60 * 1000 },
    });
    let searchEnabled = includes(
      userResource.data().instance.active_interfaces,
      'overheidio_bag'
    );

    let initializeValues = (type) => {
      return searchEnabled
        ? seamlessImmutable({}).merge([
            defaultValues[type],
            { fieldDisabled: true },
          ])
        : seamlessImmutable({}).merge(defaultValues[type]);
    };

    values = initializeValues('natuurlijk_persoon');

    const countriesResource = resource(
      {
        url: '/api/v1/general/country_codes',
        params: {
          rows_per_page: 400,
        },
      },
      {
        scope,
        cache: {
          every: 30 * 10000,
        },
      }
    ).reduce((requestOptions, data) => {
      return (data || seamlessImmutable([]))
        .map((country) => {
          return {
            label: country.instance.label,
            value: country.instance.dutch_code,
          };
        })
        .asMutable({ deep: true });
    });

    const legalEntitiesResource = resource(
      {
        url: '/api/v1/general/legal_entity_types',
        params: {
          rows_per_page: 60,
        },
      },
      {
        scope,
        cache: {
          every: 30 * 10000,
        },
      }
    ).reduce((requestOptions, data) => {
      return (data || seamlessImmutable([]))
        .map((legalEntityType) => {
          return {
            label: legalEntityType.instance.label,
            value: legalEntityType.instance.code,
          };
        })
        .asMutable({ deep: true });
    });

    const fieldReducer = composedReducer(
      { scope },
      countriesResource,
      legalEntitiesResource,
      () => values
    ).reduce((countries, entities, vals) => {
      let visibleFields = getFields().fields;

      switch (vals.betrokkene_type) {
        case 'natuurlijk_persoon':
          visibleFields = visibleFields.concat(
            getFields({ countries }).citFields
          );

          if (vals['np-landcode'] === '6030') {
            visibleFields = visibleFields.concat(
              getFields({ vals }).persDomesticAddrFields
            );
          } else {
            visibleFields = visibleFields.concat(
              getFields().persForeignAddrFields
            );
          }

          if (vals.briefadres) {
            visibleFields = visibleFields.concat(
              getFields({ vals }).corrFields
            );
          }

          break;
        case 'bedrijf':
          visibleFields = visibleFields.concat(
            getFields({ countries, entities, vals }).orgFields
          );

          if (vals.vestiging_landcode === '6030') {
            visibleFields = visibleFields.concat(
              getFields({ vals }).orgDomesticAddrFields
            );
          } else {
            visibleFields = visibleFields.concat(
              getFields().orgForeignAddrFields
            );
          }

          if (vals['org-briefadres']) {
            visibleFields = visibleFields.concat(
              getFields({ countries, vals }).corrAddrFields
            );
          }

          if (vals.correspondentie_landcode === '6030') {
            visibleFields = visibleFields.concat(
              getFields({ vals }).corrDomesticAddrFields
            );
          }

          if (
            vals.correspondentie_landcode !== '6030' &&
            vals.correspondentie_landcode !== undefined
          ) {
            visibleFields = visibleFields.concat(
              getFields({ vals }).corrForeignAddrFields
            );
          }

          break;
      }

      return visibleFields;
    });

    const validityReducer = composedReducer(
      { scope },
      fieldReducer,
      () => values
    ).reduce((fields, vals) => {
      return vormValidator(fields, vals);
    });

    ctrl.getFields = fieldReducer.data;

    ctrl.isPristine = () => pristine;

    ctrl.values = () => values;

    ctrl.isValid = () => get(validityReducer.data(), 'valid', false);

    ctrl.getValidity = () => get(validityReducer.data(), 'validations');

    let unlockFields = (fieldType) => {
      $timeout.cancel(unlockTimeout);

      unlockTimeout = $timeout(() => {
        unlockTimeout = null;

        values = values.merge({ [`${fieldType}Disabled`]: false });
      }, 600);
    };

    let searchForAddress = (name) => {
      const fieldType = first(
        name.match(/([a-z]+[-]+\b(correspondentie)+[_])|([a-z]+[-|_])/)
      );
      const zipcode = get(values, `${fieldType}postcode`);
      const number = get(values, `${fieldType}huisnummer`);

      if (
        (endsWith(name, 'postcode') || endsWith(name, 'huisnummer')) &&
        zipcode &&
        number
      ) {
        $http({
          url: `/api/v1/address/search/?query:match:zipcode=${zipcode}&query:match:street_number=${number}`,
          method: 'GET',
        })
          .then((response) => {
            const result = first(response.data.result.instance.rows);

            if (result) {
              const street = get(result, 'instance.street');
              const suffix = get(result, 'instance.street_number_suffix');
              const city = get(result, 'instance.city');

              $timeout.cancel(unlockTimeout);
              unlockTimeout = null;

              values = values.merge([
                { [`${fieldType}straatnaam`]: street },
                {
                  [`${fieldType}huisnummertoevoeging`]:
                    result.length === 1 ? suffix : '',
                },
                { [`${fieldType}woonplaats`]: city },
                { [`${fieldType}Disabled`]: true },
              ]);
            } else {
              unlockFields(fieldType);
            }
          })
          .catch(() => {
            values = values.merge({ fieldDisabled: false });
          });
      }
    };

    ctrl.handleChange = (name, value) => {
      pristine = false;

      if (name === 'betrokkene_type') {
        values = initializeValues(value);
      } else {
        const hiddenFieldNames = [];

        if (this.areFieldsBeingHidden(name)) {
          hiddenFieldNames.push(...this.getHiddenFieldNames(name, value));
        }

        values = values.merge({ [name]: value }).without(hiddenFieldNames);

        if (this.isOrgCorrAddress(name) && value) {
          values = values.merge({ ['correspondentie_landcode']: '6030' });
        }
      }

      if (searchEnabled) {
        searchForAddress(name);
      }
    };

    ctrl.handleSubmit = () => {
      let params = {
          confirmed: 1,
          do_validation: 1,
        },
        promise;

      params = values.merge(params);

      if (params.briefadres) {
        params = params.merge({ briefadres: 1 });
      } else {
        params = params.merge({ briefadres: 0 });
      }

      promise = $http({
        url: '/betrokkene/0/bewerken',
        method: 'POST',
        params,
      }).then((response) => {
        let result = response.data.json,
          success = result.success === '1';

        if (!success) {
          return $q.reject(result);
        }

        // On successful validation from backend, redo request
        params = params.without('do_validation');

        return $http({
          url: '/betrokkene/0/bewerken',
          method: 'POST',
          params,
        });
      });

      ctrl.onClose({ $promise: promise });

      snackbarService.wait('Contactpersoon wordt aangemaakt', {
        promise,
        then: (result) => {
          const message = get(result, 'data.json.bericht');
          const url = first(
            message.match(
              /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/
            )
          );

          return {
            message: 'Het contact is aangemaakt.',
            timeout: 90000,
            actions: [{ type: 'link', label: 'Contact bekijken', link: url }],
          };
        },
        catch: (result) => {
          let errorMessages = [];
          const fieldsByName = keyBy(ctrl.getFields(), 'name');

          errorMessages = keys(
            mapKeys(
              get(result, 'msgs'),
              (value, key) =>
                `<strong>${fieldsByName[key].label}</strong>: ${value}`
            )
          );

          return `Het contact kon niet worden aangemaakt. <ul><li>${errorMessages.join(
            '</li><li>'
          )}</ul>`;
        },
      });
    };
  }

  isCountryCode(name) {
    return name === 'np-landcode' || name === 'vestiging_landcode';
  }

  isCorrAddress(name) {
    return name === 'briefadres';
  }

  isOrgCountryCode(name) {
    return name === 'vestiging_landcode';
  }

  isOrgCorrAddress(name) {
    return name === 'org-briefadres';
  }

  isOrgCorrCountryCode(name) {
    return name === 'correspondentie_landcode';
  }

  areFieldsBeingHidden(name) {
    return (
      this.isCountryCode(name) ||
      this.isCorrAddress(name) ||
      this.isOrgCountryCode(name) ||
      this.isOrgCorrAddress(name) ||
      this.isOrgCorrCountryCode(name)
    );
  }

  getHiddenFieldNames(name, value) {
    const fieldNames = [];
    const FieldsDefinitions = getFields();
    const getFieldNamesFromGroups = (groups) =>
      groups.map((group) =>
        FieldsDefinitions[group].map((fieldDefinition) => fieldDefinition.name)
      );
    const fieldGroups = [
      'persDomesticAddrFields',
      'persForeignAddrFields',
      'corrFields',
      'orgDomesticAddrFields',
      'orgForeignAddrFields',
      'corrAddrFields',
      'corrDomesticAddrFields',
      'corrForeignAddrFields',
    ];
    const [
      persDomesticAddrFields,
      persForeignAddrFields,
      corrFields,
      orgDomesticAddrFields,
      orgForeignAddrFields,
      corrAddrFields,
      corrDomesticAddrFields,
      corrForeignAddrFields,
    ] = getFieldNamesFromGroups(fieldGroups);

    if (this.isCountryCode(name)) {
      const fieldsBeingHidden =
        value === '6030'
          ? persForeignAddrFields
          : [...persDomesticAddrFields, ...corrFields];

      fieldNames.push(...fieldsBeingHidden);
    }

    if (this.isCorrAddress(name) && value === false) {
      fieldNames.push(...corrFields);
    }

    if (this.isOrgCountryCode(name)) {
      const fieldsBeingHidden =
        value === '6030' ? orgDomesticAddrFields : orgForeignAddrFields;

      fieldNames.push(...fieldsBeingHidden);
    }

    if (this.isOrgCorrAddress(name) && value === false) {
      const fieldsBeingHidden = [
        ...corrAddrFields,
        ...corrDomesticAddrFields,
        ...corrForeignAddrFields,
      ];

      fieldNames.push(...fieldsBeingHidden);
    }

    if (this.isOrgCorrCountryCode(name)) {
      const fieldsBeingHidden =
        value === '6030' ? corrForeignAddrFields : corrDomesticAddrFields;

      fieldNames.push(...fieldsBeingHidden);
    }

    return fieldNames;
  }
}
