// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouter from 'angular-ui-router';

export default angular
  .module('observableStateParams', [angularUiRouter, 'ui.router.util'])
  .factory('observableStateParams', [
    '$rootScope',
    '$location',
    '$state',
    ($rootScope, $location, $state) => {
      let getParamsFromUrl = () => {
        return (
          ($state &&
            $state.$current &&
            $state.$current.url.exec($location.path(), $location.search())) ||
          {}
        );
      };

      return {
        get: (key) => {
          let params = getParamsFromUrl();

          return key ? params[key] : params;
        },
      };
    },
  ]).name;
