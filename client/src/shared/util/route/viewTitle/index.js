// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import isArray from 'lodash/isArray';
import get from 'lodash/get';
import find from 'lodash/find';
import getActiveStates from './../getActiveStates';

export default angular
  .module('viewTitle', [angularUiRouter])
  .factory('viewTitle', [
    '$rootScope',
    '$document',
    '$injector',
    '$state',
    ($rootScope, $document, $injector, $state) => {
      let currentTitle;

      const getTitle = () => {
        let current = $state.$current,
          tree = getActiveStates(current),
          stateWithTitle = find(
            tree.concat().reverse(),
            (state) => !!state.title
          ),
          title = get(stateWithTitle, 'title', '');

        if (typeof title === 'function' || isArray(title)) {
          title = $injector.invoke(title, null, current.locals.globals);
        }

        return title;
      };

      $rootScope.$on('$stateChangeSuccess', () => {
        const title = getTitle();

        currentTitle = typeof title === 'string' ? { mainTitle: title } : title;
        $document.find('title').text(currentTitle.mainTitle);
      });

      return {
        get: () => currentTitle,
      };
    },
  ]).name;
