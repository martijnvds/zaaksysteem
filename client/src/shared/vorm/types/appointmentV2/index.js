// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import uniqueId from 'lodash/uniqueId';
import vormTemplateServiceModule from '../../vormTemplateService';
import vormInvokeModule from '../../vormInvoke';
import resourceModule from '../../../api/resource';
import composedReducerModule from '../../../api/resource/composedReducer';
import zsModalModule from '../../../ui/zsModal';
import zsAppointmentModule from '../../../ui/zsAppointment';
import template from './appointmentV2.html';

import './styles.scss';

export default angular
  .module('vorm.types.appointmentV2', [
    vormTemplateServiceModule,
    vormInvokeModule,
    resourceModule,
    composedReducerModule,
    zsModalModule,
    zsAppointmentModule,
  ])
  .directive('vormAppointmentV2', [
    '$compile', 'resource', 'composedReducer', 'zsModal',
    ($compile, resource, composedReducer, zsModal) => {
      return {
        template,
        restrict: 'E',
        require: ['vormAppointmentV2', 'ngModel'],
        scope: {
          delegate: '&',
          inputId: '&',
          tabindex: '&',
        },
        bindToController: true,
        controller: [ '$scope',
          function (scope) {
            let ctrl = this;
            let ngModel;
            let name;

            const integrationReducer = composedReducer(
              { scope },
              resource(
                () => `/api/v1/sysin/interface/get_by_module_name/koppelapp_appointments`,
                { scope, cache: { every: 60 * 60 * 1000 } }
              )
            ).reduce((integrationResource) => get(integrationResource, '[0].instance.interface_config', {}));

            ctrl.link = (controllers) => {
              [ngModel] = controllers;
              ctrl.value = ngModel.$modelValue;

              name = uniqueId('appointmentV2-');

              ngModel.$formatters.push((val) => {
                ctrl.value = val;
                return val;
              });
            };

            ctrl.getName = () => name;

            ctrl.isAvailable = () => Boolean(integrationReducer.data().endpoint);

            const onChange = (value) => {
              ctrl.value = value;
            };

            ctrl.getValue = () => get(ctrl.value, 'label');

            ctrl.getButtonLabel = () => {
              const editLabel = integrationReducer.data().update_button_label || 'Wijzigen of Verwijderen';
              const createLabel = integrationReducer.data().create_button_label || 'Aanmaken';

              return ctrl.value ? editLabel : createLabel;
            };

            ctrl.openModal = () => {
              let modal;
              let modalScope = scope.$new();

              modalScope.route = () => integrationReducer.data().endpoint;

              modalScope.appointmentId = () => get(ctrl.value, 'id', null);

              const closeModal = () => {
                modal.close();
                modalScope.$destroy();
                modal = modalScope = null;
              };

              modalScope.onSubmit = (value) => {
                ngModel.$setViewValue(value);
                onChange(value);
                closeModal();
                scope.$apply();
              };

              modal = zsModal({
                title: ctrl.value ? 'Afspraak wijzigen of verwijderen' : 'Afspraak aanmaken',
                fullWidth: true,
                el: $compile(
                  `<zs-appointment
                    calendar-url="route()"
                    on-submit="onSubmit(value)"
                    appointment-id="appointmentId()"
                  ></zs-appointment>`
                )(modalScope),
              });

              modal.open();
            };
          },
        ],
        controllerAs: 'vm',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ])
  .directive('vormAppointmentV2Display', [
    () => {
      return {
        restrict: 'E',
        scope: {
          delegateValue: '&',
          inputId: '&',
        },
        bindToController: true,
        controller: [
          '$element',
          function ($element) {
            const ctrl = this;

            $element[0].innerText = get(ctrl.delegateValue(), 'label', '-');
          },
        ],
        controllerAs: 'vm',
      };
    },
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('appointmentV2', {
        control: angular.element(
          '<vorm-appointment-v2 ng-model role="apointment-v2" aria-labelledby="{{vm.getInputId()}}-label"></vorm-appointment-v2>'
        ),
        display: angular.element(
          '<vorm-appointment-v2-display delegate-value="delegate.value" role="apointment-v2" aria-labelledby="{{vm.getInputId()}}-label"></vorm-appointment-v2-display>'
        ),
        defaults: {
          disableClear: true,
        },
      });
    },
  ]).name;
