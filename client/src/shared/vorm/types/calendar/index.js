// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import resourceModule from './../../../api/resource';
import composedReducerModule from './../../../api/resource/composedReducer';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import zsSpinnerModule from './../../../ui/zsSpinner';
import find from 'lodash/find';
import get from 'lodash/get';
import values from 'lodash/values';
import first from 'lodash/head';
import assign from 'lodash/assign';
import uniqBy from 'lodash/uniqBy';
import pickBy from 'lodash/pickBy';
import map from 'lodash/map';
import template from './template.html';
import './styles.scss';

let createProductReducer = (
  resource,
  composedReducer,
  scope,
  providerGetter
) => {
  return composedReducer(
    { scope },
    resource(
      () =>
        `/api/v1/sysin/interface/get_by_module_name/${providerGetter().type}`,
      { scope, cache: { every: 60 * 60 * 1000 } }
    ).reduce((requestOptions, data) => first(data))
  ).reduce((module) => {
    let product;

    if (module) {
      product = find(
        module.instance.interface_config.attribute_mapping,
        (attr) =>
          get(attr, 'internal_name.searchable_object_id') ===
          providerGetter().attributeName
      );
    }

    return product;
  });
};

export default angular
  .module('vorm.types.calendar', [
    vormTemplateServiceModule,
    resourceModule,
    composedReducerModule,
    snackbarServiceModule,
    zsSpinnerModule,
  ])
  .directive('vormCalendar', [
    '$http',
    'resource',
    'composedReducer',
    'dateFilter',
    'snackbarService',
    ($http, resource, composedReducer, dateFilter, snackbarService) => {
      return {
        restrict: 'E',
        template,
        require: ['vormCalendar', 'ngModel'],
        scope: {
          templateData: '&',
          provider: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              interacted,
              ngModel,
              dateResource,
              timeResource,
              productReducer,
              linkIdReducer,
              dateOptionReducer,
              timeOptionReducer,
              selectedDate,
              selectedTime;

            let getTimeLabel = (range) => {
              return `${dateFilter(
                new Date(range.start),
                'HH:mm'
              )} - ${dateFilter(new Date(range.finish), 'HH:mm')}`;
            };

            productReducer = createProductReducer(
              resource,
              composedReducer,
              scope,
              ctrl.provider
            );

            linkIdReducer = composedReducer({ scope }, productReducer).reduce(
              (product) => {
                let linkId;

                if (product) {
                  linkId = get(
                    product,
                    ctrl.provider().type === 'qmatic' ? 'linkId' : 'schedule_id'
                  );
                }

                return linkId;
              }
            );

            dateResource = resource(
              () => {
                let linkId = linkIdReducer.data(),
                  opts;

                if (linkId && interacted) {
                  opts = {
                    url: `/api/${ctrl.provider().type}/index`,
                    params: {
                      action: 'getAvailableAppointmentDays',
                      zapi_no_pager: 1,
                      productLinkID: linkId,
                    },
                  };
                }

                return opts;
              },
              { scope, cache: { disabled: true } }
            );

            dateOptionReducer = composedReducer({ scope }, dateResource).reduce(
              (dates) => {
                return dates
                  ? dates.map((date) => {
                      let myDate = new Date(date);

                      return {
                        name: date,
                        label: `${dateFilter(
                          myDate,
                          'dd-MM-yyyy'
                        )} - ${myDate.toLocaleDateString('nl-NL', {
                          weekday: 'long',
                        })}`,
                        click: () => {
                          selectedDate = date;
                        },
                      };
                    })
                  : null;
              }
            );

            timeResource = resource(
              () => {
                let linkId = linkIdReducer.data(),
                  opts;

                if (linkId && selectedDate) {
                  opts = {
                    url: `/api/${ctrl.provider().type}/index`,
                    params: {
                      action: 'getAvailableAppointmentTimes',
                      appDate: selectedDate,
                      zapi_no_pager: 1,
                      productLinkID: linkId,
                    },
                  };
                }

                return opts;
              },
              { scope, cache: { disabled: true } }
            ).reduce((requestOptions, data) => {
              let times;

              selectedTime = null;

              if (data && ctrl.provider().type === 'qmatic') {
                times = data.map((time) => {
                  return {
                    start: time,
                    finish: new Date(
                      new Date(time).getTime() +
                        Number(
                          get(productReducer.data(), 'appointLength', 30)
                        ) *
                          60 *
                          1000
                    ).toISOString(),
                  };
                });
              } else if (data && ctrl.provider().type === 'supersaas') {
                times = map(
                  uniqBy(data, (time) => time.start && time.finish),
                  (time) => {
                    return {
                      start: time.start,
                      finish: time.finish,
                      resourceID: time.resource_id,
                    };
                  }
                );
              } else {
                times = data;
              }

              return times;
            });

            timeOptionReducer = composedReducer({ scope }, timeResource).reduce(
              (times) => {
                return times
                  ? times.map((time) => {
                      return {
                        name: time.start,
                        click: () => {
                          selectedTime = time;
                        },
                        label: getTimeLabel(time),
                      };
                    })
                  : null;
              }
            );

            ctrl.link = (...controllers) => {
              [ngModel] = controllers;
            };

            ctrl.getDateLabel = () => {
              return selectedDate
                ? dateFilter(new Date(selectedDate), 'dd-MM-yyyy')
                : 'Kies een dag';
            };

            ctrl.getTimeLabel = () => {
              return selectedTime
                ? getTimeLabel(selectedTime)
                : 'Kies een tijd';
            };

            ctrl.isTimeVisible = () => !!selectedDate;

            ctrl.canConfirm = () => selectedDate && selectedTime;

            ctrl.confirmAppointment = () => {
              let data = {
                  action: 'bookAppointment',
                  productLinkID: linkIdReducer.data(),
                  aanvrager: ctrl.provider().requestor,
                },
                appValues = {};

              if (ctrl.provider().type === 'qmatic') {
                appValues = {
                  appDate: selectedDate,
                  appTime: selectedTime.start,
                };
              } else if (ctrl.provider().type === 'supersaas') {
                appValues = {
                  appTimeStart: selectedTime.start,
                  appTimeFinish: selectedTime.finish,
                  resourceID: selectedTime.resourceID,
                };
              } else {
                appValues = {
                  appTimeStart: selectedTime.start,
                  appTimeFinish: selectedTime.finish,
                };
              }

              snackbarService.wait('Afspraak wordt ingepland', {
                collapse: 0,
                promise: $http({
                  method: 'POST',
                  url: `/api/${ctrl.provider().type}/index`,
                  data: assign(data, appValues),
                }).then((response) => {
                  let value = values(
                    pickBy(appValues, (val, key) => key !== 'resourceID')
                  ).concat(get(response, 'data.result[0].appointmentId'));

                  ngModel.$setViewValue(value.join(';'), 'click');
                }),
                catch: () =>
                  'Afspraak kon niet worden ingepland. Neem contact op met uw beheerder voor meer informatie.',
              });
            };

            ctrl.closeForm = () => {
              selectedDate = selectedTime = null;
            };

            ctrl.isUnavailable = () =>
              productReducer.state() === 'resolved' && !productReducer.data();

            ctrl.getDateOptions = dateOptionReducer.data;
            ctrl.getTimeOptions = timeOptionReducer.data;

            ctrl.onDateOpen = () => {
              interacted = true;
            };

            ctrl.isDateLoading = () => dateOptionReducer.state() === 'pending';

            ctrl.isTimeLoading = () => timeOptionReducer.state() === 'pending';
          },
        ],
        controllerAs: 'vm',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(...controllers);
        },
      };
    },
  ])
  .directive('vormCalendarDisplay', [
    '$http',
    'resource',
    'composedReducer',
    'dateFilter',
    'snackbarService',
    ($http, resource, composedReducer, dateFilter, snackbarService) => {
      return {
        restrict: 'E',
        template: `<div class="object-list-item">
						<zs-icon icon-type="calendar">
						</zs-icon>
						<span class="calendar-display-text">{{vm.getLabel()}}</span>
						<button
							class="button-reset calendar-display-text-button-close"
							type="button"
							ng-click="vm.handleRemoveClick()"
							ng-disabled="vm.isRemoving()"
						>
							<zs-icon icon-type="close" ng-show="!vm.isRemoving()"></zs-icon>
							<zs-spinner class="spinner-tiny" is-loading="vm.isRemoving()"></zs-spinner>
						</button>
					</div>`,
        scope: {
          value: '&',
          onRemove: '&',
          provider: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              removing = false,
              productReducer = createProductReducer(
                resource,
                composedReducer,
                scope,
                ctrl.provider
              );

            ctrl.getLabel = () => {
              let vals = ctrl.value() ? ctrl.value().split(';') : [],
                date = dateFilter(new Date(vals[0]), 'dd-MM-yyyy'),
                time = dateFilter(
                  new Date(
                    ctrl.provider().type === 'qmatic' ? vals[1] : vals[0]
                  ),
                  'HH:mm'
                );

              return `U heeft een afspraak ingepland op ${date} om ${time}`;
            };

            ctrl.isRemoving = () => removing;

            ctrl.handleRemoveClick = () => {
              let appointmentId = ctrl.value().split(';')[2];

              removing = true;

              snackbarService
                .wait('Afspraak wordt verwijderd', {
                  collapse: 0,
                  promise: $http({
                    method: 'POST',
                    url: `/api/${ctrl.provider().type}/index`,
                    data: {
                      action: 'deleteAppointment',
                      productLinkID: get(
                        productReducer.data(),
                        ctrl.provider().type === 'qmatic'
                          ? 'linkId'
                          : 'schedule_id'
                      ) /* Only appointmentID is needed when removing Qmatic appointments */,
                      appointmentId,
                    },
                  }).then(() => {
                    ctrl.onRemove();
                  }),
                  catch: () =>
                    'Afspraak kon niet worden verwijderd. Neem contact op met uw beheerder voor meer informatie',
                })
                .catch(() => {
                  removing = false;
                });
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ])
  .run([
    '$http',
    '$q',
    'vormTemplateService',
    ($http, $q, vormTemplateService) => {
      vormTemplateService.registerType('calendar', {
        control: angular.element(
          `<vorm-calendar
							ng-model
							data-provider="vm.invokeData('provider')"
						>
						</vorm-calendar>`
        ),
        display: angular.element(
          `<vorm-calendar-display
							data-value="delegate.value"
							data-provider="vm.invokeData('provider')"
							on-remove="vm.clearDelegate(delegate)"
						></vorm-calendar-display>`
        ),
        defaults: {
          editMode: 'empty',
          disableClear: true,
        },
      });
    },
  ]).name;
