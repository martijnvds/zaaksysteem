// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../vormTemplateService';
import vormInvokeModule from './../../vormInvoke';
import composedReducerModule from './../../../api/resource/composedReducer';
import template from './template.html';
import get from 'lodash/get';
import assign from 'lodash/assign';
import map from 'lodash/map';
import sortBy from 'lodash/sortBy';
import identity from 'lodash/identity';

export default angular
  .module('vorm.types.checkboxList', [
    vormTemplateServiceModule,
    vormInvokeModule,
    composedReducerModule,
  ])
  .directive('vormCheckboxList', [
    'vormInvoke',
    (vormInvoke) => {
      return {
        restrict: 'E',
        require: ['vormCheckboxList', 'ngModel'],
        scope: {
          delegate: '&',
          templateData: '&',
        },
        template,
        bindToController: true,
        controller: [
          function () {
            let ctrl = this,
              ngModel;

            ctrl.link = (controllers) => {
              [ngModel] = controllers;
            };

            ctrl.handleCheckboxClick = (value) => {
              const modelValue = assign({}, ngModel.$modelValue, {
                [value]: !ctrl.isChecked(value),
              });
              const checkForTrue = (object) =>
                Object.keys(object).some((key) => object[key] === true);

              const newModelValue = checkForTrue(modelValue)
                ? modelValue
                : null;

              ngModel.$setViewValue(newModelValue, 'click');
            };

            ctrl.isChecked = (value) =>
              !!get(ngModel.$modelValue, value, false);

            ctrl.getOptions = () => vormInvoke(ctrl.templateData().options);
          },
        ],
        controllerAs: 'vormCheckboxList',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ])
  .directive('vormCheckboxListDisplay', [
    'composedReducer',
    (composedReducer) => {
      return {
        restrict: 'E',
        scope: {
          delegateValue: '&',
          templateData: '&',
        },
        template: `<ul>
						<li ng-repeat="item in vm.getValues() track by item.name">
							{{::item.name}}
						</li>
				 	</ul>`,
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              valueReducer;

            valueReducer = composedReducer(
              { scope },
              ctrl.delegateValue,
              ctrl.templateData
            ).reduce((values, options) => {
              let sortOrder = options.options.map((option) => option.value);

              return sortBy(
                map(values, (value, key) => {
                  if (value) {
                    return { name: key, value };
                  }
                  return false;
                }).filter(identity),
                (val) => sortOrder.indexOf(val.name)
              );
            });

            ctrl.getValues = valueReducer.data;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ])
  .run([
    'vormTemplateService',
    function (vormTemplateService) {
      const el = angular.element(
          '<vorm-checkbox-list ng-model></vorm-checkbox-list>'
        ),
        elDisp = angular.element(
          '<vorm-checkbox-list-display delegate-value="delegate.value"></vorm-checkbox-list-display>'
        );

      vormTemplateService.registerType('checkbox-list', {
        control: el,
        display: elDisp,
      });
    },
  ]).name;
