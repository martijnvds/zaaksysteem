// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import filter from './../../zql/zqlEscapeFilter/filter';
import assign from 'lodash/assign';

export default () => {
  return {
    request: (values) => {
      return {
        url: '/api/v1/casetype',
        params: {
          zql: `SELECT {} FROM casetype MATCHING ${filter(
            values.name || ''
          )} WHERE offline = "0"`,
        },
      };
    },
    reduce: (items) => {
      return items;
    },
    format: (item) => {
      return assign({}, item, {
        id: item.reference,
        label: item.instance.title,
      });
    },
    fields: [
      {
        name: 'name',
        label: 'Naam',
        template: 'text',
      },
    ],
    columns: [
      {
        id: 'name',
        label: 'Zaaktype',
        template: '<span>{{::item.instance.title}}</span>',
      },
    ],
  };
};
