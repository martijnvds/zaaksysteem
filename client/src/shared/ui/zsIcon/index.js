// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

export default angular
  .module('shared.ui.zsIcon', [])
  .directive('zsIcon', () => {
    return {
      restrict: 'E',
      scope: {
        iconType: '@',
      },
      template: '<i class="mdi mdi-{{iconType}}"></i>',
    };
  }).name;
