// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default class MessageListController {
  constructor() {
    this.hiddenByUser = [];
  }

  hideMessage(message) {
    this.hiddenByUser.push(message.id);
  }

  isHidden({ id }) {
    return this.hiddenByUser.indexOf(id) === -1;
  }

  filter(message) {
    return this.isHidden(message);
  }

  get activeMessages() {
    return this.messages().filter(this.filter, this);
  }
}
