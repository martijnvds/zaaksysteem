// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import seamlessImmutable from 'seamless-immutable';
import './email-preview.scss';

export default angular
  .module('shared.ui.zsEmailPreview', [])
  .directive('zsEmailPreview', [
    '$q',
    '$http',
    ($q, $http) => {
      return {
        restrict: 'E',
        template,
        scope: {
          previewData: '=',
          caseId: '&',
        },
        controller: [
          '$scope',
          '$element',
          function ($scope) {
            let loading = false;
            let previewData = seamlessImmutable({});

            /**
             * @param {object} data
             * @param {string} data.to
             * @param {string} data.cc
             * @param {string} data.bcc
             * @param {string} data.subject
             * @param {string} data.body
             */
            function fetchPreviewData(data) {
              const deferred = $q.defer();
              const postData = seamlessImmutable(data).merge({
                case_id: $scope.caseId(),
              });
              const options = {
                method: 'POST',
                data: postData,
                url: '/api/mail/preview',
              };

              loading = true;

              $http(options)
                .success((response) => {
                  deferred.resolve(postData.merge(response));
                })
                .error(() => {
                  deferred.reject();
                })
                .finally(() => {
                  loading = false;
                });

              return deferred.promise;
            }

            function updatePreview(values) {
              fetchPreviewData(values).then((data) => {
                const to = []
                  .concat(data.to.split(';'), data.cc)
                  .filter(Boolean)
                  .join(', ');

                previewData = previewData.merge({
                  to,
                  attachments: values.attachments,
                  from: data.from,
                  bcc: data.bcc,
                  subject: data.subject,
                  body: data.body,
                });
              });
            }

            $scope.isLoading = () => loading;

            $scope.$watch('previewData', (values, oldValues) => {
              if (values && !angular.equals(values, oldValues)) {
                updatePreview(values);
              }
            });

            $scope.getPreviewData = () => previewData;
          },
        ],
      };
    },
  ]).name;
