// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import meetingAppModule from './shared/meetingApp';
import meetingList from './meetingList';
import proposalsList from './proposalsList';
import proposalDetail from './proposalDetail';
import groupList from './groupList';
import flatten from 'lodash/flatten';
import snackbarServiceModule from './../shared/ui/zsSnackbar/snackbarService';
import sessionServiceModule from '../shared/user/sessionService';
import merge from 'lodash/merge';

let IS_STATE_CHANGE_RESOLVED = true;

export default angular
  .module('Zaaksysteem.meeting.routing', [
    uiRouter,
    meetingList.moduleName,
    proposalsList.moduleName,
    proposalDetail.moduleName,
    groupList.moduleName,
    snackbarServiceModule,
    meetingAppModule,
    sessionServiceModule,
  ])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    '$urlMatcherFactoryProvider',
    (
      $stateProvider,
      $urlRouterProvider,
      $locationProvider,
      $urlMatcherFactoryProvider
    ) => {
      $locationProvider.html5Mode(true);

      $urlMatcherFactoryProvider.strictMode(false);

      $urlRouterProvider.otherwise(($injector) => {
        let $state = $injector.get('$state');

        $state.go('groupList');
      });

      $stateProvider.state({
        name: 'root',
        template: '<meeting-app app-config="appConfig()"></meeting-app>',
        resolve: {
          user: [
            '$rootScope',
            '$q',
            '$window',
            'snackbarService',
            'sessionService',
            ($rootScope, $q, $window, snackbarService, sessionService) => {
              let resource = sessionService.createResource($rootScope);

              return resource
                .asPromise()
                .then(() => resource)
                .catch(() => {
                  $window.location = `/auth/login?referer=${$window.location.pathname}`;

                  return snackbarService.error(
                    'U bent niet ingelogd. U wordt doorverwezen naar het loginscherm.'
                  );
                });
            },
          ],
          appConfig: [
            '$rootScope',
            '$stateParams',
            '$q',
            'resource',
            'snackbarService',
            ($rootScope, $stateParams, $q, resource, snackbarService) => {
              let configResource = resource('/api/v1/app/app_meeting', {
                scope: $rootScope,
                cache: {
                  every: 60 * 1000,
                },
              }).reduce((requestOptions, data) => {
                return data || {};
              });

              return configResource
                .asPromise()
                .then(() => configResource)
                .catch((error) => {
                  snackbarService.error(
                    'Er ging iets fout bij het ophalen van de configuratie. Neem contact op met uw beheerder voor meer informatie.'
                  );
                  return $q.reject(error);
                });
            },
          ],
        },
        controller: [
          '$scope',
          'appConfig',
          'composedReducer',
          ($scope, appConfig, composedReducer) => {
            let appConfigsReducer = composedReducer(
              { scope: $scope },
              appConfig
            ).reduce((configs) => configs);

            $scope.appConfig = appConfigsReducer.data;
          },
        ],
      });

      flatten(
        [meetingList, proposalsList, proposalDetail, groupList].map(
          (routeConfig) => routeConfig.config
        )
      ).forEach((route) => {
        let mergedState = merge(route.route, {
          parent: 'root',
        });

        $stateProvider.state(route.state, mergedState);
      });

      $urlRouterProvider.rule(($injector, $location) => {
        let path = $location.path(),
          hasTrailingSlash = path[path.length - 1] === '/';

        if (IS_STATE_CHANGE_RESOLVED && !hasTrailingSlash) {
          return `${path}/`;
        }
      });
    },
  ])
  .run([
    '$rootScope',
    '$state',
    ($rootScope) => {
      $rootScope.$on('$stateChangeStart', () => {
        IS_STATE_CHANGE_RESOLVED = false;
      });
      $rootScope.$on('$stateChangeSuccess', () => {
        IS_STATE_CHANGE_RESOLVED = true;
      });
      $rootScope.$on('$stateChangeError', (...rest) => {
        console.error('$stateChangeError', rest);
      });
    },
  ]).name;
