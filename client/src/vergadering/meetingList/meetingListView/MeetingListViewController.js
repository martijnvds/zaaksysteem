// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import keyBy from 'lodash/keyBy';
import every from 'lodash/every';
import find from 'lodash/find';
import assign from 'lodash/assign';
import mapValues from 'lodash/mapValues';
import get from 'lodash/get';
import sortBy from 'lodash/sortBy';
import shortid from 'shortid';
import getAttributes from '../../shared/getAttributes';
import nGramFilter from '../../shared/nGramFilter';
import getCaseValueFromAttribute from '../../shared/getCaseValueFromAttribute';
import seamlessImmutable from 'seamless-immutable';

export default class MeetingListViewController {
  static get $inject() {
    return [
      '$scope',
      '$state',
      '$stateParams',
      '$window',
      'resource',
      'composedReducer',
      'appService',
      'snackbarService',
    ];
  }

  constructor(
    $scope,
    $state,
    $stateParams,
    $window,
    resource,
    composedReducer,
    appService,
    snackbarService
  ) {
    const ctrl = this;
    const mappings = {
      date: 'vergaderingdatum',
      time: 'vergaderingtijd',
      label: 'vergaderingtitel',
      location: 'vergaderinglocatie',
      chairman: 'vergaderingvoorzitter',
    };
    const defaultMeetingTime = '00:00';
    let itemsCollapsedState = {};

    ctrl.proposals = [];
    ctrl.meetingsOfWhichTheProposalsHaveBeenRetrieved = [];

    const appConfigReducer = composedReducer(
      { scope: $scope },
      ctrl.appConfig
    ).reduce((config) => config);

    const sortedMeetingReducer = composedReducer(
      { scope: $scope },
      ctrl.meetingResource(),
      appConfigReducer,
      () => $state.params.meetingType
    ).reduce((meetings, config, meetingType) => {
      let attributes = getAttributes(config).vergadering;

      const sortAttributeDate = find(attributes, (n) => {
        return n.external_name === 'vergaderingdatum';
      });

      const sortAttributeTime = find(attributes, (n) => {
        return n.external_name === 'vergaderingtijd';
      });

      if (!sortAttributeDate) {
        return meetings;
      }

      let sortedAttributes = sortBy(meetings, (meeting) => {
        const { attributes } = meeting.instance;
        const date =
          attributes[sortAttributeDate.internal_name.searchable_object_id];
        let time;

        if (sortAttributeTime) {
          time =
            attributes[sortAttributeTime.internal_name.searchable_object_id] ||
            defaultMeetingTime;
        } else {
          time = defaultMeetingTime;
        }

        const match = time.match(/(\d{1,2})(:|\.)?(\d{2})/);
        const hours = match ? Number(match[1]) + Number(match[3]) / 60 : 0;

        return new Date(date).getTime() + hours * 60 * 60 * 1000;
      });

      if (meetingType === 'archief') {
        sortedAttributes = sortedAttributes.reverse();
      }

      return sortedAttributes;
    });

    const getScopeProposals = () => ctrl.proposals;

    const totalProposalsReducer = composedReducer(
      { scope: $scope },
      getScopeProposals,
      () => $stateParams
    ).reduce((proposals, stateParams) => proposals.length > stateParams.paging);

    ctrl.isMoreAvailable = totalProposalsReducer.data;

    const filteredProposalReducer = composedReducer(
      { scope: $scope },
      getScopeProposals,
      () => appService.state().filters
    ).reduce((proposals, filters) => {
      const attributesToIndex = nGramFilter.getAttributesToIndex();

      return filters.length
        ? proposals.filter((proposal) =>
            filters.every((filter) =>
              attributesToIndex
                .map((attr) => getCaseValueFromAttribute(attr, proposal))
                .some((val) => val.indexOf(filter) !== -1)
            )
          )
        : proposals;
    });

    const meetingReducer = composedReducer(
      { scope: $scope },
      sortedMeetingReducer,
      filteredProposalReducer,
      appConfigReducer
    ).reduce((meetings, filteredProposals, config) => {
      const proposalsById = keyBy(filteredProposals, 'reference');
      const groups = meetings.map((meeting) => {
        const meetingObj = assign(
          {
            $id: shortid(),
            id: meeting.reference,
            casenumber: meeting.instance.number,
            children: meeting.instance.relations.instance.rows
              .filter((proposal) => proposalsById[proposal.reference])
              .map((proposal, index) =>
                proposalsById[proposal.reference].merge(
                  {
                    instance: {
                      attributes: {
                        $index: index + 1,
                      },
                    },
                  },
                  { deep: true }
                )
              ),
          },
          meeting,
          mapValues(mappings, (value) => {
            const attributeName = get(
              find(getAttributes(config).all, { external_name: value }),
              'internal_name.searchable_object_id'
            );

            return meeting.instance.attributes[attributeName];
          })
        );

        return meetingObj;
      });

      return groups;
    });

    ctrl.getMeetings = meetingReducer.data;

    ctrl.isExpanded = (id) => {
      // eslint-disable-next-line no-prototype-builtins
      return itemsCollapsedState.hasOwnProperty(id)
        ? !itemsCollapsedState[id]
        : Boolean(appService.state().expanded);
    };

    ctrl.getProposalReferencesOfMeeting = (id) =>
      ctrl
        .getMeetings()
        .find((meeting) => meeting.id === id)
        .instance.relations.instance.rows.map((row) => row.reference);

    ctrl.handleToggle = (id) => {
      const isStateExpanded = appService.state().expanded;
      const allCollapsed = every(
        meetingReducer.data(),
        (meeting) => !ctrl.isExpanded(meeting.id)
      );

      itemsCollapsedState[id] = ctrl.isExpanded(id);

      if (isStateExpanded && allCollapsed) {
        appService.dispatch('toggle_expand');
      }

      if (
        !itemsCollapsedState[id] &&
        ctrl.meetingsOfWhichTheProposalsHaveBeenRetrieved.indexOf(id) === -1
      ) {
        const proposalsOfMeeting = this.getProposalsOfMeeting(id);

        $scope.$broadcast('Proposals: Started loading.', id);

        proposalsOfMeeting.then((proposalsData) => {
          ctrl.proposals = ctrl.proposals.concat(proposalsData.data());
          ctrl.meetingsOfWhichTheProposalsHaveBeenRetrieved = ctrl.meetingsOfWhichTheProposalsHaveBeenRetrieved.concat(
            id
          );
        });
      }
    };

    ctrl.isGrouped = () => Boolean($state.current.name === 'meetingList');

    // If items are expanded, fold them in on this view
    if (appService.state().expanded) {
      appService.dispatch('toggle_expand');
    }

    ctrl.isLoading = () => meetingReducer.state() === 'pending';

    ctrl.loadMore = () => {
      $state.go($state.current.name, { paging: $stateParams.paging + 50 });
    };

    $scope.$on(
      '$destroy',
      appService.on(
        'toggle_expand',
        () => {
          itemsCollapsedState = {};
        },
        'expanded'
      )
    );

    assign(this, {
      $scope,
      $stateParams,
      appConfigReducer,
      resource,
      snackbarService,
    });
  }

  getProposalsOfMeeting(uuid) {
    const proposalResource = this.resource(
      () => {
        const {
          proposal_publication_filter_attribute,
          proposal_publication_filter_attribute_value,
        } = this.appConfigReducer.data().instance.interface_config;
        let publicationQueryPartial = '';

        if (
          proposal_publication_filter_attribute &&
          proposal_publication_filter_attribute_value
        ) {
          publicationQueryPartial = `WHERE ${proposal_publication_filter_attribute.object.column_name} = "${proposal_publication_filter_attribute_value}"`;
        }

        return {
          url: `/api/v1/case/${uuid}/relation`,
          params: {
            zql: `SELECT {} FROM case ${publicationQueryPartial} ORDER BY case.case_number`,
            rows_per_page: 100,
          },
        };
      },
      { scope: this.$scope }
    ).reduce((requestOptions, data) => {
      return data || seamlessImmutable([]);
    });

    return proposalResource
      .asPromise()
      .then(() => {
        return proposalResource;
      })
      .catch(() => {
        this.snackbarService.error(
          'Er ging iets fout bij het ophalen van de voorstellen. Neem contact op met uw beheerder voor meer informatie.'
        );
      })
      .finally(() => {
        this.$scope.$broadcast('Proposals: Done loading.', uuid);
      });
  }
}
