// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import partition from 'lodash/partition';
import filter from 'lodash/filter';
import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';

const getAttributes = (config) => {
  const [voorstel, vergadering] = partition(
    filter(
      get(config, 'instance.interface_config.attribute_mapping', []),
      (attribute) =>
        // eslint-disable-next-line no-prototype-builtins
        attribute.hasOwnProperty('internal_name') &&
        attribute.internal_name !== null
    ),
    (attribute) => attribute.external_name.indexOf('voorstel') === 0
  );

  return {
    voorstel: seamlessImmutable(voorstel),
    vergadering: seamlessImmutable(vergadering),
    all: seamlessImmutable(config.instance.interface_config.attribute_mapping),
  };
};

export default getAttributes;
