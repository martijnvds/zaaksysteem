// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import mutationServiceModule from './../../../shared/api/resource/mutationService';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import propCheck from './../../../shared/util/propCheck';

export default angular
  .module('activeSubjectActions', [
    mutationServiceModule,
    snackbarServiceModule,
  ])
  .factory('activeSubjectActions', [
    'snackbarService',
    (snackbarService) => {
      return [
        {
          type: 'active_subject/disable',
          request: () => {
            return {
              url: '/betrokkene/disable_session',
            };
          },
          reduce: (/*data, mutationData*/) => {
            return seamlessImmutable([]);
          },
        },
        {
          type: 'active_subject/enable',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                subjectId: propCheck.string,
                name: propCheck.string,
              }),
              mutationData
            );

            return {
              url: '/betrokkene/enable_session',
              data: {
                identifier: mutationData.subjectId,
              },
            };
          },
          reduce: (data, mutationData) => {
            return data.concat({
              city: null,
              email_addresses: [],
              gmid: mutationData.subjectId.split('-')[2].toString(),
              id: mutationData.subjectId,
              name: mutationData.name,
              postal_code: null,
              street: null,
              telephone_numbers: [],
              type: mutationData.subjectId.split('-')[1],
            });
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('Contact wordt geactiveerd', {
              promise,
              catch: () =>
                'Contact kon niet worden geactiveerd. Neem contact op met uw beheerder voor meer informatie',
            });
          },
        },
        {
          type: 'active_subject/kcc/enable',
          request: (/*mutationData*/) => {
            return {
              url: '/api/kcc/user/enable',
            };
          },
          reduce: (data /*, mutationData*/) => {
            return data.map((setting) => setting.merge({ user_status: 1 }));
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('', {
              promise,
              collapse: 0,
              catch: () =>
                'Aanmelden niet geslaagd. Neem contact op met uw beheerder voor meer informatie',
            });
          },
        },
        {
          type: 'active_subject/kcc/disable',
          request: (/*mutationData*/) => {
            return {
              url: '/api/kcc/user/disable',
            };
          },
          reduce: (data /*, mutationData*/) => {
            return data.map((setting) => setting.merge({ user_status: 0 }));
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('', {
              promise,
              collapse: 0,
              catch: () =>
                'Afmelden niet geslaagd. Neem contact op met uw beheerder voor meer informatie',
            });
          },
        },
        {
          type: 'active_subject/kcc/accept',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                callId: propCheck.number,
              }),
              mutationData
            );

            return {
              url: '/api/kcc/call/accept',
              data: {
                call_id: mutationData.callId,
              },
            };
          },
          reduce: (data, mutationData) => {
            return data.filter((call) => call.id !== mutationData.callId);
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('', {
              promise,
              collapse: 0,
              catch: () => 'Oproep kan niet worden aangenomen',
            });
          },
          options: {
            reloadOnComplete: true,
          },
        },
        {
          type: 'active_subject/kcc/reject',
          request: (mutationData) => {
            propCheck.throw(
              propCheck.shape({
                callId: propCheck.number,
              }),
              mutationData
            );

            return {
              url: '/api/kcc/call/reject',
              data: {
                call_id: mutationData.callId,
              },
            };
          },
          reduce: (data, mutationData) => {
            return data.filter((call) => call.id !== mutationData.callId);
          },
          wait: (mutationData, promise) => {
            return snackbarService.wait('', {
              promise,
              collapse: 0,
              catch: () => 'Oproep kan niet worden afgewezen',
            });
          },
          options: {
            reloadOnComplete: true,
          },
        },
      ];
    },
  ])
  .run([
    'mutationService',
    'activeSubjectActions',
    (mutationService, actions) => {
      actions.forEach((action) => {
        mutationService.register(action);
      });
    },
  ]).name;
