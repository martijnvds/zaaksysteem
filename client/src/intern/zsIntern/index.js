// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import controller from './controller';
import assign from 'lodash/assign';
import './styles.scss';

export default angular.module('zsIntern', [controller]).directive('zsIntern', [
  '$controller',
  ($controller) => {
    return {
      restrict: 'E',
      template,
      scope: {
        onOpen: '&',
        onClose: '&',
      },
      bindToController: true,
      controller: [
        '$scope',
        function ($scope) {
          let ctrl = this;

          assign(this, $controller(controller, { $scope }));

          ctrl.useLocation = () => true;
        },
      ],
      controllerAs: 'vm',
    };
  },
]).name;
