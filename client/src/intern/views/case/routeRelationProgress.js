// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import assign from 'lodash/assign';

import zsIconModule from '../../../shared/ui/zsIcon';
import zsModalModule from '../../../shared/ui/zsModal';
import zsSpinnerModule from '../../../shared/ui/zsSpinner';

const MODAL_TITLE = 'Alle relaties worden opgehaald';
const MODAL_DESCRIPTION =
  'Een ogenblik geduld alstublieft. Dit kan even duren…';

/**
 * @see https://mintlab.atlassian.net/browse/ZS-16102
 *
 * Improvement of perceived performance:
 * Create a modal dialog for the relations tab that gives
 * feedback about the progress of loading multiple resources.
 *
 * `route.js` depends on the relation tab URL and the
 * progress service that are provided by this module.
 */
export default angular
  .module('relationProgressService', [
    zsIconModule,
    zsModalModule,
    zsSpinnerModule,
  ])
  .constant('RELATION_TAB_URL', '/relaties')
  // This corresponds with the hardcoded section labels in the view.
  // The relation between resources and sections is **not** one to one.
  // See the relation route `resolve` object.
  .constant('caseRelationSections', [
    'Hoofd- en deelzaken', // 0
    'Gerelateerde zaken', // 1
    'Geplande zaken', // 2
    'Betrokkenen', // 3
    'Gerelateerde objecten', // 4
    'Gerelateerde objecten (beta)', // 5
    'Geplande e-mails', // 6
  ])
  .factory('relationProgressService', [
    '$rootScope',
    '$compile',
    '$state',
    '$timeout',
    'zsModal',
    'caseRelationSections',
    function relationProgressService(
      $rootScope,
      $compile,
      $state,
      $timeout,
      zsModal,
      caseRelationSections
    ) {
      const MAGIC_TIMEOUT_OPEN = 10;
      const MAGIC_TIMEOUT_CLOSE = 500;
      const MAGIC_MINIMUM_VISIBILITY = 1500;

      const caseRelationResources = caseRelationSections.reduce(
        (accumulator, label) =>
          assign(accumulator, {
            [label]: false,
          }),
        {}
      );

      const scope = assign($rootScope.$new(true), {
        sections: caseRelationSections,
        resources: caseRelationResources,
      });

      const modal = zsModal({
        title: MODAL_TITLE,
        scope,
        classes: 'relation-resource-progress-modal',
        from: {
          opacity: 0,
        },
        to: {
          opacity: 1,
        },
        el: $compile(
          `<div class="relation-resource-progress">
             <p>${MODAL_DESCRIPTION}</p>
             <ul>
               <li 
                 ng-repeat="label in sections"
               >
                 <span
                   ng-class="{ 'relation-resource-unresolved': !resources[label] }"
                 >
                 <zs-icon
                   icon-type="check"
                 ></zs-icon>
                 {{label}}
                 </span>
                 <zs-spinner
                   class="spinner-small"
                   is-loading="!resources[label]"
                 ></zs-spinner>
               </li> 
             </ul>
           </div>`
        )(scope),
      });

      /**
       * @return {boolean}
       */
      const allResourcesResolved = () =>
        caseRelationSections.every((key) => caseRelationResources[key]);

      /**
       * @returns {number}
       */
      const getTimestamp = () => Number(new Date());

      /**
       * The minimum delay ensures that the last visual feedback in
       * the dialog can be seen at all.
       * The minimum visibility prevents page flicker in case the
       * resources are loaded very fast.
       *
       * @param {number} elapsed
       * @returns {number}
       */
      const getModalCloseDelay = (elapsed) =>
        Math.max(MAGIC_TIMEOUT_CLOSE, MAGIC_MINIMUM_VISIBILITY - elapsed);

      let timestamp = 0;

      /**
       * Hide the dialog if it is open.
       */
      function hide() {
        if (timestamp > 0) {
          const elapsed = getTimestamp() - timestamp;
          const delay = getModalCloseDelay(elapsed);

          $timeout(() => {
            modal.close();
            timestamp = 0;
          }, delay);
        }
      }

      /**
       * Public API that can be injected.
       */
      return {
        /**
         * Open the dialog if there are pending resources.
         * A short delay ensures the values from the
         * local storage 'cache' (if any) can be read.
         */
        show() {
          $timeout(() => {
            if (!allResourcesResolved()) {
              timestamp = getTimestamp();
              modal.open();
            }
          }, MAGIC_TIMEOUT_OPEN);
        },
        /**
         * @param {number} index
         */
        resolve(index) {
          caseRelationResources[caseRelationSections[index]] = true;

          if (allResourcesResolved()) {
            hide();
          }
        },
        /**
         * Reset the resolution of the resources.
         * This has no performance implications because the
         * resources are cached in local storage next time.
         */
        reset() {
          for (const key of caseRelationSections) {
            caseRelationResources[key] = false;
          }
        },
      };
    },
  ])
  .run([
    '$rootScope',
    'relationProgressService',
    'RELATION_TAB_URL',
    function routeRelationProgressRun(
      $rootScope,
      relationProgressService,
      RELATION_TAB_URL
    ) {
      /**
       * @param {string} nextRouteUrl
       * @param {boolean} isCurrentRouteAuxiliary
       * @return {boolean}
       */
      const relationTabEnter = (nextRouteUrl, isCurrentRouteAuxiliary) =>
        nextRouteUrl === RELATION_TAB_URL && !isCurrentRouteAuxiliary;

      /**
       * @param {string} currentRouteUrl
       * @param {boolean} isNextRouteAuxiliary
       * @return {boolean}
       */
      const relationTabLeave = (currentRouteUrl, isNextRouteAuxiliary) =>
        currentRouteUrl === RELATION_TAB_URL && !isNextRouteAuxiliary;

      $rootScope.$on(
        '$stateChangeStart',
        /**
         * @see https://github.com/angular-ui/ui-router/wiki#state-change-events
         *
         * NB: `auxiliary` routes are URL addressable modal dialogs.
         * The previous route is visible beneath and maintains its state.
         *
         * ZS-TODO: Edge case: user navigates away (e.g. with the back button)
         * while resources are still loading.
         */
        (event, toState, toParams, fromState) => {
          if (relationTabEnter(toState.url, fromState.auxiliary)) {
            relationProgressService.show();
          }

          if (relationTabLeave(fromState.url, toState.auxiliary)) {
            relationProgressService.reset();
          }
        }
      );
    },
  ]).name;
