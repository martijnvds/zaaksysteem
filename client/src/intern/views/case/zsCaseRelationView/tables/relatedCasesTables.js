// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import shortid from 'shortid';
import capitalize from 'lodash/capitalize';
import flatten from 'lodash/flatten';
import get from 'lodash/get';
import getRelationLink from './library/getRelationLink';

const parentAndChildCasesColumns = [
  {
    id: 'drag',
    label: '',
    template: `<div class="widget-favorite-drag list-item-drag">
        <zs-icon icon-type="drag-vertical"></zs-icon>
      </div>`,
  },
  {
    id: 'type',
    label: 'Type',
    template: '<span>{{::item.relation_type_label}}</span>',
  },
];

const relatedCasesColumns = [
  {
    id: 'level',
    label: 'Niveau',
    template: '<span>{{::item.level}}</span>',
  },
];

let defaultCaseColumns = [
  {
    id: 'number',
    label: 'Nr',
    template: '<span>{{::item.number}}</span>',
  },
  {
    id: 'progress',
    label: 'Voortgang',
    template:
      '<zs-progress-bar progress="{{::item.progress}}"></zs-progress-bar>',
  },
  {
    id: 'casetype',
    label: 'Zaaktype',
    template: '<span>{{::item.casetype}}</span>',
  },
  {
    id: 'subject',
    label: 'Extra informatie',
    template: '<span>{{::item.subject}}</span>',
  },
  {
    id: 'assignee',
    label: 'Behandelaar',
    template: '<a href="{{::item.assigneeHref}}">{{::item.assigneeName}}</a>',
  },
  {
    id: 'result',
    label: 'Resultaat',
    template: '<span>{{::item.result}}</span>',
  },
];

const relationTypeDict = {
  initiator: 'Initiator',
  continuation: 'Vervolgzaak',
  plain: 'Gerelateerd',
};

const formatCase = (
  {
    reference,
    number,
    progress,
    casetype,
    subject,
    result,
    assignee,
    relatedType,
  },
  $state,
  level
) => ({
  id: shortid(),
  level,
  number,
  progress,
  casetype,
  subject,
  assigneeHref: assignee
    ? getRelationLink(
        get(assignee, 'data.id'),
        get(assignee, 'apiType')
      )
    : null,
  assigneeName: assignee ? get(assignee, 'meta.display_name') : null,
  result: result ? capitalize(result) : 'Onbekend',
  reference,
  href: $state.href('case', { caseId: number }),
  relation_type_label: get(relationTypeDict, relatedType),
});

export const parentAndChildCasesTable = (
  $state,
  parentCase,
  siblingCases,
  childCases
) => {
  const groups = parentCase
    ? { A: [parentCase], B: siblingCases, C: childCases }
    : { A: siblingCases, B: childCases };

  const groupedItems = Object.keys(groups).map((level) =>
    groups[level].map((group) => formatCase(group, $state, level))
  );

  const items = flatten(groupedItems);

  return {
    name: 'sub-cases',
    label: 'Hoofd- en deelzaken',
    columns: relatedCasesColumns.concat(defaultCaseColumns),
    items,
    actions: [],
    href: true,
  };
};

export const relatedCasesTable = (
  $state,
  relatedCases,
  canRelate,
  onCaseRelate,
  onCaseUnrelate,
  zsConfirm
) => {
  const actions = canRelate
    ? [
        {
          name: 'remove',
          type: 'click',
          label: 'Verbreek relatie',
          icon: 'close',
          click: (item, event) => {
            event.stopPropagation();
            event.preventDefault();

            zsConfirm(
              'Weet u zeker dat u de relatie met deze zaak wilt verbreken?',
              'Verwijderen'
            ).then(() => {
              onCaseUnrelate({ $reference: item.reference });
            });
          },
          visible: () => true,
        },
      ]
    : [];

  const add = {
    type: 'suggest',
    data: {
      type: 'case',
      placeholder: 'Begin te typen…',
      onSelect: (item) => {
        onCaseRelate({ $reference: item.id });
      },
    },
  };

  return {
    name: 'related-cases',
    label: 'Gerelateerde zaken',
    columns: parentAndChildCasesColumns.concat(defaultCaseColumns),
    items: relatedCases.map((relatedCase) => formatCase(relatedCase, $state)),
    actions,
    add,
    href: true,
  };
};
