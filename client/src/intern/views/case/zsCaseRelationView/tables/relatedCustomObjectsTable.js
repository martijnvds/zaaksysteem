// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import shortid from 'shortid';

const columns = [
  {
    id: 'type',
    label: 'Type',
  },
  {
    id: 'label',
    label: 'Naam',
  },
];

const relatedObjectsTable = (
  $state,
  relatedCustomObjects,
  canRelate,
  onCustomObjectRelate,
  onCustomObjectUnrelate,
  zsConfirm
) => {
  const items = relatedCustomObjects.map((customObject) => ({
    id: shortid(),
    type: customObject.attributes.name,
    label: customObject.attributes.title,
    href: `/main/object/${customObject.id}`,
    reference: customObject.id,
  }));

  const actions = canRelate
    ? [
        {
          name: 'remove',
          type: 'click',
          label: 'Verbreek objectrelatie',
          icon: 'close',
          click: (item, event) => {
            event.stopPropagation();
            event.preventDefault();

            zsConfirm(
              'Weet u zeker dat u de relatie met dit object wilt verbreken?',
              'Verwijderen'
            ).then(() => {
              onCustomObjectUnrelate({ $reference: item.reference });
            });
          },
          visible: () => true,
        },
      ]
    : [];

  const add = {
    type: 'suggest',
    data: {
      type: 'custom-object',
      placeholder: 'Begin te typen…',
      onSelect: (item) => {
        onCustomObjectRelate({ $reference: item.id });
      },
    },
  };

  return {
    name: 'related_custom_objects',
    label: 'Gerelateerde objecten (beta)',
    href: true,
    columns,
    items,
    actions,
    add,
  };
};

export default relatedObjectsTable;
