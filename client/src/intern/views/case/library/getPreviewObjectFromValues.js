// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const getPreviewObjectFromValues = (scope) => (values) => {
  const to = values.recipient_address
    ? values.recipient_address
    : values.behandelaar
        .map((behandelaar) => behandelaar.data.email)
        .join(', ');

  return {
    to,
    recipient_type:
      values.recipient_type === 'behandelaar'
        ? undefined
        : values.recipient_type,
    recipient_role: values.betrokkene_role,
    cc: values.recipient_cc,
    bcc: values.recipient_bcc,
    subject: values.email_subject,
    body: values.email_content,
    case_id: scope.caseId,
  };
};

export default getPreviewObjectFromValues;
