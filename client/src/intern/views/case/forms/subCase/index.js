// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import first from 'lodash/head';
import propCheck from './../../../../../shared/util/propCheck';

export default function getSubCase(data) {
  let capabilities = {
    expand: true,
  };

  let subjectOptions = [
    {
      value: 'natuurlijk_persoon',
      label: 'Persoon',
    },
    {
      value: 'bedrijf',
      label: 'Organisatie',
    },
  ];

  let typeOptions = [
    {
      value: 'deelzaak',
      label: 'Deelzaak',
    },
    {
      value: 'gerelateerd',
      label: 'Gerelateerde zaak',
    },
  ];

  propCheck.throw(
    propCheck.shape({
      requestorName: propCheck.string.optional,
      phaseOptions: propCheck.array,
      isLastPhase: propCheck.bool,
    }),
    data
  );

  if (data.isLastPhase) {
    typeOptions = [
      {
        value: 'vervolgzaak_datum',
        label: 'Vervolgzaak (datum)',
      },
      {
        value: 'vervolgzaak',
        label: 'Vervolgzaak (periode)',
      },
    ].concat(typeOptions);
  }

  return {
    getDefaults() {
      return {
        subcase_requestor_type: 'aanvrager',
        requestor_type: 'natuurlijk_persoon',
        type: 'deelzaak',
        resolve_in_phase: get(first(data.phaseOptions), 'value'),
        creation_style: 'form',
      };
    },

    getCapabilities() {
      return capabilities;
    },

    processChange(name, value, values) {
      let vals = values;

      if (name === 'requestor_type') {
        vals = vals.merge({ requestor: null });
      }

      return vals.merge({ [name]: value });
    },

    fields(roleOptions) {
      return [
        {
          name: 'request_title',
          template: 'title',
          label: 'Aanvragen',
          hideLabel: true,
        },
        {
          name: 'type',
          template: 'select',
          label: 'Soort',
          data: {
            options: typeOptions,
          },
          when: ['$values', (values) => values.request_title],
        },
        {
          name: 'subcase_requestor_type',
          label: 'Aanvrager deel-/vervolgzaak',
          template: 'select',
          data: {
            options: [
              {
                value: 'aanvrager',
                label: `Aanvrager van de huidige zaak (${data.requestorName})`,
              },
              {
                value: 'anders',
                label: 'Andere aanvrager',
              },
              {
                value: 'behandelaar',
                label: 'Behandelaar van de huidige zaak',
              },
              {
                value: 'betrokkene',
                label: 'Betrokkene rol van de huidige zaak',
              },
              {
                value: 'ontvanger',
                label: 'Ontvanger van de huidige zaak',
              },
            ],
          },
          required: true,
          when: ['$values', (values) => values.request_title],
        },
        {
          name: 'subcase_requestor_role',
          label: 'Type betrokkene',
          template: 'select',
          data: {
            options: roleOptions,
          },
          when: [
            '$values',
            (values) =>
              values.subcase_requestor_type === 'betrokkene' &&
              values.request_title,
          ],
        },
        {
          name: 'requestor_type',
          label: 'Andere aanvrager type',
          template: 'radio',
          data: {
            options: subjectOptions,
          },
          required: true,
          when: [
            '$values',
            (values) =>
              values.subcase_requestor_type === 'anders' &&
              values.request_title,
          ],
        },
        {
          name: 'requestor',
          label: 'Andere aanvrager',
          template: 'object-suggest',
          data: {
            objectType: ['$values', (values) => values.requestor_type],
          },
          when: [
            '$values',
            (values) =>
              values.subcase_requestor_type === 'anders' &&
              values.request_title,
          ],
        },
        {
          name: 'copy_title',
          template: 'title',
          label: 'Kopiëren',
          hideLabel: true,
        },
        {
          name: 'copy_attributes',
          template: 'checkbox',
          label: 'Kenmerken kopiëren',
          when: ['$values', (values) => values.copy_title],
        },
        {
          name: 'copy_related_objects',
          template: 'checkbox',
          label: 'Gerelateerde objecten kopiëren',
          when: ['$values', (values) => values.copy_title],
        },
        {
          name: 'copy_related_cases',
          template: 'checkbox',
          label: 'Gerelateerde zaken kopiëren',
          when: ['$values', (values) => values.copy_title],
        },
        {
          name: 'copy_subject_role',
          template: 'checkbox',
          label: 'Gerelateerde betrokkenen kopiëren',
          when: ['$values', (values) => values.copy_title],
        },
        {
          name: 'copy_roles',
          label: 'Betrokkenen met deze rollen kopiëren',
          template: 'checkbox-list',
          data: {
            options: roleOptions,
          },
          when: [
            '$values',
            (values) => values.copy_subject_role && values.copy_title,
          ],
        },
        {
          name: 'copy_selected_attributes',
          template: 'checkbox',
          label: 'Geselecteerde attributen kopiëren',
          hideField: true,
        },
        {
          name: 'handle_title',
          template: 'title',
          label: 'Behandelen',
          hideLabel: true,
        },
        {
          name: 'allocation',
          label: 'Toewijzing',
          template: 'org-unit',
          required: true,
          when: ['$values', (values) => values.handle_title],
        },
        {
          name: 'resolve_in_phase',
          template: 'select',
          label: 'Afhandelen in fase',
          data: {
            options: data.phaseOptions,
          },
          required: true,
          when: [
            '$values',
            (values) => values.type === 'deelzaak' && values.handle_title,
          ],
        },
        {
          name: 'start_date',
          template: 'date',
          label: 'Starten na',
          required: true,
          when: [
            '$values',
            (values) =>
              values.type === 'vervolgzaak_datum' && values.handle_title,
          ],
        },
        {
          name: 'start_after',
          template: {
            inherits: 'text',
            control: (el) =>
              angular.element(
                `<div>
                  ${el[0].outerHTML} dagen
                </div>`
              ),
          },
          label: 'Starten na',
          required: true,
          when: [
            '$values',
            (values) => values.type === 'vervolgzaak' && values.handle_title,
          ],
        },
        {
          name: 'automatic_assignment',
          template: 'checkbox',
          label: 'Zaak automatisch in behandeling nemen',
          when: ['$values', (values) => values.handle_title],
        },
        {
          name: 'other_title',
          template: 'title',
          label: 'Overig',
          hideLabel: true,
        },
        {
          name: 'relaties_betrokkene_role_set',
          template: 'checkbox',
          label: 'Betrokkenen toevoegen',
          when: ['$values', (values) => values.other_title],
        },
        {
          name: 'creation_style',
          template: 'select',
          label: 'Manier van starten',
          data: {
            options: [
              {
                value: 'background',
                label: 'Op de achtergrond',
              },
              {
                value: 'form',
                label: 'Webformulier (interactief)',
              },
            ],
          },
          when: [
            '$values',
            (values) => values.other_title && data.relatedCasetypeUuid,
          ],
        },
      ];
    },
  };
}
