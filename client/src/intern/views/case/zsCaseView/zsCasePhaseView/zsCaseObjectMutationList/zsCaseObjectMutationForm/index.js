// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

import caseAttrTemplateCompilerModule from './../../../../../../../shared/case/caseAttrTemplateCompiler';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import resourceModule from './../../../../../../../shared/api/resource';
import vormFieldsetModule from './../../../../../../../shared/vorm/vormFieldset';
import vormValidatorModule from './../../../../../../../shared/vorm/util/vormValidator';

import controller from './CaseObjectMutationFormController';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseObjectMutationForm', [
    vormFieldsetModule,
    composedReducerModule,
    resourceModule,
    vormValidatorModule,
    caseAttrTemplateCompilerModule,
  ])
  .component('zsCaseObjectMutationForm', {
    bindings: {
      objectTypeResource: '&',
      objectTypeLabel: '&',
      mutationType: '&',
      mutationVerb: '@',
      objectId: '&',
      defaults: '&',
      onSubmit: '&',
      onCancel: '&',
    },
    controller,
    template,
  }).name;
