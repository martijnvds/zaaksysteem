// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import reactIframeModule from '../../../shared/ui/zsReactIframe';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsIntakeView', [angularUiRouterModule, reactIframeModule])
  .directive('zsIntakeView', [
    '$state',
    '$stateParams',
    ($state) => {
      return {
        restrict: 'E',
        template,
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.onLocationChange = () => {
              this.updateLocation();
            };

            ctrl.updateLocation = () => {
              $state.go(
                'intake',
                {},
                {
                  notify: false,
                  location: 'replace',
                }
              );
            };

            ctrl.getStartUrl = () => {
              return '/main/intake';
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
