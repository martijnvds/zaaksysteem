// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import resourceModule from './../../../shared/api/resource';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import rwdServiceModule from './../../../shared/util/rwdService';

export default angular
  .module('ContactController', [
    angularUiRouterModule,
    resourceModule,
    snackbarServiceModule,
    composedReducerModule,
    rwdServiceModule,
  ])
  .controller('ContactController', [
    '$scope',
    '$state',
    'subject',
    'composedReducer',
    'rwdService',
    function (scope, $state, subjectResource, composedReducer, rwdService) {
      let ctrl = this,
        collapsedReducer,
        styleReducer;

      collapsedReducer = composedReducer({ scope }, () =>
        rwdService.isActive('small-medium-and-down')
      ).reduce((collapsedDefault) => {
        return collapsedDefault;
      });

      ctrl.isSidebarCollapsed = collapsedReducer.data;

      styleReducer = composedReducer({ scope }, () => $state.current).reduce(
        (state) => {
          return !!(state.name === 'contact.summary');
        }
      );

      ctrl.isSummary = styleReducer.data;

      ctrl.getSubject = subjectResource.data;

      const messageDictionary = {
        person: {
          address_correspondence: 'Betrokkene heeft een briefadres',
          date_of_death: 'Betrokkene is overleden',
          is_secret: 'Betrokkene heeft een indicatie “Geheim”',
        },
      };

      const messageFilter = (value) => value !== null && value !== false;

      const messageReducer = composedReducer(
        { scope },
        subjectResource,
        messageDictionary
      ).reduce((subject, messages) => {
        const { subject_type } = subject.instance;

        // eslint-disable-next-line no-prototype-builtins
        if (messages.hasOwnProperty(subject_type)) {
          const { instance } = subject.instance.subject;
          const dictionary = messages[subject_type];

          return Object.keys(dictionary)
            .filter((key) => messageFilter(instance[key]))
            .map((key, index) => ({
              id: index,
              message: dictionary[key],
            }));
        }

        return [];
      });

      ctrl.getMessages = messageReducer.data;

      scope.$on('$destroy', () => {
        [subjectResource].forEach((res) => {
          res.destroy();
        });
      });
    },
  ]).name;
