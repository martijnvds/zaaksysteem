// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import {
  getValue,
  getAddressValue,
  getCompanyTypeValue,
  getEmailValue,
  getSourceValue,
} from './format';

const summary = true;

export default [
  {
    summary,
    key: 'external_subscription',
    label: 'Bron',
    getValue: getSourceValue,
  },
  {
    summary,
    key: 'coc_number',
    label: 'KvK-nummer',
    getValue,
  },
  {
    key: 'coc_location_number',
    label: 'Vestigingsnummer',
    getValue,
  },
  {
    summary,
    key: 'company',
    label: 'Handelsnaam',
    getValue,
  },
  {
    summary,
    key: 'company_type',
    label: 'Rechtsvorm',
    getValue: getCompanyTypeValue,
  },
  {
    summary,
    key: 'address_residence',
    label: 'Vestigingsadres',
    getValue: getAddressValue,
  },
  {
    key: 'phone_number',
    label: 'Telefoonnummer',
    getValue,
  },
  {
    key: 'mobile_phone_number',
    label: 'Telefoonnummer (mobiel)',
    getValue,
  },
  {
    key: 'email_address',
    label: 'E-mailadres',
    getValue: getEmailValue,
  },
];
