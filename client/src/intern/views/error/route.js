// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './index.html';
import parseErrorData from './parseErrorData';

export default angular
  .module('Zaaksysteem.intern.error.route', [angularUiRouterModule])
  .config([
    '$stateProvider',
    ($stateProvider) => {
      $stateProvider.state('error', {
        url: '/fout/:status',
        title: ['errorData', (errorData) => errorData.header],
        template,
        params: {
          status: null,
          data: null,
          message: null,
        },
        resolve: {
          errorData: [
            '$stateParams',
            ($stateParams) => {
              return parseErrorData(
                Number($stateParams.status),
                $stateParams.data,
                $stateParams.message
              );
            },
          ],
          previousState: [
            '$state',
            ($state) => {
              return {
                name: $state.current.name,
                params: $state.current.params,
                link: $state.href($state.current.name, $state.params),
              };
            },
          ],
        },
        controller: [
          '$state',
          'previousState',
          'errorData',
          function ($state, previousState, errorData) {
            let ctrl = this;

            ctrl.errorData = errorData;

            ctrl.link = previousState.link || $state.href('home');
          },
        ],
        controllerAs: '$ctrl',
      });
    },
  ]).name;
