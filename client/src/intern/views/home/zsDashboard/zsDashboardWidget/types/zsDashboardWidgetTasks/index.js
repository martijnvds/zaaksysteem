// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import controller from './controller';
import resourceModule from './../../../../../../../shared/api/resource';
import snackbarServiceModule from './../../../../../../../shared/ui/zsSnackbar/snackbarService';
import zsObjectSuggestModule from './../../../../../../../shared/object/zsObjectSuggest';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import './styles.scss';

export default angular
  .module('intern.home.zsDashboard.zsDashboardWidget.zsDashboardWidgetTasks', [
    resourceModule,
    zsObjectSuggestModule,
    snackbarServiceModule,
    composedReducerModule,
  ])
  .directive('zsDashboardWidgetTasks', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          widgetTitle: '&',
        },
        bindToController: true,
        controller: [
          'resource',
          'composedReducer',
          'sessionService',
          '$state',
          '$scope',
          function (resource, composedReducer, sessionService, $state, $scope) {
            let ctrl = this;

            ctrl.widgetTitle({
              $getter: () =>
                ctrl.getTasks()
                  ? `Mijn openstaande taken (${ctrl.getTasks().length})`
                  : 'Mijn openstaande taken',
            });

            ctrl.placeholder = 'Zoek een zaaktype';

            controller.call(
              ctrl,
              resource,
              composedReducer,
              sessionService,
              $state,
              $scope
            );
          },
        ],
        controllerAs: 'zsDashboardWidgetTasks',
      };
    },
  ]).name;
