// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import _ from 'lodash';
import zsDashboardWidgetSearch from './types/zsDashboardWidgetSearch';
import zsDashboardWidgetCasetype from './types/zsDashboardWidgetCasetype';
import zsDashboardWidgetTasks from './types/zsDashboardWidgetTasks';
import zsDashboardWidgetCreate from './types/zsDashboardWidgetCreate';
import zsDashboardWidgetExternalUrl from './types/zsDashboardWidgetExternalUrl';
import zsDashboardWidgetTasksIframe from './types/zsDashboardWidgetTasksIframe';
import sessionServiceModule from './../../../../../shared/user/sessionService';
import template from './template.html';
import oneWayBind from './../../../../../shared/util/oneWayBind';
import zsModalModule from './../../../../../shared/ui/zsModal';
import snackbarServiceModule from './../../../../../shared/ui/zsSnackbar/snackbarService';
import exportModalTemplate from './exportModal.html';

import find from 'lodash/find';
import zsScrollFadeModule from './../../../../../shared/ui/zsScrollFade';
import './styles.scss';

export default angular
  .module('Zaaksysteem.intern.home.zsDashboard.zsDashboardWidget', [
    zsDashboardWidgetSearch,
    zsDashboardWidgetCasetype,
    zsDashboardWidgetCreate,
    zsDashboardWidgetTasks,
    zsDashboardWidgetExternalUrl,
    zsDashboardWidgetTasksIframe,
    snackbarServiceModule,
    zsScrollFadeModule,
    sessionServiceModule,
    zsModalModule,
  ])
  .directive('zsDashboardWidget', [
    '$timeout',
    'snackbarService',
    'sessionService',
    'zsModal',
    ($timeout, snackbarService, sessionService, zsModal) => {
      return {
        restrict: 'E',
        scope: {
          widget: '&',
          onWidgetCreate: '&',
          onWidgetRemove: '&',
          onWidgetDataChange: '&',
          disabled: '&',
          widgets: '&',
          compact: '&',
          settings: '&',
        },
        template,
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          '$compile',
          function ($scope, $element, $compile) {
            let ctrl = this,
              compiled,
              widgetType = ctrl.widget().widget,
              widgetUuid = ctrl.widget().id,
              tag = `zs-dashboard-widget-${widgetType}`,
              el = angular.element(`<${tag}></${tag}>`),
              filterVisible = false,
              widgetId = _.uniqueId('widget'),
              sessionResource = sessionService.createResource($scope),
              modal;

            $scope.widgetId = widgetId;
            $element.attr('id', widgetId);
            $element.attr('aria-labelledby', `${widgetId}-title`);
            $element.attr('role', 'region');
            switch (widgetType) {
              case 'create':
                el.attr(
                  'on-widget-create',
                  'zsDashboardWidget.handleWidgetCreate($widgetType)'
                );
                break;


              case 'tasks-iframe':
                el.attr(
                  'widget-uuid',
                  'zsDashboardWidget.getWidgetUuid()'
                );
                el.attr(
                  'handle-remove-click',
                  'zsDashboardWidget.handleRemoveClick()'
                );
                el.attr(
                  'on-data-change',
                  'zsDashboardWidget.handleDataChange($newData)'
                );
                break;

              default:
                el.attr('filter-query', 'zsDashboardWidget.query');
                el.attr('widget-data', 'zsDashboardWidget.getWidgetData()');
                el.attr(
                  'on-data-change',
                  'zsDashboardWidget.handleDataChange($newData)'
                );
                el.attr(
                  'is-widget-loading',
                  'zsDashboardWidget.isWidgetLoading($getter)'
                );

                break;
            }

            if (widgetType === 'search') {
              el.attr('on-select', 'zsDashboardWidget.clearQuery()');
            }

            el.attr(
              'widget-title',
              'zsDashboardWidget.getWidgetTitle($getter)'
            );
            el.attr('compact', 'zsDashboardWidget.compact()');
            el.attr('settings', 'zsDashboardWidget.settings()');

            ctrl.handleWidgetCreate = (widgetType) => {
              let forceSingleInstance = ['casetype'].indexOf(widgetType) !== -1;

              if (
                forceSingleInstance &&
                !!find(ctrl.widgets(), { widget: widgetType })
              ) {
                snackbarService.error(
                  'Dit type widget mag maar één keer worden toegevoegd.'
                );
              } else {
                ctrl.onWidgetCreate({ $widgetType: widgetType });
              }
            };

            ctrl.handleRemoveClick = () => {
              ctrl.onWidgetRemove({ $widget: ctrl.widget() });
            };

            ctrl.handleDataChange = (data) => {
              ctrl.onWidgetDataChange({ $id: widgetUuid, $data: data });
            };

            ctrl.getWidgetData = () => ctrl.widget().data;

            ctrl.getWidgetUuid = () => widgetUuid;

            ctrl.getWidgetTitle = oneWayBind();
            ctrl.isWidgetLoading = oneWayBind();

            ctrl.getModelOptions = () => [];

            ctrl.isFilterVisible = () => filterVisible;

            ctrl.showHeader = () => widgetType !== 'tasks-iframe';

            ctrl.isExportVisible = () => {
              const session = sessionResource.data();
              if (widgetType === 'search' && session) {
                const capabilities =
                  session.instance.logged_in_user.capabilities;

                return capabilities.join('') === 'dashboardgebruiker';
              } else {
                return false;
              }
            };

            ctrl.openExportModal = () => {
              modal = zsModal({
                el: $compile(angular.element(exportModalTemplate))($scope),
                title: 'Exporteren als',
                classes: 'export-modal center-modal',
              });
              modal.open();
            };

            ctrl.triggerExport = (ev) => {
              const exportType = ev.target.parentElement.querySelector(
                'input:checked'
              ).value;
              ev.preventDefault();
              modal.close();

              $scope.$broadcast('export', exportType);
            };

            ctrl.handleFilterToggleClick = () => {
              if (filterVisible) {
                ctrl.query = '';
              }

              filterVisible = !filterVisible;

              if (filterVisible) {
                $timeout(
                  () => {
                    $element.find('input')[0].focus();
                  },
                  0,
                  false
                );
              }
            };

            ctrl.clearQuery = () => {
              ctrl.query = '';

              if (filterVisible) {
                ctrl.handleFilterToggleClick();
              }
            };

            compiled = $compile(el)($scope.$new());

            // make sure close button doesn't trigger a widget drag
            $element.find('button').bind('mousedown', (event) => {
              event.stopPropagation();
            });

            $element.find('zs-dashboard-widget-content').replaceWith(compiled);
          },
        ],
        controllerAs: 'zsDashboardWidget',
      };
    },
  ]).name;
