[% MACRO rechtsvorm_display(rechtsvorm_code) BLOCK %]
    [% SET rechtsvormmap = ZCONSTANTS.kvk_rechtsvormen %]
    [% rechtsvormmap.${rechtsvorm_code} %]
[% END %]

[% MACRO land_display(code) BLOCK %]
    [% SET land_display = '' %]
    [% FOREACH landcode IN landcodes %]
        [% IF code == landcode.value %]
            [% land_display = landcode.label %]
        [% END %]
    [% END %]
    [% land_display %]
[% END %]

[% BLOCK class_row %]
    <div class="row">
        <div class="column large-4"><label class="titel">[% label | html_entity %]</label></div>
        <div class="column large-8">[% value || '-' | html_entity %]</div>
    </div>
[% END %]

[% BLOCK class_row_label %]
    <div class="row">
        <div class="column large-4"><label><strong>[% label | html_entity %]</strong></label></div>
        <div class="column large-8"></div>
    </div>
[% END %]

<div class="grid-table">

    [% UNLESS hide_bron_gegevens || pip %]
    [% PROCESS class_row label = "Type"
        value = brontype.${betrokkene.btype} %]
    [% PROCESS class_row label = "Bron"
        value = !betrokkene.authenticated ? 'Zaaksysteem' : broninfo.${betrokkene.btype} %]
    [% END %]

    [%

        PROCESS class_row
            label = "Registratiedatum"
            value = betrokkene.date_registration.set_time_zone('Europe/Amsterdam').set_locale('nl_NL').strftime('%e %B %Y')
        ;

        PROCESS class_row
            label = "Oprichtingsdatum"
            value = betrokkene.date_founded.set_time_zone('Europe/Amsterdam').set_locale('nl_NL').strftime('%e %B %Y')
        ;

        PROCESS class_row
            label = "Opheffingsdatum"
            value = betrokkene.date_ceased.set_time_zone('Europe/Amsterdam').set_locale('nl_NL').strftime('%e %B %Y')
        ;

        PROCESS class_row
            label = "RSIN"
            value = betrokkene.rsin
        ;

        PROCESS class_row
            label = "OIN"
            value = betrokkene.oin
        ;

        PROCESS class_row
            label = "KVK-nummer"
            value = betrokkene.dossiernummer
        ;

        PROCESS class_row
            label = "Vestigingsnummer"
            value = betrokkene.vestigingsnummer
        ;

        PROCESS class_row
            label = "Handelsnaam"
            value = betrokkene.handelsnaam
        ;

        PROCESS class_row
            label = "Rechtsvorm"
            value = rechtsvorm_display( betrokkene.rechtsvorm )
        ;

        PROCESS class_row
            label = "Correspondentieadres"
            value = betrokkene.is_briefadres ? 'Ja' : 'Nee'
        ;

        PROCESS class_row_label
            label = "Activiteiten";

        PROCESS class_row
            label = "Hoofdactiviteit"
            value = betrokkene.main_activity
        ;

        PROCESS class_row
            label = "Nevenactiviteiten"
            value = betrokkene.secondairy_activities
        ;

        PROCESS class_row_label
            label = "Adresgegevens";

    %]

    [% PROCESS class_row label = "Vestiging land"
        value = land_display( betrokkene.vestiging_landcode ) %]
    [% IF ( betrokkene.vestiging_landcode ) == 6030 %]
        [% PROCESS class_row label = "Vestiging straatnaam"
            value = betrokkene.vestiging_straatnaam %]
        [% PROCESS class_row label = "Vestiging huisnummer"
            value = betrokkene.vestiging_huisnummer %]
        [% PROCESS class_row label = "Vestiging huisnummerletter"
            value = betrokkene.vestiging_huisletter %]
        [% PROCESS class_row label = "Vestiging huisnummertoevoeging"
            value = betrokkene.vestiging_huisnummertoevoeging %]
        [% PROCESS class_row label = "Vestiging postcode"
            value = betrokkene.vestiging_postcode %]
        [% PROCESS class_row label = "Vestiging woonplaats"
            value = betrokkene.vestiging_woonplaats %]
    [% ELSE %]
        [% PROCESS class_row label = "Vestiging adresregel 1"
            value = betrokkene.vestiging_adres_buitenland1 %]
        [% PROCESS class_row label = "Vestiging adresregel 2"
            value = betrokkene.vestiging_adres_buitenland2 %]
        [% PROCESS class_row label = "Vestiging adresregel 3"
            value = betrokkene.vestiging_adres_buitenland3 %]
    [% END %]
    
    [% IF betrokkene.is_briefadres %]

    [% PROCESS class_row_label label = "Correspondentieadres" %]

    [% PROCESS class_row label = "Correspondentie land"
        value = land_display( betrokkene.correspondentie_landcode ) %]
    [% IF ( betrokkene.correspondentie_landcode ) == 6030 %]
        [% PROCESS class_row label = "Correspondentie straatnaam"
            value = betrokkene.correspondentie_straatnaam %]
        [% PROCESS class_row label = "Correspondentie huisnummer"
            value = betrokkene.correspondentie_huisnummer %]
        [% PROCESS class_row label = "Correspondentie huisnummerletter"
            value = betrokkene.correspondentie_huisletter %]
        [% PROCESS class_row label = "Correspondentie huisnummertoevoeging"
            value = betrokkene.correspondentie_huisnummertoevoeging %]
        [% PROCESS class_row label = "Correspondentie postcode"
            value = betrokkene.correspondentie_postcode %]
        [% PROCESS class_row label = "Correspondentie woonplaats"
            value = betrokkene.correspondentie_woonplaats %]
    [% ELSE %]
        [% PROCESS class_row label = "Correspondentie adresregel 1"
            value = betrokkene.correspondentie_adres_buitenland1 %]
        [% PROCESS class_row label = "Correspondentie adresregel 2"
            value = betrokkene.correspondentie_adres_buitenland2 %]
        [% PROCESS class_row label = "Correspondentie adresregel 3"
            value = betrokkene.correspondentie_adres_buitenland3 %]
    [% END %]

    [% END %]

    [% PROCESS class_row_label label = "Contactpersoon" %]

    [% PROCESS class_row label = "Contactpersoon voorletters"
        value = betrokkene.contact_voorletters %]
    [% PROCESS class_row label = "Contactpersoon tussenvoegsel"
        value = betrokkene.contact_voorvoegsel %]
    [% PROCESS class_row label = "Contactpersoon achternaam"
        value = betrokkene.contact_geslachtsnaam %]

    <div class="row">
        <div class="column large-4"><label>Telefoonnummer:</label></div>
        <div class="column large-8 validator-next-line">
            <input type="text" name="npc-telefoonnummer" class="input_medium" value="[% betrokkene.telefoonnummer %]" />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>
    <div class="row">
        <div class="column large-4"><label>Telefoonnummer (mobiel):</label></div>
        <div class="column large-8 validator-next-line">
            <input type="text" name="npc-mobiel" class="input_medium" value="[% betrokkene.mobiel | html_entity %]" />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>
    <div class="row">
        <div class="column large-4"><label>E-mail:</label></div>
        <div class="column large-8 validator-next-line">
            <input type="text" name="npc-email" class="input_medium" value="[% betrokkene.email | html_entity %]" />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>

    [%
        IF !betrokkene_anonymous;
            PROCESS betrokkene/preferred_contact_channel.tt;
        END;
    %]

    [% UNLESS pip %]
    <div class="row">
        <div class="column large-4"><label>Interne notitie:</label></div>
        <div class="column large-8"><input type="text" name="npc-note" class="infinite" value="[% betrokkene.note | html_entity %]"><span class="validator"></span></div>
    </div>
    [% END %]
</div>

[% IF pip %]
<div class="form-actions">
     <input type="submit" value="Opslaan" class="button button-primary" />    
</div>       
[% END %]
