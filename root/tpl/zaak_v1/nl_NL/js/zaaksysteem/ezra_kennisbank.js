/* global ezra_dialog,getOptions,zvalidate */
$(document).ready( function() {
    $('.ezra_dagobert_editor').each( function() { $(this).hide() });

    $('.ezra_kennisbank_submit input,.ezra_kennisbank_submit select,.ezra_kennisbank_submit textarea')
        .change( function() {
            $(this).closest('form').removeClass('invalidation');
    });

    if ($('.ezra_kennisbank_submit').length) {
        $('.ezra_kennisbank_submit').submit(function() {

            var obj = $(this);
            var callback = function () {
                obj.closest('form').removeClass('invalidation');

                ezra_dialog({
                    url: obj.attr('action') + '?confirm=1',
                    title: 'Omschrijving wijziging'
                }, function() {

                    // Set focus at end of textbox
                    $(this)
                        .find('input[name="commit_message"]')
                        .focus(function() {
                            $(this).val($(this).val());
                        });

                    $('.ezra_kennisbank_confirm').submit(function() {

                        $('.ezra_kennisbank_submit').append(
                            '<input type="hidden" name="confirmed" value="1" />'
                        );

                        var commit_message = $(this)
                            .find('input[name="commit_message"]').val();

                        $('.ezra_kennisbank_submit').append(
                            '<input type="hidden" name="commit_message"'
                            + ' value="' + commit_message
                            + '" />'
                        );

                        $('.ezra_kennisbank_submit').submit();

                        return false;
                    });
                });
            }

            if (obj.find('input[name="confirmed"]').length) {
                return true;
            }

            return zvalidate(obj, {
                callback: callback
            });
        });
    };

    if ($('.ezra_kennisbank_add_relatie').length) {
        $('.ezra_kennisbank_add_relatie').each(function() {
            var obj = $(this);
            var options = getOptions(obj.attr('rel'));

            var ulelem = '.ezra_kennisbank_add_relatie-container '
                + 'ul.ezra_kennisbank_add_relatie-id-'
                + options.relatie;


            $('.ezra_kennisbank_add_relatie-remove').click(function() {
                ezra_kennisbank_add_relatie_remove($(this), $(ulelem));
                return false;
            });

            obj.ezra_search_box({
                url: obj.attr('href'),
                title: obj.attr('title'),
                select_handler: function(selected_obj) {
                    var id = selected_obj.attr('id');
                    id = id.match(/zaaktype_id-(\d+)/).pop();

                    var name = selected_obj.find('td').html();

// TODO move html to backend
                    var newelem = $('<li>' + name
                        + '<input type="hidden" name="'
                        + options.relatie + '" value="'
                        + id + '" />'
                        + '<a href="/noscript" class="'
                        + 'ezra_kennisbank_add_relatie-remove right icon-del icon"'
                        + '></a>'
                    );

                    var ulelem = '.ezra_kennisbank_add_relatie-container '
                        + 'ul.ezra_kennisbank_add_relatie-id-'
                        + options.relatie;

                    newelem.find('.ezra_kennisbank_add_relatie-remove').click(function() {
                        ezra_kennisbank_add_relatie_remove($(this), $(ulelem));
                        return false;
                    });

                    if (
                        $(ulelem)
                        .find('li')
                        .not('ezra_kennisbank_add_relatie-'
                            +'geenrelaties')
                        .length > 0
                    ) {
                        $(ulelem)
                            .find('li.ezra_kennisbank_add_relatie-'
                                +'geenrelaties')
                            .hide();
                    }

                    $(
                        '.ezra_kennisbank_add_relatie-container '
                        + 'ul.ezra_kennisbank_add_relatie-id-'
                        + options.relatie
                    ).append(newelem);
                }
            });
        });
    }

    $(document).on('click','.ezra_kennisbank_content a', function() {
        window.open($(this).attr('href'));
        return false;
    });

});



function ezra_kennisbank_add_relatie_remove(clicked_obj, parent_element) {
    clicked_obj.closest('li').remove();
    if (
        parent_element
        .find('li')
        .not('ezra_kennisbank_add_relatie-'
            + 'geenrelaties')
        .length < 2
    ) {
        parent_element
        .find('li.ezra_kennisbank_add_relatie-'
            +'geenrelaties')
        .show();
    }
}
