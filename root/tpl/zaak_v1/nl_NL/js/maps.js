/* global
   GEvent,GLatLng,GMarker,address,geocoder,
   mapnik,markers,place,point,width_match */
// ZS-TODO: create a 'private' scope to declare implicit (above) and protect explicit (below) globals.
var SHADOW_Z_INDEX = 10;
var MARKER_Z_INDEX = 11;
var DIAMETER = 200;
var NUMBER_OF_FEATURES = 15;
var m;
var map;
var start;
var marker;

function ezra_openlayers()
{
    /*
        TODO Open layers integratie
    */

    OpenLayers.Layer.OSM.MapnikLocalProxy = OpenLayers.Class(OpenLayers.Layer.OSM, {
        /**
         * Constructor: OpenLayers.Layer.OSM.MapnikLocalProxy
         *
         * Parameters:
         * name - {String}
         * options - {Object} Hashtable of extra options to tag onto the layer
         */
        initialize: function(name, options) {
            var url = [
                "/maps-tiles/${z}/${x}/${y}.png"
            ];
            options = OpenLayers.Util.extend({ numZoomLevels: 19 }, options);
            var newArguments = [name, url, options];
            OpenLayers.Layer.OSM.prototype.initialize.apply(this, newArguments);
        },

        CLASS_NAME: "OpenLayers.Layer.OSM.MapnikLocalProxy"
    });

    var lat = $('input[name=latitude]').val();
    var lon = $('input[name=longitude]').val();

    var zoom = 3;

    if ($("#ezramap").hasClass('olMap')) {
        return;
    }
    /* Check for custom width */
    var width_class     = $('#ezramap').attr('class');
    var ezramap_width   = 400;

    if (width_class && width_class.match(/custom_width/)) {
        width_match = width_class.match(/.*custom_width_(\d+)/);

        ezramap_width = width_match[1];
    }

    $("#ezramap").css({width: ezramap_width + "px",height:"300px"})


    var map = new OpenLayers.Map("ezramap");

    if ("https:" == document.location.protocol) {
        mapnik = new OpenLayers.Layer.OSM.MapnikLocalProxy();
    } else {
        mapnik = new OpenLayers.Layer.OSM();
    }

    map.addLayer(mapnik);
    map.setCenter(new OpenLayers.LonLat(lon,lat) // Center of the map
         .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            new OpenLayers.Projection("EPSG:900913") // to Spherical Mercator Projection
          ), 13 // Zoom level
    );

    // Layer toevoegen voor de marker(s)
    markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);

// Already longitude and latitude are known, why talk to the server about it?
// because earlier design decisions.. can be refactored to right away have the right coordinates
    if (
        $('.ezramap_container input[type="text"]').length ||
        $('.ezramap_container input[type="hidden"]').length
    ) {
        var address = $('.ezramap_container input[type="text"]').val()
            || $('.ezramap_container input[type="hidden"]').val();

        $.getJSON('/plugins/maps/retrieve',
            {'query':address},
            function(data) {
                if (data.json.maps.succes == '1')
                {
                    var geo = data.json.maps.coordinates.split(' ');
                    lat = geo[0];
                    lon = geo[1];
                    var lonlat = new OpenLayers.LonLat(lon,lat);
                    map.setCenter(lonlat // Center of the map
                      .transform(
                        new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                        new OpenLayers.Projection("EPSG:900913") // to Spherical Mercator Projection
                      ), 15 // Zoom level
                    );
                    var size = new OpenLayers.Size(21,25);
                    var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
                    var icon    = new OpenLayers.Icon('/images/marker.png', size, offset);
                    marker = new OpenLayers.Marker(lonlat,icon.clone());
                    markers.addMarker(marker);
                }
            }
        );
    }

        function handleMapClick(e) {

            var lonlat = map.getLonLatFromViewPortPx(e.xy);
            var popup;

            lonlat.transform(
                new OpenLayers.Projection("EPSG:900913"),
                new OpenLayers.Projection("EPSG:4326") // to Spherical Mercator Projection
            );
            $.getJSON('/plugins/maps/retrieve',
                {'query':lonlat.lat+' '+lonlat.lon},
                function(data) 	{
                    if (data.json.maps.succes == '1')
                    {
                        var geo = data.json.maps.coordinates.split(' ');
                        lat = geo[0];
                        lon = geo[1];
                        var lonlat = new OpenLayers.LonLat(lon,lat);
                        lonlat // Center of the map
                          .transform(
                            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                            new OpenLayers.Projection("EPSG:900913") // to Spherical Mercator Projection
                          );
                        //setMarker(lonlat,1);
                        //return;
                        if (marker && marker.isDrawn()) {
                            marker.destroy();

                            if (popup) {
                                popup.destroy();
                            }
                        }

                        $('.ezramap_container input[type="text"]').val(data.json.maps.adres);
                        setMarker(lonlat);
                        var size = new OpenLayers.Size(21,25);
                        var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
                        var icon    = new OpenLayers.Icon('/images/marker.png', size, offset);
                        marker = new OpenLayers.Marker(lonlat,icon.clone());
                        markers.addMarker(marker);
                        popup = new OpenLayers.Popup.AnchoredBubble("Test",
                            lonlat,
                             new OpenLayers.Size(150,60),
                             "<font size=-2>"+data.json.maps.adres);
                        map.addPopup(popup);
                        popup.hide();
                        popup.opacity=0.5;
                        marker.events.register('mouseover', marker, function (e) { popup.toggle(); OpenLayers.Event.stop (e); } );
                        marker.events.register('mouseout', marker, function (e) { popup.hide(); OpenLayers.Event.stop (e); } );
                    } else {
                        $('.ezramap_container input[type="text"]').val('Fout bij het ophalen van adres');
                    }
                }
            );
        }

        function setMarker(lonLatMarker){
            var feature = new OpenLayers.Feature(markers, lonLatMarker);
            feature.closeBox = true;
            feature.popupClass = OpenLayers.Class(OpenLayers.Popup.AnchoredBubble, {minSize: new OpenLayers.Size(300, 180) } );
            feature.data.popupContentHTML = '-';
            feature.data.overflow = "hidden";

            var size = new OpenLayers.Size(21,25);
            var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
            var icon    = new OpenLayers.Icon('/images/marker.png', size, offset);
            var marker = new OpenLayers.Marker(lonLatMarker, icon);
            marker.feature = feature;

            var markerClick = function(evt) {
                if (this.popup == null) {
                    this.popup = this.createPopup(this.closeBox);
                    map.addPopup(this.popup);
                    this.popup.show();
                } else {
                    this.popup.toggle();
                }
                OpenLayers.Event.stop(evt);
            };
            marker.events.register("mousedown", feature, markerClick);

            markers.addMarker(marker);
        }

        map.events.register('click', map, handleMapClick);
//    }
}


function drawFeatures() {
}

function ezra_maps_retrieve(query)
{
    $.getJSON('/plugins/maps/retrieve',
        {'query':query},
        function(data) 	{
            if (data.json.maps.succes == '1')
            {
                var address = $('.ezramap_container input[type="hidden"]').val(data.json.maps.adres);
            } else {
            }
        }
    );
}


function ezra_update_address(place, position) {
    $('.ezramap_container input[type="text"]').val(place.address);
}


/* not called anywhere
function ezra_gmaps_initialize() {
    map = new GMap2(m);
    map.setCenter(start, 13);
    map.addControl(new GLargeMapControl());
    map.addControl(new GMapTypeControl());
    map.addControl(new GScaleControl());
    geocoder = new GClientGeocoder();

    if ($('.ezramap_container input[type="hidden"]').length) {
        var address = $('.ezramap_container input[type="hidden"]').val();
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
                return;
            } else {
              map.setCenter(point, 13);
              var marker = new GMarker(point);
              map.addOverlay(marker);
              // marker.openInfoWindowHtml(address);
            }
          }
        );
    } else {
        GEvent.addListener(map, "click", getAddress);
    }
    $('#tabinterface').bind('tabsshow', function(event, ui) {
        if (ui.panel.id == "zaak-elements-case") {
            map.checkResize();
        }
    });
    $('#zaak_zaakinformatie_accordion').bind(
            'accordionchange',
            function (event,ui) {
                map.checkResize();
            }
    );
}
*/

function getAddress(overlay, latlng) {
    if (latlng != null) {
        address = latlng;
        geocoder.getLocations(latlng, showAddress);
    }
}

function showAddress(response) {
    map.clearOverlays();
    if (!response || response.Status.code != 200) {
        alert("Google kan momenteel niet aan deze aanvraag voldoen");
    } else {
        place = response.Placemark[0];
        point = new GLatLng(
            place.Point.coordinates[1],
            place.Point.coordinates[0]
        );

        marker = new GMarker(point, { draggable: true });
        map.addOverlay(marker);

        marker.openInfoWindowHtml(
            '<b>Exacte locatie: </b>' + place.Point.coordinates[1] + ", " + place.Point.coordinates[0] + '<br>' +
            '<b>Adres: </b>' + place.address + '<br>'
        );

        GEvent.addListener(marker, 'dragend', function(position) {
                // position is a GLatLng containing the position of
                // of where the marker was dropped
                getAddress(null, position);
        });

        ezra_update_address(place, point);
    }
}
