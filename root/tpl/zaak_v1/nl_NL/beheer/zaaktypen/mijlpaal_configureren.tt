[% BLOCK milestone_table_kenmerken %]

[%
  is_object=0;
  is_object_v2=0;

  object;

  # Clunky, but the DB and ZTB don't really mix well because of
  # different naming schemes. Consistency is key.. :/
  attribute_value = mkenmerk.object_id || mkenmerk.custom_object_uuid || mkenmerk.bibliotheek_kenmerken_id;

  attribute_label = mkenmerk.naam;
  class_group = '';


  IF (mkenmerk.type == 'objecttype' || mkenmerk.object_id );
  THEN;
    is_object = 1;
    object = c.model('Object').retrieve('uuid', attribute_value);
  ELSIF mkenmerk.type == 'custom_object';
    is_object_v2 = 1;

    object = c.model('DB').resultset('CustomObjectType').search_rs({uuid => attribute_value }).first;
  END;

  IF mkenmerk.is_group;
  THEN;
    class_group = 'ezra_kenmerk_grouping_group mijlpaal_element_rij_groep';
  END;
%]

<tr
    class="mijlpaal_element_rij ezra_table_row[% (!mkenmerk ?  '_template' : '') %]
            [% class_group %]"
           id="ezra_table_kenmerk_row_number[% (mkenmerk ? '_' _ mkenmerki : '') %]">
    <td class="td15 ver_center no-border drag"></td>
    <td class="td_14px tdvar">
        [% IF is_object || is_object_v2 %]
        <i class="mdi mdi-hexagon-outline icon-light-blue icon-lebensraum"></i>
        [% END %]
        <input type="hidden"
               class="rowid ezra_table_row_edit_identifier"
               name="params.status.kenmerken.bibliotheek_kenmerken_id[% (mkenmerk ? '.' _ mkenmerki : '') %]"
               value="[% attribute_value | html %]"
        />
        <input type="hidden" class="rownaam"
               name="params.status.kenmerken.naam[% (mkenmerk ? '.' _ mkenmerki : '') %]"
               value="[% attribute_label | html %]"
        />
        <input type="hidden" class="roworder"
               name="params.status.kenmerken.mijlsort[% (mkenmerk ? '.' _ mkenmerki : '') %]"
               value="[% mkenmerki %]" />
        <input type="hidden" class="rowtype"
               name="params.status.kenmerken.type[% (mkenmerk ? '.' _ mkenmerki : '') %]"
               value="[% mkenmerk.type %]" />

        <span class="rownaam [% mkenmerk.properties.text_content && 'text-block-title' %]">
          [%
            IF mkenmerk.is_group;
                GET mkenmerk.label;
            ELSIF is_object;
                GET object.name;
            ELSIF is_object_v2;
                GET object.custom_object_type_version_id.name;
                %]
                <strong><i>(beta)</i></strong>
                [%
            ELSE;
                GET mkenmerk.naam || mkenmerk.label;
            END;
          %]
        </span>
    </td>
    <td class="row-actions">
        <ul class="row-details">
            <li class="row-detail"/>
                [% IF mkenmerk.referential %]
                    <i class="icon-font-awesome icon-sitemap" title="Referentieel"></i>
                [% END %]
            </li>
            <li class="row-detail"/>
                [% IF mkenmerk.value_mandatory; %]
                    <i class="icon-font-awesome icon-asterisk" title="Verplicht"></i>
                [% END %]
            </li>
            <li class="row-detail"/>
                [% IF mkenmerk.pip || mkenmerk.publish_public; %]
                    <i
                        class="icon-font-awesome icon-eye-open"
                        title="[%
                            'Publicatie op: ' _
                            (mkenmerk.pip ? 'PIP' : '') _
                            (mkenmerk.pip && mkenmerk.publish_public ? ', ' : '') _
                            (mkenmerk.publish_public ? 'Website' : '')
                        %]">
                    </i>
                [% END %]
            </li>
            <li class="row-detail"/>
                [% IF mkenmerk.pip_can_change; %]
                    <i class="icon-font-awesome icon-keyboard" title="Aanvrager/gemachtigde kan kenmerk wijzigen in PIP"></i>
                [% END %]
            </li>
            <li class="row-detail"/>
                [% IF mkenmerk.bag_zaakadres; %]
                    <i class="icon-font-awesome icon-map-marker" title="Gebruiken als zaakadres"></i>
                [% END %]
            </li>
            <li class="row-detail"/>
                [% IF mkenmerk.is_systeemkenmerk; %]
                    <i class="icon-font-awesome icon-cogs" title="Systeemkenmerk"></i>
                [% END %]
            </li>
        </ul>
    </td>
    <td class="row-actions td100">
        <div class="row-action">

            [% IF mkenmerk.bibliotheek_kenmerken_id || is_object || is_object_v2 || mkenmerk.properties.text_content %]
                [% dropdown_content = BLOCK %]
                    [% IF mkenmerk.type == 'objecttype' %]
                        <strong>Naam:</strong> [% object.name %]<br/>
                        <strong>Type:</strong> Objecttype
                    [% ELSIF mkenmerk.type == 'custom_object' %]
                        <strong>Naam:</strong> [% object.custom_object_type_version_id.name %]
                        <br/>
                        <strong>Type:</strong> Objecttype (beta)
                    [% ELSIF mkenmerk.properties.text_content %]
                        <strong>Titel:</strong> [% mkenmerk.label | html %]<br/>
                        <strong>Tekstblok:</strong> [% mkenmerk.properties.text_content %]
                    [% ELSE %]
                        [% bibliotheek_kenmerk = c.model('DB::BibliotheekKenmerken').find(mkenmerk.bibliotheek_kenmerken_id) %]
                        <strong>Naam:</strong> [% bibliotheek_kenmerk.naam %]<br/>
                        <strong>Titel:</strong> [% mkenmerk.label || '-' %]<br/>
                        <strong>Type:</strong> [% constants.veld_opties.${bibliotheek_kenmerk.value_type}.label %]<br/>
                        <strong>Magic string:</strong> [[[% bibliotheek_kenmerk.magic_string %]]]<br/>
                        <strong>Gepubliceerde versie:</strong> [% mkenmerk.version || 'niet beschikbaar' %]<br/>
                        <strong>Actuele versie:</strong> [% bibliotheek_kenmerk.version %]
                    [% END %]
                [% END %]
                [% PROCESS widgets/general/dropdown.tt
                    icon = 'mdi-information-outline',
                    icon_type= 'mdi',
                    dropdown_content = dropdown_content
                %]
            [% END %]
        </div>
        [%- SET ezra_cb = '';
            SET ezra_layout = 'xlarge';
            SET ezra_attribute_type = 'kenmerk';
            SET ezra_label = 'Kenmerk';
            SET ezra_href = formaction;

            IF mkenmerk.is_group;

                SET ezra_cb = 'ezra_kenmerk_grouping_update_row';
                SET ezra_label = 'Kenmerkgroep';

            ELSIF mkenmerk.properties.text_content;

                SET ezra_cb = 'ezra_text_block_update_row';

            END;

            IF is_object;

                SET ezra_href = ezra_href _ '/objecttype';
                SET ezra_layout ='mediumsmall';
                SET ezra_attribute_type = 'objecttype';
                SET ezra_label = 'Object';

            ELSIF is_object_v2;
                SET ezra_href = ezra_href _ '/custom_object';
                SET ezra_layout ='mediumsmall';
                SET ezra_attribute_type = 'custom_object';
                SET ezra_label = 'Object (beta)';

            ELSIF mkenmerk.is_group;
                SET ezra_href = ezra_href _ '/kenmerkgroup';

            ELSIF mkenmerk.properties.text_content;
                SET ezra_href = ezra_href _ '/text_block';
                SET ezra_label = 'Kenmerkgroep';

            ELSE;
                SET ezra_href = ezra_href _ '/kenmerk';
            END;
            SET ezra_href = ezra_href _ '/bewerken';

        %]
        <a
            href="[% ezra_href %]"
            class="edit ezra_table_row_edit row-action"
            rel="ezra_dialog_layout: [% ezra_layout %]; callback: [% ezra_cb %]; kenmerk_type: [% ezra_attribute_type %]"
        <a
            href="#"
            class="ezra_table_row_del"
            title="[% ezra_label %] bewerken"
        >
            <i class="icon-font-awesome icon-pencil"></i>
        </a>
        <a
            href="#"
            class="ezra_table_row_del"
            title="[% ezra_label %] verwijderen"
        >
            <i class="icon-font-awesome icon-remove row-action"></i>
        </a>
    </td>
</tr>
[% END %]


[% BLOCK milestone_table_sjablonen %]
<tr
    class="ezra_table_row[% (!msjabloon ? '_template' : '') %]"
    id="ezra_table_sjabloon_row_number[% (msjabloon ? '_' _ msjablooni : '') %]">
    <td class="td15 ver_center no-border drag"></td>
    <td class="td_14px tdvar">
        <input type="hidden" class="rowid ezra_table_row_edit_identifier" name="params.status.sjablonen.bibliotheek_sjablonen_id[% (msjabloon ? '.' _ msjablooni : '') %]" value="[% msjabloon.bibliotheek_sjablonen_id %]" />
        <input type="hidden" class="rownaam"
        name="params.status.sjablonen.catalog_name[% (msjabloon ? '.' _
        msjablooni : '') %]" value="[% msjabloon.catalog_name %]" />
        <input type="hidden" class="roworder" name="params.status.sjablonen.mijlsort[% (msjabloon ? '.' _ msjablooni : '') %]" value="[% msjablooni %]" />
        <span class="rownaam" />[% msjabloon.catalog_name | html_entity %]
        </span>
    </td>
    <td class="row-actions td60">
        <a
            href="[% formaction _ '/sjabloon/bewerken' %]"
            class="edit ezra_table_row_edit row-action"
            title="Sjabloon"
            rel="callback: ">
            <i class="icon-font-awesome icon-pencil"></i>
        </a>
        <a href="#" class="ezra_table_row_del row-action">
            <i class="icon-font-awesome icon-remove"></i>
        </a>
    </td>
</tr>
[% END %]

[% UNLESS milestone_first %]
[% BLOCK milestone_table_checklist %]
<tr
    class="ezra_table_row[% (!mchecklist ? '_template' : '') %]"
    id="ezra_table_checklist_row_number[% (mchecklist ? '_' _ mchecklisti : '') %]">
    <td class="td15 ver_center no-border drag"></td>
    <td class="td_14px tdvar">
        <input type="hidden" class="roworder" name="params.status.checklists.mijlsort[% (mchecklist ? '.' _ mchecklisti : '') %]" value="[% mchecklisti %]" />
        <input
        type="text"
        size="40"
        class="infinite"
        name="params.status.checklists.label[% (mchecklist ? '.' _ mchecklisti : '') %]"
        value="[% mchecklist.label %]">
    </td>
    <td class="row-actions td40">
        <a href="#" class="ezra_table_row_del row-action">
            <i class="icon-font-awesome icon-remove"></i>
        </a>
    </td>
</tr>
[% END %]
[% END %]

[% BLOCK milestone_table_relatie %]
<tr
    class="ezra_table_row[% (!mrelatie ? '_template' : '') %]"
    id="ezra_table_relatie_row_number[% (mrelatie ? '_' _ mrelatiei : '') %]">
    <td class="td15 ver_center no-border drag"></td>
    <td class="td_14px tdvar">
        <input type="hidden" class="rowid ezra_table_row_edit_identifier" name="params.status.relaties.relatie_zaaktype_id[% (mrelatie ? '.' _ mrelatiei : '') %]" value="[% mrelatie.relatie_zaaktype_id %]" />
        <input type="hidden" class="roworder" name="params.status.relaties.mijlsort[% (mrelatie ? '.' _ mrelatiei : '') %]" value="[% mrelatiei %]" />
        <span class="rownaam" />[% IF(mrelatie.relatie_zaaktype_id)  %]
            [% c.model('DB::Zaaktype').find(mrelatie.relatie_zaaktype_id).zaaktype_node_id.titel %]
            [% END %]
        </span>
    </td>
    <td class="row-actions td60">
        <a
            href="[% formaction _ '/relatie/bewerken' %]"
            class="edit ezra_table_row_edit row-action"
            title="Relatie"
            rel="ezra_dialog_layout: large;callback: "
            >
            <i class="icon-font-awesome icon-pencil"></i>
        </a>
        <a href="#" class="ezra_table_row_del row-action">
            <i class="icon-font-awesome icon-remove"></i>
        </a>
    </td>
</tr>
[% END %]


[% BLOCK milestone_table_notificatie %]
<tr
    class="ezra_table_row[% (!mnotificatie ? '_template' : '') %]"
    id="ezra_table_notificatie_row_number[% (mnotificatie ? '_' _ mnotificatiei : '') %]">
    <td class="td15 ver_center no-border drag"></td>
    <td class="td_14px tdvar">
        <input type="hidden" class="roworder" name="params.status.notificaties.mijlsort[% (mnotificatie ? '.' _ mnotificatiei : '') %]" value="[% mnotificatiei %]" />
        <span class="ezra_ztb_notificatie_label">
            [% IF mnotificatie.bibliotheek_notificaties_id %]
                [% c.model('DB::BibliotheekNotificaties').find(mnotificatie.bibliotheek_notificaties_id).label %]
            [% ELSE %]
               -
            [% END %]
        </span>
        <!--input
        type="text"
        size="40"
        name="params.status.notificaties.label[% (mnotificatie ? '.' _ mnotificatiei : '') %]"
        value="[% mnotificatie.label %]"-->
    </td>
    <td class="row-actions td60">
        <a
            href="[% formaction _ '/notificatie/bewerken' %]"
            class="edit ezra_table_row_edit row-action"
            title="Bericht"
            rel="ezra_dialog_layout: large; callback: ezra_ztb_notificatie_update"
            ><i class="icon-font-awesome icon-pencil"></i></a>
        <a href="#" class="ezra_table_row_del row-action">
            <i class="icon-font-awesome icon-remove"></i>
        </a>
    </td>
</tr>
[% END %]

[% BLOCK milestone_table_betrokkene %]
<tr
    class="ezra_table_row[% !row_num_betrokkene ? '_template' : '' %]"
    id="ezra_table_betrokkene_row_number[% (row_num_betrokkene ? '_' _ row_num_betrokkene : '') %]">

    <td class="td15 ver_center no-border drag"></td>
    <td class="td_14px tdvar">
        <input type="hidden" class="roworder" name="params.status.standaard_betrokkenen.mijlsort[% row_num_betrokkene ?  '.' _ row_num_betrokkene : '' %]" value="[% row_num_betrokkene %]" />
        <input type="hidden" class="roworder ezra_ztb_betrokkene_name" name="params.status.standaard_betrokkenen.naam[% row_num_betrokkene ?  '.' _ row_num_betrokkene : '' %]" value="[% betrokkene.naam %]" />
        <span class="ezra_ztb_betrokkene_label">[% betrokkene.naam %]</span>
    </td>
    <td class="row-actions td60">
        <a
            href="[% formaction _ '/betrokkene/bewerken' %]"
            class="edit ezra_table_row_edit row-action"
            title="Betrokkene toevoegen"
            rel="ezra_dialog_layout: large; callback: ezra_ztb_betrokkene_update"
            ><i class="icon-font-awesome icon-pencil"></i></a>
        <a href="#" class="ezra_table_row_del row-action">
            <i class="icon-font-awesome icon-remove"></i>
        </a>
    </td>
</tr>
[% END %]

<div class="ui-accordion" id="accordion_milestones">


<h3 class="ui-accordion-header ui-state-default ui-helper-reset" id="milestone_acc_kenmerk">
    <a href="#">Kenmerken
        (<span class="number">[% milestone.elementen.kenmerken.size || 0 %]</span>)</a> </h3>
<div class="ezra_table element_tabel_kenmerk element_tabel_text_block
  element_tabel_objecttype mijlpaal_element mijlpaal_element_kenmerken
  element_tabel_custom_object">
    <div class="ui-accordion-inner">
    <table cellspacing="0" class="ezra_table_table sortable-element ezra_kenmerk_grouping table-generic">
        <tbody>
            [% INCLUDE milestone_table_kenmerken %]
            [% FOREACH mkenmerki IN milestone.elementen.kenmerken.keys.nsort %]
                [% mkenmerk = milestone.elementen.kenmerken.$mkenmerki %]
                [% INCLUDE milestone_table_kenmerken %]
            [% END %]
        </tbody>
    </table>
    <a
        class="ezra_table_row_add"
        href="[% c.uri_for('/beheer/bibliotheek/kenmerken/search'); %]"
        style="display: none;"
        title="Kenmerk"
        rel="newcallback: kenmerk"
        ></a>
    <a
        class="ezra_table_row_add_objecttype"
        href="[% c.uri_for('/beheer/bibliotheek/objecttype/search'); %]"
        style="display: none;"
        title="Objecttype"
        rel="newcallback: objecttype"
        ></a>
    <a
        class="ezra_table_row_add_custom_object"
        href="[% c.uri_for('/beheer/bibliotheek/custom_object/search'); %]"
        style="display: none;"
        title="Objecttype (beta)"
        rel="newcallback: custom_object"
        ></a>
    <a
        class="ezra_table_row_add_text_block"
        href="[% formaction _ '/text_block/bewerken' %]"
        style="display: none;"
        title="Tekstblok"
        rel="callback: ezra_text_block_add_dialog"
        ></a>
        <div class="mijlpaal_element_add_group">
        <a
            id="voegtoe"
            title="Voeg groep toe"
            class="ezra_kenmerk_grouping_add button button-primary add-group"
            href="[% formaction _ '/kenmerkgroup/bewerken' %]"
            rel="callback: ezra_kenmerk_grouping_add_dialog"
            >Voeg groep toe</a>
        </div>

    </div>
</div>



[% # enable this to include a accordion entry for regels. it got a generic name so that in the future
# every item can be routed through it. 
PROCESS beheer/zaaktypen/accordion_container.tt %]



<h3 class="ui-accordistone.elementon-header ui-state-default ui-helper-reset" id="milestone_acc_sjabloon"><a href="#">Sjablonen (<span class="number">[% milestone.elementen.sjablonen.size || 0 %]</span>)</a></h3>
<div class="ezra_table element_tabel_sjabloon mijlpaal_element">
    <div class="ui-accordion-inner">

        [% IF milestone.elementen.sjablonen.size %]
        <div class="alert alert-warning">
            <i class="icon-font-awesome icon-warning-sign"></i>
            <span>Wijzigingen in de volgorde van sjablonen kunnen gevolgen hebben voor de regels</span>
        </div>
        [% END %]
        <table cellspacing="0" class="ezra_table_table sortable-element">
        <tbody>
            [% INCLUDE milestone_table_sjablonen %]
            [% FOREACH msjablooni IN milestone.elementen.sjablonen.keys.nsort %]
                [% msjabloon = milestone.elementen.sjablonen.$msjablooni %]
                [% INCLUDE milestone_table_sjablonen %]
            [% END %]
        </tbody>
    </table>
    <a
        class="ezra_table_row_add"
        href="[% c.uri_for('/beheer/bibliotheek/sjablonen/search'); %]"
        style="display: none;"
        title="Sjabloon"
        rel="newcallback: sjabloon; callback: ezra_zaaktype_select_sjabloon_search"
        ></a>
    </div>
</div>

[% UNLESS milestone_first %]
<h3 class="ui-accordion-header ui-state-default ui-helper-reset" id="milestone_acc_checklist"><a href="#">Taken (<span class="number">[% milestone.elementen.checklists.size || 0 %]</span>)</a></h3>
<div class="ezra_table element_tabel_checklist mijlpaal_element">
    <div class="ui-accordion-inner">
        <table cellspacing="0" class="ezra_table_table sortable-element">
            <tbody>
                [% INCLUDE milestone_table_checklist %]
                [% FOREACH mchecklisti IN milestone.elementen.checklists.keys.nsort %]
                    [% mchecklist = milestone.elementen.checklists.$mchecklisti %]
                    [% INCLUDE milestone_table_checklist %]
                [% END %]
            </tbody>
        </table>
    <a
        class="ezra_table_row_add"
        href="#"
        style="display: none;"
        ></a>
    </div>
</div>
[% END %]

<h3 class="ui-accordion-header ui-state-default ui-helper-reset" id="milestone_acc_relatie"><a href="#">Zaken (<span class="number">[% milestone.elementen.relaties.size || 0 %]</span>)</a></h3>
<div class="ezra_table element_tabel_relatie mijlpaal_element">
    <div class="ui-accordion-inner">

    <table cellspacing="0" class="ezra_table_table sortable-element">
        <tbody>
            [% INCLUDE milestone_table_relatie %]

            [% FOREACH mrelatiei IN milestone.elementen.relaties.keys.nsort %]
                [% mrelatie = milestone.elementen.relaties.$mrelatiei %]
                [% INCLUDE milestone_table_relatie %]
            [% END %]
        </tbody>
    </table>
    <!-- TODO went here, zaaktype selectie window -->
    <a
        class="ezra_zaaktype_select_zaak"
        href="[% c.uri_for('/zaaktype/search'); %]"
        style="display: none;"
        title="Relatie"
        ></a>
    </div>
</div>

<h3 class="ui-accordion-header ui-state-default ui-helper-reset" id="milestone_acc_notificatie"><a href="#">Berichten (<span class="number">[% milestone.elementen.notificaties.size || 0 %]</span>)</a></h3>
<div class="ezra_table element_tabel_notificatie mijlpaal_element">
<div class="ui-accordion-inner">
    [% IF milestone.elementen.notificaties.size %]
    <div class="alert alert-warning">
        <i class="icon-font-awesome icon-warning-sign"></i>
        <span>Wijzigingen in de volgorde van berichten kunnen gevolgen hebben voor de regels</span>
    </div>
    [% END %]
    <table cellspacing="0" class="ezra_table_table sortable-element">
        <tbody>
            [% INCLUDE milestone_table_notificatie %]
            [% FOREACH mnotificatiei IN milestone.elementen.notificaties.keys.nsort %]
                [% mnotificatie = milestone.elementen.notificaties.$mnotificatiei %]
                [% INCLUDE milestone_table_notificatie %]
            [% END %]
        </tbody>
    </table>
    <a
        class="ezra_table_row_add"
        href="#"
        title="E-mails"
        style="display: none;"
        rel="callback: ezra_ztb_notificatie_edit"
        ></a>
    </div>
</div>

[% size = milestone.elementen.standaard_betrokkenen.size || 0; %]
<h3 class="ui-accordion-header ui-state-default ui-helper-reset" id="milestone_acc_betrokkene"><a href="#">Betrokkene (<span class="number">[% size %]</span>)</a></h3>
<div class="ezra_table element_tabel_betrokkene mijlpaal_element">
<div class="ui-accordion-inner">
<!--
      <div class="alert alert-warning">
        <i class="icon-font-awesome icon-warning-sign"></i>
        <span>Wijzigingen in de volgorde van e-mails kunnen gevolgen hebben voor de regels</span>
    </div>
-->
    <table cellspacing="0" class="ezra_table_table sortable-element">
        <tbody>
            [% INCLUDE milestone_table_betrokkene %]
            [% FOREACH betrokkene IN milestone.elementen.standaard_betrokkenen.keys.nsort;
                row_num_betrokkene = loop.count;
                betrokkene = milestone.elementen.standaard_betrokkenen.$betrokkene;
                INCLUDE milestone_table_betrokkene;
            END; %]
        </tbody>
    </table>
    <a
        class="ezra_table_row_add"
        href="#"
        title="Betrokkene"
        style="display: none;"
        rel="callback: ezra_ztb_betrokkene_edit"
        ></a>
    </div>
</div>


[% IF milestone_last %]
    [% PROCESS beheer/zaaktypen/milestones/resultaten_accordion.tt %]
[% END %]


</div>

<div class="form-actions form-actions-sticky clearfix">

    <input type="submit" name="goback" value="Vorige" class="button-secondary button left">

    [% PROCESS beheer/zaaktypen/voortgang.tt %]

    <input type="submit" value="Volgende" class="button-primary button right">
    <input name="destination" type="hidden" value="" />

</div>

