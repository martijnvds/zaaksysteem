[% USE JSON %]
[% BLOCK case_action_row %]
    <div class="zaaktypebeheer-actie">
        [% IF action.head %]
        <h3 class="zaaktypebeheer-actie-header">[% action.head %]</h2>
        [% ELSE %]
        <input type="hidden" name="ezra_checkboxes" value="[% action.field %]" />
        <label for="[% action.field %]">
        <input class="voor_label" id="[% action.field %]" type="checkbox" name="[% action.field %]" value="1"
        [% action.value ? 'checked="checked"' : '' %]/> [% action.label %]</label>
        [% END %]
    </div>
[% END %]

<div class="zaaktypebeheer-acties" class="ztb">
    <form method="POST" action="[% formaction %]" class="zvalidate use_submit ezra_zaaktypebeheer" enctype="multipart/form-data">
        <input type="hidden" name="zaaktype_update" value="1" />
        <div class="block clearfix">
            <div class="block-header ztb-header clearfix">
                [% IF(zaaktype_id) %]
                    <div class="block-header-title">[% params.node.titel | html_entity %] -
                [% ELSE %]
                    Zaaktype aanmaken
                [% END %]
                    <span>Zaakacties</span>
                </div>
            </div>
            <div class="block-content form zaaktypebeheer-acties-lijst">
                [% FOREACH action = case_action_checkboxes; PROCESS case_action_row ; END %]

                <div class="zaaktypebeheer-actie">
                    <h3 class="zaaktypebeheer-actie-header">Standaard documentmappen</h3>
                </div>
                <div class="zaaktypebeheer-actie">
                    <input type="hidden" name="default_directory_marker" value="1">
                    <table>
                        <tr class="multiple-options" data-ng-controller="nl.mintlab.admin.EditKenmerkController">
                            <td>
                                <script type="text/zs-scope-data">
                                    { "options":
                                        [%- 
                                            dir_list = [];
                                            IF params.node.properties.default_directories;
                                                FOREACH dir IN params.node.properties.default_directories;
                                                    dir_list.push({ "value" = dir });
                                                END;
                                            END;
                                            JSON.encode(dir_list)
                                        %] }
                                </script>

                                <div class="kenmerk-option-table">
                                    <ul class="kenmerk-option-table-body" data-zs-sort>
                                        <li class="kenmerk-option" data-ng-repeat="option in options | filter:isOptionVisible:option" data-ng-draggable data-ng-drag-mimetype="zs/kenmerk-option" data-zs-sortable="option">
                                            <div class="kenmerk-option-value">
                                                <[option.value]>
                                                <input type="hidden" name="node.properties.default_directories" value="<[option.value]>">
                                            </div>
                                            <div class="kenmerk-option-actions">
                                                <button type="button" data-ng-click="deleteOption(option)" data-ng-show="!option.id">
                                                    <i class="icon icon-font-awesome icon-remove"></i>
                                                </button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div data-ng-form name="addopt" class="kenmerk-option-add">
                                    <input type="text" name="newOption" data-ng-model="newOption" data-zs-placeholder="'Voeg een mogelijkheid toe'" data-zs-autogrow="1">
                                    <button type="button" data-ng-click="addOption(newOption)" data-ng-disabled="!newOption" class="button button-small button-secondary">
                                        <i class="icon icon-font-awesome icon-plus"></i>
                                    </button>
                                </div>
                            </td>
                            <td>
                                <div class="tooltip-test-wrap">
                                    <div class="tooltip-test rounded">
                                        Voer een nieuwe mogelijkheid en klik op de "plus"-knop</div>
                                    <div class="tooltip-test-tip"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                [% IF email_config.rich_email -%]
                <div class="zaaktypebeheer-actie">
                    <h3 class="zaaktypebeheer-actie-header">Berichtopmaak</h3>
                </div>
                <div class="zaaktypebeheer-actie">
                    <input type="hidden" name="default_html_email_template_marker" value="1">
                    <select name="node.properties.default_html_email_template">
                        <option value="">-</option>
                        [% FOREACH template = email_config.rich_email_template_names -%]
                        <option value="[% template | html_entity %]"
                            [% IF template == params.node.properties.default_html_email_template %]selected[% END %]>[% template | html_entity %]</option>
                        [% END -%]
                    </select>
                    <br>
                    Als er geen standaardsjabloon geselecteerd is, wordt het eerste sjabloon in de configuratie gebruikt.
                </div>
                [% END -%]

                <div class="zaaktypebeheer-actie">
                    <h3 class="zaaktypebeheer-actie-header">Standaard teksten</h3>
                </div>

                <div class="zaaktypebeheer-actie">
                    <h4 class="zaaktypebeheer-acties-standaard-teksten-header">Titel publiek bevestigingsbericht</h4>
                    <input type="text" name="node.properties.public_confirmation_title" class="input_large" value="[% (params.node.properties.public_confirmation_title || ZOPTIONS.PUBLIC_CASE_FINISH_TITLE) | html %]">
                </div>

                <div class="zaaktypebeheer-actie">
                    <h4
                        class="zaaktypebeheer-acties-standaard-teksten-header">
                        Tekst bij adrescontrole
                    </h4>
                    <input type="text"
                    name="node.properties.case_location_message"
                    class="input_large"
                    value="[% params.node.properties.case_location_message | html %]"
                    >
                </div>

                <div class="zaaktypebeheer-actie">
                    <h4 class="zaaktypebeheer-acties-standaard-teksten-header">Publiek bevestigingsbericht</h4>
                    <input type="hidden" class="zs-rich-text-editor" value="[% (params.node.properties.public_confirmation_message || ZOPTIONS.PUBLIC_CASE_FINISH_MESSAGE) | html %]" features="bold italic underline bullet ordered link" name="node.properties.public_confirmation_message" />
                </div>

                <div class="form-actions form-actions-sticky clearfix">
                    <input type="button" name="goback" value="Vorige" class="go_back button-secondary button left" />

                    [% PROCESS beheer/zaaktypen/voortgang.tt %]

                    <input type="submit" value="Volgende" class="button-primary button right" />
                    <input name="destination" type="hidden" value="" />
                </div>
            </div>
        </div>
    </form>
</div>
