[%
#
# generates the dropdown box for field selection, which covers a lot of options. there's a
# hazard with the way TT handles variables, so consecutive calls to this template may result
# in bleed. a solution could be to pass the config as a hash parameter, thereby forcing a
# reset everytime.
#
# [ % PROCESS field_select
#     settings = {  show_all_kenmerken => 1, hide_aanvrager => 0 }
# % ]
#
%]
[% USE Dumper %]

[% UNLESS allowed_kenmerk_types || show_group_fields || show_text_blocks %]
    NEED allowed_kenmerk_types, aborting
    [% RETURN %]
[% END %]

[% global.kenmerk_selection = undef %]
[%- IF milestone_number == last_milestone; show_result_kenmerk = 1; END; %]

<select name="[% kenmerk_name | html %]">
[% FOREACH kenmerk_index IN milestone.elementen.kenmerken.keys.nsort %]
    [% kenmerk_item = milestone.elementen.kenmerken.$kenmerk_index %]
    [% NEXT IF kenmerk_item.is_group && !show_group_fields %]
    [% NEXT IF kenmerk_item.properties.text_content && !show_text_blocks %]

    [% bibliotheek_kenmerken_id = kenmerk_item.bibliotheek_kenmerken_id %]
    [% NEXT IF bibliotheek_kenmerken_id && !allowed_kenmerk_types %]

    [% IF bibliotheek_kenmerken_id %]
        [% NEXT IF !populate_avoid_array && avoid_kenmerken.$bibliotheek_kenmerken_id %]
    [% END %]

    [% SET kenmerk_item_type    = kenmerk_item.type;

       IF show_text_only;

         IF kenmerk_item_type == 'text' || kenmerk_item_type == 'url' ||
            kenmerk_item_type == 'textarea';

           SET show_this_kenmerk = 1;

         ELSE;
           SET show_this_kenmerk = 0;
         END;
       ELSE;
         SET show_this_kenmerk = (show_all_kenmerken || allowed_kenmerk_types.$kenmerk_item_type);
       END;

       SET show_this_group      = (show_group_fields && kenmerk_item.is_group && kenmerk_item.label != 'Benodigde gegevens');
       SET show_this_text_block = show_text_blocks;

       IF show_this_kenmerk || show_this_group || show_this_text_block;
           IF !(show_value_kenmerken && kenmerk_item.type == 'bag_adressen');
               IF !global.kenmerk_selection;
                   original_kenmerk = value;

               IF original_kenmerk && !avoid_kenmerken.$original_kenmerk;
                   global.kenmerk_selection = original_kenmerk;
               ELSE;
                   global.kenmerk_selection = bibliotheek_kenmerken_id;
               END;
           END;

           IF kenmerk_item.is_group;
               SET option_value = kenmerk_item.label;
               SET option_label = kenmerk_item.label;
           ELSIF kenmerk_item.properties.text_content;
               SET option_value = kenmerk_item.label;
               SET option_label = kenmerk_item.label;
           ELSE;
               SET option_value = bibliotheek_kenmerken_id;
               SET option_label = kenmerk_item.naam || kenmerk_item.label;
           END;
        %]

        <option value="[% option_value | html %]"
            [% IF bibliotheek_kenmerken_id == global.kenmerk_selection %]
                [% IF populate_avoid_array; avoid_kenmerken.$bibliotheek_kenmerken_id = 1; END %]
                selected="selected"
            [% END %]
            [% IF kenmerk_item.is_group && kenmerk_item.label == global.kenmerk_selection %]
                selected="selected"
            [% END %]
            [% IF kenmerk_item.properties.text_content && kenmerk_item.label == global.kenmerk_selection %]
                selected="selected"
            [% END %]
        >[% option_label | html %]</option>
        [% END %]
    [% END %]

    [% IF show_wijk_options && kenmerk_types_for_wijk_display.$kenmerk_item_type %]
        [% wijk_kenmerk_id = bibliotheek_kenmerken_id _ '-wijk' %]
        <option value="[% wijk_kenmerk_id %]"
            [% IF value == wijk_kenmerk_id %][% global.kenmerk_selection = wijk_kenmerk_id %]selected="selected"[% END %]
            >[% kenmerk_item.naam %] - wijk
        </option>
        [% wijk_kenmerken.$wijk_kenmerk_id = 1 %]
    [% END %]
[% END %]


[% # aanvrager option, a special case %]
[% IF !hide_aanvrager_option %]
    <option value="aanvrager"
        [% IF value == 'aanvrager' || !global.kenmerk_selection %][% global.kenmerk_selection = 'aanvrager' %]selected="selected"[% END %]
        >Aanvrager</option>
    <option value="aanvrager_postcode"
        [% IF value == 'aanvrager_postcode' %][% global.kenmerk_selection = 'aanvrager_postcode' %]selected="selected"[% END %]
        >Aanvrager postcode</option>
    <option value="preset_client"
        [% IF value == 'preset_client' %][% global.kenmerk_selection =
        'preset_client' %]selected="selected"[% END %]
        >Vooringevulde aanvrager</option>
[% END %]


[% IF show_wijk_options %]
    [% wijk_kenmerk_id = 'aanvrager_wijk' %]
    <option value="[% wijk_kenmerk_id %]"
        [% IF value == wijk_kenmerk_id %][% global.kenmerk_selection = wijk_kenmerk_id %]selected="selected"[% END %]
        >Aanvrager wijk</option>
    [% wijk_kenmerken.$wijk_kenmerk_id = 1 %]
[% END %]


[% IF show_result_kenmerk %]
    [% result_kenmerk_id = 'case_result' %]
    <option value="[% result_kenmerk_id %]"
        [% IF value == result_kenmerk_id %][% global.kenmerk_selection = result_kenmerk_id %]selected="selected"[% END %]
        >Resultaat</option>
    [% IF !global.kenmerk_selection %]
        [% global.kenmerk_selection = result_kenmerk_id %]
    [% END %]
[% END %]

[% IF show_phase_transition %]
    <option value="phase_transition"
        [% IF value == 'phase_transition' || !global.kenmerk_selection %][% global.kenmerk_selection = 'phase_transition' %]selected="selected"[% END %]
        >Fase afronding</option>
[% END %]


[% IF show_price %]
    <option value="price"
        [% IF value == 'price' || !global.kenmerk_selection %][% global.kenmerk_selection = 'price' %]selected="selected"[% END %]
        >Bedrag (Web)</option>
[% END %]


[% IF show_condition_options %]
    [% PROCESS beheer/zaaktypen/milestones/rules/condition_options.tt %]
[% END %]

</select>

<input type="hidden" name="[% kenmerk_name %]_previous" value="[% global.kenmerk_selection %]"/>

