import cases from './../../../utilities/cases';
import findUtilityProperty from './../findUtilityProperty';

const paths = {
    sessionCurrent: '/api/v1/session/current',
    cases: '/api/v1/case'
};

const parseResponse = res => res === 'Authorization required.' ? res : JSON.parse(res);

const performXhrRequest = (type, headers = {}) => {
    const path = paths[type] || type;

    return browser
        .driver
        .executeAsyncScript((path, headers, callback) => {
            const xhr = new XMLHttpRequest();

            xhr.open('GET', path, true);

            Object.keys(headers).map(key => {
                xhr.setRequestHeader(key, headers[key]);
            });

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    callback(xhr.responseText);
                }
            };

            xhr.send('');
        }, path, headers)
        .then(res =>
            parseResponse(res)
        );
};

export const callSpecificCase = caseNumber => performXhrRequest(`/api/v1/case/${findUtilityProperty(cases, 'caseNumber', caseNumber, 'uuid')}`);

export const performApiCall = (type, id, key) => performXhrRequest(type, {'API-Interface-Id': id, 'API-Key': key});

export const getUsername = () =>
    performXhrRequest('sessionCurrent')
        .then(res => {
            const user = res.result.instance.logged_in_user;

            return user ? user.display_name.toLowerCase() : undefined;
        });

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
