import {
    morLogout
} from './../../functions/mor/mor';
import {
    logout
} from './../../functions/common/auth/loginPage'
import {
    openPage,
    openPageAs
} from './../../functions/common/navigate';

describe('when navigating to the mor app', () => {
    beforeAll(() => {
        logout();
        openPage('/mor/');
    });

    it('it should redirect to the login page', () => {
        expect(browser.getCurrentUrl()).toContain('/auth/login');
    });
});

describe('when logging in to the mor app with valid credentials', () => {

    beforeAll(() => {
        openPageAs('admin', '/mor/');
    });

    it('it should redirect to the application', () => {
        expect(browser.getCurrentUrl()).toMatch(/mor/);
    });

    describe('and when logging out', () => {
        beforeAll(() => {
            morLogout();
        });

        it('it should redirect to the login page', () => {
            expect(browser.getCurrentUrl()).toContain('/auth/login');
        });

        it('it should remember where it came from', () => {
            expect(browser.getCurrentUrl()).toContain('/mor/');
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
