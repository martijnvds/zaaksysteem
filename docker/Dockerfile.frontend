#
# Welcome to the Dockerfile.frontend.
# This is how the Dockerfile is setup. And why!
#
# build) Build the frontend src code + build the client src code.
#
# production) Build the production deployment, this is the trivial bit.
#
# development) Build a development layer for perl developers. You don't
# get to have all the things of the production layer plus a self-signed
# ssl certificate
#
# npm-development) Build a development layer for js developers, you get
# to have all the build artifacts, npm and utilities.
#

FROM node:erbium as build

WORKDIR /opt/zaaksysteem/client
COPY client/package*.json ./
COPY client/vendors.bundle.js ../root/assets/
RUN npm ci --production --no-optional

WORKDIR /opt/zaaksysteem/frontend
COPY frontend/package*.json ./
RUN npm ci --production --no-optional

WORKDIR /opt/zaaksysteem/client

COPY root/ ../root
COPY frontend ../frontend
COPY client/ .
RUN npm run build --silent

WORKDIR /opt/zaaksysteem/frontend
RUN npm run build --silent

# Production layer build
FROM nginx:stable as production
USER root
WORKDIR /etc/nginx
COPY docker/inc/frontend/entrypoint.sh /entrypoint.sh
COPY docker/inc/frontend/etc/nginx/ /etc/nginx/
COPY share/apidocs /opt/zaaksysteem/apidocs
COPY --from=build /opt/zaaksysteem/root/ /opt/zaaksysteem/root/

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

# Backend development layer build, does not install npm and such
FROM nginx:stable as development
USER root
WORKDIR /etc/nginx
ENV DEBIAN_FRONTEND=noninteractive
ENV NODE_ENV=development
COPY dev-bin/generate_dev_certs.sh /tmp/
RUN apt-get update \
    && apt-get install --no-install-recommends -y openssl \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /etc/nginx/ssl
COPY docker/inc/frontend/entrypoint.sh /entrypoint.sh
COPY docker/inc/frontend/etc/nginx/ /etc/nginx/
COPY --from=production /opt/zaaksysteem /opt/zaaksysteem/
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

# Frontend development layer build, does install npm
FROM node:erbium as npm-development
ENV DEBIAN_FRONTEND=noninteractive
ENV NODE_ENV=development
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        libfontconfig1-dev xz-utils gnupg2 ca-certificates \
        vim-tiny less sudo nano \
    && apt-get install --no-install-recommends --no-install-suggests -y nginx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && npm config set ca "" \
    && npm config set loglevel=warn \
    && npm config set progress=false

COPY dev-bin/generate_dev_certs.sh /tmp/
COPY --from=development /etc/nginx /etc/nginx
COPY --from=development /opt/zaaksysteem /opt/zaaksysteem/
COPY client/package.json /opt/zaaksysteem/client/package.json
COPY --from=build /opt/zaaksysteem/root /opt/zaaksysteem/root
COPY --from=build /opt/zaaksysteem/frontend /opt/zaaksysteem/frontend
COPY --from=build /opt/zaaksysteem/client /opt/zaaksysteem/client

COPY dev-bin /opt/zaaksysteem/dev-bin

COPY docker/inc/frontend/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
WORKDIR /opt/zaaksysteem
